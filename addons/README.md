# Godot Addons

This is the standard path in which Godot looks for addons and assets. Many
asset packages produced for Godot will actually assume they are in this path.

Annoyingly, those packages are generally maintained with their own repositories
where their actual distributions are nestled within their own `addons` path. You
would have to copy their addons path to this one which makes it difficult to
easily manage these third party libraries with Git submodules.

Therefore, this directory is generally empty and its contents are copied from the
`Integrations` path instead where such external libraries live as submodules. The
`install.sh` or `install.bat` files will copy those libraries here as part of the
installation process.

## Adding a Godot addon

To add a Godot asset that is built as an addon, you would want to attach that
library's repository as a submodule to the `Integrations` path and then add the
appropriate lines to copy the `addons` path as appropriate to this directory to
the install scripts (both `install.sh` and `install.bat` for the different
platform installs.)
