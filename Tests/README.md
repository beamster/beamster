# Testing

This directory contains the various tests that can be run to test behaviors of
the application while it is running.

The test suite uses the [Godot Unit Testing Project](https://gut.readthedocs.io/en/latest/)
(GUT) which is a nice unit testing framework for GDScript and Godot.

## Organization and Philosophy

Currently, unit tests which test each class file-by-file are implemented in the `Unit` directly under
the same hierarchy as they are found in the normal source tree. That is, `Storage` is implemented
in the `Lib/Storage.gd` file and the test is in the `Tests/Unit/Lib/StorageTest.gd` file.

Each test has the `Test.gd` suffix and they will include a set of classes that divide the tests for
that class up into groups typically based on each public function for that class. Each test within
those subclasses is implemented as a function with the `test_` prefix.

Generally, the names of those functions follows the BDD-style naming where the test is roughly a
sentence describing the behavior we expect to see. So you will likely see tests that have a
`test_should_` prefix. For example, `test_should_create_a_metadata_file_based_on_the_id`.

This way, a full log of passing tests will write out the behaviors of the class as a form of extra
documentation. Furthermore, an error will highlight a lot of context about what is expected through
the more human-like description of the test name.
