extends GutTest

class Util:
	static func clear_test_data(subPath: String):
		var delete_dir = func(delete_dir_r, path):
			for dir in DirAccess.get_directories_at(path):
				delete_dir_r.call(delete_dir_r, "%s/%s" % [path, dir])
			for file in DirAccess.get_files_at(path):
				DirAccess.remove_absolute("%s/%s" % [path, file])
			DirAccess.remove_absolute(path)
		delete_dir.call(delete_dir, "user://%s" % subPath)

class Base:
	extends GutTest

	var StorageSpy: Object = null
	var storage: Storage = null
	var obj: Dictionary = {}
	var subPath: String = 'test-storage'

	func before_all():
		# Clear out any old test data
		Util.clear_test_data(self.subPath)

		# Create a spy for ourselves
		self.StorageSpy = partial_double(Storage)

	func before_each():
		self.storage = self.StorageSpy.new(self.subPath)

		# Store some objects
		self.obj = {'id': 'cafedeed42', 'thing': 42.0}
		self.storage.store(self.obj)

	func after_each():
		# Clear out any old test data
		Util.clear_test_data(self.subPath)

class BaseStubbed:
	extends Base

	func before_each():
		super()

		stub(self.storage, 'lock')
		stub(self.storage, 'unlock')
		stub(self.storage, 'lock_profiles')
		stub(self.storage, 'unlock_profiles')

class TestStore:
	extends BaseStubbed

	func test_should_write_out_to_metadata_file_based_on_id():
		self.storage.store({'id': 'cafe9999'})
		assert_true(FileAccess.file_exists("user://%s/objects/ca/fe/cafe9999/metadata.json" % self.subPath), 'object metadata should be written to expected place')

	func test_should_define_an_identifier_if_none_is_set():
		var empty_obj: Dictionary = {}
		self.storage.store(empty_obj)
		assert_has(empty_obj, "id")

	func test_should_write_the_given_object_contents_to_metadata_json_as_json():
		var some_obj: Dictionary = {
			'id': 'cafedad69420',
			'type': 'TextSource',
		}
		self.storage.store(some_obj)

		var file = FileAccess.open("user://%s/objects/ca/fe/cafedad69420/metadata.json" % self.subPath, FileAccess.READ)
		var content = JSON.parse_string(file.get_as_text())

		assert_eq(content['type'], some_obj['type'], 'object metadata should be correctly written')

	func test_should_return_the_same_id_as_given():
		assert_eq(self.storage.store(self.obj), self.obj['id'], 'returned id should be equal to the given id')

	func test_should_lock_the_file():
		var empty_obj: Dictionary = {}

		self.storage.store(empty_obj)

		assert_called(storage, 'lock')
		assert_eq(get_call_parameters(self.storage, 'lock'), [empty_obj['id']])

	func test_should_unlock_the_file():
		var empty_obj: Dictionary = {}

		self.storage.store(empty_obj)

		assert_called(storage, 'unlock')
		assert_eq(get_call_parameters(self.storage, 'unlock'), [empty_obj['id']])

	func test_should_record_a_refcount_for_a_new_object():
		self.storage.store({'id': 'cafe7777'})

		var file = FileAccess.open("user://%s/objects/ca/fe/cafe7777/refcount.json" % self.subPath, FileAccess.READ)
		var content = JSON.parse_string(file.get_as_text())

		assert_eq(content['count'], 0.0, 'refcount should be set to 0 for a new object')

class TestRestore:
	extends BaseStubbed

	func test_should_return_empty_dictionary_when_not_found():
		assert_eq(self.storage.restore('deedcafe24'), {}, 'should return an empty dictionary when the id is completely unique')

	func test_should_return_empty_dictionary_when_object_metadata_is_missing():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/metadata.json" % self.subPath)
		assert_eq(self.storage.restore('cafedeed42'), {}, 'should return an empty dictionary when the object metadata is missing')

	func test_should_return_empty_dictionary_when_object_metadata_is_corrupted():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/metadata.json" % self.subPath)
		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/metadata.json" % self.subPath, FileAccess.WRITE)
		file.store_line("{'a': }")
		assert_eq(self.storage.restore('cafedeed42'), {}, 'should return an empty dictionary when the object metadata is corrupt')

	func test_should_return_the_saved_object_when_found():
		assert_eq_deep(self.storage.restore('cafedeed42'), self.obj)

class TestLock:
	extends Base

	func test_should_not_wait_if_object_does_not_exist():
		self.storage.unlock('unknown42')
		self.storage.lock('unknown42')

		var thread = Thread.new()
		thread.start(func():
			self.storage.lock('unknown42')
		)

		await wait_seconds(0.1)
		assert_false(thread.is_alive(), 'lock should not block until unlocked')

	func test_should_delete_the_lockfile():
		self.storage.unlock('cafedeed42')
		self.storage.lock('cafedeed42')
		assert_false(FileAccess.file_exists("user://%s/objects/ca/fe/cafedeed42/lock" % self.subPath), 'object lockfile should not exist after lock is called')

	func test_should_wait_for_unlock():
		self.storage.unlock('cafedeed42')
		self.storage.lock('cafedeed42')

		var thread = Thread.new()
		thread.start(func():
			self.storage.lock('cafedeed42')
		)

		await wait_seconds(0.25)
		assert_true(thread.is_alive(), 'lock should block until unlocked')
		self.storage.unlock('cafedeed42')

		for i in [0, 1, 2]:
			if thread.is_alive():
				await wait_seconds(0.1)

		assert_false(thread.is_alive(), 'lock should be unblocked after unlock')

class TestUnlock:
	extends Base

	func test_should_create_the_lock_file():
		# Mimic a 'lock':
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/lock" % self.subPath)

		# Call an arbitrary unlock
		self.storage.unlock('cafedeed42')
		assert_true(FileAccess.file_exists("user://%s/objects/ca/fe/cafedeed42/lock" % self.subPath), 'object lockfile should exist after unlock is called')

	func test_should_do_nothing_when_it_cannot_find_the_object():
		self.storage.unlock('unknown69')
		assert_false(FileAccess.file_exists("user://%s/objects/un/kn/unknown69/lock" % self.subPath), 'object lockfile should not exist for an unknown object')

class TestRefer:
	extends BaseStubbed

	func test_should_increment_the_reference_count_for_the_given_object():
		self.storage.refer('cafedeed42')

		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.READ)
		var content = JSON.parse_string(file.get_as_text())
		file.close()

		assert_eq(content['count'], 1.0, 'refcount should be incremented from 0 to 1')
		self.storage.refer('cafedeed42')

		file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.READ)
		content = JSON.parse_string(file.get_as_text())

		assert_eq(content['count'], 2.0, 'refcount should be incremented from 1 to 2')

	func test_should_do_nothing_when_given_an_object_the_does_not_exist():
		self.storage.refer('unknown99')
		assert_false(FileAccess.file_exists("user://%s/objects/un/kn/unknown99/refcount.json" % self.subPath), 'object refcount should not exist for an unknown object')

	func test_should_lock_the_file():
		self.storage.refer('cafedeed42')

		assert_called(storage, 'lock')

	func test_should_unlock_the_file():
		self.storage.refer('cafedeed42')

		assert_called(storage, 'unlock')

class TestUnrefer:
	extends BaseStubbed

	func before_each():
		super()

		# Ensure it is referred to at least twice
		self.storage.refer('cafedeed42')
		self.storage.refer('cafedeed42')

	func test_should_decrement_the_reference_count_for_the_given_object():
		self.storage.unrefer('cafedeed42')

		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.READ)
		var content = JSON.parse_string(file.get_as_text())
		file.close()

		assert_eq(content['count'], 1.0, 'refcount should be incremented from 2 to 1')

	func test_should_do_nothing_when_given_an_object_the_does_not_exist():
		self.storage.unrefer('unknown99')
		assert_false(FileAccess.file_exists("user://%s/objects/un/kn/unknown99/refcount.json" % self.subPath), 'object refcount should not exist for an unknown object')

	func test_should_lock_the_file():
		self.storage.unrefer('cafedeed42')

		assert_called(storage, 'lock')
		assert_eq(get_call_parameters(self.storage, 'lock'), ['cafedeed42'])

	func test_should_unlock_the_file():
		self.storage.unrefer('cafedeed42')

		assert_called(storage, 'unlock')
		assert_eq(get_call_parameters(self.storage, 'unlock'), ['cafedeed42'])

	func test_should_not_call_delete_when_the_refcount_is_not_zero():
		self.storage.unrefer('cafedeed42')

		assert_not_called(storage, 'delete')

	func test_should_delete_the_object_when_the_refcount_reaches_zero():
		self.storage.unrefer('cafedeed42')
		self.storage.unrefer('cafedeed42')

		assert_called(storage, 'delete')
		assert_eq(get_call_parameters(self.storage, 'delete'), ['cafedeed42', false])

class TestRefcount:
	extends BaseStubbed

	func before_each():
		super()

		# Ensure an object is referred to at least twice
		self.storage.refer('cafedeed42')
		self.storage.refer('cafedeed42')

	func test_should_return_negative_one_if_object_is_unknown():
		assert_eq(self.storage.refcount('unknown99'), -1, 'should return -1 when object is unknown')

	func test_should_return_negative_one_if_refcount_metadata_is_missing():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath)
		assert_eq(self.storage.refcount('cafedeed42'), -1, 'should return -1 when object is missing refcount metadata')

	func test_should_return_negative_one_if_refcount_metadata_is_corrupt():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath)
		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.WRITE)
		file.store_line("{'a': }")
		assert_eq(self.storage.refcount('cafedeed42'), -1, 'should return -1 when object has corrupt refcount metadata')

	func test_should_return_negative_one_if_refcount_metadata_is_missing_information():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath)
		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.WRITE)
		file.store_line("{}")
		assert_eq(self.storage.refcount('cafedeed42'), -1, 'should return -1 when object is missing the refcount `count` field')

	func test_should_return_the_value_stored_in_the_refcount_json_metadata():
		assert_eq(self.storage.refcount('cafedeed42'), 2, 'should return the stored data in normal conditions')

class TestSetRefcount:
	extends BaseStubbed

	func before_each():
		super()

		# Ensure an object is referred to at least twice
		self.storage.refer('cafedeed42')
		self.storage.refer('cafedeed42')

	func test_should_do_nothing_if_object_is_unknown():
		self.storage.set_refcount('unknown34', 0)
		assert_eq(self.storage.refcount('unknown34'), -1, 'should not record information when object is unknown')

	func test_should_resave_file_if_refcount_metadata_is_missing():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath)
		self.storage.set_refcount('cafedeed42', 42)
		assert_eq(self.storage.refcount('cafedeed42'), 42, 'should still record given refcount when object is missing refcount metadata')

	func test_should_still_save_file_if_refcount_metadata_is_corrupt():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath)
		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.WRITE)
		file.store_line("{'a': }")
		self.storage.set_refcount('cafedeed42', 42)
		assert_eq(self.storage.refcount('cafedeed42'), 42, 'should still record the given refcount when object has corrupt refcount metadata')

	func test_should_still_save_file_if_refcount_metadata_is_missing_information():
		DirAccess.remove_absolute("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath)
		var file = FileAccess.open("user://%s/objects/ca/fe/cafedeed42/refcount.json" % self.subPath, FileAccess.WRITE)
		file.store_line("{}")
		self.storage.set_refcount('cafedeed42', 42)
		assert_eq(self.storage.refcount('cafedeed42'), 42, 'should still record the given refcount when object is missing the refcount `count` field')

	func test_should_return_the_value_stored_in_the_refcount_json_metadata():
		self.storage.set_refcount('cafedeed42', 42)
		assert_eq(self.storage.refcount('cafedeed42'), 42, 'should set the recorded refcount to the given value')

class TestDelete:
	extends BaseStubbed

	func test_should_do_nothing_and_return_false_for_unknown_objects():
		assert_false(self.storage.delete('unknown44', false), 'should return false when an unknown object is given')

	func test_should_do_nothing_and_return_false_when_the_object_has_references():
		self.storage.refer('cafedeed42')
		assert_false(self.storage.delete('cafedeed42', false), 'should return false when an unknown object is given')

	func test_should_delete_an_unreferred_object():
		assert_true(self.storage.delete('cafedeed42', false), 'should return true for an appropriate object')
		assert_false(FileAccess.file_exists("user://%s/objects/ca/fe/cafedeed42/metadata.json" % self.subPath), 'object metadata should no longer exist')

	func test_should_delete_a_referred_object_when_forced():
		self.storage.refer('cafedeed42')
		assert_true(self.storage.delete('cafedeed42', true), 'should return true even for a referred object')
		assert_false(FileAccess.file_exists("user://%s/objects/ca/fe/cafedeed42/metadata.json" % self.subPath), 'object metadata should no longer exist')

class TestReparent:
	extends BaseStubbed

	func test_should_do_nothing_and_return_empty_dictionary_when_object_is_unknown():
		assert_eq(self.storage.reparent('unknown22', 'cafed00d24'), {}, 'should return empty dictionary')

	func test_should_update_object_without_parent_field_and_without_updating_refcount():
		assert_has(self.storage.reparent('cafedeed42', 'cafed00d24'), 'parent', 'should set parent field')
		assert_has(self.storage.restore('cafedeed42'), 'parent', 'should write out that parent field')

	func test_should_update_object_with_parent_field_and_without_updating_refcount():
		self.obj['parent'] = 'd00d44'
		self.storage.store(self.obj)
		assert_has(self.storage.reparent('cafedeed42', 'cafed00d24'), 'parent', 'should set parent field')
		assert_eq(self.storage.restore('cafedeed42')['parent'], 'cafed00d24', 'should write out that parent field with the value we expect')

	func test_should_lock_the_file():
		self.storage.reparent('cafedeed42', 'cafed00d24')

		assert_called(storage, 'lock')
		assert_eq(get_call_parameters(self.storage, 'lock'), ['cafedeed42'])

	func test_should_unlock_the_file():
		self.storage.reparent('cafedeed42', 'cafed00d24')

		assert_called(storage, 'unlock')
		assert_eq(get_call_parameters(self.storage, 'unlock'), ['cafedeed42'])

class TestProfiles:
	extends BaseStubbed

	func test_should_return_an_empty_dictionary_when_no_profile_data_path_exists():
		assert_eq(self.storage.profiles(), {}, 'should return empty dictionary when the profile path is missing')

	func test_should_return_an_empty_dictionary_when_the_profile_data_is_missing():
		self.storage.update_profiles({})
		DirAccess.remove_absolute("user://%s/profiles/metadata.json" % self.subPath)
		assert_eq(self.storage.profiles(), {}, 'should return empty dictionary when the profile data is missing')

	func test_should_return_empty_dictionary_when_profile_metadata_is_corrupted():
		self.storage.update_profiles({})
		DirAccess.remove_absolute("user://%s/profiles/metadata.json" % self.subPath)
		var file = FileAccess.open("user://%s/profiles/metadata.json" % self.subPath, FileAccess.WRITE)
		file.store_line("{'a': }")
		assert_eq(self.storage.profiles(), {}, 'should return an empty dictionary when the profile metadata is corrupt')

	func test_should_return_the_saved_profiles_data():
		self.storage.update_profiles({
			'id': 'd00dcafe42',
			'type': 'profile',
			'data': 'my-data',
		})
		assert_has(self.storage.profiles(), 'd00dcafe42', 'should return prior saved profile information')

class TestUpdateProfiles:
	extends BaseStubbed

	var defaultProfileInfo: Dictionary = {}

	func before_each():
		super()

		self.defaultProfileInfo = {
			'id': 'd00dcafe42',
			'type': 'profile',
			'name': 'My Profile',
			'data': 'my-data',
		}

	func test_should_store_a_profile_when_none_preexist():
		var ret = self.storage.update_profiles({})
		assert_has(self.storage.profiles(), ret, 'should store the profile using the id that is returned')

	func test_should_return_the_newly_stored_id():
		var first = self.storage.update_profiles({})
		var ret = self.storage.update_profiles({})
		assert_ne(first, ret, 'should store unique identifiers when performed twice')
		assert_has(self.storage.profiles(), ret, 'should store the profile using the id that is returned')

	func test_should_update_a_previously_stored_profile():
		var first = self.storage.update_profiles(self.defaultProfileInfo)
		self.defaultProfileInfo.merge({
			'name': 'My Updated Profile',
			'data': 'my-new-data',
		}, true)
		var ret = self.storage.update_profiles(self.defaultProfileInfo)
		assert_eq(first, ret, 'should return the same identifier for the same object')
		assert_eq(self.storage.restore('d00dcafe42')['data'], 'my-new-data', 'should store newest data provided when updating an existing profile')

	func test_should_return_the_given_id():
		assert_eq(self.storage.update_profiles(self.defaultProfileInfo), 'd00dcafe42', 'should return the same is as the one provided for the profile')

	func test_should_return_the_saved_profiles_data():
		self.storage.update_profiles(self.defaultProfileInfo)
		assert_has(self.storage.profiles(), 'd00dcafe42', 'should return prior saved profile information')

	func test_should_lock_the_profiles_path():
		self.storage.update_profiles(self.defaultProfileInfo)
		assert_called(storage, 'lock_profiles')

	func test_should_unlock_the_profiles_path():
		self.storage.update_profiles(self.defaultProfileInfo)
		assert_called(storage, 'unlock_profiles')

	func test_should_store_the_profile_data_as_an_object():
		self.storage.update_profiles(self.defaultProfileInfo)
		assert_called(storage, 'lock')
		assert_eq_deep(get_call_parameters(self.storage, 'store'), [self.defaultProfileInfo])

	func test_should_lock_the_profile_object_path():
		self.storage.update_profiles(self.defaultProfileInfo)
		assert_called(storage, 'lock')
		assert_eq(get_call_parameters(self.storage, 'lock'), ['d00dcafe42'])

	func test_should_unlock_the_profile_object_path():
		self.storage.update_profiles(self.defaultProfileInfo)
		assert_called(storage, 'unlock')
		assert_eq(get_call_parameters(self.storage, 'unlock'), ['d00dcafe42'])

class TestLockProfiles:
	extends Base

	func test_should_not_wait_if_no_profile_data_exists():
		self.storage.unlock_profiles()
		self.storage.lock_profiles()

		var thread = Thread.new()
		thread.start(func():
			self.storage.lock_profiles()
		)

		await wait_seconds(0.1)
		assert_false(thread.is_alive(), 'lock_profiles should not block until unlocked')

	func test_should_delete_the_lockfile():
		self.storage.update_profiles({})
		self.storage.unlock_profiles()
		self.storage.lock_profiles()

		assert_false(FileAccess.file_exists("user://%s/profiles/lock" % self.subPath), 'profiles lockfile should not exist after lock_profiles is called')

	func test_should_wait_for_unlock_profiles():
		self.storage.update_profiles({})
		self.storage.unlock_profiles()
		self.storage.lock_profiles()

		var thread = Thread.new()
		thread.start(func():
			self.storage.lock_profiles()
		)

		await wait_seconds(0.25)
		assert_true(thread.is_alive(), 'lock_profiles should block until unlocked')
		self.storage.unlock_profiles()

		for i in [0, 1, 2]:
			if thread.is_alive():
				await wait_seconds(0.1)

		assert_false(thread.is_alive(), 'lock_profiles should be unblocked after unlock')

class TestUnlockProfiles:
	extends Base

	func test_should_create_the_lock_file():
		self.storage.update_profiles({})

		# Mimic a 'lock':
		DirAccess.remove_absolute("user://%s/profiles/lock" % self.subPath)

		# Call an arbitrary unlock
		self.storage.unlock_profiles()
		assert_true(FileAccess.file_exists("user://%s/profiles/lock" % self.subPath), 'object lockfile should exist after unlock is called')

	func test_should_do_nothing_when_it_cannot_find_profile_data():
		self.storage.unlock_profiles()
		assert_false(FileAccess.file_exists("user://%s/profiles/lock" % self.subPath), 'object lockfile should not exist if no profile data exists')
