# This keeps track of all possible Source objects we can create and discover.

class_name SourceManager
extends Manager


# All references to Source classes
var _sources: Array[GDScript] = []

# A lookup by name
var _sources_by_name: Dictionary = {}

# All source names
var _source_names: Array[String] = []


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Source"], Color(0.3, 0.9, 0.3, 1.0))

# Attach to a Toolbox component
func attach():
	pass

func get_sources() -> Array[GDScript]:
	return self._sources

func get_source_by_name(source_name: String) -> GDScript:
	return self._sources_by_name[source_name]

func get_source_names() -> Array[String]:
	return self._source_names

func load_all(texture_manager: TextureManager):
	# Grab all sources found in the Sources/ path
	var dir: DirAccess = DirAccess.open("res://Sources")
	dir.list_dir_begin()
	var filename = dir.get_next()
	while filename != "":
		if dir.current_is_dir():
			# TODO: recurse
			pass
		else:
			if filename.ends_with("Source.gd"):
				# Load it, if needed
				var source_name = filename.split(".")[0]
				self.get_log_manager().register_class(source_name, Color(0.7, 1.0, 0.7, 1.0))
				self.log("Loading Source {name}".format({"name": source_name}))
				var source = load("res://Sources/" + filename)
				self._sources.append(source)
				self._sources_by_name[source_name] = source
				self._source_names.append(source_name)
				texture_manager.add_icon_texture_for(source)
		filename = dir.get_next()

	# Add icons for base sources
	texture_manager.add_icon_texture_for(TransformableSource)
