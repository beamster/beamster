# This represents any logging we wish to do.

class_name LogManager
extends Manager


var _initial_log: Array[String] = []
var log_text_edit: TextEdit:
	set = set_log_text_edit,
	get = get_log_text_edit

var _highlighter: LogHighlighter

const WARN_COLOR = Color(1.0, 1.0, 0.0, 1.0)
const ERROR_COLOR = Color(1.0, 0.0, 0.0, 1.0)

class LogHighlighter:
	extends SyntaxHighlighter


	var _context: Array = []
	var _highlights: Dictionary = {}
	var _highlight_regex: RegEx = null

	var text_color: Color = Color(0.7, 0.7, 0.7, 1.0):
		set = set_text_color,
		get = get_text_color

	func set_text_color(color: Color):
		text_color = color

	func get_text_color() -> Color:
		return text_color

	func add_context(context: String, color: Color):
		self._context.append([context, color])

	func add_highlight(word: String, color: Color):
		self._highlights[word] = color

		# Compile the highlight regular expression
		var expression = "[[]![]]|[[][*][]]|"
		for find in self._highlights.keys():
			expression += "\\b(?<{0}>{0})\\b|".format([find])

		var re = RegEx.new()
		re.compile(expression.substr(0, expression.length() - 1))
		self._highlight_regex = re

	func _get_line_syntax_highlighting(line: int):
		var color_map: Dictionary = {
			0: {
				"color": self.text_color,
			},
		}

		var text_edit: TextEdit = self.get_text_edit()
		var text: String = text_edit.get_line(line)

		var start: int = 0

		for context_info in self._context:
			var context: String = context_info[0]
			var color: Color = context_info[1]
			if text.begins_with(context + ": "):
				color_map[0]["color"] = color
				start = text.find(":")
				color_map[start] = {
					"color": self.text_color,
				}

		# Find all matches
		if self._highlight_regex:
			for found in self._highlight_regex.search_all(text, start):
				var color = null

				if found.strings[0] == '[*]':
					color = WARN_COLOR
				elif found.strings[0] == '[!]':
					color = ERROR_COLOR
				elif found.names:
					var word = found.names.keys()[0]
					color = self._highlights[word]

				if color:
					color_map[found.get_start()] = {
						"color": color,
					}
					if found.get_end() not in color_map:
						color_map[found.get_end()] = {
							"color": self.text_color,
						}

		# Highlight strings via `
		var index = text.find('`')
		while index > -1:
			start = index

			var end = text.find('`', start + 1)
			if end > -1:
				# Include the '
				end += 1

				# Mark it
				color_map[start - 1] = {
					"color": Color(0.7, 0.4, 0.2, 1.0),
				}

				# Void out any other regions within this one
				for i in range(start, end):
					var last: Dictionary = {}
					if i in color_map:
						last = color_map[i]
						color_map.erase(i)
					if not last.is_empty() and end not in color_map:
						color_map[end] = last

				# Mark the end of it
				if end not in color_map:
					color_map[end] = {
						"color": self.text_color,
					}

				start = end
			else:
				break

			index = text.find('`', start)

		return color_map


func _init():
	self._highlighter = LogHighlighter.new()
	self._highlighter.text_color = Color(0.8, 0.8, 0.8, 1.0)

	self._highlighter.add_highlight('Warning', Color(1.0, 1.0, 0.0, 1.0))
	self._highlighter.add_highlight('Error', Color(1.0, 0.0, 0.0, 1.0))

	super(self, Color(0.65, 0.4, 0.9, 1.0))
	super.log("Logger initialized.")
	self.warn("Warn!", "CONTEXT!")

func register_context(context: String, color: Color):
	self._highlighter.add_context(context, color)

func register_class(name_of_class: String, color: Color):
	self._highlighter.add_highlight(name_of_class, color)

func set_log_text_edit(text_edit: TextEdit):
	log_text_edit = text_edit
	log_text_edit.syntax_highlighter = self._highlighter

	for message in self._initial_log:
		self.log(message)

func get_log_text_edit() -> TextEdit:
	return log_text_edit

func warn(message: String, context: String = ""):
	self.log("[*] " + "Warning" + ": " + message, context)

func error(message: String, context: String = ""):
	self.log("[!] " + "Error" + ": " + message, context)

func log(message: String, context: String = ""):
	if context != "":
		message = context + ": " + message

	if self.log_text_edit == null:
		# For debugging... just use the Godot logger
		print(message)

		# But keep track of it
		self._initial_log.append(message)
		return

	print(message)

	self.log_text_edit.insert_line_at(
		self.log_text_edit.get_line_count() - 1,
		message
	)
