# This manages all Action classes we have initialized so far.

class_name ActionManager
extends Manager


# A dictionary tracking all known and loaded actions by path.
var _actions_by_path: Dictionary = {}

# A dictionary tracking actions by their instance ids.
var _actions_by_id: Dictionary = {}

# A dictionary tracking categories of actions.
var _actions_by_category: Dictionary = {}

# The set of all known categories of actions.
var _actions_categories: Array[String] = []


# Get a reference to an actcion by its path.
func get_action_by_path(path: String) -> Action:
	return self._actions_by_path[path]

# Get a reference to an action by its id.
func get_action_by_id(id: String) -> Action:
	return self._actions_by_id[id]

# Get a list of references to each action for a particular category.
func get_actions_by_category(type: String) -> Array[Dictionary]:
	return self._actions_by_category[type]

# Get a list of all known categories of actions.
func get_action_categories() -> Array[String]:
	return self._actions_categories

func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Action"], Color(0.9, 0.3, 0.3, 1.0))

# Loads references to all base actions in the actions library paths.
func load_all():
	var action_categories = []

	# Read the Actions path for all directories.
	var base_dir: DirAccess = DirAccess.open("res://Actions")
	base_dir.list_dir_begin()

	var filename: String = base_dir.get_next()
	while filename != "":
		if base_dir.current_is_dir():
			action_categories.append(filename)
		filename = base_dir.get_next()

	# For each of those subdirectories, which represent categories, load all
	# scripts that are representing Action classes.
	for category in action_categories:
		if not self._actions_by_category.has(category):
			var actions_list: Array[Dictionary] = []
			self._actions_by_category[category] = actions_list

			self._actions_categories.append(category)

		var dir: DirAccess = DirAccess.open("res://Actions/" + category)
		dir.list_dir_begin()
		filename = dir.get_next()

		while filename != "":
			if dir.current_is_dir():
				# TODO: recurse
				pass
			else:
				if filename.ends_with("Action.gd"):
					# Load it, if needed
					var action_name = filename.split(".")[0]
					self.log("Loading Action {category}/{name}".format(
						{
							"category": category,
							"name": action_name
						}
					))
					var action = load("res://Actions/" + category + "/" + filename)
					self._actions_by_category[category].append({
						"name": action_name,
						"class": action,
					})
			filename = dir.get_next()
