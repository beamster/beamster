# Managers

These classes are object stores for each of the different types of serialized
objects in our system.

## ActionManager

## AudioManager

## EffectManager

## ElementManager

## SceneManager

## SourceManager

## TextureManager


Log Manager
Analytics Manager
Storage Manager
Texture Manager

Interface Manager
  -> Presentation Manager
	 -> Action Manager
	 -> Animation Manager
	 -> Stream Manager
  -> Editor Manager
	 -> Panel Manager
	   -> open()

Profile Manager
  -> Scene Manager
	-> Source Manager
  -> World Manager
	-> Element Manager
  -> Effect Manager

Audio Manager
