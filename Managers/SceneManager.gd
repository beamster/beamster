# This manages the Scenes and Sources throughout the loaded Profile.

class_name SceneManager
extends Manager


# Manages all Source definitions
var _source_manager: SourceManager = null

# Manages all Transition definitions
var _transition_manager: TransitionManager = null

# A reference to the presentation animation manager
var _animation_manager: AnimationManager = null

# A reference to the effect manager
var _effect_manager: EffectManager = null

# Keeps an order listing of all scenes
var _scenes: Array[String] = []

# Some textures
var _transition_to_texture: Texture2D
var _transition_from_texture: Texture2D

# Whether or not editing is locked
# For instance, we lock the interface while an animation/transition is being
# previewed.
var locked: bool = false:
	set = set_locked,
	get = get_locked

var edit_panel: EditPanel = null:
	set = set_edit_panel,
	get = get_edit_panel

# Keep track of the currently edited scene on the current edit panel
var _current_edited_scene: Scene = null

# Keep track of the currently edited frame on the current edit panel
var current_edited_frame: Frame = null:
	set = set_editing_frame,
	get = get_editing_frame

# Keep track of the frame that is currently previewing its transition
var current_previewing_frame: Frame = null:
	set = set_previewing_frame,
	get = get_previewing_frame

var last_edited_frame: Frame = null

# Keep track of the currently presented scene/frame
# TODO: keep track of current frame per scene
var _current_scene: Scene = null

var _realized: Dictionary = {}

# Keeps a lookup table for scenes via their SerializedObject id
var _scene_by_id: Dictionary = {}

# Keeps track of the undo/redo history
var _undo_redo: UndoRedo = UndoRedo.new()

# Keeps all active rendering context for scenes
var _rendering_viewport: SubViewport = null

func _init(log_manager: LogManager, animation_manager: AnimationManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Scene"], Color(0.9, 0.6, 0.9, 1.0))

	# Maintain Source definition classes
	self._source_manager = SourceManager.new(log_manager)
	self._effect_manager = EffectManager.new(log_manager)
	self._animation_manager = animation_manager
	self._transition_manager = TransitionManager.new(log_manager, self._effect_manager, self._animation_manager)

	# Bind events
	_animation_manager.completed.connect(self.on_animation_complete)

	# Load textures
	self._transition_to_texture = load("res://Assets/Images/transition_to.png")
	self._transition_from_texture = load("res://Assets/Images/transition_from.png")

func get_source_manager() -> SourceManager:
	return self._source_manager

func get_transition_manager() -> TransitionManager:
	return self._transition_manager

func get_effect_manager() -> EffectManager:
	return self._effect_manager

func get_animation_manager() -> AnimationManager:
	return self._animation_manager

func load_all(texture_manager: TextureManager):
	self._source_manager.load_all(texture_manager)
	self._effect_manager.load_all()
	self._transition_manager.load_all(texture_manager)

# TODO: remove... only the presentation manager should care about rendering a
# scene.
func set_viewport(viewport: SubViewport):
	self._rendering_viewport = viewport

# Attach to a Tree component
func attach(_tree: Tree):
	pass

# Undoes an action.
func undo():
	if self._undo_redo.has_undo():
		self._undo_redo.undo()

# Redoes an action previously undone.
func redo():
	if self._undo_redo.has_redo():
		self._undo_redo.redo()

# Returns `true` if there is an action to undo.
func has_undo() -> bool:
	return self._undo_redo.has_undo()

# Returns `true` if there is an action to redo.
func has_redo() -> bool:
	return self._undo_redo.has_redo()

func create_scene() -> Scene:
	# Create the scene
	var scene: Scene = Scene.new()

	# Create the frame and set its name to the initial frame
	var frame: Frame = Frame.new()
	frame.set_name("Initial Frame")
	scene.add_child(frame)
	frame = Frame.new()
	frame.set_name("Closeup")
	scene.add_child(frame)

	# Create the initial transition object
	var transition: Transition = Transition.new()
	scene.add_child(transition)

	# Return the created scene
	return scene

# Adds a scene to the Profile
func add_scene(scene: Scene):
	self._undo_redo.create_action("Add scene")
	self._undo_redo.add_do_method(self._add_scene.bind(scene))
	self._undo_redo.add_undo_method(self._remove_scene.bind(scene))
	self._undo_redo.commit_action()

# Internal method to add a scene
func _add_scene(scene: Scene):
	self._scenes.append(scene.id())
	self._scene_by_id[scene.id()] = scene

# Removes a scene from the Profile
func remove_scene(scene: Scene):
	self._undo_redo.create_action("Remove scene")
	self._undo_redo.add_do_method(self._remove_scene.bind(scene))
	self._undo_redo.add_undo_method(self._add_scene.bind(scene))
	self._undo_redo.commit_action()

# Internal method to remove a scene
func _remove_scene(scene: Scene):
	self._scenes.remove_at(self._scenes.find(scene.id()))
	self._scene_by_id.erase(scene.id())

	# Remove in scene listing

# Adds a source to the Profile.
func add_source(_source):
	pass

# Removes a source from the Profile.
func remove_source(_source):
	pass

# Reorders the scene.
func move_scene(scene, position):
	var index = self._scenes.find(scene.id())
	if position > index:
		position += 1

	self._scenes.remove_at(index)
	self._scenes.insert(position, scene)

	# TODO: adjust in scene listing

func get_scenes() -> Array[String]:
	return self._scenes

func get_scene(id: String) -> Scene:
	var scene = self._scene_by_id[id]
	if scene:
		if not self._realized.has(id):
			# Add the scene to the rendering path
			if self._rendering_viewport:
				self._rendering_viewport.add_child(scene.get_viewport_container())

			# Give the scene a source list, finally
			var source_list: Tree = SourceTree.new()
			source_list.name = "SourceTree"
			source_list.size_flags_vertical = Control.SIZE_EXPAND_FILL
			var source_container: VBoxContainer = VBoxContainer.new()
			source_container.add_child(source_list)

			# Add the base transitions
			var root = source_list.get_root()
			if not root:
				root = source_list.create_item()

			source_list.hide_root = true

			var transition_tree_item = source_list.create_item(root)
			transition_tree_item.set_text(0, 'Transitions')

			var before_tree_item = transition_tree_item.create_child()
			before_tree_item.set_text(0, 'Cut (To)')
			before_tree_item.set_meta('class', CutTransition.new().instantiate())
			before_tree_item.set_icon(0, self._transition_to_texture)
			before_tree_item.set_meta('transition_type', '(To)')

			var after_tree_item = transition_tree_item.create_child()
			after_tree_item.set_text(0, 'None (From Override)')
			after_tree_item.set_meta('class', NoneTransition.new().instantiate())
			after_tree_item.set_icon(0, self._transition_from_texture)
			after_tree_item.set_meta('transition_type', '(From Override)')

			scene.set_source_list(source_container)
			var action_list: Tree = Tree.new()
			scene.set_action_list(action_list)

			# Mark it as realized
			self._realized[id] = true
	return scene

# Get the currently focused edit panel, if any
func get_edit_panel() -> EditPanel:
	return null

# Set the currently focused edit panel
func set_edit_panel(panel: EditPanel):
	edit_panel = panel

func set_current_scene(scene: Scene):
	self._current_scene = scene

func get_current_scene() -> Scene:
	return self._current_scene

func set_editing_scene(scene: Scene):
	self._current_edited_scene = scene

func get_editing_scene() -> Scene:
	return self._current_edited_scene

func set_editing_frame(frame: Frame):
	if current_edited_frame:
		last_edited_frame = current_edited_frame
	current_edited_frame = frame
	print("applying frame ", frame)
	frame.apply()

	frame.on_activate()

func get_editing_frame() -> Frame:
	return current_edited_frame

func set_previewing_frame(frame: Frame):
	print('set_previewing_frame')
	if self.last_edited_frame and frame == self.current_edited_frame:
		self.last_edited_frame.apply()
	current_previewing_frame = frame
	if not frame:
		return

	# Dequeue it, if it already exists
	_animation_manager.dequeue(frame)

	# Activate the frame
	frame.on_activate()

	# Queue and lock the editor until it completes
	set_locked(true)
	print('queuing')
	_animation_manager.queue(frame)
	print('done')

func on_animation_complete(frame: Frame):
	if self.current_previewing_frame == frame:
		self.current_previewing_frame = null

	# Go back to the currently edited frame
	if frame != self.current_edited_frame:
		self.current_edited_frame.apply()

func get_previewing_frame() -> Frame:
	return current_previewing_frame

# Returns the number of scenes
func size() -> int:
	return self._scenes.size()

func set_locked(value: bool):
	locked = value

func get_locked() -> bool:
	return locked
