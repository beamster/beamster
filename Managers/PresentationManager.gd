class_name PresentationManager
extends Manager


# Properties

# Manages all active animations in the presentation
var _animation_manager: AnimationManager = null

# Manages all active action scripts
var _action_manager: ActionManager = null

# Manages types of possible stream outputs for the presentation
var _stream_manager: StreamManager = null


# Create the manager
func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0))

	# Initialize sub-managers
	self._animation_manager = AnimationManager.new(log_manager)
	self._action_manager = ActionManager.new(log_manager)
	self._stream_manager = StreamManager.new(log_manager)

# Returns an instance of the AnimationManager
func get_animation_manager() -> AnimationManager:
	return self._animation_manager

# Returns an instance of the ActionManager
func get_action_manager() -> ActionManager:
	return self._action_manager

# Returns an instance of the StreamManager
func get_stream_manager() -> StreamManager:
	return self._stream_manager

func load_all(_texture_manager: TextureManager):
	# Load encoders
	self._stream_manager.load_all()

	# Load actions
	self._action_manager.load_all()

# Show the given scene
func present(_scene: Scene):
	# Determine transition

	# Add next scene on top of view

	# Show transition

	pass

# Get actions
func get_actions():
	pass

# Apply action
func perform(_action):
	pass
