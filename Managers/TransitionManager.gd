class_name TransitionManager
extends Manager


# All references to Transition classes
var _transitions: Array[GDScript] = []

# A lookup by name
var _transitions_by_name: Dictionary = {}

# All transition names
var _transition_names: Array[String] = []

# A reference to the effect manager
var _effect_manager: EffectManager = null

# A reference to the animation manager
var _animation_manager: AnimationManager = null


func _init(log_manager: LogManager, effect_manager: EffectManager, animation_manager: AnimationManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Transition"], Color(0.9, 0.8, 0.25, 1.0))

	self._effect_manager = effect_manager
	self._animation_manager = animation_manager

func load_all(_texture_manager: TextureManager):
	# Grab all sources found in the Sources/ path
	var dir: DirAccess = DirAccess.open("res://Transitions")
	dir.list_dir_begin()
	var filename = dir.get_next()
	while filename != "":
		if dir.current_is_dir():
			# TODO: recurse
			pass
		else:
			if filename.ends_with("Transition.gd"):
				# Load it, if needed
				var transition_name = filename.split(".")[0]
				self.get_log_manager().register_class(transition_name, Color(0.7, 0.6, 0.35, 1.0))
				self.log("Loading Transition {name}".format({"name": transition_name}))
				var transition = load("res://Transitions/" + filename)
				self._transitions.append(transition)
				self._transitions_by_name[transition_name] = transition
				self._transition_names.append(transition_name)
				#texture_manager.add_icon_texture_for(transition)
		filename = dir.get_next()

func get_transitions() -> Array[GDScript]:
	return self._transitions

func get_transition_by_name(transition_name: String) -> GDScript:
	return self._transitions_by_name[transition_name]

func get_transition_names() -> Array[String]:
	return self._transition_names

func get_transition_shaders() -> Array[Effect]:
	return self._effect_manager.get_effects_by_type('Transitions')

func perform(transition: Transition.TransitionInstance, from: Scene, to: Scene, within: SubViewportContainer):
	var global_time: float = self._animation_manager.get_frame_seconds()
	transition.get_base().perform(transition, from, to, within, global_time)
	if transition.get_base().has_method('process'):
		# TODO: have the animation manager queue the transition between the time period
		self._animation_manager.queue(transition)
