# This manages all Effect classes we have initialized so far.

class_name EffectManager
extends Manager


# A dictionary tracking all known and loaded effects by path.
var _effects_by_path: Dictionary = {}

# A dictionary tracking base effects by their instance ids.
var _effects_by_id: Dictionary = {}

# A dictionary tracking categories of effects.
var _effects_by_type: Dictionary = {}

# The set of all known categories
var _effects_categories: Array[String] = []

# Effect cache based on path
var _effects: Dictionary = {}


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Effect", "Shader"], Color(0.3, 0.5, 0.9, 1.0))

# Get a reference to an effect by its effect path.
func get_effect_by_path(path: String) -> Effect:
	return self._effects_by_path[path]

# Get a reference to an effect by its base id.
func get_effect_by_id(id: String) -> Effect:
	return self._effects_by_id[id]

# Get a list of references to each effect for a particular category.
func get_effects_by_type(type: String) -> Array[Effect]:
	return self._effects_by_type[type]

# Get a list of all known categories of effects.
func get_effect_categories() -> Array[String]:
	return self._effects_categories

func get_realized_effect(path: String) -> Effect:
	if not self._effects.has(path):
		var effect = Effect.new()
		effect.set_path(path)
		self._effects[path] = effect

	return self._effects[path]

# Loads references to all effects in the effects library paths.
func load_all():
	var shader_types = []

	# Read the Shaders path for all directories. Each directory represents a
	# type of shader (Filters, Skies, Textures, etc)
	var base_dir: DirAccess = DirAccess.open("res://Shaders")
	base_dir.list_dir_begin()

	var filename: String = base_dir.get_next()
	while filename != "":
		if base_dir.current_is_dir():
			shader_types.append(filename)
		filename = base_dir.get_next()

	# For each of those subdirectories, which represent categories, load all
	# shader files we find there as Effect references.
	for shader_type in shader_types:
		if not self._effects_by_type.has(shader_type):
			var effects_list: Array[Effect] = []
			self._effects_by_type[shader_type] = effects_list

			self._effects_categories.append(shader_type)

		var dir: DirAccess = DirAccess.open("res://Shaders/" + shader_type)
		dir.list_dir_begin()
		filename = dir.get_next()
		while filename != "":
			if dir.current_is_dir():
				# TODO: recurse
				pass
			else:
				if filename.ends_with(".gdshader"):
					var shader_path = "res://Shaders/" + shader_type + "/" + filename

					# Load it
					var effect = Effect.new()
					effect.set_path(shader_path)
					self._effects_by_type[shader_type].append(effect)
					effect.category = shader_type

					var shader_name = effect.get_name()
					self.log("Loading Shader {type}/{name}".format(
						{
							"type": shader_type,
							"name": shader_name,
						}
					))
			filename = dir.get_next()
