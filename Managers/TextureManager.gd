# This class manages instances of textures and icons.

class_name TextureManager
extends Manager


# The icon images used for all sources in different sizes
var _icon_textures_32: Dictionary = {}
var _icon_textures_16: Dictionary = {}


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Texture"], Color(0.9, 0.6, 0.9, 1.0))

func add_icon_texture_for(object_class):
	self._icon_textures_32[object_class.name()] = load(object_class.icon())

	var icon_small_path = object_class.icon_small()
	if icon_small_path == "":
		icon_small_path = object_class.icon()

	var texture = ImageTexture.create_from_image(load(icon_small_path).get_image())
	texture.set_size_override(Vector2(16, 16))
	self._icon_textures_16[object_class.name()] = texture

func icon_texture_32_for(object_name: String) -> Texture2D:
	return self._icon_textures_32.get(object_name)

func icon_texture_16_for(object_name: String) -> Texture2D:
	return self._icon_textures_16.get(object_name)
