# This manages any Audio that is going on.

class_name AudioManager
extends Manager


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Sample"], Color(0.3, 0.9, 0.3, 1.0))

# Returns the number of buses currently known to the audio server.
#
# This may differ from the number of buses currently available to the current
# scene. It refers to the global notion of buses that are currently loaded.
# Since audio buses might be lazily loaded, this does not reflect the total
# possible number of buses.
func get_bus_count() -> int:
	return AudioServer.bus_count

# Returns an Array of Strings consisting of the names of input audio devices.
func get_input_listing() -> Array:
	return Array(AudioServer.get_input_device_list())

# Returns an Array of Strings consisting of the names of output audio devices.
func get_output_listing() -> Array:
	return Array(AudioServer.get_output_device_list())
