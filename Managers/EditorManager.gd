# This manages the Edit view and all of the associated UI features of that view
# outside of the actual editing panels. Those are controlled via the
# PanelManager instead.
class_name EditorManager
extends Manager


# Maintains all the possible edit panels
var _panel_manager: PanelManager = null

# The current profile
var _profile_manager: ProfileManager = null

# The Node containing the editor functions
var _container: Control = null

# Keeps track of the Source classes we can create
var _sources: Array[Script] = []

# Keeps track of the source classes by lookup from their types
var _sourcesByType: Dictionary = {}

# The current source tree nodes
var _sources_list: Control = null

# The currently edited scene
var _scene: Scene = null

# The currently selected item
var _selected: SerializedObject = null


# Construct the manager
func _init(log_manager: LogManager, profile_manager: ProfileManager, container: Control):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0))

	# Keep track of our editing container
	self._container = container

	# Retain a reference to the object store and profile
	self._profile_manager = profile_manager

	# Initialize the panels
	self._panel_manager = PanelManager.new(log_manager, self)

	# Bind events to common components
	self._panel_manager.edit_panel_focus.connect(self.on_edit_panel_focus)
	self.get_scene_tree().scene_activated.connect(self.on_scene_selected)
	self.get_actions_tree().item_activated.connect(self.on_action_activated)
	self.get_effects_tree().effect_activated.connect(self.on_effect_activated)
	self.get_effects_tree().effect_opened.connect(self.open_effect)
	self.get_transitions_tree().item_selected.connect(self.on_transition_selected)
	self.get_transitions_tree().item_activated.connect(self.on_transition_activated)

	var context_menu = TreeContextMenu.new(
		[
			{
				"name": "Open in Transition Editor",
				"call": func(item: TreeItem):
					var transition_effect: Effect = item.get_meta('class') as Effect
					var transition: ShaderTransition = ShaderTransition.new()
					transition.effect = transition_effect.instantiate()
					self.open_transition(transition),
			},
		]
	)
	self.get_transitions_tree().add_child(context_menu)

	# Bind events to property changes
	self.get_property_tree().updated.connect(self.on_property_updated)

	# And bind events to handle profile loads
	self.get_profile_manager().open_profile.connect(self.load_profile)

	# TODO: remove in preference for a select instead
	var tab: TabContainer = self.get_container().get_node("%CreateContainer")
	tab.set_tab_hidden(self.get_transitions_tree().get_index(), true)
	self.get_transitions_tree().visible = false

# Retrieves the node into which all editor content resides.
func get_container() -> Control:
	return self._container

func get_create_tabbar() -> TabContainer:
	return self.get_container().get_node("%CreateContainer")

# Retrieves the PanelManager which handles interactions and updating the actual
# editing panels.
func get_panel_manager() -> PanelManager:
	return self._panel_manager

# Retrieves the ProfileManager this is an editor for.
func get_profile_manager() -> ProfileManager:
	return self._profile_manager

# Get the scene tree.
func get_scene_tree() -> SceneEditTree:
	return self._container.get_node("%SceneEditTree")

# Get the world tree.
func get_world_tree() -> Tree:
	return self._container.get_node("%WorldTree")

func get_toolbox() -> PanelContainer:
	return self._container.get_node("%Toolbox")

# Get the source toolbox which contains the various Source objects one can use.
func get_source_toolbox() -> ItemList:
	return self.get_toolbox().get_node("SourceToolbox")

# Get the element toolbox which contains the various Element objects one can use.
func get_element_toolbox() -> ItemList:
	return self.get_toolbox().get_node("ElementToolbox")

func open_toolbox():
	self.show_toolbox()
	self.get_create_tabbar().current_tab = self.get_toolbox().get_index()

func hide_toolbox():
	self.get_create_tabbar().set_tab_hidden(self.get_toolbox().get_index(), true)

func show_toolbox():
	self.get_create_tabbar().set_tab_hidden(self.get_toolbox().get_index(), false)

# Get the property listing component.
func get_property_tree() -> Tree:
	return self._container.get_node("%PropertyTree")

# Provides a new tree of sources.
#
# The Scene object maintains a set of its own sources as a Tree and maintains
# it, which is then passed here when the Scene is loaded or focused upon.
func set_sources_list(sources: Control):
	if self._sources_list and self._sources_list.get_parent():
		# Disconnect events
		self.get_source_tree().source_selected.disconnect(self.select_source)
		self.get_source_tree().effect_selected.disconnect(self.select_effect)
		self.get_source_tree().effect_activated.disconnect(self.open_effect)
		self.get_source_tree().action_selected.disconnect(self.select_action)
		self.get_source_tree().action_activated.disconnect(self.open_action)
		self.get_source_tree().transition_selected.disconnect(self.select_transition)

		# Remove this source tree from view
		self._sources_list.get_parent().remove_child(self._sources_list)
		self._sources_list.visible = false

	# Remove any other instance of a tree in that area
	var source_tree_container: TabContainer = self._container.get_node("%ItemContainer")
	for child in source_tree_container.get_children():
		source_tree_container.remove_child(child)

	# Keep track of it
	self._sources_list = sources

	# Add it to the editor view and reveal it
	source_tree_container.add_child(sources)
	source_tree_container.set_tab_title(0, "Sources")
	sources.visible = true

	# Bind events to the SourceTree
	self.get_source_tree().source_selected.connect(self.select_source)
	self.get_source_tree().effect_selected.connect(self.select_effect)
	self.get_source_tree().effect_activated.connect(self.open_effect)
	self.get_source_tree().action_selected.connect(self.select_action)
	self.get_source_tree().action_activated.connect(self.open_action)
	self.get_source_tree().transition_selected.connect(self.select_transition)

# Returns the currently used source tree, or null if there is none.
func get_sources_list() -> Control:
	return self._sources_list

func get_source_tree() -> SourceTree:
	var sources: Control = self.get_sources_list()
	if sources == null:
		return null

	return sources.get_node("SourceTree")

# Returns the currently selected TreeItem for the source in the source tree.
func get_source_item() -> TreeItem:
	var tree: Tree = self.get_source_tree()
	if tree == null or not tree.get_selected():
		return null

	return tree.get_selected()

# Adds the Source objects to the editor that were loaded by the SourceManager.
func add_sources(source_manager: SourceManager, texture_manager: TextureManager):
	for source_name in source_manager.get_source_names():
		var source_class: Script = source_manager.get_source_by_name(source_name)
		self.add_source(source_class, texture_manager)

# Adds a Source component to make it available to the editor.
func add_source(source_class: Script, texture_manager: TextureManager):
	var icon_texture: Texture2D = texture_manager.icon_texture_32_for(source_class.name())
	var index: int = self.get_source_toolbox().add_icon_item(icon_texture)
	self.get_source_toolbox().set_item_tooltip(index, source_class.name())
	var source_type: String = source_class.name()
	source_class.set_meta("source_type", source_type)
	self._sourcesByType[source_type] = source_class
	self._sources.append(source_class)

func load_profile(profile: Profile):
	# Reload the scenes and such
	print("hiya. loading profile: ", profile.get_name())

# Deselect all things in the editor toolboxes.
func deselect_all():
	self.get_source_toolbox().deselect_all()
	#self.get_element_toolbox().deselect_all()

# Get the current selected source from the toolbox as its component class.
func get_source() -> Script:
	if not self.get_source_toolbox().is_anything_selected():
		return null

	var index: int = self.get_source_toolbox().get_selected_items()[0]
	return self._sources[index]

# Get the current selected element from the toolbox as its component class.
func get_element() -> Script:
	if not self.get_element_toolbox().is_anything_selected():
		return null

	var index: int = self.get_element_toolbox().get_selected_items()[0]
	return self._elements[index]

# Retrieve the Tree that lists possible Actions to add to a scene.
func get_actions_tree() -> Tree:
	return self._container.get_node("%ActionsTree")

func open_actions_tree():
	self.show_actions_tree()
	self.get_create_tabbar().current_tab = self.get_actions_tree().get_index()

func show_actions_tree():
	self.get_create_tabbar().set_tab_hidden(self.get_actions_tree().get_index(), false)

func hide_actions_tree():
	self.get_create_tabbar().set_tab_hidden(self.get_actions_tree().get_index(), true)

# Adds the Action objects known by the given ActionManager.
func add_actions(action_manager: ActionManager, _texture_manager: TextureManager):
	#ActionsTree.load_all(self.get_interface_manager().get_presentation_manager().get_action_manager())
	var actions_tree: Tree = self.get_actions_tree()
	actions_tree.clear()

	# Create actions root
	actions_tree.create_item()
	actions_tree.hide_root = true

	for category in action_manager.get_action_categories():
		for action in action_manager.get_actions_by_category(category):
			self.add_action(category, action["name"], action["class"])

# Adds a particular Action component to make it available to the editor.
func add_action(category: String, action_name: String, action_class: Script):
	var actions_tree: Tree = self.get_actions_tree()
	var root: TreeItem = actions_tree.get_root()

	# Get the category root, if it exists
	var category_root: TreeItem = null
	for item in root.get_children():
		if item.get_text(0) == category:
			category_root = item
			break

	if category_root == null:
		category_root = root.create_child(0)
		category_root.set_text(0, category)

	var action_item: TreeItem = category_root.create_child()
	action_item.set_text(0, action_name)
	action_item.set_meta("class", action_class)

# Get the Tree that lists all possible Effect objects available to the editor.
func get_effects_tree() -> EffectsTree:
	return self._container.get_node("%EffectsTree")

func open_effects_tree():
	self.show_effects_tree()
	self.get_create_tabbar().current_tab = self.get_effects_tree().get_index()

func show_effects_tree():
	self.get_create_tabbar().set_tab_hidden(self.get_effects_tree().get_index(), false)

func hide_effects_tree():
	self.get_create_tabbar().set_tab_hidden(self.get_effects_tree().get_index(), true)

# Adds all Effect components known by the given EffectManager.
func add_effects(_texture_manager: TextureManager):
	var effect_manager: EffectManager = self.get_profile_manager().get_scene_manager().get_effect_manager()
	for shader_type in effect_manager.get_effect_categories():
		for effect in effect_manager.get_effects_by_type(shader_type):
			effect_manager.get_realized_effect(effect.get_path())
			self.add_effect(effect)

# Adds an Effect component to make it available to the editor.
# TODO: use an actual Effect here.
func add_effect(effect: Effect):
	var effect_tree: EffectsTree = self.get_effects_tree()
	effect_tree.add_object(effect)

# Returns the currently focused and edited Scene.
func get_current_scene() -> Scene:
	return self._scene

# Open an object for editing by loading it into an editor panel.
func open(object: SerializedObject):
	if object is Scene:
		# Cast object
		var scene: Scene = object

		# Retain knowledge of this scene
		self._scene = scene

		# Reveal in the panel
		self.get_panel_manager().update_panel(scene, 0)

		# Make sure we load the sources list
		var sources: Node = scene.get_source_list()
		self.set_sources_list(sources)

# When an editor panel receives focus.
func on_edit_panel_focus(object: SerializedObject, _panel: EditPanel):
	# If it is editing a Scene/Frame, reveal that context
	if object is Scene:
		# Reveal source box, etc
		# Load and reveal the source listing for the scene
		var scene: Scene = object
		var sources: Node = scene.get_source_list()
		self.set_sources_list(sources)

		# Load and reveal the action listing for the scene
		#var action_list: Tree = scene.get_action_list()

		# Tell the source list about this (TODO: remove)
		sources.get_node("SourceTree").on_scene()

		# Show the Source toolbox
		self.get_container().get_node("%CreateContainer").show_toolbox()
		self.get_source_toolbox()
		self.get_source_toolbox().visible = true
		self.get_element_toolbox().visible = false
	elif object is World:
		# If it is editing a World, reveal that context
		pass
	elif object is Action:
		# If it is editing an Action, let's open the Scene/World it is
		# connected to.
		pass
	elif object is Effect:
		# An Effect is just generally available
		pass

# Load all components used by this or sub-managers.
#
# In the case of the editor, this will load the editor panels.
func load_all(_texture_manager: TextureManager):
	self._panel_manager.load_all()

# Select the given scene.
func select_scene(_selected_scene: Scene):
	pass

# Select the given frame.
func select_frame(frame: Frame):
	self.get_profile_manager().get_scene_manager().current_edited_frame = frame

	# Show the Toolbox, Effects, etc tabs in the create container
	self.show_effects_tree()
	self.show_actions_tree()
	self.show_toolbox()

	# Hide the transitions tab
	self.hide_transitions_tree()

	# Set the current tab
	self.open_toolbox()

# Select the given source.
func select_source(source: SerializedObject):
	# Clear properties from last selected source
	self.get_property_tree().clearAll()

	# Have the property tree populate the source properties
	var has_properties = self.get_property_tree().load_properties_for(source)
	if not has_properties:
		self.get_property_tree().hide_property_tree()

	# Show relevant effects
	self.get_effects_tree().filter_for(source)

	# Show the Toolbox, Effects, etc tabs in the create container
	self.show_effects_tree()
	self.show_actions_tree()
	self.show_toolbox()

	# Hide the transitions tab
	self.hide_transitions_tree()

	# Set the current tab
	self.open_toolbox()

	# Signal the edit panel
	var panel: EditPanel = 	self.get_panel_manager().get_active_panel_for(self.get_current_scene())
	if panel:
		panel.select(source, self)
	else:
		self.error("Could not select Source because we could not find an active panel.")

	# Keep track
	self._selected = source

# Select an effect within the scene
func select_effect(effect: SerializedObject):
	# Clear properties from last selected source/etc
	self.get_property_tree().clearAll()

	# Have the property tree populate the effect properties
	var has_properties = self.get_property_tree().load_properties_for(effect)
	if not has_properties:
		self.get_property_tree().hide_property_tree()

	# Keep track
	self._selected = effect

# When an Action is selected from the SourceTree.
func select_action(action: SerializedObject):
	# Keep track
	self._selected = action

# When a transition or the transition section is selected from the SceneTree.
func select_transition(transition: SerializedObject):
	# Only show the Transitions tab in the create container
	self.hide_toolbox()
	self.hide_effects_tree()
	self.hide_actions_tree()

	# Show and tab over to the Transitions tab
	self.open_transitions_tree()

	# Clear properties from last selected source/etc
	self.get_property_tree().clearAll()

	# Have the property tree populate the transition properties
	var has_properties = self.get_property_tree().load_properties_for(transition)
	if not has_properties:
		self.get_property_tree().hide_property_tree()

	# Signal the edit panel
	var panel: EditPanel = 	self.get_panel_manager().get_active_panel_for(self.get_current_scene())
	if panel:
		panel.select(transition, self)
	else:
		self.error("Could not select Transition because we could not find an active panel.")

	# Keep track
	self._selected = transition

# Retrieve the Tree that lists possible Transitions to add to a scene.
func get_transitions_tree() -> Tree:
	return self._container.get_node("%TransitionsTree")

func open_transitions_tree():
	self.show_transitions_tree()
	self.get_create_tabbar().current_tab = self.get_transitions_tree().get_index()

func show_transitions_tree():
	self.get_create_tabbar().set_tab_hidden(self.get_transitions_tree().get_index(), false)

func hide_transitions_tree():
	self.get_create_tabbar().set_tab_hidden(self.get_transitions_tree().get_index(), true)

# When a Transition is selected in the transition tree. We want to show a preview.
func on_transition_selected():
	# Get the transition info
	var transition_item: TreeItem = self.get_transitions_tree().get_selected()
	if not transition_item.has_meta("class"):
		self.warn("Selected Transition does not have an associated class")
		return
	self.warn("Selected Transition does not have an associated class")

	var transition_class: Object = transition_item.get_meta("class")

	var transition = null
	if transition_class is Effect:
		# This is a shader-based Transition
		var transition_effect: Effect = transition_class
		transition = ShaderTransition.new()
		transition.effect = transition_effect.instantiate()
	elif transition_class is GDScript:
		# This is a script-based Transition
		transition = transition_class.new()
	else:
		self.error("Selected Transition is not a usable type")

	# Now, preview the selected transition
	if transition:
		self.preview_transition(transition.instantiate())

func preview_transition(transition: Transition.TransitionInstance):
	# TODO: get the appropriate panel holding the current scene
	var panel: EditPanel = 	self.get_panel_manager().get_active_panel_for(self.get_current_scene())
	if panel:
		panel.preview(transition, self)
	else:
		self.error("Could not preview Transition because we could not find an active panel.")

# When a Transition is activated in the transition tree.
func on_transition_activated():
	var transition_item: TreeItem = self.get_transitions_tree().get_selected()
	if not transition_item.has_meta("class"):
		self.warn("Activated transition does not have an associated class")
		return

	var transition_name: String = transition_item.get_text(0)
	var transition_class: Object = transition_item.get_meta("class")

	var transition: Transition = null
	var transition_instance: SerializedObject.SerializedInstance = null
	if transition_class is Effect:
		# This is a shader-based Transition
		var transition_effect: Effect = transition_class
		transition = ShaderTransition.new()
		transition.effect = transition_effect.instantiate()
	elif transition_class is GDScript:
		# This is a script-based Transition
		transition = transition_class.new()
	else:
		self.error("Activated Transition is not a usable type")

	# Now, add the selected transition to the currently selected transition slot
	if transition:
		transition_instance = transition.instantiate()
		var selected_item: TreeItem = self.get_source_item()
		if selected_item:
			selected_item.set_text(0, transition_name + ' ' + selected_item.get_meta('transition_type'))
			selected_item.set_meta('class', transition_instance)

func add_transitions(transition_manager: TransitionManager, texture_manager: TextureManager):
	for transition_name in transition_manager.get_transition_names():
		if transition_name == 'ShaderTransition':
			continue
		var transition_class: Script = transition_manager.get_transition_by_name(transition_name)
		self.add_transition(transition_class, texture_manager)

	# Also go through the shader Effects that are transitions.
	for effect in transition_manager.get_transition_shaders():
		self.add_transition_effect(effect)

# Adds a Transition component to make it available to the editor.
func add_transition(transition_class: GDScript, _texture_manager: TextureManager):
	var transitions_tree: Tree = self.get_transitions_tree()
	var root: TreeItem = transitions_tree.get_root()
	if not root:
		# Create root
		root = transitions_tree.create_item()
		transitions_tree.hide_root = true

	var item: TreeItem = root.create_child()
	item.set_text(0, transition_class.name())
	item.set_meta("class", transition_class)

# Add a shader Effect that serves as a Transition as a choice in the editor.
func add_transition_effect(effect: Effect):
	var transitions_tree: Tree = self.get_transitions_tree()
	var root: TreeItem = transitions_tree.get_root()
	if not root:
		# Create root
		root = transitions_tree.create_item()
		transitions_tree.hide_root = true

	var shader_item: TreeItem = root.create_child()
	var shader_type: String = effect.category
	var shader_name: String = effect.get_human_name()
	var shader_path: String = effect.get_path()

	shader_item.set_text(0, shader_name)
	shader_item.set_meta("path", shader_path)
	shader_item.set_meta("type", shader_type)
	shader_item.set_meta("class", effect)

func open_effect(effect: SerializedObject):
	self.get_panel_manager().update_panel(effect, 1)

func open_transition(transition: SerializedObject):
	self.get_panel_manager().update_panel(transition, 1)

# Open the given action (generally activated via the SourceTree).
func open_action(action: SerializedObject):
	self.get_panel_manager().update_panel(action, 1)

# When an Action template is selected in the action template tree.
func on_action_activated():
	# Create an Action
	# Add it to the selected action list (global or local?)

	# Add action to selected source
	var action_item: TreeItem = self.get_actions_tree().get_selected()
	if not action_item.has_meta("class"):
		return

	var action_name: String = action_item.get_text(0)
	var action_class: GDScript = action_item.get_meta("class")

	# Add it to the selected source
	var item = self.get_source_item()
	if item:
		item.get_tree().add_action(action_name, action_class)

# When a scene in the scene tree is selected.
func on_scene_selected(_scene_item: TreeItem, scene: Scene):
	# Open the scene for editing
	self.open(scene)

	# Reveal in the property listing
	self.get_property_tree().clear()
	self.get_property_tree().load_properties_for(scene)

# When a frame in the frame tree is selected.
func on_frame_selected():
	pass

# When an effect is the effects tree is activated.
func on_effect_activated(effect: Effect):
	# Instantiate the effect
	var shader: Effect.EffectInstance = effect.instantiate()

	# Add it to the source
	var source_item: TreeItem = self.get_source_item()
	if source_item:
		source_item.get_tree().add_object(shader)

# When a property is updated in the editor
func on_property_updated(_key: String, _value: Variant):
	if self._selected is Transition.TransitionInstance:
		var transition: Transition.TransitionInstance = self._selected
		self.preview_transition(transition)
