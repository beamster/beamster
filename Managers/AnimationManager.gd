class_name AnimationManager
extends Manager


# The set of Frame/Transition/etc objects to handle on a process() call.
var _objects: Array[Object] = []
var _global_time: float = 0.0


signal completed(frame: Object)


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Frame"], Color(0.3, 0.9, 0.3, 1.0))

	# See get_global_time() for a description of this variable.
	self._global_time = Time.get_ticks_msec() / 1000.0

# A upcall from a _process() call that will be used to tell Frame objects to
# update their interpolated properties.
func process(delta: float):
	self._global_time += delta

	var to_remove = []
	for object in self._objects:
		if not object.process(delta):
			to_remove.append(object)

	for object in to_remove:
		self.dequeue(object)

# Retrieve the accumulated computation time since start-up.
#
# Shader 'TIME' is actually the accumulated delta of frame times so does not
# account for computations during the frame building. It skews from real-time
# significantly and loops around every hour.
#
# To compensate, we keep track of this timestamp the same way to more accurately
# predict the shader TIME variable, which we do not have access to just yet.
#
# This value is set to the system timestamp to begin with, in seconds, and then
# the delta is added every processing frame.
func get_frame_seconds() -> float:
	return self._global_time

# Remove the given Frame from the set of objects to call process().
func dequeue(queuable: Object):
	print("dequeue")
	self._objects.erase(queuable)
	self.completed.emit(queuable)

# Add the given Frame or other object with a 'process' method to the set of objects that will receive a process() call.
func queue(queuable: Object, _callback: Callable = func():):
	if queuable.has_method('process'):
		if not self._objects.has(queuable):
			print("queue frame! ", self)
			self._objects.append(queuable)
