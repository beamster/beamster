class_name InterfaceManager
extends Manager


# The Studio instance is the main Node of the application
var _studio: Studio = null

# The manager for the edit tab
var _editor_manager: EditorManager = null

# The container for the profiles
var _profile_container: Control = null

func _init(log_manager: LogManager, profile_manager: ProfileManager, studio: Studio):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0))

	# Retain the studio instance
	self._studio = studio

	# Set the logger's text interface
	log_manager.set_log_text_edit(self.get_log_text_edit())

	# The profile manager is strange in that it needs recognition of the profile
	# container.
	self._profile_container = studio.get_node("%ProfileContainer")
	self._profile_container.open_profile.connect(profile_manager.load_profile)

	# We should then populate the ProfileContainer with the existing profiles
	for profile in profile_manager.get_profiles():
		self._profile_container.add_profile(profile)

	# Create the editor and presentation managers
	self._editor_manager = EditorManager.new(log_manager, profile_manager, self.get_edit_tab_container())

# Returns the Studio instance. This is the top-level Node of the application.
func get_studio() -> Studio:
	return self._studio

func get_editor_manager() -> EditorManager:
	return self._editor_manager

func get_overlay_panel() -> Control:
	return self._studio.get_node("OverlayPanel")

func get_stats_panel() -> Control:
	return self.get_overlay_panel().get_node("StatsPanel")

func show_stats_panel():
	self.get_stats_panel().visible = true

func hide_stats_panel():
	self.get_stats_panel().visible = false

func get_live_status_panel() -> Control:
	return self.get_overlay_panel().get_node("LiveStatusPanel")

func show_live_status_panel():
	self.get_live_status_panel().visible = true

func hide_live_status_panel():
	self.get_live_status_panel().visible = false

func load_all(texture_manager: TextureManager):
	self._editor_manager.load_all(texture_manager)

# Selects the given object for editing.
func select(_object: SerializedObject):
	pass

# Open the given object for editing.
func open(object: SerializedObject):
	self._editor_manager.open(object)

# Returns the main tab container
func get_application_tab_container() -> TabContainer:
	return self._studio.get_node("AppTabContainer")

# Returns the "Profile" tab panel
func get_profile_tab_container() -> Control:
	return self.get_application_tab_container().get_node("ProfileContainer")

# Returns the "Edit" tab panel
func get_edit_tab_container() -> Control:
	return self.get_application_tab_container().get_node("EditContainer")

# Returns the "Present" tab panel
func get_present_tab_container() -> Control:
	return self.get_application_tab_container().get_node("PresentContainer")

# Returns the "System" tab panel
func get_system_tab_container() -> Control:
	return self.get_application_tab_container().get_node("SystemContainer")

# Returns the logger edit box
func get_log_text_edit() -> TextEdit:
	return self.get_system_tab_container().get_node("PanelContainer/LoggerTextEdit")
