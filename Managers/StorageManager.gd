class_name StorageManager
extends Manager


# The storage engine
var _storage: Storage = null

# A cache of all known objects by type
var _objects: Dictionary = {}


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0))

	# Create an instance of our storage engine
	self._storage = Storage.new()

# Get a set of root objects of a particular category.
#
# For instance, we might store all Profile objects, which are root objects that
# keep track of a set of Scene and World objects, under "profiles".
func get_list(category: String) -> Array[SerializedObject]:
	# Sanitize the name of the category by hashing it
	var _hash_path: String = category.md5_text()

	# We will build up a list
	var ret: Array[SerializedObject] = []

	# Read that category's path and the list file within

	# Open and parse that list of ids

	# For each id, restore that object and place in our list
	var object: SerializedObject = null
	ret.append(object)

	return ret

# Append the given item to the reference list for the particular category.
#
# This will increase the reference count for the specified object in the object
# store as well. This means that a list reference will retain the object in the
# object store even if no other object refers to it.
func append_to_list(_category: String, _object: SerializedObject) -> bool:
	return false

# Remove the given item from the reference list for the particular category.
#
# This will decrease the reference count for the specified object in the object
# store as well. If it is no longer referenced, it will be deleted from the
# object store.
func remove_from_list(_category: String, _object: SerializedObject) -> bool:
	return false

# Store the given object.
func store(object: SerializedObject) -> String:
	# Get the serialized form of the object
	var serialization = object.serialize()

	# Store it using the storage engine
	var id: String = self._storage.store(serialization)

	# Return the identifier
	return id

# Restore an object by its identifier.
func restore(id: String) -> SerializedObject:
	# Get the stored serialization
	var serialization: Dictionary = self._storage.restore(id)

	# Based on the type, instantiate that object
	var object: SerializedObject = null
	var type: String = serialization.get("type", "SerializedObject")

	# Build up an object store
	if self._objects.is_empty():
		for class_info in ProjectSettings.get_global_class_list():
			self._objects[class_info["class"]] = class_info["path"]

	# If we are aware of this object type, instantiate it
	if type in self._objects:
		if self._objects[type] is String:
			# Load the object class
			self._objects[type] = load(self._objects[type])

		# Create the object and deserialize it
		object = self._objects[type].new()
		object.restore(serialization)
		self.log("Loaded {type} {name}".format(
			{
				"type": type,
				"name": serialization.get("name", "Unknown"),
			}
		))
	else:
		# We are not aware of this object type.
		self.log("Cannot load the given type: {type}".format(
			{
				"type": type,
			}
		))

	return object
