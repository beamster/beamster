# This keeps track of all possible Element objects we can create and discover.

class_name ElementManager
extends Manager


# All references to Element classes
var _elements: Array[GDScript] = []

# A lookup by name
var _elements_by_name: Dictionary = {}

# All element names
var _element_names: Array[String] = []


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Element"], Color(0.9, 0.6, 0.9, 1.0))

func get_elements() -> Array[GDScript]:
	return self._elements

func get_element_by_name(element_name: String) -> GDScript:
	return self._elements_by_name[element_name]

func get_element_names() -> Array[String]:
	return self._element_names

func load_all(texture_manager: TextureManager):
	# Grab all sources found in the Sources/ path
	var dir: DirAccess = DirAccess.open("res://Elements")
	dir.list_dir_begin()
	var filename = dir.get_next()
	while filename != "":
		if dir.current_is_dir():
			# TODO: recurse
			pass
		else:
			if filename.ends_with("Element.gd"):
				# Load it, if needed
				var element_name = filename.split(".")[0]
				self.get_log_manager().register_class(element_name, Color(0.9, 0.75, 0.9, 1.0))
				self.log("Loading Element {name}".format({"name": element_name}))
				var element = load("res://Elements/" + filename)
				self._elements.append(element)
				self._elements_by_name[element_name] = element
				self._element_names.append(element_name)
				texture_manager.add_icon_texture_for(element)
		filename = dir.get_next()
