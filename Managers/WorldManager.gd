class_name WorldManager
extends Manager


# Properties

# Maintains all Element definitions
var _element_manager: ElementManager = null


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0))

	self._element_manager = ElementManager.new(log_manager)

func get_element_manager() -> ElementManager:
	return self._element_manager

func load_all(texture_manager: TextureManager):
	self._element_manager.load_all(texture_manager)
