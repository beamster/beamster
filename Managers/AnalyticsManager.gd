class_name AnalyticsManager
extends Manager


# Instantiate a platform performance counter engine.
var _perf: PerformanceCounters = PerformanceCounters.new()

# The time since the last computation of the cpu usage
var _last_cpu_query: float = 0.0

# The current known cpu usage
var _cpu_usage: float = 0.0

# The time since the last computation of the cpu usage (per process)
var _last_process_cpu_query: float = 0.0

# The current known cpu usage
var _cpu_process_usage: float = 0.0

# The time since the last computation of the gpu usage
var _last_gpu_query: float = 0.0

# The current known gpu usage
var _gpu_usage: float = 0.0

# The time since the last computation of the gpu usage
var _last_gpu_temp_query: float = 0.0

# The current known gpu temperature
var _gpu_temp: float = 0.0

# The time since the last computation of the gpu power
var _last_gpu_power_query: float = 0.0

# The current known gpu power in milliwatts
var _gpu_power: float = 0.0


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0))

func is_debug_mode() -> bool:
	return OS.has_feature("editor")

func get_total_memory_usage() -> int:
	return OS.get_static_memory_usage()

func get_frames_per_second() -> float:
	return Engine.get_frames_per_second()

func get_architecture_name() -> String:
	return Engine.get_architecture_name()

func get_total_video_memory_usage() -> int:
	return RenderingServer.get_rendering_info(RenderingServer.RENDERING_INFO_VIDEO_MEM_USED)

func get_texture_video_memory_usage() -> int:
	return RenderingServer.get_rendering_info(RenderingServer.RENDERING_INFO_TEXTURE_MEM_USED)

# Returns the CPU usage of the process as a percentage out of 100.
func get_cpu_usage() -> float:
	# We ask the performance counter to sample the CPU at most once every second
	var cur_time: float = Time.get_ticks_msec() / 1000.0
	if self._last_cpu_query == 0.0 or self._last_cpu_query + 1.0 < cur_time:
		self._cpu_usage = _perf.query_cpu_usage()
		self._last_cpu_query = cur_time

	return self._cpu_usage

# Returns the CPU usage of the process as a percentage out of 100.
func get_process_cpu_usage() -> float:
	# We ask the performance counter to sample the CPU at most once every second
	var cur_time: float = Time.get_ticks_msec() / 1000.0
	if self._last_process_cpu_query == 0.0 or self._last_process_cpu_query + 1.0 < cur_time:
		self._cpu_process_usage = _perf.query_process_cpu_usage()
		self._last_process_cpu_query = cur_time

	return self._cpu_process_usage

# Returns the GPU usage across the system as a percentage out of 100.
func get_gpu_usage() -> float:
	# We ask the performance counter to sample the GPU at most once every second
	var cur_time: float = Time.get_ticks_msec() / 1000.0
	if self._last_gpu_query == 0.0 or self._last_gpu_query + 1.0 < cur_time:
		self._gpu_usage = 1.0 #_perf.query_process_gpu_usage()
		if self._gpu_usage < 0.0:
			self._gpu_usage = _perf.query_gpu_usage()
		self._last_gpu_query = cur_time

	return self._gpu_usage

# Returns the GPU temperature in degrees Celcius or negative if not implemented.
func get_gpu_temperature() -> float:
	# We ask the performance counter to sample the GPU at most once every second
	var cur_time: float = Time.get_ticks_msec() / 1000.0
	if self._last_gpu_temp_query == 0.0 or self._last_gpu_temp_query + 1.0 < cur_time:
		self._gpu_temp = _perf.query_gpu_temperature()
		self._last_gpu_temp_query = cur_time

	return self._gpu_temp

# Returns the GPU power usage in milliwatts or negative if not implemented.
func get_gpu_power() -> float:
	# We ask the performance counter to sample the GPU at most once every second
	var cur_time: float = Time.get_ticks_msec() / 1000.0
	if self._last_gpu_power_query == 0.0 or self._last_gpu_power_query + 1.0 < cur_time:
		self._gpu_power = _perf.query_gpu_power()
		self._last_gpu_power_query = cur_time

	return self._gpu_power

func log_statistics():
	# Report whether we are running within the Godot Editor
	if self.is_debug_mode():
		self.log("Running in Godot Editor.")

	# Report OS info
	var os_distribution = OS.get_distribution_name()
	var os_name = OS.get_name()

	if os_distribution != os_name:
		os_name += " [" + os_distribution + "]"

	self.log("OS: {name} {version}".format({
		"name": os_name,
		"version": OS.get_version(),
	}))

	# Report the CPU info
	self.log("CPU: {architecture}".format({
		"architecture": self.get_architecture_name(),
	}))

	# Determine the human string for the adapter type
	var gpu_type: String = [
		"other", "integrated", "discrete", "virtual", "software-renderer",
	][RenderingServer.get_video_adapter_type()]

	# Report GPU info
	self.log("GPU: {name} - type: {type} - vendor: {vendor} - api: {adapter}".format({
		"name": RenderingServer.get_video_adapter_name(),
		"adapter": RenderingServer.get_video_adapter_api_version(),
		"vendor": RenderingServer.get_video_adapter_vendor(),
		"type": gpu_type,
	}))

	self.log("Base memory usage: {usage}".format({
		"usage": self.get_total_memory_usage(),
	}))

	self.log("Base video memory usage: {usage} ({texture}: textures)".format({
		"usage": self.get_total_video_memory_usage(),
		"texture": self.get_texture_video_memory_usage(),
	}))

func log_memory_usage():
	self.log("Memory usage: {usage}".format({
		"usage": self.get_total_memory_usage(),
	}))

	self.log("Video memory usage: {usage} ({texture}: textures)".format({
		"usage": self.get_total_video_memory_usage(),
		"texture": self.get_texture_video_memory_usage(),
	}))
