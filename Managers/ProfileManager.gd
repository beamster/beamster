class_name ProfileManager
extends Manager


var _scene_manager: SceneManager = null
var _world_manager: WorldManager = null


# When a profile is loaded
signal open_profile(profile: Profile)


func _init(log_manager: LogManager, animation_manager: AnimationManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Profile"], Color(0.9, 0.3, 0.3, 1.0))

	# Create the SceneManager
	self._world_manager = WorldManager.new(log_manager)
	self._scene_manager = SceneManager.new(log_manager, animation_manager)

func get_scene_manager() -> SceneManager:
	return self._scene_manager

func get_world_manager() -> WorldManager:
	return self._world_manager

func load_all(texture_manager: TextureManager):
	# Load sources
	self._scene_manager.load_all(texture_manager)

	# Load elements
	self._world_manager.load_all(texture_manager)

# Returns a set of potentially unrealized Profile objects representing the
# known Profile objects within the system.
func get_profiles() -> Array[Profile]:
	return [Profile.new({"name": "Hello Profile"})]

func load_profile(profile: Profile):
	self.open_profile.emit(profile)

# Opens the given Profile.
func open(_profile_name: String):
	# Load the profile object

	# Tell the Scene manager about the scenes

	pass

# Returns a list of known Profiles.
func list() -> Array[String]:
	return []

# Saves the given Profile
func save(profile: Profile):
	var _serialization = profile.serialize()
