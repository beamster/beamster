class_name StreamManager
extends Manager


# The registered encoders
var _encoders: Array[GDScript] = []

# The stream source
var _source: ViewportTexture = null

# A 'muted' source
var _muted_source: Texture = null

# The streams configured for this profile
var _streams: Array[Stream] = []


func _init(log_manager: LogManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["Encoder"], Color(0.65, 0.4, 0.9, 1.0))

# Load all encoder implementations
func load_all() -> Array[GDScript]:
	if self._encoders.size() == 0:
		# Grab all encoders found in the Encoders/ path
		var dir: DirAccess = DirAccess.open("res://Encoders")
		dir.list_dir_begin()
		var filename = dir.get_next()
		while filename != "":
			if dir.current_is_dir():
				# TODO: recurse
				pass
			else:
				if filename.ends_with("Encoder.gd"):
					# Load it, if needed
					var inferred_name = filename.split(".")[0]
					self.get_log_manager().register_class(inferred_name, Color(0.75, 0.5, 1.0, 1.0))
					self.log("Loading Encoder {name}".format({"name": inferred_name}))
			filename = dir.get_next()

	return self._encoders

func get_streams() -> Array[Stream]:
	return self._streams

# Add a stream to the manager
func add_stream(stream: Stream):
	self._streams.append(stream)

# Set the source for the stream
func source(texture: ViewportTexture):
	# Generate the correct muted source the same dimensions as the incoming
	# source texture
	var size: Vector2 = texture.get_size()
	var mute_image = Image.new()
	var data: PackedByteArray = []
	data.resize(int(size.x * size.y) * 4)
	data.fill(0)
	mute_image.set_data(int(size.x), int(size.y), false, Image.FORMAT_RGBA8, data)

	var mute_texture = ImageTexture.create_from_image(mute_image)

	self._muted_source = mute_texture

# Start all streams
func start():
	if self._source:
		for stream in self.get_streams():
			stream.start()

# Stop all streams
func stop():
	if self._source:
		for stream in self.get_streams():
			stream.stop()

# Pass nothing to the streams
func mute():
	if self._muted_source and self._source:
		for stream in self.get_streams():
			stream.set_video_source(self._muted_source)

# Pass back the normal stream
func unmute():
	if self._source:
		for stream in self.get_streams():
			stream.set_video_source(self._source)
