# This manages the loaded panels for editing.

class_name PanelManager
extends Manager


# The editor instance
var _editor: EditorManager

# The registered panels
var _panels: Array[GDScript] = []

# Lookup for the editor panel for a particular type of object.
var _panel_class_by_type: Dictionary = {}
var _panel_associated_types: Array[GDScript] = []
var _instantiated_panels: Dictionary = {}

# Keeps track of the active panels by index
var _active_panels: Array[EditPanel] = []
var _active_things: Array[SerializedObject] = []

# Whether or not we have bound a focus event on the app viewport
var _focus_event_bound: bool = false
var _focus_stylebox: StyleBox = null

# Which panel currently has focus
var _focused_panel: Panel = null
var _focused_panel_thing: SerializedObject = null


# Emitted when an edit panel gets focus
signal edit_panel_focus(thing: SerializedObject, panel: EditPanel)


# This helps us capture focus inside of an edit panel
class CapturePanelFocusNode:
	extends Node


	signal focus()


	func _input(event: InputEvent):
		if event is InputEventMouseButton:
			var button_event: InputEventMouseButton = event
			var outer_panel: Panel = self.get_parent()

			if button_event.pressed:
				var rect: Rect2 = outer_panel.get_global_rect()
				if rect.has_point(outer_panel.get_global_mouse_position()):
					# We are focused on our panel
					self.focus.emit()


func _init(log_manager: LogManager, editor: EditorManager):
	# Register terms in the logger
	super(log_manager, Color(0.65, 0.4, 0.9, 1.0), ["EditPanel"], Color(1.0, 1.0, 1.0, 1.0))

	# Retain the instance to the editor
	self._editor = editor

# Load all EditPanel implementations
func load_all() -> Array[GDScript]:
	if self._panels.size() == 0:
		# Grab all panels found in the EditPanels/ path
		var dir: DirAccess = DirAccess.open("res://UI/Edit/EditPanels")
		dir.list_dir_begin()
		var filename = dir.get_next()
		while filename != "":
			if dir.current_is_dir():
				# TODO: recurse
				pass
			else:
				if filename.ends_with("EditPanel.gd"):
					# Load it, if needed
					var panel_name = filename.split(".")[0]
					self.log("Loading EditPanel {name}".format({
						"name": panel_name
					}))
					var source = load("res://UI/Edit/EditPanels/" + filename)
					self._panels.append(source)
			filename = dir.get_next()

		for panel in self._panels:
			for type: GDScript in panel.get_associated_types():
				self._panel_class_by_type[type] = panel
				self._panel_associated_types.append(type)

	return self._panels

# Get the editor instance this is managing.
func get_editor() -> EditorManager:
	return self._editor

func get_panel(index: int) -> EditPanel:
	return self._active_panels[index]

func get_active_panel_for(thing: SerializedObject) -> EditPanel:
	for i in range(self._active_things.size()):
		var panel: EditPanel = self._active_panels[i]
		var opened_thing: SerializedObject = self._active_things[i]

		if not panel or not opened_thing:
			continue

		if opened_thing == thing:
			return panel

	return null

func get_focused_panel() -> Panel:
	return self._focused_panel

func get_panel_container() -> Container:
	return self.get_editor().get_container().get_node("TopPanel")

func update_panel(thing: SerializedObject, index: int) -> EditPanel:
	if index < 0 or index > 4:
		self.error("Tried to create a panel with an index too large.")
		return null

	var top_panel = self.get_panel_container()

	if not self._focus_event_bound:
		top_panel.get_viewport().gui_focus_changed.connect(self.on_gui_focus_changed.bind())
		self._focus_event_bound = true

	# Instantiate a panel for the given content (or pass it along a different
	# object, if it exists already.)
	var panel_class: GDScript = null
	for panel_type in self._panel_associated_types:
		var script = thing.get_script()
		while script:
			if script == panel_type:
				panel_class = self._panel_class_by_type[panel_type]
				break
			script = script.get_base_script()
		if panel_class != null:
			break

	# Determine if we have an instance of this editor already
	var edit_panel: EditPanel = null
	if panel_class:
		# See if we have stored an existing panel that can handle this type
		if not self._instantiated_panels.has(index):
			self._instantiated_panels[index] = {}
		elif self._instantiated_panels[index].has(panel_class):
			edit_panel = self._instantiated_panels[index][panel_class]

		# If not, we create a new panel
		if not edit_panel:
			edit_panel = panel_class.new()
			edit_panel.visible = false

			self._instantiated_panels[index][panel_class] = edit_panel

			# Take the current child at the index and move it to the bottom of
			# the stack.
			var current_panel = top_panel.get_child(index)
			if current_panel:
				top_panel.move_child(current_panel, -1)
				current_panel.visible = false

			# Add a Panel
			var outer_panel = top_panel.get_node("%OuterEditPanel").duplicate()
			for child in outer_panel.get_children():
				outer_panel.remove_child(child)
			outer_panel.visible = true

			# Get a reference to the stylebox
			var stylebox: StyleBox = outer_panel.get_theme_stylebox("panel")
			if not self._focus_stylebox:
				self._focus_stylebox = stylebox
			outer_panel.set_meta("thing", thing)
			outer_panel.remove_theme_stylebox_override("panel")

			# Add the EditPanel instance to the outer panel
			outer_panel.add_child(edit_panel)
			top_panel.add_child(outer_panel)
			top_panel.move_child(outer_panel, index)

			# Add a focus capture node
			var capture_node: CapturePanelFocusNode = CapturePanelFocusNode.new()
			outer_panel.add_child(capture_node)
			print("added capture node to ", outer_panel)
			capture_node.focus.connect(func():
				self.grab_panel_focus(edit_panel)
			)

			# Ensure the edit panel shows off the hover texture of the outer panel
			edit_panel.anchor_left = 0
			edit_panel.anchor_right = 1
			edit_panel.anchor_top = 0
			edit_panel.anchor_bottom = 1
			edit_panel.offset_left = 2
			edit_panel.offset_right = -2
			edit_panel.offset_top = 2
			edit_panel.offset_bottom = -2
			edit_panel.grow_horizontal = Control.GROW_DIRECTION_BOTH
			edit_panel.grow_vertical = Control.GROW_DIRECTION_BOTH

			# Show the edit panel
			edit_panel.visible = true

		var thing_name: String = ""
		if thing.has_method("get_name"):
			thing_name = thing.get_name()
		if thing_name == "" and thing.has_method("name"):
			thing_name = thing.get_script().name()

		# Load the thing!
		self.log("Opening {type} {thing_name} into panel {index}".format(
			{
				"name": "`" + thing_name + "`",
				"type": thing.system_name(),
				"index": index,
			}
		))
		edit_panel.open(thing, self.get_editor())
		while self._active_panels.size() <= index:
			self._active_panels.append(null)
			self._active_things.append(null)
		self._active_panels[index] = edit_panel
		self._active_things[index] = thing
		self.grab_panel_focus(edit_panel)

	if index == 0:
		var total_size = top_panel.get_rect().size
		if total_size.x > 0:
			top_panel.split_offset = total_size.x * 0.60

	# Return our instantiated editor panel
	return edit_panel

func on_edit_panel_focus(thing: SerializedObject, panel: EditPanel):
	# Put the focus ring on
	var outer_panel: Panel = panel.get_parent()
	outer_panel.add_theme_stylebox_override("panel", self._focus_stylebox)

	# Emit the event for the editor to respond
	self.edit_panel_focus.emit(thing, panel)

func on_edit_panel_blur(_thing: SerializedObject, panel: EditPanel):
	# Take the focus ring off
	var outer_panel: Panel = panel.get_parent()
	if outer_panel.has_theme_stylebox_override("panel"):
		outer_panel.remove_theme_stylebox_override("panel")

func grab_panel_focus(panel: EditPanel):
	# Is this panel already focused? Bail if so
	var outer_panel: Panel = panel.get_parent()
	if self._focused_panel == outer_panel:
		return
	elif self._focused_panel and self._focused_panel != outer_panel:
		# We have an existing focused panel!
		# Blur it!
		var edit_panel: EditPanel = self._focused_panel.get_child(0)
		var thing: SerializedObject = self._focused_panel_thing
		self._focused_panel = null
		self._focused_panel_thing = null
		self.on_edit_panel_blur(thing, edit_panel)

	# Focus this panel!
	self._focused_panel = outer_panel
	self._focused_panel_thing = outer_panel.get_meta("thing")
	self.on_edit_panel_focus(self._focused_panel_thing, panel)

# Every time focus changes, ask ourselves if the control is within a panel or
# not.
func on_gui_focus_changed(control: Control):
	# Now check to see if a panel should be focused instead
	for panel in self.get_panel_container().get_children():
		if panel.is_ancestor_of(control):
			self.grab_panel_focus(panel.get_child(0))
