extends Tree


var _object: SerializedObject = null
var _shader_texture: Texture2D
var _choose_texture: Texture2D
var _reset_texture: Texture2D
var _lookup: Dictionary = {}


signal updated(key: String, value: Variant)


# Called when the node enters the scene tree for the first time.
func _ready():
	self.item_edited.connect(self.on_item_edited.bind())
	self.item_selected.connect(self.on_item_selected.bind())

	# Capture button presses
	self.button_clicked.connect(self.on_button_clicked.bind())

	self._shader_texture = load("res://Assets/Images/Shader.png")
	self._choose_texture = load("res://Assets/Images/choose.png")
	self._reset_texture = load("res://Assets/Images/reset.png")

	var parent: TabContainer = self.get_parent()
	parent.set_tab_title(self.get_index(), "Properties")

# Populates the properties listing with the items described within the given
# property description.
func add_property_header(header: String, icon: Texture2D = null) -> TreeItem:
	var root: TreeItem = self.get_root()
	var propHeader: TreeItem = self.create_item(root)
	propHeader.set_custom_minimum_height(20)

	propHeader.set_text(0, header)
	propHeader.set_meta("text", header)
	if icon != null:
		propHeader.set_meta("icon", icon)
	propHeader.set_cell_mode(0, TreeItem.CELL_MODE_CUSTOM)
	propHeader.set_cell_mode(1, TreeItem.CELL_MODE_CUSTOM)
	propHeader.set_custom_draw(0, self, "draw_header_cell")
	propHeader.set_custom_draw(1, self, "draw_null_cell")
	propHeader.set_editable(0, false)
	propHeader.set_editable(1, false)
	propHeader.set_selectable(0, false)
	propHeader.set_selectable(1, false)

	return propHeader

# Updates the property item value in the UI
func update_property(propItem: TreeItem, value: Variant = null):
	var type = propItem.get_meta("type")
	if typeof(type) == TYPE_ARRAY:
		if value == null or (value is String and value == ""):
			value = type[0]
		propItem.set_text(1, value)
		propItem.set_meta("text", value)
	elif type == "color":
		if value == null or (value is String and value == ""):
			value = Color(1, 1, 1, 1)
		propItem.set_meta("value", value)
	elif type == "bool":
		if typeof(value) != TYPE_BOOL:
			value = false
		propItem.set_checked(1, value)
	elif type == "float" or type == "int":
		if typeof(value) != TYPE_FLOAT and typeof(value) != TYPE_INT:
			value = 0
		propItem.set_range(1, value)
	elif type == "scene":
		propItem.set_text(1, value.get_name())
	else: # 'file', etc
		propItem.set_text(1, str(value))

	var button: int = propItem.get_button_by_id(1, 1024)
	if button >= 0:
		propItem.erase_button(1, propItem.get_button_by_id(1, 1024))
		if typeof(value) == TYPE_STRING:
			if value != propItem.get_meta("default"):
				propItem.add_button(1, self._reset_texture, 1024, false, "Revert to default")

func commit_property(propItem: TreeItem, value: Variant):
	var key = propItem.get_meta("key")
	var object: SerializedObject = propItem.get_meta("base")

	# Get the Frame and set the property on that Frame
	var scene_manager: SceneManager = $/root/Studio.get_profile_manager().get_scene_manager()
	var frame: Frame = scene_manager.get_editing_frame()
	if not object is SerializedObject.SerializedInstance or propItem.get_meta("instance") == true:
		# Update the frame
		frame.update_property(object, key, value, 1.0)

		# Actually apply it to the object
		object.set_property(key, value, true)

		# Reclaim the value (in case it has a constraint)
		value = object.get_property(key)
	else:
		# Update the frame
		frame.update_property(object.get_base(), key, value, 1.0)

		# Actually apply it to the object
		object.get_base().set_property(key, value, true)

		# Reclaim the value (in case it has a constraint)
		value = object.get_base().get_property(key)

	# Upcall event
	self.updated.emit(key, value)

	# Update the UI of our property with the established value
	self.update_property(propItem, value)

func property_item_for(key: String) -> TreeItem:
	return self._lookup[key]

# Creates the property item for the given property description
func add_object_property(key: String, info: Dictionary, value: Variant = null) -> TreeItem:
	var default: Variant = null
	if info.has("default") and (value == null or (value is String and value == "")):
		value = info["default"]
		default = value

	var root = self.get_root()
	var propItem: TreeItem = self.create_item(root)
	self._lookup[key] = propItem
	propItem.set_text(0, info.get("label", key))
	propItem.set_meta("key", key)
	propItem.set_meta("type", info["type"])
	propItem.set_selectable(0, false)
	propItem.set_editable(0, false)
	propItem.set_editable(1, true)
	propItem.set_meta("reset", info.get("reset", ""))

	if typeof(info["type"]) == TYPE_ARRAY:
		propItem.set_cell_mode(1, TreeItem.CELL_MODE_CUSTOM)
		propItem.set_custom_draw(1, self, "draw_enum_cell")
		if value == null or (value is String and value == ""):
			value = info["type"][0]
		if default == null or (default is String and default == ""):
			default = info["type"][0]
		# If the value is an integer, it is an index
		if value is int:
			value = info['type'][value]

		propItem.set_text(1, value)
		propItem.set_meta("text", value)

		var popup = PopupMenu.new()
		for item in info["type"]:
			popup.add_item(item)
		popup.visible = false
		self.add_child(popup)
		propItem.set_meta("popup", popup)
		popup.id_pressed.connect(self.on_enum_item_selected.bind(propItem, popup))
		popup.index_pressed.connect(self.on_enum_item_selected.bind(propItem, popup))
	elif info["type"] == "scene":
		propItem.set_cell_mode(1, TreeItem.CELL_MODE_CUSTOM)
		propItem.set_custom_draw(1, self, "draw_enum_cell")
		if value == null or (value is String and value == ""):
			value = info["type"][0]
		if default == null or (default is String and default == ""):
			default = info["type"][0]

		if value is Object:
			var scene: Scene = value
			propItem.set_text(1, scene.get_name())
			propItem.set_meta("text", scene.get_name())

		var popup = PopupMenu.new()
		popup.visible = false
		self.add_child(popup)
		propItem.set_meta("popup", popup)
		popup.id_pressed.connect(self.on_scene_item_selected.bind(propItem, popup))
		popup.index_pressed.connect(self.on_scene_item_selected.bind(propItem, popup))
	elif info["type"] == "color":
		if value == null or (value is String and value == ""):
			value = Color(1, 1, 1, 1)
		if default == null or (default is String and default == ""):
			default = Color(1, 1, 1, 1)
		propItem.set_cell_mode(1, TreeItem.CELL_MODE_CUSTOM)
		propItem.set_meta("value", value)
		propItem.set_custom_draw(1, self, "draw_color_cell")
	elif info["type"] == "bool":
		propItem.set_cell_mode(1, TreeItem.CELL_MODE_CHECK)
		if typeof(value) != TYPE_BOOL:
			value = false
		if typeof(default) != TYPE_BOOL:
			default = false
		propItem.set_checked(1, value)
	elif info["type"] == "float" or info["type"] == "int":
		if typeof(value) != TYPE_FLOAT and typeof(value) != TYPE_INT:
			value = 0
		if typeof(default) != TYPE_FLOAT and typeof(default) != TYPE_INT:
			default = 0

		propItem.set_cell_mode(1, TreeItem.CELL_MODE_RANGE)
		var min_value: float = info.get("min", -9999)
		var max_value: float = info.get("max", 9999)
		var step: float = info.get("step", 0.001)
		propItem.set_range_config(1, min_value, max_value, step)
		propItem.set_range(1, value)
	elif info["type"] == "file":
		if default == null:
			default = ""
		propItem.set_cell_mode(1, TreeItem.CELL_MODE_STRING)
		propItem.add_button(1, self._choose_texture, 0, false, "Choose File...")
		propItem.set_text(1, str(value))
		if info.has("filter"):
			propItem.set_meta("filter", info["filter"])
	else:
		if default == null:
			default = ""
		propItem.set_cell_mode(1, TreeItem.CELL_MODE_STRING)
		propItem.set_text(1, str(value))

	propItem.set_meta("default", default)
	if typeof(value) == TYPE_STRING:
		if value != propItem.get_meta("default") and propItem.get_meta("reset", "") != "disabled":
			propItem.add_button(1, self._reset_texture, 1024, false, "Revert to default")

	return propItem

func on_button_clicked(item: TreeItem, _column: int, id: int, _mouse_button_index: int):
	var type = item.get_meta("type")

	if type is String and type == "file" and id == 0:
		# Choose file
		var dialog = FileDialog.new()
		dialog.exclusive = true
		dialog.access = FileDialog.Access.ACCESS_FILESYSTEM
		self.add_child(dialog)

		var main_window_size = DisplayServer.window_get_size(0)
		dialog.visible = true
		if item.has_meta("filter"):
			dialog.filters = item.get_meta("filter")
		dialog.file_mode = FileDialog.FILE_MODE_OPEN_FILE
		dialog.popup_centered(main_window_size * 0.75)
		dialog.file_selected.connect(self.on_file_item_edited.bind(item))
	elif id == 1024:
		# This is the revert to default button
		var value = item.get_meta("default")
		self.update_property(item, value)
		self.commit_property(item, value)

func on_file_item_edited(value: String, propItem: TreeItem):
	propItem.set_meta("value", value)
	propItem.set_text(1, value)

	self.commit_property(propItem, value)

func on_object_updated(key: String, value: Variant):
	var propItem: TreeItem = self.property_item_for(key)
	self.update_property(propItem, value)
	self.commit_property(propItem, value)

func load_properties_for(object: SerializedObject) -> bool:
	self.clear()
	self._lookup = {}

	if self._object:
		self._object.updated.disconnect(self.on_object_updated)

	self._object = object
	self._object.updated.connect(self.on_object_updated)

	return self.add_object_properties(object)

# Populates the properties list with the properties of the given source
func add_object_properties(object: SerializedObject, object_class: GDScript = null) -> bool:
	var object_base = object
	if object is SerializedObject.SerializedInstance:
		object_base = object.get_base()

	if not object_class:
		object_class = object_base.get_script()

	var root: TreeItem = self.get_root()
	if not root:
		root = self.create_item()
	self.hide_root = true
	self.columns = 2

	# Get a reference to the inherited source, if any
	var parent: Script = object_class.get_base_script()

	var has_properties: bool = false
	var properties: Dictionary = object_class.properties()

	if not object == object_base:
		var dynamic_base_properties: Dictionary = object_base.dynamic_base_properties(object_base, object_class)
		properties.merge(dynamic_base_properties, true)

	var instance_properties: Dictionary = {}
	var dynamic_instance_properties: Dictionary = {}

	if object is SerializedObject.SerializedInstance:
		instance_properties.merge(object_class.instance().properties(), true)
		if object_class == object_base.get_script():
			dynamic_instance_properties = object.dynamic_properties()
			instance_properties.merge(dynamic_instance_properties, true)

	var parent_instance_properties = {}
	if object is SerializedObject.SerializedInstance and parent:
		parent_instance_properties = parent.instance().properties()

	if parent_instance_properties == instance_properties:
		instance_properties = {}

	if not instance_properties.is_empty() or not properties.is_empty():
		# Get the properties section title
		# This is either the one provided by the properties_name field
		# or, failing that, the source name
		var parent_properties_name = ""
		if parent:
			parent_properties_name = parent.properties_header()

		# If there is no defined properties header, use the name
		var properties_name = object_class.properties_header()
		if properties_name == parent_properties_name:
			properties_name = object_class.name()

		# Add the property header

		var texture_manager: TextureManager = $/root/Studio.get_texture_manager()
		var icon = texture_manager.icon_texture_16_for(object_class.name())
		var propHeader: TreeItem = self.add_property_header(properties_name, icon)

		# Update properties view (TODO: caching by source type)
		for key in properties:
			var info = properties[key]

			# Get current value, if any
			var value: Variant = object_base.get_property(key)

			# Add property
			var propItem: TreeItem = self.add_object_property(key, info, value)
			if not propItem:
				continue

			# Reference the source
			has_properties = true
			propItem.set_meta("base", object)
			propItem.set_meta("instance", false)

		for key in instance_properties:
			var info = instance_properties[key]

			# Get current value, if any
			var value: Variant = object.get_property(key)

			# Add property
			var propItem: TreeItem = self.add_object_property(key, info, value)
			if not propItem:
				continue

			# Reference the object
			has_properties = true
			propItem.set_meta("base", object)
			propItem.set_meta("instance", true)

		if not has_properties:
			propHeader.free()

	# Now get any inherited properties
	if parent and parent != object_base:
		return self.add_object_properties(object, parent) or has_properties

	return has_properties

func on_item_edited():
	var propItem: TreeItem = self.get_edited()

	var type = propItem.get_meta("type")
	var value: Variant = ""

	if typeof(type) == TYPE_ARRAY:
		return
	elif type == "scene":
		return
	elif type == "color":
		return
	elif type == "bool":
		value = propItem.is_checked(1)
	elif type == "float" or type == "int":
		value = propItem.get_range(1)
	else:
		value = propItem.get_text(1)

	self.commit_property(propItem, value)

func on_item_selected():
	var propItem: TreeItem = self.get_selected()
	var index: int = self.get_selected_column()

	if index != 1:
		return

	if propItem.has_meta("popup"):
		var popup: PopupMenu = propItem.get_meta("popup")
		if popup:
			var type = propItem.get_meta("type")
			if typeof(type) == TYPE_STRING and type == "scene":
				# Populate dropdown with list of scenes
				popup.clear()
				var scene_manager: SceneManager = $/root/Studio.get_profile_manager().get_scene_manager()
				for scene_id in scene_manager.get_scenes():
					var scene: Scene = scene_manager.get_scene(scene_id)
					popup.add_item(scene.get_name())
					popup.set_item_metadata(-1, scene)
			popup.position = (DisplayServer.window_get_position(0) * 1.0) + self.global_position + self.get_item_area_rect(propItem, 1).position - self.get_scroll()
			popup.size.x = int(self.get_item_area_rect(propItem, 1).size.x)
			popup.visible = true
			popup.reset_size()
			if popup.size.y + popup.position.y > (DisplayServer.window_get_position(0) * 1.0).y + (DisplayServer.window_get_size(0) * 1.0).y:
				popup.position.y = int((DisplayServer.window_get_position(0) * 1.0).y + (DisplayServer.window_get_size(0) * 1.0).y - popup.size.y)
	elif propItem.get_meta("type") == "color":
		propItem.deselect(1)
		var popup: Popup = Popup.new()
		var picker: ColorPicker = ColorPicker.new()
		self.add_child(popup)
		picker.color = propItem.get_meta("value")
		picker.size_flags_horizontal = SIZE_EXPAND_FILL
		picker.size_flags_vertical = SIZE_EXPAND_FILL
		picker.color_changed.connect(self.on_color_item_edited.bind(propItem))
		popup.add_child(picker)
		popup.position = (DisplayServer.window_get_position(0) * 1.0) + self.global_position + self.get_item_area_rect(propItem, 1).position - self.get_scroll()
		popup.size.x = int(self.get_item_area_rect(propItem, 1).size.x)
		popup.reset_size()
		if popup.size.y + popup.position.y > (DisplayServer.window_get_position(0) * 1.0).y + (DisplayServer.window_get_size(0) * 1.0).y:
			popup.position.y = int((DisplayServer.window_get_position(0) * 1.0).y + (DisplayServer.window_get_size(0) * 1.0).y - popup.size.y)
		popup.visible = true

func on_color_item_edited(value: Color, propItem: TreeItem):
	propItem.set_meta("value", value)
	self.queue_redraw()

	self.commit_property(propItem, value)

func on_scene_item_selected(index: int, propItem: TreeItem, popup: PopupMenu):
	var value: String = popup.get_item_text(index)
	propItem.set_text(1, value)
	propItem.set_meta("text", value)
	propItem.deselect(1)

	var scene = popup.get_item_metadata(index)

	self.commit_property(scene, propItem)

func on_enum_item_selected(index: int, propItem: TreeItem, popup: PopupMenu):
	var value: String = popup.get_item_text(index)
	propItem.set_text(1, value)
	propItem.set_meta("text", value)
	propItem.deselect(1)

	self.commit_property(propItem, value)

func draw_null_cell(_item: TreeItem, _rect: Rect2):
	pass

func draw_enum_cell(_item: TreeItem, _rect: Rect2):
	pass

func draw_header_cell(item: TreeItem, rect: Rect2):
	var PropertyTree: Tree = $%PropertyTree
	rect.size.x = PropertyTree.size.x - 11
	rect.size.y -= 6

	# Get the color from the theme
	var current_theme: Theme = $/root/Studio.theme
	var color: Color = current_theme.get_stylebox("normal", "Button").bg_color

	# Draw the rectangle behind the text
	PropertyTree.draw_rect(rect, color)

	# Get the text for the item and we will determine the offset to draw it
	var text: String = item.get_meta("text")
	var offset: float = 0.0

	# Draw, if any, the icon for the source class
	if item.has_meta("icon"):
		var icon: Texture2D = item.get_meta("icon")
		var header_font: Font = ThemeDB.fallback_font
		var header_font_size: int = ThemeDB.fallback_font_size
		var text_size: Vector2 = header_font.get_string_size(text, HORIZONTAL_ALIGNMENT_LEFT, -1, header_font_size)
		var icon_position = Vector2((rect.size.x - text_size.x) / 2 - 4, rect.position.y + 2)
		offset = 17.0
		PropertyTree.draw_texture(icon, icon_position)

	# Draw the text
	PropertyTree.draw_string(
		ThemeDB.fallback_font,
		Vector2(offset + rect.position.x, rect.position.y + rect.size.y - 6),
		text,
		HORIZONTAL_ALIGNMENT_CENTER,
		rect.size.x - 10,
		ThemeDB.fallback_font_size
	)

func draw_color_cell(item: TreeItem, rect: Rect2):
	var PropertyTree: Tree = $%PropertyTree
	var value: Color = item.get_meta("value")
	rect.size -= Vector2(20, 6)
	rect.position += Vector2(3, 3)
	PropertyTree.draw_rect(rect, value)

func clearAll():
	self.clear()

	# Make sure script editor is not visible
	self.hide_script_editor()

	# Make sure the property list is visible
	self.show_property_tree()

func hide_script_editor():
	self.get_parent().set_tab_hidden(1, true)
	self.get_parent().current_tab = 0

func show_script_editor():
	self.get_parent().set_tab_hidden(1, false)

func get_script_editor() -> CodeEdit:
	return self.get_parent().get_child(1)

func hide_property_tree():
	self.get_parent().set_tab_hidden(0, true)
	self.get_parent().current_tab = 1

func show_property_tree():
	self.get_parent().set_tab_hidden(0, false)
