class_name SkyElement
extends EnvironmentalElement


var _environment: Environment
var _sky: Sky

static func icon() -> String:
	return "res://Elements/Icons/SkyElement.png"

static func name() -> String:
	return "Sky Element"

static func properties() -> Dictionary:
	return {
		"background": {
			"type": "color",
			"label": "Background Color"
		},
		"color": {
			"type": "color",
			"label": "Color"
		}
	}

func set_background(value: Color):
	self._environment.background_color = value
	self.should_update_environment()

func get_background() -> Color:
	return self._environment.background_color

func set_color(_value: Color):
	self.should_update_environment()

func get_color() -> Color:
	return self._environment.ambient_light_color

func update_environment(environment: Environment):
	environment.background_mode = Environment.BG_SKY
	environment.sky = self._sky

func _on_init():
	self._environment = Environment.new()
	self._sky = Sky.new()

func attach_material(material: Material):
	self._sky.sky_material = material
	self.should_update_environment()
