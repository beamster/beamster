class_name ProjectorElement
extends Element


class SubDecal:
	extends Decal

	var _viewport = null

	func _process(_delta: float):
		# Currently, the viewport will not update unless, each frame, we force
		# it to copy the viewport texture over.
		if self._viewport:
			var texture: ViewportTexture = self._viewport.get_texture()
			self.texture_emission = null
			self.texture_emission = texture

var _decal: Decal
var _source: Scene

static func icon() -> String:
	return "res://Elements/Icons/CubeMeshElement.png"

static func name() -> String:
	return "Projector Element"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Color"
		},
		"source": {
			"type": "scene",
			"label": "Source",
		},
	}

func get_native_node_class():
	return MeshInstance3D

func get_native_node() -> Node3D:
	return self._node

func _on_init():
	var element_mesh: MeshInstance3D = self.get_native_node()
	element_mesh.set_meta("element", self)
	element_mesh.mesh = BoxMesh.new()
	#element_mesh.scale = Vector3(0.5, 0.5, 0.5)
	element_mesh.layers = 0x80000
	element_mesh.create_convex_collision()
	element_mesh.transparency = 1.0

	# Create the outline mesh
	var outline_mesh = element_mesh.get_mesh().create_outline(0.05)
	var mesh = MeshInstance3D.new()
	mesh.mesh = outline_mesh
	mesh.layers = 0x80000
	mesh.visible = false

	var outline_material: Material = StandardMaterial3D.new()
	outline_material.albedo_color = Color("b380ffff")
	outline_material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
	mesh.material_override = outline_material

	element_mesh.add_child(mesh)
	element_mesh.set_meta("outline", mesh)

	var decal: Decal = SubDecal.new()
	#decal.scale = Vector3(3, 3, 3)
	#decal.size = Vector3(10, 10, 10)
	decal.cull_mask = 0xffff
	element_mesh.add_child(decal)

	self._decal = decal

func _on_add():
	var parent: Node
	parent = self.get_native_node().get_tree().get_root()
	#self._decal._viewport = parent.get_child(0).get_node("%PresentationWindow")

func set_source(scene: Scene):
	self._source = scene
	self._decal._viewport = scene.get_viewport()

func get_source() -> Scene:
	return self._source
