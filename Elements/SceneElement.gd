class_name SceneElement
extends Element


static func icon() -> String:
	return "res://Elements/Icons/CubeMeshElement.png"

static func name() -> String:
	return "Scene Element"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Color"
		}
	}

func get_native_node_class():
	return MeshInstance3D

func get_native_node() -> Node3D:
	return self._node

func _on_init():
	var element_mesh: MeshInstance3D = self.get_native_node()
	element_mesh.set_meta("element", self)
	element_mesh.mesh = PlaneMesh.new()
	element_mesh.create_convex_collision()

	# Create the outline mesh
	var outline_mesh = element_mesh.get_mesh().create_outline(0.05)
	var mesh = MeshInstance3D.new()
	mesh.mesh = outline_mesh
	mesh.layers = 0x80000
	mesh.visible = false

	var outline_material: Material = StandardMaterial3D.new()
	outline_material.albedo_color = Color("b380ffff")
	outline_material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
	mesh.material_override = outline_material

	element_mesh.add_child(mesh)
	element_mesh.set_meta("outline", mesh)

func _on_add():
	var parent: Node
	parent = self.get_native_node().get_tree().get_root()
	var viewport: Viewport = parent.get_child(0).get_node("%PresentationWindow")
	var texture: ViewportTexture = viewport.get_texture()

	var material: Material = StandardMaterial3D.new()
	var element_mesh: MeshInstance3D = self.get_native_node()
	material.albedo_texture = texture
	element_mesh.material_override = material
