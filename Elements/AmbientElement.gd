class_name AmbientElement
extends EnvironmentalElement


var _environment: Environment

static func icon() -> String:
	return "res://Elements/Icons/FogElement.png"

static func name() -> String:
	return "Ambient Element"

static func properties() -> Dictionary:
	return {
		"background": {
			"type": "color",
			"label": "Background Color"
		},
		"color": {
			"type": "color",
			"label": "Color"
		},
		"energy": {
			"type": "float",
			"label": "Energy",
			"min": 0.0,
			"max": 16.0
		}
	}

func set_background(value: Color):
	self._environment.background_color = value
	self.should_update_environment()

func get_background() -> Color:
	return self._environment.background_color

func set_color(value: Color):
	self._environment.ambient_light_color = value
	self.should_update_environment()

func get_color() -> Color:
	return self._environment.ambient_light_color

func set_energy(value: float):
	self._environment.ambient_light_energy = value
	self.should_update_environment()

func get_energy() -> float:
	return self._environment.ambient_light_energy

func update_environment(environment: Environment):
	environment.ambient_light_source = Environment.AMBIENT_SOURCE_COLOR

	# Allow a SkyElement to override this
	if environment.background_mode != Environment.BG_SKY:
		environment.background_mode = Environment.BG_CLEAR_COLOR
	environment.background_color = self._environment.background_color
	environment.ambient_light_color = self._environment.ambient_light_color
	environment.ambient_light_energy = self._environment.ambient_light_energy

func _on_init():
	self._environment = Environment.new()
