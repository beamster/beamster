class_name CubeMeshElement
extends Element


static func icon() -> String:
	return "res://Elements/Icons/CubeMeshElement.png"

static func name() -> String:
	return "Cube Mesh Element"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Color"
		}
	}

func get_native_node_class():
	return MeshInstance3D

func get_native_node() -> Node3D:
	return self._node

func _on_init():
	var element_mesh: MeshInstance3D = self.get_native_node()
	element_mesh.set_meta("element", self)
	element_mesh.mesh = BoxMesh.new()
	element_mesh.create_convex_collision()

	# Create the outline mesh
	var outline_mesh = element_mesh.get_mesh().create_outline(0.05)
	var mesh = MeshInstance3D.new()
	mesh.mesh = outline_mesh
	mesh.layers = 0x80000
	mesh.visible = false

	var outline_material: Material = StandardMaterial3D.new()
	outline_material.albedo_color = Color("b380ffff")
	outline_material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
	mesh.material_override = outline_material

	element_mesh.add_child(mesh)
	element_mesh.set_meta("outline", mesh)
