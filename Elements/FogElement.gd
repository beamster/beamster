class_name FogElement
extends EnvironmentalElement


var _environment: Environment

static func icon() -> String:
	return "res://Elements/Icons/FogElement.png"

static func name() -> String:
	return "Fog Element"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Light Color"
		},
		"energy": {
			"type": "float",
			"label": "Light Energy",
			"min": 0.0,
			"max": 16.0
		},
		"sun_scatter": {
			"type": "float",
			"label": "Sun Scatter",
			"min": 0.0,
			"max": 1.0
		},
		"density": {
			"type": "float",
			"label": "Density",
			"min": 0.0,
			"max": 1.0
		},
		"sky_affect": {
			"type": "float",
			"label": "Sky Affect",
			"min": 0.0,
			"max": 1.0
		},
		"height": {
			"type": "float",
			"label": "Height",
			"min": -1024.0,
			"max": 1024.0
		},
		"height_density": {
			"type": "float",
			"label": "Height Density",
			"min": -16.0,
			"max": 16.0
		}
	}

func set_color(value: Color):
	self._environment.fog_light_color = value
	self.should_update_environment()

func get_color() -> Color:
	return self._environment.fog_light_color

func set_energy(value: float):
	self._environment.fog_light_energy = value
	self.should_update_environment()

func get_energy() -> float:
	return self._environment.fog_light_energy

func set_sun_scatter(value: float):
	self._environment.fog_sun_scatter = value
	self.should_update_environment()

func get_sun_scatter() -> float:
	return self._environment.fog_sun_scatter

func set_density(value: float):
	self._environment.fog_density = value
	self.should_update_environment()

func get_density() -> float:
	return self._environment.fog_density

func set_sky_affect(value: float):
	self._environment.fog_sky_affect = value
	self.should_update_environment()

func get_sky_affect() -> float:
	return self._environment.fog_sky_affect

func set_height(value: float):
	self._environment.fog_height = value
	self.should_update_environment()

func get_height() -> float:
	return self._environment.fog_height

func set_height_density(value: float):
	self._environment.fog_height_density = value
	self.should_update_environment()

func get_height_density() -> float:
	return self._environment.fog_height_density

func update_environment(environment: Environment):
	environment.fog_enabled = true
	environment.fog_sun_scatter = self._environment.fog_sun_scatter
	environment.fog_light_color = self._environment.fog_light_color
	environment.fog_light_energy = self._environment.fog_light_energy
	environment.fog_sky_affect = self._environment.fog_sky_affect
	environment.fog_density = self._environment.fog_density
	environment.fog_height = self._environment.fog_height
	environment.fog_height_density = self._environment.fog_height_density

func _on_init():
	self._environment = Environment.new()
	self._environment.fog_enabled = true
	self._environment.fog_sky_affect = 0.4
