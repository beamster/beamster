class_name EnvironmentalElement
extends Element


static func name() -> String:
	return "Environmental Element"

static func properties_header() -> String:
	return "Environment Properties"

static func icon() -> String: # TODO: update with a "globe icon"
	return "res://Base/Icons/TransformableSource.svg"

# Called by an element when it wants to update the environment
func should_update_environment():
	# Perform an upcall to the World to produce a new Environment
	var world_viewport = self.get_world_viewport()
	world_viewport.update_environment()

# Called when the element should update the given environment.
func update_environment(_environment: Environment):
	pass

func get_visible() -> bool:
	var node: Node3D = self.get_native_node()
	if node:
		return node.visible

	return false

func set_visible(value: bool):
	self.get_native_node().visible = value
	self.should_update_environment()
