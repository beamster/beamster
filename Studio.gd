# The entrypoint and root of the application.
class_name Studio
extends PanelContainer


# Create a logger
var _log_manager: LogManager = LogManager.new()

# Create a manager to track performance
var _analytics_manager: AnalyticsManager = AnalyticsManager.new(_log_manager)

# Create a manager to track persisted data
var _storage_manager: StorageManager = StorageManager.new(_log_manager)

# Create a manager to manage loaded textures
var _texture_manager: TextureManager = TextureManager.new(_log_manager)

# Create a manager to maintain the audio system
var _audio_manager: AudioManager = AudioManager.new(_log_manager)

# Create a manager to handle the application interface
var _interface_manager: InterfaceManager = null

# The manager for the profile tab
var _presentation_manager: PresentationManager = null

# Create a manager to handle loading profiles and scenes
var _profile_manager: ProfileManager = null

var _stats_cpu_label: Label
var _stats_cpu_label_format: String
var _stats_gpu_label: Label
var _stats_gpu_label_format: String
var _stats_fps_label: Label
var _stats_fps_label_format: String


func get_log_manager() -> LogManager:
	return self._log_manager

func get_analytics_manager() -> AnalyticsManager:
	return self._analytics_manager

func get_storage_manager() -> StorageManager:
	return self._storage_manager

func get_texture_manager() -> TextureManager:
	return self._texture_manager

func get_audio_manager() -> AudioManager:
	return self._audio_manager

func get_interface_manager() -> InterfaceManager:
	return self._interface_manager

# Returns the presentation manager.
func get_presentation_manager() -> PresentationManager:
	return self._presentation_manager

func get_profile_manager() -> ProfileManager:
	return self._profile_manager

# Before all else, we load the subclasses
# TODO: load lazily, and show spinners on things like the toolbox, etc
func _init():
	var log_manager: LogManager = self.get_log_manager()
	log_manager.register_context("Studio", Color(0.65, 0.4, 0.9, 1.0))
	log_manager.log("Starting.", "Studio")

	# Log basic version info of Godot
	var version_info: Dictionary = Engine.get_version_info()
	log_manager.log(
		(
			"Godot: version {major}.{minor}.{patch} ({status})" +
			"[{build}@{hash}] released: {year}"
		).format(version_info), "Studio"
	)

	# Report base analytics
	self.get_analytics_manager().log_statistics()

	self._presentation_manager = PresentationManager.new(log_manager)

	self._profile_manager = ProfileManager.new(_log_manager, self.get_presentation_manager().get_animation_manager())

# Called when the program starts
func _ready():
	# Create the interface manager (which attaches the logger as well)
	self._interface_manager = InterfaceManager.new(self._log_manager, self._profile_manager, self)

	# Load components
	self._presentation_manager.load_all(self._texture_manager)
	self._profile_manager.load_all(self._texture_manager)
	self._interface_manager.load_all(self._texture_manager)

	# Add sources to editor
	self._interface_manager.get_editor_manager().add_sources(
		self._profile_manager.get_scene_manager().get_source_manager(),
		self._texture_manager
	)

	self._interface_manager.get_editor_manager().add_transitions(
		self._profile_manager.get_scene_manager().get_transition_manager(),
		self._texture_manager
	)

	# Add elements to editor
	#self._interface_manager.get_editor_manager().add_elements(
	#	self._profile_manager.get_world_manager().get_element_manager()
	#)

	# Add effects to editor
	self._interface_manager.get_editor_manager().add_effects(
		self._texture_manager
	)

	# Add actions templates to editor
	self._interface_manager.get_editor_manager().add_actions(
		self.get_presentation_manager().get_action_manager(),
		self._texture_manager
	)

	# Set stats label
	self._stats_cpu_label = self.get_node("OverlayPanel/StatsPanel/HBoxContainer/StatsCPULabel")
	self._stats_cpu_label_format = self._stats_cpu_label.text
	self._stats_gpu_label = self.get_node("OverlayPanel/StatsPanel/HBoxContainer/StatsGPULabel")
	self._stats_gpu_label_format = self._stats_gpu_label.text
	self._stats_fps_label = self.get_node("OverlayPanel/StatsPanel/HBoxContainer/StatsFPSLabel")
	self._stats_fps_label_format = self._stats_fps_label.text
	self.update_statistics()

	# Give the SceneManager a rendering root so viewports go into the drawing
	# queues. Otherwise, scene rendering contexts get skipped.
	self._profile_manager.get_scene_manager().set_viewport($RenderContainer)

	# Focus on the editor window and maximize it
	get_tree().get_root().grab_focus()
	DisplayServer.window_set_min_size(self.get_custom_minimum_size(), 0)
	#get_tree().get_root().mode = Window.MODE_MAXIMIZED

	# Create initial Scene (load previously saved Scene)
	var scene_item: TreeItem = $%SceneEditTree.add_scene("Scene")
	$%SceneEditTree.add_scene("Scene 2")
	$%SceneEditTree.add_scene("Scene 3")

	# TODO: On failure, if any, load backup Scene
	pass

	# TODO: On successful launch, backup this Scene
	pass

	var world_item: TreeItem = $%WorldEditTree.add_world("World")
	world_item.select(0)

	#$%WorldViewport.add_element($%WorldViewport.create_element(CubeMeshElement), CubeMeshElement)
	$%WorldViewport.add_element($%WorldViewport.create_element(ProjectorElement), ProjectorElement)
	$%WorldViewport.add_element($%WorldViewport.create_element(FogElement), FogElement)
	$%WorldViewport.add_element($%WorldViewport.create_element(SkyElement), SkyElement)
	$%WorldViewport.add_element($%WorldViewport.create_element(AmbientElement), AmbientElement)

	# Create initial Script
	$%ScriptEditTree.add_script("Script", "func on_ready():\n	pass")

	# Look at the current scene
	scene_item.select(0)

	# Open an action
	#$/root/Studio/AppTabContainer/EditContainer.update_panel(action, 1)

	#var opencv = OpenCV.new()
	#var opencv_camera = OpenCVCamera.new()
	#var ffmpeg_camera = FFMpegCamera.new()
	#ffmpeg_camera.set_source("x")
	#var ffmpeg = FFMpegStreamPlayer.new()
	#ffmpeg.set_file("e:/hilda.mkv")
	#print(ffmpeg.get_frames())
	#print(ffmpeg.get_texture_size())
	#var image: ImageTexture = opencv_camera.get_video_texture()
	#var sprite = $AppTabContainer/Panel.get_child(0)
	#sprite.texture = image
	#sprite.transform = Transform2D(0.0, Vector2(0.5, 0.5), 0.0, Vector2(540, 300))
	#var duration = ffmpeg.get_duration()
	#var minutes = floor(duration / 60)
	#var seconds = roundi(duration - (minutes * 60))
	#if seconds < 10:
	#	seconds = "0" + seconds
	#print("duration: ", minutes, ":", seconds)
	#ffmpeg_camera.centered = false
	#$AppTabContainer/Panel.add_child(ffmpeg_camera)
	#ffmpeg.centered = false
	#$AppTabContainer/Panel.add_child(ffmpeg)

	# Set the current tab to the Profile tab
	#self.get_node("AppTabContainer").set_current_tab(self.get_edit_container().get_index())

	#var encoder = FFMpegStreamEncoder.new()
	#encoder.texture = scene.get_viewport().get_texture()
	#encoder.target = "output.avi"
	#encoder.record()

	#var ghk = GlobalHotkey.new()
	#ghk.key_event.connect(func(event: InputEventKey):
	#	if event is InputEventKey and event.is_pressed():
	#		print(event.as_text_keycode())
	#)

var _log_timing: float = 0

func _physics_process(delta: float):
	self._log_timing += delta
	if self._log_timing > 2.0:
		self._log_timing = 0

		# Update statistics on screen
		self.update_statistics()

	self.get_presentation_manager().get_animation_manager().process(delta)

func update_statistics():
	# Update the CPU label
	self._stats_cpu_label.text = _stats_cpu_label_format.format({
		"cpu_usage": str(round(self._analytics_manager.get_process_cpu_usage())) + "%",
	})

	# Update the GPU label
	self._stats_gpu_label.text = _stats_gpu_label_format.format({
		"gpu_usage": str(self._analytics_manager.get_gpu_usage()) + "%",
		"gpu_memory": str(ceil(self._analytics_manager.get_total_video_memory_usage() / 1024.0 / 1024.0)) + "MB",
		"gpu_temperature": str(self._analytics_manager.get_gpu_temperature()) + "°C",
		"gpu_power": str(round(self._analytics_manager.get_gpu_power() / 1000.0)) + "W",
	})

	# Update the FPS label
	self._stats_fps_label.text = _stats_fps_label_format.format({
		"fps": self._analytics_manager.get_frames_per_second(),
	})
