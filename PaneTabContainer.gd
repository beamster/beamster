extends TabContainer


# Called when the node enters the scene tree for the first time.
func _ready():
	self.tab_changed.connect(self.on_tab_changed.bind())

func on_tab_changed(_tab: int):
	for child in self.get_current_tab_control().get_child(0).get_children():
		if child is Tree:
			child.on_activation()
