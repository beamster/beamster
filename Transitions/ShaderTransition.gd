class_name ShaderTransition
extends Transition


static func name() -> String:
	return "Shader"

# The human header for properties of this component.
static func properties_header() -> String:
	return "Effect Properties"

var effect: Effect.EffectInstance = null:
	set = set_effect,
	get = get_effect

func set_effect(value: Effect.EffectInstance):
	effect = value

	# Upcall the effect's updates
	effect.updated.connect(func(k, v):
		self.updated.emit(k, v)
	)

func get_effect() -> Effect.EffectInstance:
	return effect

func get_property(key: String) -> Variant:
	# Just get the property from the base material
	if self.effect.has_property(key):
		print('getting ', key, ' = ', self.effect.get_property(key))
		return self.effect.get_property(key)

	return super.get_property(key)

func set_property(key: String, value: Variant, silent: bool = false):
	if self.effect.has_property(key):
		print('setting ', key, ' to ', value)
		self.effect.set_property(key, value, silent)

	# Upcall
	super.set_property(key, value, silent)

static func properties() -> Dictionary:
	return {}

# A set of static instanced properties, dynamically
static func dynamic_base_properties(base: SerializedObject, base_class: GDScript) -> Dictionary:
	if base_class != ShaderTransition:
		return {}

	if not base.effect:
		return {}

	# Extend the properties with the ones from the shader
	var ret: Dictionary = base.effect.get_base().get_shader_properties()

	# Ignore the duration variable and start_time
	if ret.has('START_TIME'):
		ret.erase('START_TIME')
	if ret.has('duration'):
		ret.erase('duration')

	return ret

# Perform a transition between the given scenes.
func perform(transition_instance: Transition.TransitionInstance, from: Scene, to: Scene, within: SubViewportContainer, global_time: float):
	# Attach the from Scene's texture to the shader 'TEXTURE_FROM' sampler
	var texture_from: Texture = null
	var texture_to: Texture = null

	if from:
		texture_from = from.get_viewport().get_texture()
	else:
		# Get a default texture
		texture_from = GradientTexture1D.new()
		var gradient = Gradient.new()
		var color: Color = Color.from_hsv(1.0, 0.6, 0.7)
		gradient.colors = [color]
		texture_from.width = 1
		texture_from.gradient = gradient

	if to:
		texture_to = to.get_viewport().get_texture()
	else:
		# Get a default texture
		texture_to = GradientTexture1D.new()
		var gradient = Gradient.new()
		var color: Color = Color.from_hsv(0.5, 0.6, 0.7)
		gradient.colors = [color]
		texture_to.width = 1
		texture_to.gradient = gradient

	# Get the shader
	var effect_instance: Effect.EffectInstance = self.effect

	# Create the shader material to assign the scene textures to
	var shader_material: ShaderMaterial = effect_instance.get_material()
	shader_material.set_shader_parameter("TEXTURE_FROM", texture_from)
	shader_material.set_shader_parameter("TEXTURE_TO", texture_to)
	shader_material.set_shader_parameter("START_TIME", global_time + 0.2)

	var duration = transition_instance.get_duration_override()
	if duration == 0:
		duration = self.get_duration()
	shader_material.set_shader_parameter('duration', duration)

	var sub_mats: Array = []
	for sub_material in effect.get_buffers():
		if sub_material is ShaderMaterial:
			# TODO: create a render stack for this somehow
			#var container = self.new_viewport_sibling_node(sub_material)
			#var sub_effect: Effect = effect.get_base().get_buffers()[buffer_index]
			#sub_mats.append([sub_effect.get_name(), container])
			pass
		elif sub_material is Texture:
			sub_mats.append([sub_material.get_meta("name"), sub_material])

	# Assign buffer textures
	for sub_mat in sub_mats:
		var buffer_name: String = sub_mat[0]
		var texture: Texture = null
		if sub_mat[1] is Texture:
			texture = sub_mat[1]
		else:
			var buffer: Sprite2D = sub_mat[1]
			texture = buffer.get_parent().get_texture()

		# Assign to main texture
		shader_material.set_shader_parameter(buffer_name, texture)

		# Assign texture parameters
		for parameter in texture.get_meta("parameters", []):
			shader_material.set_shader_parameter(parameter[0], parameter[1])

		# Assign to all other sub_mats
		for sub_material in effect.get_buffers():
			if sub_material is ShaderMaterial:
				sub_material.set_shader_parameter(buffer_name, texture)
				sub_material.set_shader_parameter("START_TIME", Time.get_ticks_msec() / 1000.0)

				# Assign texture parameters
				for parameter in texture.get_meta("parameters", []):
					sub_material.set_shader_parameter(parameter[0], parameter[1])

	# Attach the effect material to the rendering viewport
	within.material = shader_material
