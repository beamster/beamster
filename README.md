# Beamster

This project implements a full multimedia designer and broadcaster geared toward
real-time streaming and rich video recording.

It is written on top of and using the
[Godot Game Engine](https://godotengine.org/), a free and open-source game
engine in the likes of Unity.

Features:

* Separate Editor and Presentation Interfaces
* Easy Scene Import/Export Across Profiles
* Built-in Support for Scene Animations
* Full video support using FFMpeg
* Build 3D Interactive Worlds
* Both Block-based and Code Scripting
* First-class Shader Support

# Scene Editing

# Animation Editing

# World Editing

# Presenting

# Twitch Integration
