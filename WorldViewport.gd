extends SubViewportContainer


var _worlds: Dictionary = {}
var _world: Node3D
var _hovered: Element
var _selected_element: Element

# editor/editor_data.cpp

class EditorData:
	pass

class _Node3DEditorSelectedItem:
	var original: Transform3D
	var original_local: Transform3D
	var last_xform: Transform3D # last transform
	var last_xform_dirty: bool = true
	var sp: Node3D = null
	#var sbox_instance: RID
	#var sbox_instance_offset: RID
	#var sbox_instance_xray: RID
	#var sbox_instance_xray_offset: RID
	var gizmo: Node3D = null
	var subgizmos: Dictionary = {} # map ID -> initial transform (Transform3D)

class _EditorSelection:
	var _selection: Dictionary = {}
	var _emitted: bool = false
	var _changed: bool = false
	var _node_list_changed: bool = false
	var _editor_plugins: Array = []
	var _selected_node_list: Array = []

	signal selection_changed

	func get_node_editor_data(p_node: Node) -> Variant:
		if not self._selection.has(p_node):
			return null

		return self._selection[p_node]

	func add_node(p_node: Node):
		if p_node == null:
			return

		if not p_node.is_inside_tree():
			return

		if self._selection.has(p_node):
			return

		self._changed = true;
		self._node_list_changed = true;
		var meta: Variant = null

		for plugin in self._editor_plugins:
			meta = plugin._get_editor_data(p_node)
			if meta:
				break

		self._selection[p_node] = meta

		p_node.tree_exiting.connect(self._node_removed.bind(p_node), CONNECT_ONE_SHOT)

	func remove_node(p_node: Node):
		if p_node == null:
			return

		if not self._selection.has(p_node):
			return

		self._changed = true
		self._node_list_changed = true
		var _meta = self._selection[p_node]

		self._selection.erase(p_node)

		p_node.tree_exiting.disconnect(self._node_removed)

	func is_selected(p_node: Node):
		return self._selection.has(p_node)

	func _update_node_list() -> void:
		if not self._node_list_changed:
			return

		self._selected_node_list.clear();

		# If the selection does not have the parent of the selected node, then add the node to the node list.
		# However, if the parent is already selected, then adding this node is redundant as
		# it is included with the parent, so skip it.
		for sp in self._selection:
			var parent = sp.get_parent()
			var skip: bool = false
			while parent:
				if self._selection.has(parent):
					skip = true
					break
				parent = parent.get_parent()

			if skip:
				continue

			self._selected_node_list.append(sp)

		self._node_list_changed = true

	func _get_transformable_selected_nodes() -> Array:
		var ret: Array = []

		for sp in self._selected_node_list:
			ret.append(sp)

		return ret;

	func _emit_change() -> void:
		emit_signal("selection_changed")

	func _node_removed(p_node: Node3D) -> void:
		if not self._selection.has(p_node):
			return

		var _meta = self._selection[p_node]

		self._selection.erase(p_node)
		self._changed = true
		self._node_list_changed = true

	func update():
		self._update_node_list()

		if not self._changed:
			return
		self._changed = false

		if not self._emitted:
			self._emitted = true
		call_deferred("_emit_change")

	func clear():
		for key in self._selection:
			self.remove_node(key)

		self._changed = true
		self._node_list_changed = true


	func get_selected_nodes() -> Array:
		var ret: Array = []

		for sp in self._selection:
			ret.append(sp)

		return ret

	func get_selected_node_list() -> Array:
		if self._changed:
			self.update()
		else:
			self._update_node_list()

		return self._selected_node_list

	func get_full_selected_node_list() -> Array:
		var node_list: Array = []

		for key in self._selection:
			node_list.append(key)

		return node_list

	func get_selection() -> Dictionary:
		return self._selection

	func add_editor_plugin(p_object):
		self._editor_plugins.append(p_object)

# A lot of this is a GDScript version of the Godot source that handles the
# 3D node editing. (godot/editor/plugins/node_3d_editor_plugin.cpp)

var _current_hover_gizmo: Node3D = null
var _current_hover_gizmo_handle: int = -1
var _current_hover_gizmo_handle_secondary: bool = false

var _clicked_wants_append: bool = false
var _selection_in_progress: bool = false
var _clicked: Variant
var _selected: Node3D
var _last: Node3D
var _gizmo: Node3D = Node3D.new()
var _gizmo_scale: float
var _editor_selection: _EditorSelection = _EditorSelection.new()
var _local_coords: bool = false

var _undo_redo: UndoRedo = UndoRedo.new()

var _snap_enabled: bool = false
var _snap_key_enabled: bool = false

var _move_gizmo: Array = [null, null, null]
var _move_gizmo_instance: Array = [null, null, null]
var _move_plane_gizmo: Array = [null, null, null]
var _move_plane_gizmo_instance: Array = [null, null, null]
var _rotate_gizmo: Array = [null, null, null, null]
var _rotate_gizmo_instance: Array = [null, null, null, null]
var _scale_gizmo: Array = [null, null, null]
var _scale_gizmo_instance: Array = [null, null, null]
var _scale_plane_gizmo: Array = [null, null, null]
var _scale_plane_gizmo_instance: Array = [null, null, null]
var _axis_gizmo: Array = [null, null, null]
var _axis_gizmo_instance: Array = [null, null, null]

var _gizmo_color: Array = [null, null, null]
var _plane_gizmo_color: Array = [null, null, null]
var _rotate_gizmo_color: Array = [null, null, null]
var _gizmo_color_hl: Array = [null, null, null]
var _plane_gizmo_color_hl: Array = [null, null, null]
var _rotate_gizmo_color_hl: Array = [null, null, null]

const CMP_EPSILON: float = 0.00001
const EDSCALE: float = 1.0
const VIEWPORTS_COUNT: int = 4

# We can't use the same layers as the godot editor :(
# We have to now also take away layers for ourselves
const GIZMO_BASE_LAYER: int = 19
const GIZMO_EDIT_LAYER: int = 18
const GIZMO_GRID_LAYER: int = 17
const MISC_TOOL_LAYER: int = 16

enum TransformMode {
	TRANSFORM_NONE,
	TRANSFORM_ROTATE,
	TRANSFORM_TRANSLATE,
	TRANSFORM_SCALE
}

enum TransformPlane {
	TRANSFORM_VIEW,
	TRANSFORM_X_AXIS,
	TRANSFORM_Y_AXIS,
	TRANSFORM_Z_AXIS,
	TRANSFORM_YZ,
	TRANSFORM_XZ,
	TRANSFORM_XY
}

enum NavigationZoomStyle {
	NAVIGATION_ZOOM_VERTICAL,
	NAVIGATION_ZOOM_HORIZONTAL
}

enum NavigationScheme {
	NAVIGATION_GODOT,
	NAVIGATION_MAYA,
	NAVIGATION_MODO,
}

var _nav_scheme: NavigationScheme = NavigationScheme.NAVIGATION_GODOT

enum NavigationMode {
	NAVIGATION_NONE,
	NAVIGATION_PAN,
	NAVIGATION_ZOOM,
	NAVIGATION_ORBIT,
	NAVIGATION_LOOK,
	NAVIGATION_MOVE
}

var _nav_mode: NavigationMode = NavigationMode.NAVIGATION_NONE

enum ViewType {
	VIEW_TYPE_USER,
	VIEW_TYPE_TOP,
	VIEW_TYPE_BOTTOM,
	VIEW_TYPE_LEFT,
	VIEW_TYPE_RIGHT,
	VIEW_TYPE_FRONT,
	VIEW_TYPE_REAR
}

var _view_type: ViewType = ViewType.VIEW_TYPE_USER

enum FreelookNavigationScheme {
	FREELOOK_DEFAULT,
	FREELOOK_PARTIALLY_AXIS_LOCKED,
	FREELOOK_FULLY_AXIS_LOCKED,
}

enum ToolMode {
	TOOL_MODE_SELECT,
	TOOL_MODE_MOVE,
	TOOL_MODE_ROTATE,
	TOOL_MODE_SCALE,
	TOOL_MODE_LIST_SELECT,
	TOOL_LOCK_SELECTED,
	TOOL_UNLOCK_SELECTED,
	TOOL_GROUP_SELECTED,
	TOOL_UNGROUP_SELECTED,
	TOOL_MAX
}

var _tool_mode: ToolMode = ToolMode.TOOL_MODE_SELECT

enum ToolOptions {
	TOOL_OPT_LOCAL_COORDS,
	TOOL_OPT_USE_SNAP,
	TOOL_OPT_OVERRIDE_CAMERA,
	TOOL_OPT_MAX
}

# Called when the node enters the scene tree for the first time.
func _ready():
	self._editor_selection.add_editor_plugin(self)
	$SubViewport/World.visible = false
	self._init_indicators()
	self._init_gizmo_instance()
	self._process(0)

func _get_editor_data(p_what: Node) -> Variant:
	var sp: Node3D = p_what
	if not sp:
		return null

	var si: _Node3DEditorSelectedItem = _Node3DEditorSelectedItem.new()
	si.sp = sp

	# TODO: Create the selection box geometry
	return si

func xform_inv(a: Transform3D, b: Vector3) -> Vector3:
	# transform any transform.xform_inv(Vector3) to xform_inv(transform, Vector3)
	var v: Vector3 = b - a.origin

	return Vector3(
		(a.basis[0][0] * v.x) + (a.basis[1][0] * v.y) + (a.basis[2][0] * v.z),
		(a.basis[0][1] * v.x) + (a.basis[1][1] * v.y) + (a.basis[2][1] * v.z),
		(a.basis[0][2] * v.x) + (a.basis[1][2] * v.y) + (a.basis[2][2] * v.z)
	)

func xform(a: Transform3D, b: Vector3) -> Vector3:
	# transform any transform.xform(Vector3) to xform(transform, Vector3)
	return Vector3(
		a.basis.x.dot(b) + a.origin.x,
		a.basis.y.dot(b) + a.origin.y,
		a.basis.z.dot(b) + a.origin.z
	)

func basis_xform_inv(a: Basis, b: Vector3) -> Vector3:
	return Vector3(
		(a[0][0] * b.x) + (a[1][0] * b.y) + (a[2][0] * b.z),
		(a[0][1] * b.x) + (a[1][1] * b.y) + (a[2][1] * b.z),
		(a[0][2] * b.x) + (a[1][2] * b.y) + (a[2][2] * b.z)
	)

func basis_xform(a: Basis, b: Vector3) -> Vector3:
	# transform any transform.basis.xform(Vector3) to basis_xform(transform.basis, Vector3)
	return Vector3(
		a.x.dot(b),
		a.y.dot(b),
		a.z.dot(b)
	)

func basis_scaled_orthogonal(a: Basis, b: Vector3) -> Basis:
	var s: Vector3 = Vector3(-1, -1, -1) + b
	var dots: Vector3 = Vector3.ZERO
	var x: Basis = Basis.IDENTITY
	for i in 3:
		for j in 3:
			dots[j] += s[i] * abs(a[i].normalized().dot(x[j]))
	a = a * Basis.from_scale(Vector3(1, 1, 1) + dots)
	return a

func basis_orthogonalized(a: Basis) -> Basis:
	var scl: Vector3 = a.get_scale()
	var ret: Basis = a.orthonormalized()
	return ret * Basis.from_scale(scl)

func _get_camera_transform() -> Transform3D:
	var camera: Camera3D = self.get_camera()
	return camera.get_global_transform()

func _get_ray_pos(p_pos: Vector2) -> Vector3:
	var camera: Camera3D = self.get_camera()
	return camera.project_ray_origin(p_pos / self.get_stretch_shrink())

func _get_camera_normal() -> Vector3:
	return -self._get_camera_transform().basis[2]

func _get_ray(p_pos: Vector2) -> Vector3:
	var camera: Camera3D = self.get_camera()
	return camera.project_ray_normal(p_pos / self.get_stretch_shrink())

# TODO: turn back on
func __physics_process(_delta):
	# Ignore when the world editor is not active
	if not self.get_parent().visible:
		return

	# Look at any object under the event
	var camera: Camera3D = self.get_camera()

	var ray_origin = camera.project_ray_origin(self.get_local_mouse_position())
	var ray_end = ray_origin + camera.project_ray_normal(self.get_local_mouse_position()) * 2000.0

	var intersection_parameters = PhysicsRayQueryParameters3D.new()
	intersection_parameters.from = ray_origin
	intersection_parameters.to = ray_end
	var intersection = $SubViewport.get_world_3d().direct_space_state.intersect_ray(intersection_parameters)

	if not intersection.is_empty():
		# Dehover the last hovered object
		if self._last:
			self._last.visible = false

		# Determine the mesh and show the outline mesh for it
		var collider = intersection["collider"].get_parent()
		if collider and collider.has_meta("outline"):
			self._last = collider.get_meta("outline")
			self._last.visible = true
		if collider and collider.has_meta("element"):
			self._hovered = collider.get_meta("element")
	else:
		# Nothing hit the ray, so we are hovering over nothing...
		# Dehover anything that was last hovered over
		if self._last:
			self._last.visible = false
			self._last = null

		self._hovered = null

const MIN_Z = 0.01
const MAX_Z = 1000000.0

const MIN_FOV = 0.01
const MAX_FOV = 179

const DISTANCE_DEFAULT = 4.0

const GIZMO_ARROW_SIZE = 0.35
const GIZMO_RING_HALF_WIDTH = 0.1
const GIZMO_PLANE_SIZE = 0.2
const GIZMO_PLANE_DST = 0.3
const GIZMO_CIRCLE_SIZE = 1.1
const GIZMO_SCALE_OFFSET = GIZMO_CIRCLE_SIZE + 0.3
const GIZMO_ARROW_OFFSET = GIZMO_CIRCLE_SIZE + 0.3

const ZOOM_FREELOOK_MIN = 0.01
const ZOOM_FREELOOK_MULTIPLIER = 1.08
const ZOOM_FREELOOK_INDICATOR_DELAY_S = 1.5
const ZOOM_FREELOOK_MAX = 10000.0

# Keeps track of the current / next camera position
# The default constructor places the camera at +X, +Y, +Z
# aka south east, facing north east.
class Cursor:
	var pos: Vector3 = Vector3(0.0, 0.0, 0.0)
	var x_rot: float = 0.5
	var y_rot: float = -0.5
	var distance: float = 4
	var fov_scale: float = 1.0
	var eye_pos: Vector3 = Vector3(0.0, 0.0, 0.0) # Used in free-look mode
	var region_select: bool = false
	var region_begin: Vector2
	var region_end: Vector2

	func _init(cursor: Cursor = null):
		if cursor:
			self.pos = Vector3(cursor.pos)
			self.x_rot = cursor.x_rot
			self.y_rot = cursor.y_rot
			self.distance = cursor.distance
			self.fov_scale = cursor.fov_scale
			self.eye_pos = Vector3(cursor.eye_pos)
			self.region_select = cursor.region_select
			self.region_begin = Vector2(cursor.region_begin)
			self.region_end = Vector2(cursor.region_end)

class EditData:
	var mode: TransformMode
	var plane: TransformPlane
	var original: Transform3D
	var click_ray: Vector3
	var click_ray_pos: Vector3
	var center: Vector3
	var mouse_pos: Vector2
	var original_mouse_pos: Vector2
	var snap: bool = false
	var show_rotation_line: bool = false
	var gizmo: Node3D = null
	var gizmo_handle: int = 0
	var gizmo_handle_secondary: bool = false
	var gizmo_initial_value: Variant
	var original_local: bool
	var instant: bool

var _edit: EditData = EditData.new()

# Whether or not the current camera is orthogonal
var _orthogonal: bool = false
var _auto_orthogonal: bool = false
var _lock_rotation: bool = false

# Where the camera is right now
var _camera_cursor = Cursor.new()

# Where the camera wants to be
var _cursor = Cursor.new()

# Keeps track of how many times we could not zoom
var _zoom_failed_attempts_count: int = 0
var _zoom_indicator_delay: float = 0.0

var _freelook_active: bool = false
var _freelook_speed: float

func scale_cursor_distance(zoom_factor: float):
	var camera: Camera3D = self.get_camera()
	var min_distance = max(camera.get_near() * 4, ZOOM_FREELOOK_MIN)
	var max_distance = min(camera.get_far() / 4, ZOOM_FREELOOK_MAX)
	if min_distance > max_distance:
		self._cursor.distance = (min_distance + max_distance) / 2
	else:
		self._cursor.distance = clamp(self._cursor.distance * zoom_factor, min_distance, max_distance)

	if self._cursor.distance == max_distance or self._cursor.distance == min_distance:
		self._zoom_failed_attempts_count += 1
	else:
		self._zoom_failed_attempts_count = 0

	self._zoom_indicator_delay = ZOOM_FREELOOK_INDICATOR_DELAY_S
	#surface->queue_redraw()

func get_tool_mode() -> ToolMode:
	return self._tool_mode

func scale_freelook_speed(to_scale: float):
	var camera: Camera3D = self.get_camera()
	var min_speed: float = max(camera.get_near() * 4.0, ZOOM_FREELOOK_MIN)
	var max_speed: float = min(camera.get_far() / 4.0, ZOOM_FREELOOK_MAX)
	if min_speed > max_speed:
		self._freelook_speed = (min_speed + max_speed) / 2.0
	else:
		self._freelook_speed = clamp(self._freelook_speed * to_scale, min_speed, max_speed)

	self._zoom_indicator_delay = ZOOM_FREELOOK_INDICATOR_DELAY_S
	#surface->queue_redraw()

func is_freelook_active():
	return self._freelook_active

func is_snap_enabled():
	return self._snap_enabled != self._snap_key_enabled

func scale_fov(fov_offset: float):
	self._cursor.fov_scale = clamp(self._cursor.fov_scale + fov_offset, 0.1, 2.5)

func reset_fov():
	self._cursor.fov_scale = 1.0

func get_fov():
	var camera: Camera3D = self.get_camera()
	return clamp(camera.fov * self._cursor.fov_scale, MIN_FOV, MAX_FOV)

func get_zfar():
	var camera: Camera3D = self.get_camera()
	return camera.far

func get_znear():
	var camera: Camera3D = self.get_camera()
	return camera.near

func to_camera_transform(cursor: Cursor) -> Transform3D:
	var camera_transform: Transform3D
	camera_transform = camera_transform.translated_local(cursor.pos)
	camera_transform = camera_transform.rotated(Vector3(1.0, 0.0, 0.0), -cursor.x_rot)
	camera_transform = camera_transform.rotated(Vector3(0.0, 1.0, 0.0), -cursor.y_rot)

	if self._orthogonal:
		camera_transform = camera_transform.translated_local(Vector3(0, 0, (self.get_zfar() - self.get_znear()) / 2.0))
	else:
		camera_transform = camera_transform.translated_local(Vector3(0, 0, cursor.distance))

	return camera_transform

func _get_warped_mouse_motion(event: InputEventMouseMotion) -> Vector2:
	var relative: Vector2
	# TODO: allow this setting?
	# if (bool(EDITOR_GET("editors/3d/navigation/warped_mouse_panning"))) {
	#    relative = Input::get_singleton()->warp_mouse_motion(p_ev_mouse_motion, surface->get_global_rect())
	relative = event.relative
	return relative

func get_single_selected_node() -> Node3D:
	return self._selected

# Update the camera
func _process(delta):
	var camera: Camera3D = self.get_camera()
	var old_camera_cursor: Cursor = Cursor.new(self._camera_cursor)
	self._camera_cursor = Cursor.new(self._cursor)

	var is_orthogonal: bool = (camera.projection == Camera3D.PROJECTION_ORTHOGONAL)

	if delta > 0:
		# Update camera and perform smoothing
		if self.is_freelook_active():
			pass
		else:
			var orbit_inertia = 0.0 # TODO: setting
			var translation_inertia = 0.05 # TODO: setting
			var zoom_inertia = 0.05 # TODO: setting

			self._camera_cursor.x_rot = lerp(
				old_camera_cursor.x_rot,
				self._cursor.x_rot,
				min(1.0, delta * (1.0 / orbit_inertia))
			)

			self._camera_cursor.y_rot = lerp(
				old_camera_cursor.y_rot,
				self._cursor.y_rot,
				min(1.0, delta * (1.0 / orbit_inertia))
			)

			if abs(self._camera_cursor.x_rot - self._cursor.x_rot) < 0.1:
				self._camera_cursor.x_rot = self._cursor.x_rot

			if abs(self._camera_cursor.y_rot - self._cursor.y_rot) < 0.1:
				self._camera_cursor.y_rot = self._cursor.y_rot

			self._camera_cursor.pos = lerp(
				old_camera_cursor.pos,
				self._cursor.pos,
				min(1.0, delta * (1.0 / translation_inertia))
			)

			self._camera_cursor.distance = lerp(
				old_camera_cursor.distance,
				self._cursor.distance,
				min(1.0, delta * (1.0 / zoom_inertia))
			)

	var equal: bool = true
	if not is_equal_approx(old_camera_cursor.x_rot, self._camera_cursor.x_rot) or not is_equal_approx(old_camera_cursor.y_rot, self._camera_cursor.y_rot):
		equal = false
	elif not is_equal_approx(old_camera_cursor.pos.x, self._camera_cursor.pos.x) or not is_equal_approx(old_camera_cursor.pos.y, self._camera_cursor.pos.y):
		equal = false
	elif not is_equal_approx(old_camera_cursor.distance, self._camera_cursor.distance):
		equal = false
	elif not is_equal_approx(old_camera_cursor.fov_scale, self._camera_cursor.fov_scale):
		equal = false

	if not equal or delta == 0 or is_orthogonal != self._orthogonal:
		camera.set_transform(self.to_camera_transform(self._camera_cursor))

		if self._orthogonal:
			var half_fov: float = deg_to_rad(self.get_fov()) / 2.0
			var height: float = 2.0 * self._cursor.distance * tan(half_fov)
			camera.set_orthogonal(height, self.get_znear(), self.get_zfar())
		else:
			camera.set_perspective(self.get_fov(), self.get_znear(), self.get_zfar())

		#rotation_control->queue_redraw()
		#position_control->queue_redraw()
		#look_control->queue_redraw()
		#spatial_editor->update_grid()
	self.update_transform_gizmo_view()

func _nav_pan(event: InputEventMouseMotion, relative: Vector2):
	var pan_speed: float = 1.0 / 150.0

	if event.shift_pressed and self._nav_scheme == NavigationScheme.NAVIGATION_MAYA:
		pan_speed *= 10

	var camera_transform: Transform3D

	camera_transform = camera_transform.translated_local(self._cursor.pos)
	camera_transform.basis.rotated(Vector3(1, 0, 0), -self._cursor.x_rot)
	camera_transform.basis.rotated(Vector3(0, 1, 0), -self._cursor.y_rot)
	var invert_x_axis: bool = false # EDITOR_GET("editors/3d/navigation/invert_x_axis")
	var invert_y_axis: bool = false # EDITOR_GET("editors/3d/navigation/invert_y_axis")
	var translation: Vector3 = Vector3(
		(-1 if invert_x_axis else 1) * -relative.x * pan_speed,
		(-1 if invert_y_axis else 1) * relative.y * pan_speed,
		0
	)

	translation *= self._cursor.distance / DISTANCE_DEFAULT
	camera_transform = camera_transform.translated_local(translation)
	self._cursor.pos = camera_transform.origin

func _nav_orbit(event: InputEventMouseMotion, relative: Vector2):
	if self._lock_rotation:
		self._nav_pan(event, relative)
		return

	if self._orthogonal and self._auto_orthogonal:
		# _menu_option(VIEW_PERSPECTIVE)
		pass

	var degrees_per_pixel: float = 0.25 # EDITOR_GET("editors/3d/navigation_feel/orbit_sensitivity")
	var radians_per_pixel: float = deg_to_rad(degrees_per_pixel)
	var invert_y_axis: bool = false # EDITOR_GET("editors/3d/navigation/invert_y_axis")
	var invert_x_axis: bool = false # EDITOR_GET("editors/3d/navigation/invert_x_axis")

	if invert_y_axis:
		self._cursor.x_rot -= relative.y * radians_per_pixel
	else:
		self._cursor.x_rot += relative.y * radians_per_pixel

	# Clamp the Y rotation to roughly -90..90 degrees so the user can't look upside-down and end up disoriented.
	self._cursor.x_rot = clamp(self._cursor.x_rot, -1.57, 1.57)

	if invert_x_axis:
		self._cursor.y_rot -= relative.x * radians_per_pixel
	else:
		self._cursor.y_rot += relative.x * radians_per_pixel

	self._view_type = ViewType.VIEW_TYPE_USER
	#_update_name()

func _nav_zoom(event: InputEventMouseMotion, relative: Vector2):
	var zoom_speed: float = 1.0 / 80.0
	if event.shift_pressed and self._nav_scheme == NavigationScheme.NAVIGATION_MAYA:
		zoom_speed *= 10

	var zoom_style: NavigationZoomStyle = NavigationZoomStyle.NAVIGATION_ZOOM_VERTICAL #(NavigationZoomStyle)EDITOR_GET("editors/3d/navigation/zoom_style").operator int()
	if zoom_style == NavigationZoomStyle.NAVIGATION_ZOOM_HORIZONTAL:
		if relative.x > 0:
			self.scale_cursor_distance(1.0 - relative.x * zoom_speed)
		elif relative.x < 0:
			self.scale_cursor_distance(1.0 / (1 + relative.x * zoom_speed))
	else:
		if relative.y > 0:
			self.scale_cursor_distance(1 + relative.y * zoom_speed)
		elif relative.y < 0:
			self.scale_cursor_distance(1.0 / (1.0 - relative.y * zoom_speed))

func _nav_look(event: InputEventMouseMotion, relative: Vector2):
	if self._orthogonal:
		self._nav_pan(event, relative)
		return

	if self._orthogonal and self._auto_orthogonal:
		#_menu_option(VIEW_PERSPECTIVE)
		pass

	# Scale mouse sensitivity with camera FOV scale when zoomed in to make it easier to point at things.
	#var degrees_per_pixel: float = # EDITOR_GET("editors/3d/freelook/freelook_sensitivity")) * MIN(1.0, cursor.fov_scale)
	var degrees_per_pixel: float = 0.25 * min(1.0, self._cursor.fov_scale) # EDITOR_GET("editors/3d/freelook/freelook_sensitivity")) * MIN(1.0, cursor.fov_scale)
	var radians_per_pixel: float = deg_to_rad(degrees_per_pixel)
	var invert_y_axis: bool = false # EDITOR_GET("editors/3d/navigation/invert_y_axis")

	# Note: do NOT assume the camera has the "current" transform, because it is interpolated and may have "lag".
	var prev_camera_transform: Transform3D = self.to_camera_transform(self._cursor)

	if invert_y_axis:
		self._cursor.x_rot -= relative.y * radians_per_pixel
	else:
		self._cursor.x_rot += relative.y * radians_per_pixel

	# Clamp the Y rotation to roughly -90..90 degrees so the user can't look upside-down and end up disoriented.
	self._cursor.x_rot = clamp(self._cursor.x_rot, -1.57, 1.57)

	self._cursor.y_rot += relative.x * radians_per_pixel

	# Look is like the opposite of Orbit: the focus point rotates around the camera
	var camera_transform: Transform3D = self.to_camera_transform(self._cursor)
	#var pos: Vector3 = camera_transform.xform(Vector3(0, 0, 0))
	var pos: Vector3 = Vector3(
		camera_transform.basis.x.dot(Vector3(0, 0, 0)) + camera_transform.origin.x,
		camera_transform.basis.y.dot(Vector3(0, 0, 0)) + camera_transform.origin.y,
		camera_transform.basis.z.dot(Vector3(0, 0, 0)) + camera_transform.origin.z
	)
	#var prev_pos: Vector3 = prev_camera_transform.xform(Vector3(0, 0, 0))
	var prev_pos: Vector3 = Vector3(
		prev_camera_transform.basis.x.dot(Vector3(0, 0, 0)) + prev_camera_transform.origin.x,
		prev_camera_transform.basis.y.dot(Vector3(0, 0, 0)) + prev_camera_transform.origin.y,
		prev_camera_transform.basis.z.dot(Vector3(0, 0, 0)) + prev_camera_transform.origin.z
	)
	var diff: Vector3 = prev_pos - pos
	self._cursor.pos += diff

	self._view_type = ViewType.VIEW_TYPE_USER
	#_update_name()

func intersects_point(point):
	var left = self.get_transform().get_origin().x
	var top = self.get_transform().get_origin().y
	var right = left + self.get_size().x
	var bottom = top + self.get_size().y

	if left <= point.x and right > point.x:
		if top <= point.y and bottom > point.y:
			return true

	return false

# TODO: turn back on
func __input(event):
	var mouse_position = self.get_local_mouse_position()
	var camera: Camera3D = null
	var result = null

	if event is InputEventMouseButton:
		var zoom_factor: float = 1 + (ZOOM_FREELOOK_MULTIPLIER - 1) * event.factor
		self._nav_mode = NavigationMode.NAVIGATION_NONE

		if event.button_index == MOUSE_BUTTON_WHEEL_UP:
			# Only if it is over the viewport
			if not self.intersects_point(mouse_position):
				return
			self.scale_cursor_distance(1.0 / zoom_factor)
		elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
			# Only if it is over the viewport
			if not self.intersects_point(mouse_position):
				return
			self.scale_cursor_distance(zoom_factor)
		elif event.button_index == MOUSE_BUTTON_RIGHT:
			var nav_scheme = self._nav_scheme

			if event.is_pressed() and self._edit.gizmo:
				# restore
				#_edit.gizmo->commit_handle(_edit.gizmo_handle, _edit.gizmo_handle_secondary, _edit.gizmo_initial_value, true);
				#_edit.gizmo = Ref<EditorNode3DGizmo>();
				pass

			if self._edit.mode == TransformMode.TRANSFORM_NONE and event.is_pressed():
				if event.alt_pressed:
					if nav_scheme != NavigationScheme.NAVIGATION_MAYA:
						# TODO:
						#self._list_select(event)
						return

			if self._edit.mode != TransformMode.TRANSFORM_NONE and event.is_pressed():
				# TODO:
				#self.cancel_transform()
				pass

			if event.is_pressed():
				if not self._orthogonal:
					# TODO : allow freelook to be activated on a modifier (godot defaults to none)
					#self.set_freelook_active(true)
					pass
			else:
				# TODO:
				#self.set_freelook_active(false)
				pass

			if self._freelook_active and not self.has_focus():
				# Focus usually doesn't trigger on right-click, but in case of freelook it should,
				# otherwise using keyboard navigation would misbehave
				self.grab_focus()
		elif event.button_index == MOUSE_BUTTON_MIDDLE:
			if event.is_pressed() and self._edit.mode == TransformMode.TRANSFORM_NONE:
				if self._edit.plane == TransformPlane.TRANSFORM_VIEW:
					self._edit.plane = TransformPlane.TRANSFORM_X_AXIS
					#set_message(TTR("X-Axis Transform."), 2);
					self._view_type = ViewType.VIEW_TYPE_USER
					#self._update_name()
				elif self._edit.plane == TransformPlane.TRANSFORM_X_AXIS:
					self._edit.plane = TransformPlane.TRANSFORM_Y_AXIS
					#set_message(TTR("Y-Axis Transform."), 2);
				elif self._edit.plane == TransformPlane.TRANSFORM_Y_AXIS:
					self._edit.plane = TransformPlane.TRANSFORM_Z_AXIS
					#set_message(TTR("Z-Axis Transform."), 2);
				elif self._edit.plane == TransformPlane.TRANSFORM_Z_AXIS:
					self._edit.plane = TransformPlane.TRANSFORM_VIEW
					#set_message(TTR("View Plane Transform."), 2);
				elif self._edit.plane == TransformPlane.TRANSFORM_YZ or self._edit.plane == TransformPlane.TRANSFORM_XZ or self._edit.plane == TransformPlane.TRANSFORM_XY:
					pass
		elif event.button_index == MOUSE_BUTTON_LEFT:
			if event.is_pressed():
				self._clicked_wants_append = event.shift_pressed

				var handled: bool = false
				if self._edit.mode != TransformMode.TRANSFORM_NONE and self._edit.instant:
					self.commit_transform()
					# just commit the edit, stop processing the event so we don't deselect the object
					handled = true

				var nav_scheme = self._nav_scheme
				if (nav_scheme == NavigationScheme.NAVIGATION_MAYA or nav_scheme == NavigationScheme.NAVIGATION_MODO) and event.alt_pressed:
					handled = true

				if self.get_tool_mode() == ToolMode.TOOL_MODE_LIST_SELECT:
					# TODO:
					#self._list_select(event)
					handled = true

				if not handled:
					self._edit.mouse_pos = mouse_position
					self._edit.original_mouse_pos = mouse_position
					self._edit.snap = self.is_snap_enabled()
					self._edit.mode = TransformMode.TRANSFORM_NONE
					# To prevent to break when flipping with scale.
					self._edit.original = self.get_gizmo_transform()

					var can_select_gizmos: bool = self.get_single_selected_node() != null

					#{
					#    int idx = view_menu->get_popup()->get_item_index(VIEW_GIZMOS);
					#    can_select_gizmos = can_select_gizmos && view_menu->get_popup()->is_item_checked(idx);
					#}

					# Gizmo handles
					if can_select_gizmos:
						#Vector<Ref<Node3DGizmo>> gizmos = spatial_editor->get_single_selected_node()->get_gizmos();
						var gizmos: Array = []

						var intersected_handle: bool = false
						for i in gizmos.size():
							var seg: Node3D = gizmos[i]
							if not seg:
								continue

							camera = self.get_camera()
							result = self.handles_intersect_ray(seg, camera, self._edit.mouse_pos, event.shift_pressed)
							var gizmo_handle: int = result[0]
							var gizmo_secondary: bool = result[1]

							if gizmo_handle != -1:
								self._edit.gizmo = seg
								self._edit.gizmo_handle = gizmo_handle
								self._edit.gizmo_handle_secondary = gizmo_secondary
								self._edit.gizmo_initial_value = self.get_handle_value(seg, gizmo_handle, gizmo_secondary)
								intersected_handle = true
								break

						if intersected_handle:
							handled = true

					# Transform gizmo
					if not handled:
						if self._transform_gizmo_select(_edit.mouse_pos):
							handled = true

					if not handled:
						# Subgizmos
						if can_select_gizmos:
							var se = self._editor_selection.get_node_editor_data(self.get_single_selected_node())
							#Vector<Ref<Node3DGizmo>> gizmos = spatial_editor->get_single_selected_node()->get_gizmos();
							var gizmos = []

							var intersected_subgizmo: bool = false
							for i in gizmos.size():
								var seg: Node3D = gizmos[i]

								if not seg:
									continue

								camera = self.get_camera()
								var subgizmo_id: int = self.subgizmos_intersect_ray(seg, camera, _edit.mouse_pos)
								if subgizmo_id != -1:
									if not se:
										continue

									if event.shift_pressed:
										if se.subgizmos.has(subgizmo_id):
											se.subgizmos.erase(subgizmo_id)
										else:
											se.subgizmos.insert(subgizmo_id, self.get_subgizmo_transform(seg, subgizmo_id))
									else:
										se.subgizmos.clear()
										se.subgizmos.insert(subgizmo_id, self.get_subgizmo_transform(seg, subgizmo_id))

									if se.subgizmos.is_empty():
										se.gizmo = Node3D.new()
									else:
										se.gizmo = seg

									seg.redraw()
									self.update_transform_gizmo()
									intersected_subgizmo = true
									break

							if intersected_subgizmo:
								handled = true

						if not handled:
							self._clicked = null

							if (self.get_tool_mode() == ToolMode.TOOL_MODE_SELECT and event.is_command_or_control_pressed()) or self.get_tool_mode() == ToolMode.TOOL_MODE_ROTATE:
								self.begin_transform(TransformMode.TRANSFORM_ROTATE, false)
								handled = true

							if self.get_tool_mode() == ToolMode.TOOL_MODE_MOVE:
								self.begin_transform(TransformMode.TRANSFORM_TRANSLATE, false)
								handled = true

							if self.get_tool_mode() == ToolMode.TOOL_MODE_SCALE:
								self.begin_transform(TransformMode.TRANSFORM_SCALE, false)
								handled = true

							if not handled:
								#if (after != EditorPlugin::AFTER_GUI_INPUT_CUSTOM) {
								# clicking is always deferred to either move or release
								self._clicked = self._select_ray(mouse_position)
								self._selection_in_progress = true

								# If nothing is clicked, we are selecting a region
								if self._clicked == null:
									self._cursor.region_select = true
									self._cursor.region_begin = mouse_position
									self._cursor.region_end = mouse_position

								# MY EDIT
								var element: Element = self._hovered
								if element:
									# Select in element tree
									if element.has_meta("element_item"):
										element.get_meta("element_item").select(0)
										self._selected_element = element
										self._gizmo.transform = element.get_native_node().transform
										self._editor_selection.clear()
										self._editor_selection.add_node(element.get_native_node())
										self.update_transform_gizmo()
								else:
									# nothing clicked
									self._selected_element = null
									self._editor_selection.clear()
									self.update_transform_gizmo()

								self.queue_redraw()

			else:
				if self._edit.gizmo:
					self._edit.gizmo.commit_handle(self._edit.gizmo_handle, self._edit.gizmo_handle_secondary, self._edit.gizmo_initial_value, false)
					self._edit.gizmo = null
				else:
					if true: #if (after != EditorPlugin::AFTER_GUI_INPUT_CUSTOM) {
						self._selection_in_progress = false

						if self._clicked:
							# TODO
							#self._select_clicked(false)
							pass

						if self._cursor.region_select:
							self._select_region()
							self._cursor.region_select = false
							self.queue_redraw()

						if self._edit.mode != TransformMode.TRANSFORM_NONE:
							var selected: Node3D = self.get_single_selected_node()
							var se = self._editor_selection.get_node_editor_data(selected) if selected else null

							if se and se.gizmo:
								var ids: Array = []
								var restore: Array = []

								for key in se.subgizmos:
									ids.append(key)
									restore.append(se.subgizmos[key])

								se.gizmo.commit_subgizmos(ids, restore, false)
							else:
								self.commit_transform()

							self._edit.mode = TransformMode.TRANSFORM_NONE
							self.update_transform_gizmo()

						self.queue_redraw()

	# Actually move the viewport camera
	camera = self.get_camera()
	if event is InputEventMouseMotion:
		self._edit.mouse_pos = mouse_position

		if self.get_single_selected_node():
			var found_gizmo: Node3D = null
			var found_handle: int = -1
			var found_handle_secondary: bool = false

			var gizmos = []

			for seg in gizmos:
				result = self.handles_intersect_ray(seg, camera, self._edit.mouse_pos, false)
				found_handle = result[0]
				found_handle_secondary = result[1]

				if found_handle != -1:
					found_gizmo = seg
					break

			if found_gizmo:
				self.select_gizmo_highlight_axis(-1)

			result = self.get_current_hover_gizmo_handle()
			var current_hover_handle: int = result[0]
			var current_hover_handle_secondary: bool = result[1]
			if found_gizmo != self.get_current_hover_gizmo() or found_handle != current_hover_handle or found_handle_secondary != current_hover_handle_secondary:
				self.set_current_hover_gizmo(found_gizmo)
				self.set_current_hover_gizmo_handle(found_handle, found_handle_secondary)
				self.update_gizmos(self.get_single_selected_node())

		if self.get_current_hover_gizmo() == null and (event.button_mask & MOUSE_BUTTON_MASK_LEFT == MOUSE_BUTTON_NONE) and not self._edit.gizmo:
			self._transform_gizmo_select(self._edit.mouse_pos, true)

		if self._edit.gizmo:
			pass
		elif (event.button_mask & MOUSE_BUTTON_MASK_LEFT) != MOUSE_BUTTON_NONE:
			var movement_threshold_passed: bool = self._edit.original_mouse_pos.distance_to(
				self._edit.mouse_pos
			) > 8 * EDSCALE

			# enable region-select if nothing has been selected yet or multi-select (shift key) is active
			if self._selection_in_progress and movement_threshold_passed:
				if self.get_selected_count() == 0 or self._clicked_wants_append:
					self._cursor.region_select = true
					self._cursor.region_begin = self._edit.original_mouse_pos
					self._clicked = null # ObjectID()

			if self._cursor.region_select:
				self._cursor.region_end = $SubViewport.get_mouse_position()
				self.queue_redraw()
				return

			if self._clicked and self._movement_threshold_passed:
				self._compute_edit(self._edit.original_mouse_pos)
				self._clicked = null
				self._edit.mode = TransformMode.TRANSFORM_TRANSLATE

			if self._edit.mode == TransformMode.TRANSFORM_NONE:
				return

			self.update_transform(mouse_position, event.shift_pressed)
		elif (event.button_mask & MOUSE_BUTTON_MASK_RIGHT) != MOUSE_BUTTON_NONE:
			if self._nav_scheme == NavigationScheme.NAVIGATION_MAYA:
				self._nav_mode = NavigationMode.NAVIGATION_ZOOM
			elif self._freelook_active:
				self._nav_mode = NavigationMode.NAVIGATION_LOOK
			elif self._orthogonal:
				self._nav_mode = NavigationMode.NAVIGATION_PAN
		elif (event.button_mask & MOUSE_BUTTON_MASK_MIDDLE) != MOUSE_BUTTON_NONE:
			if event.shift_pressed: # _get_key_modifier_setting("editors/3d/navigation/pan_modifier")
				self._nav_mode = NavigationMode.NAVIGATION_PAN
			elif event.ctrl_pressed: # _get_key_modifier_setting("editors/3d/navigation/zoom_modifier")
				self._nav_mode = NavigationMode.NAVIGATION_ZOOM
			else: # _get_key_modifier_setting("editors/3d/navigation/orbit_modifier")
				self._nav_mode = NavigationMode.NAVIGATION_ORBIT

		if self._nav_mode == NavigationMode.NAVIGATION_PAN:
			self._nav_pan(event, self._get_warped_mouse_motion(event))
		elif self._nav_mode == NavigationMode.NAVIGATION_ORBIT:
			self._nav_orbit(event, self._get_warped_mouse_motion(event))
		elif self._nav_mode == NavigationMode.NAVIGATION_ZOOM:
			self._nav_zoom(event, self._get_warped_mouse_motion(event))
		elif self._nav_mode == NavigationMode.NAVIGATION_LOOK:
			self._nav_look(event, self._get_warped_mouse_motion(event))

func _select_region() -> void:
	pass

func _transform_gizmo_apply(p_node: Node3D, p_transform: Transform3D, p_local: bool) -> void:
	if p_transform.basis.determinant() == 0:
		return

	if p_local:
		p_node.set_transform(p_transform)
	else:
		p_node.set_global_transform(p_transform)

func _transform_gizmo_select(p_screenpos: Vector2, p_highlight_only: bool = false) -> bool:
	if not self.is_gizmo_visible():
		return false

	if self.get_selected_count() == 0:
		if p_highlight_only:
			self.select_gizmo_highlight_axis(-1)
		return false

	var ray_pos: Vector3 = self._get_ray_pos(p_screenpos)
	var ray: Vector3 = self._get_ray(p_screenpos)

	var gt: Transform3D = self.get_gizmo_transform()

	if self.get_tool_mode() == ToolMode.TOOL_MODE_SELECT or self.get_tool_mode() == ToolMode.TOOL_MODE_MOVE:
		var col_axis: int = -1
		var col_d: float = 1e20

		for i in 3:
			var grabber_pos: Vector3 = gt.origin + gt.basis[i].normalized() * self._gizmo_scale * (GIZMO_ARROW_OFFSET + (GIZMO_ARROW_SIZE * 0.5))
			var grabber_radius: float = self._gizmo_scale * GIZMO_ARROW_SIZE

			var result = Geometry3D.segment_intersects_sphere(ray_pos, ray_pos + ray * MAX_Z, grabber_pos, grabber_radius)
			if not result.is_empty():
				var r: Vector3 = result[0]
				var d: float = r.distance_to(ray_pos)
				if d < col_d:
					col_d = d
					col_axis = i

		var is_plane_translate: bool = false
		# plane select
		if col_axis == -1:
			col_d = 1e20

			for i in 3:
				var ivec2: Vector3 = gt.basis[(i + 1) % 3].normalized()
				var ivec3: Vector3 = gt.basis[(i + 2) % 3].normalized()

				# Allow some tolerance to make the plane easier to click,
				# even if the click is actually slightly outside the plane.
				var grabber_pos: Vector3 = gt.origin + (ivec2 + ivec3) * self._gizmo_scale * (GIZMO_PLANE_SIZE + GIZMO_PLANE_DST * 0.6667)

				var plane = Plane(gt.basis[i].normalized(), gt.origin)

				var result = plane.intersects_ray(ray_pos, ray)
				if result:
					var r: Vector3 = result
					var dist: float = r.distance_to(grabber_pos)
					# Allow some tolerance to make the plane easier to click,
					# even if the click is actually slightly outside the plane.
					if dist < (self._gizmo_scale * GIZMO_PLANE_SIZE * 1.5):
						var d: float = ray_pos.distance_to(r)
						if d < col_d:
							col_d = d
							col_axis = i

							is_plane_translate = true

		if col_axis != -1:
			if p_highlight_only:
				self.select_gizmo_highlight_axis(col_axis + (6 if is_plane_translate else 0))
			else:
				self._edit.mode = TransformMode.TRANSFORM_TRANSLATE
				self._compute_edit(p_screenpos)
				self._edit.plane = (int(TransformPlane.TRANSFORM_X_AXIS) + col_axis + (3 if is_plane_translate else 0)) as TransformPlane

			return true

	if self.get_tool_mode() == ToolMode.TOOL_MODE_SELECT or self.get_tool_mode() == ToolMode.TOOL_MODE_ROTATE:
		var col_axis: int = -1

		var ray_length: float = gt.origin.distance_to(ray_pos) + (GIZMO_CIRCLE_SIZE * self._gizmo_scale) * 4.0
		var result = Geometry3D.segment_intersects_sphere(ray_pos, ray_pos + ray * ray_length, gt.origin, self._gizmo_scale * (GIZMO_CIRCLE_SIZE))
		if not result.is_empty():
			var hit_position: Vector3 = result[0]
			var hit_normal: Vector3 = result[1]

			if hit_normal.dot(self._get_camera_normal()) < 0.05:
				hit_position = self.xform_inv(gt, hit_position).abs()
				var min_axis: int = hit_position.min_axis_index()
				if hit_position[min_axis] < self._gizmo_scale * GIZMO_RING_HALF_WIDTH:
					col_axis = min_axis

		if col_axis == -1:
			var col_d: float = 1e20

			for i in 3:
				var plane = Plane(gt.basis[i].normalized(), gt.origin)

				result = plane.intersects_ray(ray_pos, ray)
				if result == null:
					continue

				var r: Vector3 = result

				var dist: float = r.distance_to(gt.origin)
				var r_dir = (r - gt.origin).normalized()

				if self._get_camera_normal().dot(r_dir) <= 0.005:
					if dist > self._gizmo_scale * (GIZMO_CIRCLE_SIZE - GIZMO_RING_HALF_WIDTH) and dist < self._gizmo_scale * (GIZMO_CIRCLE_SIZE + GIZMO_RING_HALF_WIDTH):
						var d: float = ray_pos.distance_to(r)
						if d < col_d:
							col_d = d
							col_axis = i

		if col_axis != -1:
			if p_highlight_only:
				self.select_gizmo_highlight_axis(col_axis + 3)
			else:
				self._edit.mode = TransformMode.TRANSFORM_ROTATE
				self._compute_edit(p_screenpos)
				self._edit.plane = (TransformPlane.TRANSFORM_X_AXIS + col_axis) as TransformPlane
			return true

	if self.get_tool_mode() == ToolMode.TOOL_MODE_SCALE:
		var col_axis: int = -1
		var col_d: float = 1e20

		for i in 3:
			var grabber_pos: Vector3 = gt.origin + gt.basis[i].normalized() * self._gizmo_scale * GIZMO_SCALE_OFFSET
			var grabber_radius: float = self._gizmo_scale * GIZMO_ARROW_SIZE

			var result = Geometry3D.segment_intersects_sphere(ray_pos, ray_pos + ray * MAX_Z, grabber_pos, grabber_radius)
			if not result.is_empty():
				var r: Vector3 = result[0]
				var d: float = r.distance_to(ray_pos)
				if d < col_d:
					col_d = d
					col_axis = i

		var is_plane_scale: bool = false
		# plane select
		if col_axis == -1:
			col_d = 1e20

			for i in 3:
				var ivec2: Vector3 = gt.basis[(i + 1) % 3].normalized()
				var ivec3: Vector3 = gt.basis[(i + 2) % 3].normalized()

				# Allow some tolerance to make the plane easier to click,
				# even if the click is actually slightly outside the plane.
				var grabber_pos: Vector3 = gt.origin + (ivec2 + ivec3) * self._gizmo_scale * (GIZMO_PLANE_SIZE + GIZMO_PLANE_DST * 0.6667)

				var plane = Plane(gt.basis[i].normalized(), gt.origin)

				var result = plane.intersects_ray(ray_pos, ray)
				if not result.is_empty():
					var r: Vector3 = result[0]
					var dist: float = r.distance_to(grabber_pos)
					# Allow some tolerance to make the plane easier to click,
					# even if the click is actually slightly outside the plane.
					if dist < (self._gizmo_scale * GIZMO_PLANE_SIZE * 1.5):
						var d: float = ray_pos.distance_to(r)
						if d < col_d:
							col_d = d
							col_axis = i

							is_plane_scale = true

		if col_axis != -1:
			if p_highlight_only:
				self.select_gizmo_highlight_axis(col_axis + (12 if is_plane_scale else 9))
			else:
				# handle scale
				self._edit.mode = TransformMode.TRANSFORM_SCALE
				self._compute_edit(p_screenpos)
				self._edit.plane = (TransformPlane.TRANSFORM_X_AXIS + col_axis + (3 if is_plane_scale else 0)) as TransformPlane

			return true

	if p_highlight_only:
		self.select_gizmo_highlight_axis(-1)

	return false

func begin_transform(p_mode: TransformMode, instant: bool):
	if self.get_selected_count() > 0:
		self._edit.mode = p_mode
		self._compute_edit(self._edit.mouse_pos)
		self._edit.instant = instant
		self._edit.snap = self.is_snap_enabled()

func _compute_transform(p_mode: TransformMode, p_original: Transform3D, p_original_local: Transform3D, p_motion: Vector3, p_extra: float, p_local: bool, p_orthogonal: bool) -> Transform3D:
	if p_mode == TransformMode.TRANSFORM_SCALE:
		if self._edit.snap or self.is_snap_enabled():
			snapped(p_motion, Vector3(p_extra, p_extra, p_extra))

		var s: Transform3D
		if p_local:
			# basis.scaled_local(v) is the same as basis * Basis.from_scale(v)
			#s.basis = p_original_local.basis.scaled_local(p_motion + Vector3(1, 1, 1))
			s.basis = p_original_local.basis * Basis.from_scale(p_motion + Vector3(1, 1, 1))
			s.origin = p_original_local.origin
		else:
			s.basis = s.basis.scaled(p_motion + Vector3(1, 1, 1))
			var base: Transform3D = Transform3D(Basis(), self._edit.center)
			s = base * (s * (base.inverse() * p_original))

			# Recalculate orthogonalized scale without moving origin.
			if p_orthogonal:
				#s.basis = p_original_local.basis.scaled_orthogonal(p_motion + Vector3(1, 1, 1));
				s.basis = self.basis_scaled_orthogonal(p_original_local.basis, p_motion + Vector3(1, 1, 1))
				# The scaled_orthogonal() does not require orthogonal Basis,
				# but it may make a bit skew by precision problems.
				s.basis = self.basis_orthogonalized(s.basis)

		return s;

	elif p_mode == TransformMode.TRANSFORM_TRANSLATE:
		if self._edit.snap or self.is_snap_enabled():
			#p_motion.snap(Vector3(p_extra, p_extra, p_extra))
			p_motion = snapped(p_motion, Vector3(p_extra, p_extra, p_extra))

		if p_local:
			#p_motion = p_original.basis.xform(p_motion)
			p_motion = self.basis_xform(p_original.basis, p_motion)

		# Apply translation
		var t: Transform3D = p_original
		t.origin += p_motion

		return t

	elif p_mode == TransformMode.TRANSFORM_ROTATE:
		var r: Transform3D = Transform3D.IDENTITY

		if p_local:
			#var axis: Vector3 = p_original_local.basis.xform(p_motion)
			var axis: Vector3 = self.basis_xform(p_original_local.basis, p_motion)
			r.basis = Basis(axis.normalized(), p_extra) * p_original_local.basis
			r.origin = p_original_local.origin
		else:
			var local: Basis = p_original.basis * p_original_local.basis.inverse();
			#Vector3 axis = local.xform_inv(p_motion);
			var axis: Vector3 = self.basis_xform_inv(local, p_motion)
			r.basis = local * Basis(axis.normalized(), p_extra) * p_original_local.basis
			#r.origin = Basis(p_motion, p_extra).xform(p_original.origin - self._edit.center) + self._edit.center
			r.origin = self.basis_xform(Basis(p_motion, p_extra), p_original.origin - self._edit.center) + self._edit.center

		return r

	return Transform3D()

func _compute_edit(p_point: Vector2) -> void:
	self._edit.original_local = self.are_local_coords_enabled()
	self._edit.click_ray = self._get_ray(p_point)
	self._edit.click_ray_pos = self._get_ray_pos(p_point)
	self._edit.plane = TransformPlane.TRANSFORM_VIEW
	self.update_transform_gizmo()
	self._edit.center = self.get_gizmo_transform().origin

	var selected: Node3D = self.get_single_selected_node()
	var se = self._editor_selection.get_node_editor_data(selected) if selected else null

	if se and se.gizmo:
		for key in se.subgizmos:
			var subgizmo_id: int = key
			se.subgizmos[subgizmo_id] = se.gizmo.get_subgizmo_transform(subgizmo_id)

		se.original_local = selected.get_transform()
		se.original = selected.get_global_transform()
	else:
		var selection = self._editor_selection.get_selected_node_list()

		for sp in selection:
			if not sp:
				continue

			var sel_item = self._editor_selection.get_node_editor_data(sp)

			if sel_item == null:
				continue

			sel_item.original_local = self.get_local_gizmo_transform(sel_item.sp)
			sel_item.original = self.get_global_gizmo_transform(sel_item.sp)

func get_local_gizmo_transform(sp: Node3D) -> Transform3D:
	return sp.get_transform()

func get_global_gizmo_transform(sp: Node3D) -> Transform3D:
	return sp.get_global_transform()

func _select_ray(_p_pos: Vector2) -> Node3D:
	return null

func select_gizmo_highlight_axis(p_axis: int) -> void:
	for i in 3:
		self._move_gizmo[i].surface_set_material(0, self._gizmo_color_hl[i] if i == p_axis else self._gizmo_color[i])
		self._move_plane_gizmo[i].surface_set_material(0, self._plane_gizmo_color_hl[i] if (i + 6) == p_axis else self._plane_gizmo_color[i])
		self._rotate_gizmo[i].surface_set_material(0, self._rotate_gizmo_color_hl[i] if (i + 3) == p_axis else self._rotate_gizmo_color[i])
		self._scale_gizmo[i].surface_set_material(0, self._gizmo_color_hl[i] if (i + 9) == p_axis else self._gizmo_color[i])
		self._scale_plane_gizmo[i].surface_set_material(0, self._plane_gizmo_color_hl[i] if (i + 12) == p_axis else self._plane_gizmo_color[i])

func camera_mesh():
	var camera: Camera3D = self.get_camera()

	# Get pyramid points
	var _points = []
	var projection: Projection
	var viewport: SubViewport = self.get_child(0)

	if camera.projection == Camera3D.PROJECTION_PERSPECTIVE:
		projection = Projection.create_perspective(camera.fov, Vector2(viewport.size).aspect(), camera.near, camera.far)
	elif camera.projection == Camera3D.PROJECTION_ORTHOGONAL:
		projection = Projection.create_orthogonal(0.0, viewport.size.x, viewport.size.y, 0.0, camera.near, camera.far)

func handles_intersect_ray(_seg: Node3D, _p_camera: Camera3D, _p_point: Vector2, _p_shift_pressed: bool) -> Array:
	var r_id: int = 0
	var r_secondary: bool = false

	return [r_id, r_secondary]

func _update_navigation():
	pass

func _update_freelook(delta: float):
	if not self.is_freelook_active():
		return

	var camera: Camera3D = self.get_camera()

	var navigation_scheme: FreelookNavigationScheme = FreelookNavigationScheme.FREELOOK_DEFAULT #EDITOR_GET("editors/3d/freelook/freelook_navigation_scheme")

	var _forward: Vector3 = Vector3.ZERO
	if navigation_scheme == FreelookNavigationScheme.FREELOOK_FULLY_AXIS_LOCKED:
		# Forward/backward keys will always go straight forward/backward, never moving on the Y axis.
		_forward = Vector3(0, 0, -1).rotated(Vector3(0, 1, 0), camera.get_rotation().y)
	else:
		# Forward/backward keys will be relative to the camera pitch.
		#forward = camera.get_transform().basis.xform(Vector3(0, 0, -1))

		_forward = Vector3(
			camera.get_transform().basis.x.dot(Vector3(0, 0, -1)),
			camera.get_transform().basis.y.dot(Vector3(0, 0, -1)),
			camera.get_transform().basis.z.dot(Vector3(0, 0, -1))
		)

	#var right: Vector3 = camera.get_transform().basis.xform(Vector3(1, 0, 0))
	var _right: Vector3 = Vector3(
		camera.get_transform().basis.x.dot(Vector3(1, 0, 0)),
		camera.get_transform().basis.y.dot(Vector3(1, 0, 0)),
		camera.get_transform().basis.z.dot(Vector3(1, 0, 0))
	)

	var _up: Vector3 = Vector3.ZERO
	if navigation_scheme == FreelookNavigationScheme.FREELOOK_PARTIALLY_AXIS_LOCKED or navigation_scheme == FreelookNavigationScheme.FREELOOK_FULLY_AXIS_LOCKED:
		# Up/down keys will always go up/down regardless of camera pitch.
		_up = Vector3(0, 1, 0)
	else:
		# Up/down keys will be relative to the camera pitch.
		# up = camera->get_transform().basis.xform(Vector3(0, 1, 0))

		_up = Vector3(
			camera.get_transform().basis.x.dot(Vector3(0, 1, 0)),
			camera.get_transform().basis.y.dot(Vector3(0, 1, 0)),
			camera.get_transform().basis.z.dot(Vector3(0, 1, 0))
		)

	var direction: Vector3 = Vector3.ZERO

	# Use actions from the inputmap, as this is the only way to reliably detect input in this method.
	# See #54469 for more discussion and explanation.
	#Input *inp = Input::get_singleton()
	#if (inp->is_action_pressed("spatial_editor/freelook_left")):
	#	direction -= right
	#if (inp->is_action_pressed("spatial_editor/freelook_right")):
	#	direction += right
	#if (inp->is_action_pressed("spatial_editor/freelook_forward")) {
	#	direction += forward
	#if (inp->is_action_pressed("spatial_editor/freelook_backwards")) {
	#	direction -= forward
	#if (inp->is_action_pressed("spatial_editor/freelook_up")) {
	#	direction += up
	#if (inp->is_action_pressed("spatial_editor/freelook_down")) {
	#	direction -= up

	var speed: float = self._freelook_speed

	#if (inp->is_action_pressed("spatial_editor/freelook_speed_modifier")):
	#	speed *= 3.0

	#if (inp->is_action_pressed("spatial_editor/freelook_slow_modifier")):
	#	speed *= 0.333333

	var motion: Vector3 = direction * speed * delta
	self._cursor.pos += motion
	self._cursor.eye_pos += motion

func _init_indicators() -> void:
	# move gizmo

	# Inverted zxy.
	var ivec: Vector3 = Vector3(0, 0, -1)
	var nivec: Vector3 = Vector3(-1, -1, 0)
	var ivec2: Vector3 = Vector3(-1, 0, 0)
	var ivec3: Vector3 = Vector3(0, -1, 0)

	for i in 3:
		var col: Color
		if i == 0:
			col =  Color(0.96, 0.20, 0.32) #get_theme_color(SNAME("axis_x_color"), SNAME("Editor"));
		elif i == 1:
			col = Color(0.53, 0.84, 0.01) #get_theme_color(SNAME("axis_y_color"), SNAME("Editor"));
		elif i == 2:
			col = Color(0.16, 0.55, 0.96) #get_theme_color(SNAME("axis_z_color"), SNAME("Editor"));
		else:
			col = Color();

		col.a = 0.9 #EDITOR_GET("editors/3d/manipulator_gizmo_opacity");

		self._move_gizmo[i] = ArrayMesh.new()
		self._move_plane_gizmo[i] = ArrayMesh.new()
		self._rotate_gizmo[i] = ArrayMesh.new()
		self._scale_gizmo[i] = ArrayMesh.new()
		self._scale_plane_gizmo[i] = ArrayMesh.new()
		self._axis_gizmo[i] = ArrayMesh.new()

		var mat: StandardMaterial3D = StandardMaterial3D.new()
		mat.set_shading_mode(BaseMaterial3D.SHADING_MODE_UNSHADED)
		# The following does more or less this: mat->set_on_top_of_alpha();
		mat.set_flag(BaseMaterial3D.FLAG_DISABLE_DEPTH_TEST, true)
		mat.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA
		mat.albedo_color = col
		self._gizmo_color[i] = mat;

		var mat_hl: StandardMaterial3D = mat.duplicate()
		var albedo: Color = Color.from_hsv(col.h, 0.25, 1.0, 1)
		mat_hl.albedo_color = albedo
		self._gizmo_color_hl[i] = mat_hl

		#translate
		var surftool: SurfaceTool = SurfaceTool.new()
		surftool.begin(Mesh.PRIMITIVE_TRIANGLES)

		# Arrow profile
		var arrow_points: int = 5
		var arrow: Array = [
			nivec * 0.0 + ivec * 0.0,
			nivec * 0.01 + ivec * 0.0,
			nivec * 0.01 + ivec * GIZMO_ARROW_OFFSET,
			nivec * 0.065 + ivec * GIZMO_ARROW_OFFSET,
			nivec * 0.0 + ivec * (GIZMO_ARROW_OFFSET + GIZMO_ARROW_SIZE),
		]

		var arrow_sides: int = 16

		var ma: Basis = Basis.IDENTITY
		var points: Array = []

		var arrow_sides_step: float = TAU / arrow_sides
		for k in arrow_sides:
			ma = Basis(ivec, k * arrow_sides_step)
			var mb = Basis(ivec, (k + 1) * arrow_sides_step)

			for j in (arrow_points - 1):
				points = [
					#ma.xform(arrow[j]),
					self.basis_xform(ma, arrow[j]),
					#mb.xform(arrow[j]),
					self.basis_xform(mb, arrow[j]),
					#mb.xform(arrow[j + 1]),
					self.basis_xform(mb, arrow[j + 1]),
					#ma.xform(arrow[j + 1]),
					self.basis_xform(ma, arrow[j + 1])
				]

				surftool.add_vertex(points[0])
				surftool.add_vertex(points[1])
				surftool.add_vertex(points[2])

				surftool.add_vertex(points[0])
				surftool.add_vertex(points[2])
				surftool.add_vertex(points[3])

		surftool.set_material(mat)
		surftool.commit(self._move_gizmo[i])

		# Plane Translation
		surftool = SurfaceTool.new()
		surftool.begin(Mesh.PRIMITIVE_TRIANGLES);

		var vec: Vector3 = ivec2 - ivec3
		var plane: Array = [
			vec * GIZMO_PLANE_DST,
			vec * GIZMO_PLANE_DST + ivec2 * GIZMO_PLANE_SIZE,
			vec * (GIZMO_PLANE_DST + GIZMO_PLANE_SIZE),
			vec * GIZMO_PLANE_DST - ivec3 * GIZMO_PLANE_SIZE
		]

		ma = Basis(ivec, PI / 2)

		points = [
			#ma.xform(plane[0]),
			self.basis_xform(ma, plane[0]),
			#ma.xform(plane[1]),
			self.basis_xform(ma, plane[1]),
			#ma.xform(plane[2]),
			self.basis_xform(ma, plane[2]),
			#ma.xform(plane[3]),
			self.basis_xform(ma, plane[3]),
		]

		surftool.add_vertex(points[0])
		surftool.add_vertex(points[1])
		surftool.add_vertex(points[2])

		surftool.add_vertex(points[0])
		surftool.add_vertex(points[2])
		surftool.add_vertex(points[3])

		var plane_mat: StandardMaterial3D = StandardMaterial3D.new()
		plane_mat.set_shading_mode(BaseMaterial3D.SHADING_MODE_UNSHADED)
		# The following does more or less this: plane_mat->set_on_top_of_alpha();
		plane_mat.set_flag(BaseMaterial3D.FLAG_DISABLE_DEPTH_TEST, true)
		plane_mat.cull_mode = StandardMaterial3D.CULL_DISABLED
		plane_mat.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA
		plane_mat.albedo_color = col
		self._plane_gizmo_color[i] = plane_mat # needed, so we can draw planes from both sides
		surftool.set_material(plane_mat)
		surftool.commit(self._move_plane_gizmo[i])

		var plane_mat_hl: StandardMaterial3D = plane_mat.duplicate()
		plane_mat_hl.albedo_color = albedo
		self._plane_gizmo_color_hl[i] = plane_mat_hl # needed, so we can draw planes from both sides

		# Rotate

		surftool = SurfaceTool.new()
		surftool.begin(Mesh.PRIMITIVE_TRIANGLES)

		var n: int = 128 # number of circle segments
		var m: int = 3 # number of thickness segments

		var step: float = TAU / n;
		for j in n:
			var basis = Basis(ivec, j * step)

			var vertex: Vector3 = self.basis_xform(basis, ivec2 * GIZMO_CIRCLE_SIZE)

			for k in m:
				var ofs: Vector2 = Vector2(cos((TAU * k) / m), sin((TAU * k) / m))
				var normal: Vector3 = ivec * ofs.x + ivec2 * ofs.y

				surftool.set_normal(self.basis_xform(basis, normal))
				surftool.add_vertex(vertex);

		for j in n:
			for k in m:
				var current_ring: int = j * m
				var next_ring: int = ((j + 1) % n) * m
				var current_segment: int = k
				var next_segment: int = (k + 1) % m

				surftool.add_index(current_ring + next_segment)
				surftool.add_index(current_ring + current_segment)
				surftool.add_index(next_ring + current_segment)

				surftool.add_index(next_ring + current_segment)
				surftool.add_index(next_ring + next_segment)
				surftool.add_index(current_ring + next_segment)

		var rotate_shader: Shader = Shader.new()

		rotate_shader.code = "
// 3D editor rotation manipulator gizmo shader.

shader_type spatial;

render_mode unshaded, depth_test_disabled;

uniform vec4 albedo;

mat3 orthonormalize(mat3 m) {
	vec3 x = normalize(m[0]);
	vec3 y = normalize(m[1] - x * dot(x, m[1]));
	vec3 z = m[2] - x * dot(x, m[2]);
	z = normalize(z - y * (dot(y,m[2])));
	return mat3(x,y,z);
}

void vertex() {
	mat3 mv = orthonormalize(mat3(MODELVIEW_MATRIX));
	vec3 n = mv * VERTEX;
	float orientation = dot(vec3(0, 0, -1), n);
	if (orientation <= 0.005) {
		VERTEX += NORMAL * 0.02;
	}
}

void fragment() {
	ALBEDO = albedo.rgb;
	ALPHA = albedo.a;
}
"

		var rotate_mat: ShaderMaterial = ShaderMaterial.new()
		#rotate_mat.set_render_priority(Material::RENDER_PRIORITY_MAX)
		rotate_mat.set_shader(rotate_shader)
		rotate_mat.set_shader_parameter("albedo", col);
		self._rotate_gizmo_color[i] = rotate_mat;

		var arrays = surftool.commit_to_arrays()
		self._rotate_gizmo[i].add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
		self._rotate_gizmo[i].surface_set_material(0, rotate_mat)


		var rotate_mat_hl: ShaderMaterial = rotate_mat.duplicate()
		rotate_mat_hl.set_shader_parameter("albedo", albedo)
		self._rotate_gizmo_color_hl[i] = rotate_mat_hl

		if i == 2: # Rotation white outline
			var border_mat: ShaderMaterial = rotate_mat.duplicate()

			var border_shader: Shader = Shader.new()
			border_shader.code = "
// 3D editor rotation manipulator gizmo shader (white outline).

shader_type spatial;

render_mode unshaded, depth_test_disabled;

uniform vec4 albedo;

mat3 orthonormalize(mat3 m) {
	vec3 x = normalize(m[0]);
	vec3 y = normalize(m[1] - x * dot(x, m[1]));
	vec3 z = m[2] - x * dot(x, m[2]);
	z = normalize(z - y * (dot(y,m[2])));
	return mat3(x,y,z);
}

void vertex() {
	mat3 mv = orthonormalize(mat3(MODELVIEW_MATRIX));
	mv = inverse(mv);
	VERTEX += NORMAL*0.008;
	vec3 camera_dir_local = mv * vec3(0,0,1);
	vec3 camera_up_local = mv * vec3(0,1,0);
	mat3 rotation_matrix = mat3(cross(camera_dir_local, camera_up_local), camera_up_local, camera_dir_local);
	VERTEX = rotation_matrix * VERTEX;
}

void fragment() {
	ALBEDO = albedo.rgb;
	ALPHA = albedo.a;
}
"

			border_mat.set_shader(border_shader)
			border_mat.set_shader_parameter("albedo", Color(0.75, 0.75, 0.75, col.a / 3.0))

			self._rotate_gizmo[3] = ArrayMesh.new()
			self._rotate_gizmo[3].add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
			self._rotate_gizmo[3].surface_set_material(0, border_mat)

		# Scale

		surftool = SurfaceTool.new()
		surftool.begin(Mesh.PRIMITIVE_TRIANGLES)

		# Cube arrow profile
		arrow_points = 6
		arrow = [
			nivec * 0.0 + ivec * 0.0,
			nivec * 0.01 + ivec * 0.0,
			nivec * 0.01 + ivec * 1.0 * GIZMO_SCALE_OFFSET,
			nivec * 0.07 + ivec * 1.0 * GIZMO_SCALE_OFFSET,
			nivec * 0.07 + ivec * 1.11 * GIZMO_SCALE_OFFSET,
nivec * 0.0 + ivec * 1.11 * GIZMO_SCALE_OFFSET,
		]

		arrow_sides = 4
		arrow_sides_step = TAU / arrow_sides
		for k in 4:
			ma = Basis(ivec, k * arrow_sides_step)
			var mb = Basis(ivec, (k + 1) * arrow_sides_step)

			for j in (arrow_points - 1):
				points = [
					#ma.xform(arrow[j]),
					self.basis_xform(ma, arrow[j]),
					#mb.xform(arrow[j]),
					self.basis_xform(mb, arrow[j]),
					#mb.xform(arrow[j + 1]),
					self.basis_xform(mb, arrow[j + 1]),
					#ma.xform(arrow[j + 1]),
					self.basis_xform(ma, arrow[j + 1]),
				]

				surftool.add_vertex(points[0])
				surftool.add_vertex(points[1])
				surftool.add_vertex(points[2])

				surftool.add_vertex(points[0])
				surftool.add_vertex(points[2])
				surftool.add_vertex(points[3])

		surftool.set_material(mat)
		surftool.commit(self._scale_gizmo[i])

		# Plane Scale

		surftool = SurfaceTool.new()
		surftool.begin(Mesh.PRIMITIVE_TRIANGLES)

		vec = ivec2 - ivec3
		plane = [
			vec * GIZMO_PLANE_DST,
			vec * GIZMO_PLANE_DST + ivec2 * GIZMO_PLANE_SIZE,
			vec * (GIZMO_PLANE_DST + GIZMO_PLANE_SIZE),
			vec * GIZMO_PLANE_DST - ivec3 * GIZMO_PLANE_SIZE
		]

		ma = Basis(ivec, PI / 2)

		points = [
			#ma.xform(plane[0]),
			self.basis_xform(ma, plane[0]),
			#ma.xform(plane[1]),
			self.basis_xform(ma, plane[1]),
			#ma.xform(plane[2]),
			self.basis_xform(ma, plane[2]),
			#ma.xform(plane[3]),
			self.basis_xform(ma, plane[3]),
		]

		surftool.add_vertex(points[0])
		surftool.add_vertex(points[1])
		surftool.add_vertex(points[2])

		surftool.add_vertex(points[0])
		surftool.add_vertex(points[2])
		surftool.add_vertex(points[3])

		plane_mat = StandardMaterial3D.new()
		plane_mat.set_shading_mode(BaseMaterial3D.SHADING_MODE_UNSHADED)
		# The following does more or less this: plane_mat->set_on_top_of_alpha();
		plane_mat.set_flag(BaseMaterial3D.FLAG_DISABLE_DEPTH_TEST, true)
		plane_mat.cull_mode = StandardMaterial3D.CULL_DISABLED
		plane_mat.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA
		plane_mat.albedo_color = col
		self._plane_gizmo_color[i] = plane_mat # needed, so we can draw planes from both sides
		surftool.set_material(plane_mat)
		surftool.commit(self._scale_plane_gizmo[i])

		plane_mat_hl = plane_mat.duplicate()
		plane_mat_hl.albedo_color = Color.from_hsv(col.h, 0.25, 1.0, 1)
		self._plane_gizmo_color_hl[i] = plane_mat_hl # needed, so we can draw planes from both sides

		# Lines to visualize transforms locked to an axis/plane

		surftool = SurfaceTool.new()
		surftool.begin(Mesh.PRIMITIVE_LINE_STRIP)

		vec = Vector3(0, 0, 0)
		vec[i] = 1.0

		# line extending through infinity(ish)
		surftool.add_vertex(vec * -1048576);
		surftool.add_vertex(Vector3());
		surftool.add_vertex(vec * 1048576);
		surftool.set_material(mat_hl);
		surftool.commit(self._axis_gizmo[i]);

func _init_gizmo_instance() -> void:
	var layer = 1 << GIZMO_BASE_LAYER

	# For each axis
	for i in 3:
		var node = MeshInstance3D.new()
		node.mesh = self._move_gizmo[i]
		node.visible = false
		node.layers = layer
		self._move_gizmo_instance[i] = node
		$SubViewport/World.add_child(node)

		node = MeshInstance3D.new()
		node.mesh = self._move_plane_gizmo[i]
		node.visible = false
		node.layers = layer
		self._move_plane_gizmo_instance[i] = node
		$SubViewport/World.add_child(node)

		node = MeshInstance3D.new()
		node.mesh = self._scale_plane_gizmo[i]
		node.visible = false
		node.layers = layer
		self._scale_plane_gizmo_instance[i] = node
		$SubViewport/World.add_child(node)

		node = MeshInstance3D.new()
		node.mesh = self._rotate_gizmo[i]
		node.visible = false
		node.layers = layer
		self._rotate_gizmo_instance[i] = node
		$SubViewport/World.add_child(node)

		if i == 2:
			node = MeshInstance3D.new()
			node.mesh = self._rotate_gizmo[3]
			node.visible = false
			node.layers = layer
			self._rotate_gizmo_instance[3] = node
			$SubViewport/World.add_child(node)

		node = MeshInstance3D.new()
		node.mesh = self._axis_gizmo[i]
		node.visible = false
		node.layers = layer
		self._axis_gizmo_instance[i] = node
		$SubViewport/World.add_child(node)

		node = MeshInstance3D.new()
		node.mesh = self._scale_gizmo[i]
		node.visible = false
		node.layers = layer
		self._scale_gizmo_instance[i] = node
		$SubViewport/World.add_child(node)

	self.update_transform_gizmo()

func get_gizmo_transform() -> Transform3D:
	return self._gizmo.transform

func update_transform_gizmo_view() -> void:
	if not self.is_visible_in_tree():
		return

	var gizmo_xform: Transform3D = self.get_gizmo_transform()

	var camera: Camera3D = self.get_camera()

	var camera_xform: Transform3D = camera.get_transform()

	if is_equal_approx(gizmo_xform.origin.x, camera_xform.origin.x) and is_equal_approx(gizmo_xform.origin.y, camera_xform.origin.y) and is_equal_approx(gizmo_xform.origin.z, camera_xform.origin.z):
		for i in 3:
			self._move_gizmo_instance[i].visible = false
			self._move_plane_gizmo_instance[i].visible = false
			self._rotate_gizmo_instance[i].visible = false
			self._scale_gizmo_instance[i].visible = false
			self._scale_plane_gizmo_instance[i].visible = false
			self._axis_gizmo_instance[i].visible = false

		# Rotation white outline
		self._rotate_gizmo_instance[3].visible = false
		return

	var camz: Vector3 = -camera_xform.basis.z.normalized()
	var camy: Vector3 = -camera_xform.basis.y.normalized()
	var p: Plane = Plane(camz, camera_xform.origin)
	var gizmo_d: float = max(abs(p.distance_to(gizmo_xform.origin)), CMP_EPSILON)
	var d0: float = camera.unproject_position(camera_xform.origin + camz * gizmo_d).y
	var d1: float = camera.unproject_position(camera_xform.origin + camz * gizmo_d + camy).y
	var dd: float = max(abs(d0 - d1), CMP_EPSILON)

	var gizmo_size: float = 80.0 #EDITOR_GET("editors/3d/manipulator_gizmo_size");
	# At low viewport heights, multiply the gizmo scale based on the viewport height.
	# This prevents the gizmo from growing very large and going outside the viewport.
	var viewport_base_height: int = 400 * max(1, EDSCALE);
	self._gizmo_scale = (gizmo_size / abs(dd)) * max(1, EDSCALE) * min(viewport_base_height, self.get_size().y) / viewport_base_height / self.stretch_shrink
	var gizmo_scale: Vector3 = Vector3(1, 1, 1) * self._gizmo_scale

	# if the determinant is zero, we should disable the gizmo from being rendered
	# this prevents supplying bad values to the renderer and then having to filter it out again
	if gizmo_xform.basis.determinant() == 0:
		for i in 3:
			self._move_gizmo_instance[i].visible = false
			self._move_plane_gizmo_instance[i].visible = false
			self._rotate_gizmo_instance[i].visible = false
			self._scale_gizmo_instance[i].visible = false
			self._scale_plane_gizmo_instance[i].visible = false

		# Rotation white outline
		self._rotate_gizmo_instance[3].visible = false
		return

	for i in 3:
		var axis_angle: Transform3D
		if gizmo_xform.basis[i].normalized().dot(gizmo_xform.basis[(i + 1) % 3].normalized()) < 1.0:
			axis_angle = axis_angle.looking_at(
				gizmo_xform.basis[i].normalized(),
				gizmo_xform.basis[(i + 1) % 3].normalized()
			)

		axis_angle.basis = axis_angle.basis.scaled(gizmo_scale)
		axis_angle.origin = gizmo_xform.origin

		self._move_gizmo_instance[i].transform = axis_angle
		self._move_gizmo_instance[i].visible = self.is_gizmo_visible() and (self.get_tool_mode() == ToolMode.TOOL_MODE_SELECT or self.get_tool_mode() == ToolMode.TOOL_MODE_MOVE)

		self._move_plane_gizmo_instance[i].transform = axis_angle
		self._move_plane_gizmo_instance[i].visible = self.is_gizmo_visible() and (self.get_tool_mode() == ToolMode.TOOL_MODE_SELECT or self.get_tool_mode() == ToolMode.TOOL_MODE_MOVE)

		self._rotate_gizmo_instance[i].transform = axis_angle
		self._rotate_gizmo_instance[i].visible = self.is_gizmo_visible() and (self.get_tool_mode() == ToolMode.TOOL_MODE_SELECT or self.get_tool_mode() == ToolMode.TOOL_MODE_ROTATE)

		self._scale_gizmo_instance[i].transform = axis_angle
		self._scale_gizmo_instance[i].visible = self.is_gizmo_visible() and self.get_tool_mode() == ToolMode.TOOL_MODE_SCALE

		self._scale_plane_gizmo_instance[i].transform = axis_angle
		self._scale_plane_gizmo_instance[i].visible = self.is_gizmo_visible() and self.get_tool_mode() == ToolMode.TOOL_MODE_SCALE

		self._axis_gizmo_instance[i].transform = axis_angle

	var show_axes: bool = self.is_gizmo_visible() && self._edit.mode != TransformMode.TRANSFORM_NONE
	#RenderingServer *rs = RenderingServer::get_singleton();
	#rs->instance_set_visible(axis_gizmo_instance[0], show_axes && (_edit.plane == TRANSFORM_X_AXIS || _edit.plane == TRANSFORM_XY || _edit.plane == TRANSFORM_XZ));
	self._axis_gizmo_instance[0].visible = show_axes and (self._edit.plane == TransformPlane.TRANSFORM_X_AXIS or self._edit.plane == TransformPlane.TRANSFORM_XY or self._edit.plane == TransformPlane.TRANSFORM_XZ)
	#rs->instance_set_visible(axis_gizmo_instance[1], show_axes && (_edit.plane == TRANSFORM_Y_AXIS || _edit.plane == TRANSFORM_XY || _edit.plane == TRANSFORM_YZ));
	self._axis_gizmo_instance[1].visible = show_axes and (self._edit.plane == TransformPlane.TRANSFORM_Y_AXIS or self._edit.plane == TransformPlane.TRANSFORM_XY or self._edit.plane == TransformPlane.TRANSFORM_YZ)
	#rs->instance_set_visible(axis_gizmo_instance[2], show_axes && (_edit.plane == TRANSFORM_Z_AXIS || _edit.plane == TRANSFORM_XZ || _edit.plane == TRANSFORM_YZ));
	self._axis_gizmo_instance[2].visible = show_axes and (self._edit.plane == TransformPlane.TRANSFORM_Z_AXIS or self._edit.plane == TransformPlane.TRANSFORM_XZ or self._edit.plane == TransformPlane.TRANSFORM_YZ)

	# Rotation white outline
	gizmo_xform = gizmo_xform.orthonormalized()
	# xform.basis.scaled(scale) by itself crashes Godot 4.0b11... haha. why??
	gizmo_xform.basis = gizmo_xform.basis.scaled(gizmo_scale)

	#RenderingServer::get_singleton()->instance_set_transform(rotate_gizmo_instance[3], xform);
	self._rotate_gizmo_instance[3].transform = gizmo_xform
	#RenderingServer::get_singleton()->instance_set_visible(rotate_gizmo_instance[3], spatial_editor->is_gizmo_visible() && (spatial_editor->get_tool_mode() == Node3DEditor::TOOL_MODE_SELECT || spatial_editor->get_tool_mode() == Node3DEditor::TOOL_MODE_ROTATE));
	# TODO

func set_local_coords_enabled(value: bool) -> void:
	self._local_coords = value

func are_local_coords_enabled() -> bool:
	return self._local_coords

func get_undo_redo() -> UndoRedo:
	return self._undo_redo

func step_decimals(p_step: float) -> int:
	var maxn: int = 10
	var sd = [
		0.9999, # somehow compensate for floating point error
		0.09999,
		0.009999,
		0.0009999,
		0.00009999,
		0.000009999,
		0.0000009999,
		0.00000009999,
		0.000000009999,
		0.0000000009999
	]

	var _abs = abs(p_step)
	var decs = _abs - int(_abs) # Strip away integer part

	for i in maxn:
		if decs >= sd[i]:
			return i
	return 0

func range_step_decimals(p_step: float) -> int:
	if p_step < 0.0000000000001:
		return 16

	return self.step_decimals(p_step)

func update_transform(p_mousepos: Vector2, p_shift: bool) -> void:
	var ray_pos: Vector3 = self._get_ray_pos(p_mousepos)
	var ray: Vector3 = self._get_ray(p_mousepos)
	var snap: float = 0.001 #EDITOR_GET("interface/inspector/default_float_step")
	var _snap_step_decimals: int = self.range_step_decimals(snap)

	if self._edit.mode == TransformMode.TRANSFORM_SCALE:
		var motion_mask: Vector3
		var plane: Plane
		var plane_mv: bool = false

		if self._edit.plane == TransformPlane.TRANSFORM_VIEW:
			motion_mask = Vector3(0, 0, 0)
			plane = Plane(_get_camera_normal(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_X_AXIS:
			motion_mask = self.get_gizmo_transform().basis[0].normalized()
			plane = Plane(motion_mask.cross(motion_mask.cross(self._get_camera_normal())).normalized(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_Y_AXIS:
			motion_mask = self.get_gizmo_transform().basis[1].normalized()
			plane = Plane(motion_mask.cross(motion_mask.cross(self._get_camera_normal())).normalized(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_Z_AXIS:
			motion_mask = self.get_gizmo_transform().basis[2].normalized()
			plane = Plane(motion_mask.cross(motion_mask.cross(self._get_camera_normal())).normalized(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_YZ:
			motion_mask = self.get_gizmo_transform().basis[2].normalized() + self.get_gizmo_transform().basis[1].normalized()
			plane = Plane(self.get_gizmo_transform().basis[0].normalized(), self._edit.center)
			plane_mv = true
		elif self._edit.plane == TransformPlane.TRANSFORM_XZ:
			motion_mask = self.get_gizmo_transform().basis[2].normalized() + self.get_gizmo_transform().basis[0].normalized()
			plane = Plane(self.get_gizmo_transform().basis[1].normalized(), self._edit.center)
			plane_mv = true
		elif self._edit.plane == TransformPlane.TRANSFORM_XY:
			motion_mask = self.get_gizmo_transform().basis[0].normalized() + self.get_gizmo_transform().basis[1].normalized()
			plane = Plane(self.get_gizmo_transform().basis[2].normalized(), self._edit.center)
			plane_mv = true

		var intersection: Vector3 = plane.intersects_ray(ray_pos, ray)
		if intersection == null:
			return

		var click: Vector3 = plane.intersects_ray(_edit.click_ray_pos, _edit.click_ray)
		if click == null:
			return

		var motion: Vector3 = intersection - click
		if self._edit.plane != TransformPlane.TRANSFORM_VIEW:
			if not plane_mv:
				motion = motion_mask.dot(motion) * motion_mask

			else:
				# Alternative planar scaling mode
				if p_shift:
					motion = motion_mask.dot(motion) * motion_mask
		else:
			var center_click_dist: float = click.distance_to(self._edit.center)
			var center_inters_dist: float = intersection.distance_to(self._edit.center)
			if center_click_dist == 0:
				return

			var calculated_scale: float = center_inters_dist - center_click_dist
			motion = Vector3(calculated_scale, calculated_scale, calculated_scale)

		motion /= click.distance_to(self._edit.center)

		# Disable local transformation for TRANSFORM_VIEW
		var local_coords: bool = self.are_local_coords_enabled() and self._edit.plane != TransformPlane.TRANSFORM_VIEW

		if self._edit.snap or self.is_snap_enabled():
			snap = self.get_scale_snap(p_shift) / 100

		var motion_snapped: Vector3 = motion
		#motion_snapped.snap(Vector3(snap, snap, snap))
		motion_snapped = snapped(motion_snapped, Vector3(snap, snap, snap))

		# This might not be necessary anymore after issue #288 is solved (in 4.0?).
		# TRANSLATORS: Refers to changing the scale of a node in the 3D editor.
		#set_message(TTR("Scaling:") + " (" + String::num(motion_snapped.x, snap_step_decimals) + ", " +
		#		String::num(motion_snapped.y, snap_step_decimals) + ", " + String::num(motion_snapped.z, snap_step_decimals) + ")");
		#motion = self._edit.original.basis.inverse().xform(motion)
		motion = self.basis_xform(self._edit.original.basis.inverse(), motion)

		for sp in self._editor_selection.get_selected_node_list():
			if sp == null:
				continue

			var se = self._editor_selection.get_node_editor_data(sp)
			if se == null:
				continue

			if sp.has_meta("_edit_lock_"):
				continue

			if se.gizmo:
				for subgizmo in se.subgizmos:
					var subgizmo_xform: Transform3D = se.subgizmos[subgizmo]
					# Force orthogonal with subgizmo.
					var new_xform: Transform3D = self._compute_transform(TransformMode.TRANSFORM_SCALE, se.original * subgizmo_xform, subgizmo_xform, motion, snap, local_coords, true)
					if not local_coords:
						new_xform = se.original.affine_inverse() * new_xform

					se.gizmo.set_subgizmo_transform(subgizmo, new_xform)

			else:
				var new_xform: Transform3D = self._compute_transform(TransformMode.TRANSFORM_SCALE, se.original, se.original_local, motion, snap, local_coords, sp.get_rotation_edit_mode() != Node3D.ROTATION_EDIT_MODE_BASIS)
				self._transform_gizmo_apply(se.sp, new_xform, local_coords)

		self.update_transform_gizmo()
		self.queue_redraw()

	elif self._edit.mode == TransformMode.TRANSFORM_TRANSLATE:
		var motion_mask: Vector3
		var plane: Plane
		var plane_mv: bool = false

		if self._edit.plane == TransformPlane.TRANSFORM_VIEW:
			plane = Plane(self._get_camera_normal(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_X_AXIS:
			motion_mask = self.get_gizmo_transform().basis[0].normalized()
			plane = Plane(motion_mask.cross(motion_mask.cross(_get_camera_normal())).normalized(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_Y_AXIS:
			motion_mask = self.get_gizmo_transform().basis[1].normalized();
			plane = Plane(motion_mask.cross(motion_mask.cross(self._get_camera_normal())).normalized(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_Z_AXIS:
			motion_mask = self.get_gizmo_transform().basis[2].normalized()
			plane = Plane(motion_mask.cross(motion_mask.cross(self._get_camera_normal())).normalized(), self._edit.center)
		elif self._edit.plane == TransformPlane.TRANSFORM_YZ:
			plane = Plane(self.get_gizmo_transform().basis[0].normalized(), self._edit.center)
			plane_mv = true
		elif self._edit.plane == TransformPlane.TRANSFORM_XZ:
			plane = Plane(self.get_gizmo_transform().basis[1].normalized(), self._edit.center)
			plane_mv = true
		elif self._edit.plane == TransformPlane.TRANSFORM_XY:
			plane = Plane(self.get_gizmo_transform().basis[2].normalized(), self._edit.center)
			plane_mv = true

		var intersection: Variant = plane.intersects_ray(ray_pos, ray)
		if intersection == null:
			return

		var click: Variant = plane.intersects_ray(_edit.click_ray_pos, _edit.click_ray)
		if click == null:
			return

		var motion: Vector3 = intersection - click
		if self._edit.plane != TransformPlane.TRANSFORM_VIEW:
			if not plane_mv:
				motion = motion_mask.dot(motion) * motion_mask

		# Disable local transformation for TRANSFORM_VIEW
		var local_coords: bool = self.are_local_coords_enabled() and self._edit.plane != TransformPlane.TRANSFORM_VIEW

		if self._edit.snap or self.is_snap_enabled():
			snap = self.get_translate_snap(p_shift)

		var motion_snapped: Vector3 = motion
		#motion_snapped.snap(Vector3(snap, snap, snap))
		motion_snapped = snapped(motion_snapped, Vector3(snap, snap, snap))
		# TRANSLATORS: Refers to changing the position of a node in the 3D editor.
		#set_message(TTR("Translating:") + " (" + String::num(motion_snapped.x, snap_step_decimals) + ", " +
		#		String::num(motion_snapped.y, snap_step_decimals) + ", " + String::num(motion_snapped.z, snap_step_decimals) + ")");
		#motion = self.get_gizmo_transform().basis.inverse().xform(motion)
		motion = self.basis_xform(self.get_gizmo_transform().basis.inverse(), motion)

		for sp in self._editor_selection.get_selected_node_list():
			if sp == null:
				continue

			var se = self._editor_selection.get_node_editor_data(sp)
			if se == null:
				continue

			if sp.has_meta("_edit_lock_"):
				continue

			if se.gizmo:
				for subgizmo in se.subgizmos:
					var subgizmo_xform: Transform3D = se.subgizmos[subgizmo]
					# Force orthogonal with subgizmo.
					var new_xform: Transform3D = self._compute_transform(TransformMode.TRANSFORM_TRANSLATE, se.original * subgizmo_xform, subgizmo_xform, motion, snap, local_coords, true)
					new_xform = se.original.affine_inverse() * new_xform
					se.gizmo.set_subgizmo_transform(subgizmo, new_xform)
			else:
				var new_xform: Transform3D = self._compute_transform(TransformMode.TRANSFORM_TRANSLATE, se.original, se.original_local, motion, snap, local_coords, sp.get_rotation_edit_mode() != Node3D.ROTATION_EDIT_MODE_BASIS)
				self._transform_gizmo_apply(se.sp, new_xform, false)

		self.update_transform_gizmo()
		self.queue_redraw()

	elif self._edit.mode == TransformMode.TRANSFORM_ROTATE:
		var plane: Plane = Plane(self._get_camera_normal(), self._edit.center)

		var local_axis: Vector3
		var global_axis: Vector3

		if self._edit.plane == TransformPlane.TRANSFORM_VIEW:
			# local_axis unused
			global_axis = _get_camera_normal();
		elif self._edit.plane == TransformPlane.TRANSFORM_X_AXIS:
			local_axis = Vector3(1, 0, 0)
		elif self._edit.plane == TransformPlane.TRANSFORM_Y_AXIS:
			local_axis = Vector3(0, 1, 0)
		elif self._edit.plane == TransformPlane.TRANSFORM_Z_AXIS:
			local_axis = Vector3(0, 0, 1)

		if self._edit.plane != TransformPlane.TRANSFORM_VIEW:
			#global_axis = self.get_gizmo_transform().basis.xform(local_axis).normalized();
			global_axis = self.basis_xform(self.get_gizmo_transform().basis, local_axis).normalized()

		var intersection: Vector3 = plane.intersects_ray(ray_pos, ray)
		if intersection == null:
			return

		var click: Vector3 = plane.intersects_ray(_edit.click_ray_pos, _edit.click_ray)
		if click == null:
			return

		var orthogonal_threshold: float = cos(deg_to_rad(87.0))
		var axis_is_orthogonal: bool = abs(plane.normal.dot(global_axis)) < orthogonal_threshold

		var angle: float = 0.0
		if axis_is_orthogonal:
			self._edit.show_rotation_line = false
			var projection_axis: Vector3 = plane.normal.cross(global_axis)
			var delta: Vector3 = intersection - click
			var projection: float = delta.dot(projection_axis)
			angle = (projection * (PI / 2.0)) / (self._gizmo_scale * GIZMO_CIRCLE_SIZE)
		else:
			self._edit.show_rotation_line = true
			var click_axis: Vector3 = (click - self._edit.center).normalized()
			var current_axis: Vector3 = (intersection - self._edit.center).normalized()
			angle = click_axis.signed_angle_to(current_axis, global_axis)

		if self._edit.snap or self.is_snap_enabled():
			snap = self.get_rotate_snap(p_shift)
		angle = rad_to_deg(angle) + snap * 0.5 # else it won't reach +180
		angle -= fmod(angle, snap)
		#set_message(vformat(TTR("Rotating %s degrees."), String::num(angle, snap_step_decimals)));
		angle = deg_to_rad(angle)
		# Disable local transformation for TRANSFORM_VIEW
		var local_coords: bool = self.are_local_coords_enabled() and self._edit.plane != TransformPlane.TRANSFORM_VIEW

		for sp in self._editor_selection.get_selected_node_list():
			if sp == null:
				continue

			var se = self._editor_selection.get_node_editor_data(sp)
			if se == null:
				continue

			if sp.has_meta("_edit_lock_"):
				continue

			var compute_axis: Vector3 = local_axis if local_coords else global_axis
			if se.gizmo:
				for subgizmo in se.subgizmos:
					var subgizmo_xform: Transform3D = se.subgizmos[subgizmo]

					# Force orthogonal with subgizmo.
					var new_xform: Transform3D = self._compute_transform(TransformMode.TRANSFORM_ROTATE, se.original * subgizmo_xform, subgizmo_xform, compute_axis, angle, local_coords, true)
					if not local_coords:
						new_xform = se.original.affine_inverse() * new_xform

					se.gizmo.set_subgizmo_transform(subgizmo, new_xform)
			else:
				var new_xform: Transform3D = self._compute_transform(TransformMode.TRANSFORM_ROTATE, se.original, se.original_local, compute_axis, angle, local_coords, sp.get_rotation_edit_mode() != Node3D.ROTATION_EDIT_MODE_BASIS)
				self._transform_gizmo_apply(se.sp, new_xform, local_coords)

		self.update_transform_gizmo()
		self.queue_redraw()

func get_translate_snap(p_shift: bool) -> float:
	var snap_value: float = 1.0
	if p_shift:
		snap_value = snap_value / 10.0

	return snap_value

func get_rotate_snap(p_shift: bool) -> float:
	var snap_value: float = 15.0
	if p_shift:
		snap_value = snap_value / 3.0

	return snap_value

func get_scale_snap(p_shift: bool) -> float:
	var snap_value: float = 10.0

	if p_shift:
		snap_value = snap_value / 2.0

	return snap_value

func commit_transform() -> void:
	if self._edit.mode == TransformMode.TRANSFORM_NONE:
		return

	var _transform_name = ["None", "Rotate", "Translate", "Scale"]

	var undo_redo = self.get_undo_redo()
	undo_redo.create_action(_transform_name[_edit.mode])

	var selection = self._editor_selection.get_selected_node_list()

	for sp in selection:
		if not sp:
			continue

		var se = self._editor_selection.get_node_editor_data(sp)
		if se == null:
			continue

		undo_redo.add_do_method(sp.set_global_transform.bind(self.get_global_gizmo_transform(sp)))
		undo_redo.add_undo_method(sp.set_global_transform.bind(se.original))

	undo_redo.commit_action()

	self.finish_transform()

func finish_transform() -> void:
	self.set_local_coords_enabled(self._edit.original_local)
	self.update_transform_gizmo()
	self._edit.mode = TransformMode.TRANSFORM_NONE
	self._edit.instant = false
	self.queue_redraw()

func update_transform_gizmo() -> void:
	var count: int = 0
	var local_gizmo_coords: bool = self.are_local_coords_enabled()

	var gizmo_center: Vector3 = Vector3.ZERO
	var gizmo_basis: Basis = Basis.IDENTITY

	var se = self._editor_selection.get_node_editor_data(self._selected) if self._selected else null

	if se and se.gizmo:
		for subgizmo in se.subgizmos:
			var xf: Transform3D = se.sp.get_global_transform() * se.gizmo.get_subgizmo_transform()
			gizmo_center += xf.origin
			if count == 0 and local_gizmo_coords:
				gizmo_basis = xf.basis

			count += 1
	else:
		var selection = self._editor_selection.get_selected_node_list()
		for sp in selection:
			if sp == null:
				continue

			if sp.has_meta("_edit_lock_"):
				continue

			var sel_item = self._editor_selection.get_node_editor_data(sp)
			if sel_item == null:
				continue

			var xf: Transform3D = sp.get_global_transform()
			gizmo_center += xf.origin;
			if count == 0 and local_gizmo_coords:
				gizmo_basis = xf.basis

			count += 1

	self._gizmo.visible = count > 0;
	self._gizmo.transform.origin = gizmo_center / count if count > 0 else Vector3()
	self._gizmo.transform.basis = gizmo_basis if count == 1 else Basis()

	# TODO for each viewport (side, perspective, etc)
	self.update_transform_gizmo_view()

func get_selected_count() -> int:
	return 1 if self._selected_element else 0

func get_current_hover_gizmo() -> Node3D:
	return self._current_hover_gizmo

# TODO
func get_subgizmo_transform(_a, _b) -> Node3D:
	return null

# TODO
func subgizmos_intersect_ray(_a, _b, _c) -> int:
	return -1

# TODO
func get_handle_value(_a, _b, _c) -> Object:
	return null

# TODO
func set_current_hover_gizmo(_new_current_hover_gizmo) -> void:
	pass

# TODO
func update_gizmos(_selected_node) -> void:
	pass

func set_current_hover_gizmo_handle(p_id: int, p_secondary: bool) -> void:
	self._current_hover_gizmo_handle = p_id
	self._current_hover_gizmo_handle_secondary = p_secondary

func get_current_hover_gizmo_handle() -> Array:
	return [self._current_hover_gizmo_handle, self._current_hover_gizmo_handle_secondary]

func update_all_gizmos() -> void:
	pass

func is_gizmo_visible() -> bool:
	if self._selected_element:
		return self._gizmo.visible and self.is_transform_gizmo_visible(self._selected)

	return self._gizmo.visible

func is_transform_gizmo_visible(_selected_node: Node3D) -> bool:
	return true

func _draw():
	# Draw selection box
	if self._cursor.region_begin:
		var selection_rect: Rect2 = Rect2(
			self._cursor.region_begin,
			self._cursor.region_end - self._cursor.region_begin
		)

		# get_theme_color(SNAME("box_selection_fill_color"), SNAME("Editor"))
		var fill_color: Color = Color("ff8000")

		# get_theme_color(SNAME("box_selection_stroke_color"), SNAME("Editor")),
		var stroke_color: Color = Color("000000")

		self.draw_rect(selection_rect, fill_color)
		self.draw_rect(selection_rect, stroke_color, false, round(EDSCALE))



var _environmental_elements: Array = []
var _world_environment: WorldEnvironment

func update_environment():
	# Get / create the world environment for this world
	if not self._world_environment:
		self._world_environment = WorldEnvironment.new()
		var world = self.get_current_world()
		world.add_child(self._world_environment)

	self._world_environment.environment = Environment.new()
	for element in self._environmental_elements:
		if element.get_visible():
			element.update_environment(self._world_environment.environment)

func get_current_world() -> Node3D:
	if not self._world:
		return $SubViewport/World

	return self._world

func get_camera() -> Camera3D:
	var camera: Camera3D = self.get_current_world().get_node("Camera3D")
	camera.current = true
	return camera

func add_world(world_item: TreeItem) -> Node3D:
	var new_world: Node3D = $SubViewport/World.duplicate()
	$SubViewport.add_child(new_world)
	self._worlds[world_item] = new_world
	return new_world

func show_world(world_item: TreeItem) -> Node3D:
	if self._world:
		self._world.visible = false
		self._world = null

	if self._worlds.has(world_item):
		self._world = self._worlds[world_item]
		self._world.visible = true

		# Move gizmo
		for i in 3:
			self._move_gizmo_instance[i].get_parent().remove_child(self._move_gizmo_instance[i])
			self._world.add_child(self._move_gizmo_instance[i])

			self._rotate_gizmo_instance[i].get_parent().remove_child(self._rotate_gizmo_instance[i])
			self._world.add_child(self._rotate_gizmo_instance[i])

			self._scale_gizmo_instance[i].get_parent().remove_child(self._scale_gizmo_instance[i])
			self._world.add_child(self._scale_gizmo_instance[i])

			self._scale_plane_gizmo_instance[i].get_parent().remove_child(self._scale_plane_gizmo_instance[i])
			self._world.add_child(self._scale_plane_gizmo_instance[i])

			self._axis_gizmo_instance[i].get_parent().remove_child(self._axis_gizmo_instance[i])
			self._world.add_child(self._axis_gizmo_instance[i])

	return self._world

func create_element(element_class: Script) -> Element:
	var element: Element = element_class.new()
	element._init()
	return element

func add_element(element: Element, _element_class: Script):
	if element is EnvironmentalElement:
		# Ah. An environmental element.
		self._environmental_elements.append(element)

	var world = self.get_current_world()
	world.add_child(element.get_native_node())
	$%ItemContainer.add_element(element)
	element._on_add()

	var camera = Camera3D.new()
	var reference_camera: Camera3D = self.get_camera()
	camera.set_transform(reference_camera.get_transform())
	world.add_child(camera)
	camera.add_child(Camera3DGizmo.new())

	self.camera_mesh()
