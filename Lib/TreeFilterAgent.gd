# This class extends Tree to provide a means of filtering tree items using an
# edit field that pops up on CTRL+F.

class_name TreeFilterAgent
extends Node


var search_field: LineEdit = LineEdit.new()
var _hidden_items: Array[TreeItem] = []

# The tree we are attached to
var _tree: Tree


func clear_filter():
	for item in self._hidden_items:
		item.visible = true

	self._hidden_items = []

func filter_subtree(root: TreeItem, text: String) -> int:
	var visible_count: int = 0

	text = text.to_lower()

	var child: TreeItem = root.get_first_child()
	while child:
		if child.get_child_count() == 0:
			if not child.get_text(0).to_lower().contains(text):
				self._hidden_items.append(child)
				child.visible = false
			else:
				visible_count += 1
		else:
			# These are "groups", which we don't filter
			var sub_visible_count: int = self.filter_subtree(child, text)
			if sub_visible_count == 0:
				# The group is empty, hide it too
				self._hidden_items.append(child)
				child.visible = false

			visible_count += sub_visible_count

		child = child.get_next_visible()

	return visible_count

func show_search_field():
	if not self._tree:
		return

	if self.search_field.get_parent() != self:
		self._tree.add_child(self.search_field)

		# TODO: add clear button
		self.search_field.focus_exited.connect(func():
			if self.search_field.text == "":
				self.search_field.visible = false
		)

		self.search_field.text_changed.connect(func(text: String):
			var root: TreeItem = self._tree.get_root()
			self.clear_filter()

			if text != "":
				self.filter_subtree(root, text)
		)

	self.search_field.position = Vector2(0, self._tree.size.y - self.search_field.size.y)
	self.search_field.size.x = self._tree.size.x
	self.search_field.visible = true
	self.search_field.grab_focus()

# Process keyboard input for the search key binding
func _input(event):
	# Get the Tree that is our parent and bail if there is no tree as our parent
	if not self._tree:
		if self.get_parent() is Tree:
			self._tree = self.get_parent()

	if not self._tree:
		return

	# Determine the key event
	if event is InputEventKey:
		var key_event: InputEventKey = null
		key_event = event

		# If the search field has focus (or tree), listen for events
		if self.search_field.has_focus() or self._tree.has_focus():
			# When ESC is pressed on the input or tree, close search panel
			if key_event.keycode == Key.KEY_ESCAPE:
				self.search_field.text = ''
				self.clear_filter()
				self.search_field.visible = false
				self._tree.grab_focus()

		# Ensure the parent tree has focus for these events
		if self._tree.has_focus():
			# Look for the CTRL+F sequence
			if key_event.keycode == Key.KEY_F and key_event.ctrl_pressed:
				# Bring up the search panel
				self.show_search_field()
