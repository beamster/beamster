# This class extends Tree to provide a means of dragging tree items using the
# mouse to reorder them.

# The class adds the `item_moved` signal which is activated when an item is
# dragged and moved. This fires while the dragging is happening.

# This class requires the `_input` function to call its super when extending
# this class any further.

class_name TreeDragAgent
extends Node


signal item_drag_start
signal item_drag_end
signal item_moved


var drag_tolerance: float = 5.0:
	set = set_drag_tolerance,
	get = get_drag_tolerance

var _drag_collapsed: Array[TreeItem] = []
var _drag_item: TreeItem = null
var _drag_shadow: TextureRect = null

var _move_start: Vector2 = Vector2.ZERO
var _move_up: bool = false
var _move_up_elapsed: float = -1.0

# The tree we are attached to
var _tree: Tree


# Process mouse input for item drags and context menus
func _input(event):
	# Get the Tree that is our parent and bail if there is no tree as our parent
	if not self._tree:
		if self.get_parent() is Tree:
			self._tree = self.get_parent()

	if not self._tree:
		return

	var mouse_event: InputEventMouse = null
	if event is InputEventMouse:
		mouse_event = event

	# Handle mouse motion (move drag item, update borders, scroll)
	if event is InputEventMouseMotion:
		if not self._drag_item:
			if self._move_start != Vector2.ZERO:
				var position: Vector2 = self._tree.get_local_mouse_position()

				if self._move_start.distance_to(position) > self.drag_tolerance:
					self._drag_item = self._tree.get_item_at_position(
						self._tree.get_local_mouse_position()
					)
		else:
			# Create a drag item 'shadow', if needed.
			if not self._drag_shadow:
				self._drag_shadow = TextureRect.new()
				self._drag_shadow.size = Vector2(32, 32)
				self._drag_shadow.top_level = true
				self._drag_shadow.visible = false
				self._tree.add_child(self._drag_shadow)

			# The first time we activate a drag, we do a few things.
			if not self._drag_shadow.visible:
				# Set the icon
				if self._drag_item.has_meta("drag_icon"):
					self._drag_shadow.texture = self._drag_item.get_meta("drag_icon")
				else:
					self._drag_shadow.texture = self._drag_item.get_icon(0)

				# Collapse everything (we will undo afterward)
				self._drag_collapsed = []
				var child: TreeItem = self._drag_item.get_parent().get_first_child()
				while child:
					if not child.collapsed:
						self._drag_collapsed.append(child)
						child.collapsed = true
					child = child.get_next_visible()

				# Show the drag icon preview
				self._drag_shadow.visible = true

				# Tell the world
				self.item_drag_start.emit(self._drag_item)

			# Move drag item 'shadow'
			self._drag_shadow.position = mouse_event.global_position

			# Reposition our own item
			var hovered_item: TreeItem = self._tree.get_item_at_position(
				self._tree.get_local_mouse_position()
			)

			# Do not allow us to reparent the item
			if hovered_item and hovered_item.get_parent() != self._drag_item.get_parent():
				hovered_item = null

			if not hovered_item and \
				mouse_event.global_position.y > self._tree.get_global_rect().position.y - self._tree.get_item_area_rect(self._drag_item).size.y and \
				mouse_event.global_position.y <= self._tree.get_global_rect().position.y:
				# We should move it up, but only on a timer.
				if not self._move_up:
					self._move_up_elapsed = -1
					self._move_up = true
			else:
				self._move_up = false

			# Move the item
			if hovered_item and not hovered_item == self._drag_item:
				if self._drag_item.get_index() > hovered_item.get_index():
					# Move tree item
					if self._tree.has_method("can_move_item"):
						if not self._tree.call("can_move_item", self._drag_item, hovered_item.get_index()):
							return
					self._drag_item.move_before(hovered_item)
				else:
					if self._tree.has_method("can_move_item"):
						if not self._tree.call("can_move_item", self._drag_item, hovered_item.get_index() + 1):
							return
					self._drag_item.move_after(hovered_item)

				# Signal a reorder
				self.item_moved.emit(self._drag_item)

			# Scroll to moved item
			self._tree.scroll_to_item(self._drag_item)

	# Handle mouse release (commit drag item)
	if event is InputEventMouseButton and not event.is_pressed():
		self._move_up = false
		self._move_start = Vector2.ZERO

		if self._drag_item:
			# Re-expand
			for child in self._drag_collapsed:
				child.collapsed = false
			self._drag_collapsed = []

			# Remove drag shadow
			if self._drag_shadow:
				self._drag_shadow.visible = false

			# Scroll to moved item
			self._tree.scroll_to_item(self._drag_item)

			# Tell the world
			self.item_drag_end.emit(self._drag_item)

			# We are no longer dragging
			self._drag_item = null

	# Only track if the mouse is over us
	if event is InputEventMouse:
		if not self._tree.get_global_rect().has_point(mouse_event.global_position):
			return

	# Handle mouse down (start a drag event, maybe)
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == MOUSE_BUTTON_LEFT:
			self._move_start = self._tree.get_local_mouse_position()

func _process(elapsed: float):
	if self._move_up:
		self._move_up_elapsed -= elapsed
		if self._move_up_elapsed < 0:
			if self._drag_item:
				# Move it up the chain (as long as it won't reparent)
				var prev_visible_item = self._drag_item.get_prev_visible()
				if prev_visible_item and self._drag_item.get_parent() == prev_visible_item.get_parent():
					if self._tree.has_method("can_move_item"):
						if not self._tree.call("can_move_item", self._drag_item, prev_visible_item.get_index()):
							return

					self._drag_item.move_before(prev_visible_item)

					# Scroll to moved item
					self._tree.scroll_to_item(self._drag_item)

					# Emit signal
					self.item_moved.emit(self._drag_item)

			self._move_up_elapsed = 0.30

func set_drag_tolerance(value: float):
	drag_tolerance = value

func get_drag_tolerance() -> float:
	return drag_tolerance
