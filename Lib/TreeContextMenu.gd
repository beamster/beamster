class_name TreeContextMenu
extends Node

# The context menu items
var context_menu_items: Array[Dictionary] = []:
	set = set_context_menu_items,
	get = get_context_menu_items

func set_context_menu_items(items: Array[Dictionary]):
	context_menu_items = items

func get_context_menu_items() -> Array[Dictionary]:
	return context_menu_items

# The PopupMenu to show for the item
var _context_menu: PopupMenu = null

# The current conidered item
var _current_item: TreeItem

# The tree we are attached to
var _tree: Tree


signal item_selected(item: TreeItem, menu_item_id: int)


func _init(initial_context_menu_items: Array[Dictionary] = []):
	self.set_context_menu_items(initial_context_menu_items)

func _input(event: InputEvent):
	# Get the Tree that is our parent and bail if there is no tree as our parent
	if not self._tree:
		if self.get_parent() is Tree:
			self._tree = self.get_parent()

	if not self._tree:
		return

	var mouse_event: InputEventMouse = null
	if event is InputEventMouse:
		mouse_event = event

	# Only track if the mouse is over us
	if event is InputEventMouse:
		if not self._tree.get_global_rect().has_point(mouse_event.global_position):
			return

	# Handle mouse down (start a drag event, maybe)
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == MOUSE_BUTTON_RIGHT:
			# Stop propagating
			self._tree.get_tree().get_root().set_input_as_handled()

			# If this is over an item, popup the menu
			var item = self._tree.get_item_at_position(
				self._tree.get_local_mouse_position()
			)

			if item:
				# Show the context menu for this item
				var at: Vector2 = self._tree.get_local_mouse_position() - self._tree.get_item_area_rect(item).position
				self.show_context_menu(item, at)
			else:
				# If this is just over space, popup a different menu, maybe
				pass

func get_context_menu() -> PopupMenu:
	if not self._tree:
		return

	if self._context_menu == null:
		var popup = PopupMenu.new()
		for item in self.context_menu_items:
			if "name" in item:
				popup.add_item(item["name"])
			elif item.is_empty():
				popup.add_separator()

		popup.visible = false
		self._tree.add_child(popup)

		popup.index_pressed.connect(self.on_context_menu_select)
		self._context_menu = popup

	return self._context_menu

func show_context_menu(item: TreeItem, at: Vector2 = Vector2(0, 0)):
	if not self._tree:
		return

	self._current_item = item
	var popup: PopupMenu = self.get_context_menu()
	var item_rect: Rect2 = self._tree.get_item_area_rect(item, 0)
	var item_position: Vector2 = item_rect.position
	var window_position: Vector2 = DisplayServer.window_get_position(0) * 1.0
	var window_size: Vector2 = DisplayServer.window_get_size(0) * 1.0
	var tree_position: Vector2 = window_position + self._tree.global_position
	popup.position = tree_position + item_position + at - self._tree.get_scroll()
	popup.visible = true
	popup.reset_size()
	popup.position.x -= int(popup.size.x / 2.0)
	if popup.size.y + popup.position.y > window_position.y + window_size.y:
		popup.position.y = int(window_position.y + window_size.y - popup.size.y)

func on_context_menu_select(index: int):
	if self._current_item:
		var menu_item = self.context_menu_items[index]
		if "call" in menu_item:
			menu_item["call"].call(self._current_item)

		self.item_selected.emit(self._current_item, index)
