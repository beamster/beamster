class_name Storage
extends Object


# Everything is an object.
# Everything is de-duplicated but owned by one parent.
# Everything is identified by one globally unique identifier.

# Object:
#   id
#   type
#   subtype
#   name
#   parent
#   children

# Types:
#   - profile: A collection of scenes and worlds
#   - scene: A collection of sources
#   - source: An object in a scene
#   - world: A collection of elements
#   - element: An object in a world
#   - shader: A program that renders a filter
#   - material: A program that renders a texture
#   - sky: A program that renders a world background (sky)

# We have several behaviors exposed here:
#   - import: Bring in an existing exported object
#   - export: Serialize an internally known object
#   - store: Persist the object in the object store
#   - load: Realize an object in the object store
#   - unrefer: Remove a reference to an object in the object store
#   - refer: Add a reference to an object in the object store
#   - update: Make a change to an existing object
#   - reparent: Change the owner of an existing object

# With this in mind, there are several properties of the persisted object
# store. First, objects are reference counted. When they are used in a
# persisted object, the count is increased. When the object is removed from
# that context, the count is decreased. When the count is 0, it is no longer
# seen by any context and can be deleted. This is done through the internal
# `refer` and `unrefer` methods. Exported objects are not tracked in this way.
# When an object is deleted as its count reaches 0, the only means of recovery
# would be to import an existing serialized export of that object.

# Objects will be stored with the following scheme, assuming the identifiers
# are truly globally unique:

# ./objects/xx/yy/xxyy5678-1234-5678-1234-567812345678

# This identifier is a 32 character uuid represented in its traditional form.
# The first two characters coming from the first two characters in the uuid.
# The second two characters coming respectively from the second pair in the
# uuid. This being done to relieve the pressure on the directory caches and
# sidestep any OS limitations if we were to overflow the directory with too
# many files.

# Media, on the otherhand, if we want to store that should be stored in a
# content addressable way. The process we should adapt is to hash the
# content with a multihash algorithm. This would align with efforts such as
# IPFS and other technology leveraging a similar approach. This might lead
# to content being able to be addressed over a network so as to share resources
# or build more capable asset stores.

# Sources, etc, should have the ability to store a copy of media within (or move
# media into and out of) the media's Content Store. We will assume the content
# is mixed media of some kind and already compressed and do no further
# compression until it would be deemed necessary in the future.

# Media objects will have a cooresponding Object in the object store that will
# be responsible for referencing counting the media and preserving metadata,
# such as a canonical name. This means that media objects have an identifier.
# We should keep track of the identifier next to the media data such that there
# is a known mapping from hash to Object identifier.

# For media, we would expose the following behaviors:
#   - upload(data) -> hash: Store the data into the media store
#   - filepath(hash) -> str: Retrieve a file path to the data blob for the given hash
#   - delete(hash) -> bool: Delete the blob at the given hash

# The Content Store will only be interacted via an Object from the Object Store
# that owns that media.

var fileAccess: Object = null
var dirAccess: Object = null
var sub_path: String = ''

var uuid: GDScript = preload("res://addons/uuid/uuid.gd")

func _init(initial_sub_path: String = ""):
	self.fileAccess = fileAccess
	self.dirAccess = dirAccess
	if initial_sub_path.length() > 0 and not initial_sub_path.ends_with("/"):
		initial_sub_path = initial_sub_path + "/"
	self.sub_path = initial_sub_path

# Import the provided serialized Object into the Object Store.
func import(_path) -> String:
	return ""

# Output an Object to a serialized form that could be imported by another actor.
func export(_id: String, _path):
	pass

# The path to the profile listing information.
func profiles_dir() -> String:
	return "user://%sprofiles" % self.sub_path

# The path to the profile listing metadata file.
func profiles_metadata_path() -> String:
	return "%s/metadata.json" % self.profiles_dir()

# The path to the profile listing lockfile.
func profiles_lockfile_path() -> String:
	return "%s/lock" % self.profiles_dir()

func dir(id: String) -> String:
	# Find the cooresponding path
	var c0: String = id.substr(0, 2)
	var c1: String = id.substr(2, 2)
	return "user://%sobjects/%s/%s/%s" % [self.sub_path, c0, c1, id]

func metadata_path(id: String) -> String:
	return "%s/metadata.json" % self.dir(id)

func refcount_path(id: String) -> String:
	return "%s/refcount.json" % self.dir(id)

func lockfile_path(id: String) -> String:
	return "%s/lock" % self.dir(id)

# Store the given object into the Object Store.
func store(object: Dictionary) -> String:
	# Define an identifier, if one does not exist
	if not object.has("id"):
		object["id"] = self.uuid.v4()

	self.lock(object["id"])

	# Find the cooresponding path
	var base: String = self.dir(object["id"])
	var path: String = self.metadata_path(object["id"])

	DirAccess.make_dir_recursive_absolute(base)
	var is_new: bool = not FileAccess.file_exists(path)
	var file: FileAccess = FileAccess.open(path, FileAccess.WRITE)
	file.store_line(JSON.stringify(object))

	if is_new:
		self.set_refcount(object["id"], 0)

	self.unlock(object["id"])
	return object["id"]

# Load the object from the Object Store using the given id.
func restore(id: String) -> Dictionary:
	var base: String = self.dir(id)

	if not DirAccess.dir_exists_absolute(base):
		return {}

	var path: String = self.metadata_path(id)
	if not FileAccess.file_exists(path):
		return {}

	var file: FileAccess = FileAccess.open(path, FileAccess.READ)
	var json = JSON.new()
	var parse_result = json.parse(file.get_line())

	if not parse_result == OK:
		return {}

	return json.get_data()

# Returns the number of references of this object.
func refcount(id: String) -> int:
	var base_path: String = self.dir(id)

	if not DirAccess.dir_exists_absolute(base_path):
		return -1

	var path: String = self.refcount_path(id)
	if not FileAccess.file_exists(path):
		return -1

	var file: FileAccess = FileAccess.open(path, FileAccess.READ)
	var json = JSON.new()
	var json_string = file.get_line()
	var parse_result = json.parse(json_string)

	if not parse_result == OK:
		print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
		return -1

	var result = json.get_data()
	return int(result["count"])

# Set the reference count.
#
# Ideally, we would set this atomically by locking it somehow. This only really
# matters if we have multiple instances running.
func set_refcount(id: String, count: int):
	var base_path: String = self.dir(id)

	if not DirAccess.dir_exists_absolute(base_path):
		return

	var path: String = self.refcount_path(id)
	var file: FileAccess = FileAccess.open(path, FileAccess.WRITE)
	file.store_line(JSON.stringify({ "count": int(count) }))

# Lock any write access to the given object. Blocks. Returns only when it is
# allowed. The return value is the rough amount of time it waited.
func lock(id: String) -> int:
	# We do this by deleting a lock file. Only if the file is properly
	# deleted will this continue
	var base_path: String = self.dir(id)
	if not DirAccess.dir_exists_absolute(base_path):
		return 0

	var path: String = self.lockfile_path(id)
	var waiting = 0
	while (DirAccess.remove_absolute(path) != OK):
		OS.delay_msec(250)
		waiting += 250

	return waiting

func unlock(id: String):
	# We 'unlock' by touching a file, ensuring it exists.
	var base_path: String = self.dir(id)
	if not DirAccess.dir_exists_absolute(base_path):
		return

	var path: String = self.lockfile_path(id)
	var file: FileAccess = FileAccess.open(path, FileAccess.WRITE)
	file.store_line("unlocked")

# Remove a reference to an object in the object store
func unrefer(id: String):
	self.lock(id)
	var count: int = self.refcount(id) - 1
	self.set_refcount(id, count)

	# Delete the object if there are no references
	if count <= 0:
		self.delete(id, false)

	self.unlock(id)

# Add a reference to an object in the object store
func refer(id: String):
	self.lock(id)
	self.set_refcount(id, self.refcount(id) + 1)
	self.unlock(id)

# Delete the object
func delete(id: String, force: bool = false):
	if self.refcount(id) > 0:
		if not force:
			return false
		print("WARNING: force deleting referenced object")

	# Get the directory that the object data resides within
	var base: String = self.dir(id)

	# Get a directory listing
	var dir_iter: DirAccess = DirAccess.open(base)

	# Ignore unknown objects
	if not dir_iter:
		return false

	# For each file in the object path, remove it
	dir_iter.list_dir_begin()
	var filename: String = dir_iter.get_next()
	while filename != "":
		if not dir_iter.current_is_dir():
			DirAccess.remove_absolute(base + "/" + filename)

		filename = dir_iter.get_next()

	# Then, remove the directory for the object
	DirAccess.remove_absolute(base)

	# Just attempt to remove the parent paths
	base = base.get_base_dir()
	DirAccess.remove_absolute(base)
	base = base.get_base_dir()
	DirAccess.remove_absolute(base)
	return true

# Update the object to have it be owned by a new parent.
#
# This will not update its reference count.
func reparent(id: String, parent: String) -> Dictionary:
	var object: Dictionary = self.restore(id)

	# If the object did not exist, bail
	if not object.has('id'):
		return object

	object["parent"] = parent
	self.store(object)

	return object

# Get the known profiles metadata.
func profiles() -> Dictionary:
	var base: String = self.profiles_dir()

	if not DirAccess.dir_exists_absolute(base):
		return {}

	var path: String = self.profiles_metadata_path()
	if not FileAccess.file_exists(path):
		return {}

	var file: FileAccess = FileAccess.open(path, FileAccess.READ)
	var json = JSON.new()
	var parse_result = json.parse(file.get_line())

	if not parse_result == OK:
		return {}

	return json.get_data()

# Updates the given profile and return its id.
func update_profiles(profile: Dictionary) -> String:
	# We generate an 'id' for the profile, if it has none.
	# We do this by storing the profile object into the object store.
	var id = self.store(profile)

	# Now, update our list of profiles
	self.lock_profiles()
	var profiles_data = self.profiles()
	profiles_data[id] = profile.get("name", "Unknown")

	var base: String = self.profiles_dir()
	DirAccess.make_dir_recursive_absolute(base)
	var path: String = self.profiles_metadata_path()
	var file: FileAccess = FileAccess.open(path, FileAccess.WRITE)
	file.store_line(JSON.stringify(profiles_data))

	self.unlock_profiles()

	return id

func lock_profiles():
	# We do this by deleting a lock file. Only if the file is properly
	# deleted will this continue
	var base: String = self.profiles_dir()
	if not DirAccess.dir_exists_absolute(base):
		return 0

	var path: String = self.profiles_lockfile_path()
	var waiting = 0
	while (DirAccess.remove_absolute(path) != OK):
		OS.delay_msec(250)
		waiting += 250

	return waiting

func unlock_profiles():
	# We 'unlock' by touching a file, ensuring it exists.
	var base: String = self.profiles_dir()
	if not DirAccess.dir_exists_absolute(base):
		return

	var path: String = self.profiles_lockfile_path()
	var file: FileAccess = FileAccess.open(path, FileAccess.WRITE)
	file.store_line("unlocked")
