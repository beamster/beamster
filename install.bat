@echo off
REM This will setup the dev-environment for the project

git submodule init
git submodule update

mkdir addons\gut
xcopy Integrations\Gut\addons\gut addons\gut /E/H/Y

mkdir addons\uuid
xcopy Integrations\godot-uuid\addons\uuid addons\uuid /E/H/Y
