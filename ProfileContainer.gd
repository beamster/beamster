extends VBoxContainer


var _add_button: Button = null
var _profile_panel: Panel = null
var _visible_panels: int = 0


signal open_profile(profile: Profile)


# Called when the node enters the scene tree for the first time.
func _ready():
	self.get_parent().set_tab_title(self.get_index(), "Profiles")

	# Get the panel template
	self._profile_panel = self.get_node("PanelContainer/ScrollContainer/MarginContainer/PanelContainer/PanelContainer/ProfileOpenPanel")
	self._profile_panel.visible = false

	# Hook up the buttons
	self._add_button = self.get_node("PanelContainer/ScrollContainer/MarginContainer/PanelContainer/PanelContainer/ProfileAddPanel/Button")

	self._add_button.pressed.connect(func():
		self.add_profile(Profile.new({"name": "My Profile"}))
	)

	# Load profiles
	var profiles = [
		#"MeadowFauna",
		#"My Test Profile",
	]

	for profile in profiles:
		self.add_profile(Profile.new({"name": profile}))

func add_profile(profile: Profile):
	# Duplicate button
	var profile_name = profile.get_name()
	var panel = self._profile_panel.duplicate()
	panel.set_meta("class", profile)
	var panel_button: Button = panel.get_node("Button")
	panel_button.set_text(profile_name)
	self._profile_panel.get_parent().add_child(panel)
	panel.visible = true
	self._visible_panels += 1

	panel_button.pressed.connect(self.on_profile_panel_pressed.bind(panel))

func on_profile_panel_pressed(panel: Panel):
	# Open it
	self.open_profile.emit(panel.get_meta("class"))
