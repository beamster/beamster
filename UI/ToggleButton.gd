# This makes the simple change to a normal Button that, when it is toggable, it
# will show a different icon for its "on" state.

class_name ToggleButton
extends Button


@export var toggled_icon: Texture2D

var _old_icon: Texture2D


func _set_icon(is_button_pressed: bool):
	if is_button_pressed:
		self._old_icon = self.icon
		self.icon = self.toggled_icon
	else:
		if self._old_icon:
			self.icon = self._old_icon

func _ready():
	self.toggled.connect(self._set_icon)

func toggle(value: bool):
	self._set_icon(value)
	self.set_pressed_no_signal(value)
