# This component contains all of the sources or elements that can be added to
# scenes or worlds.

extends PanelContainer


# Keeps track of the Source classes we can create
var _sources: Array = []

# Keeps track of the source classes by lookup from their types
var _sourcesByType: Dictionary = {}

# Keep track of the Element classes we can create
var _elements: Array = []


# Called when the node enters the scene tree for the first time.
func _ready():
	# Get references to the source/element managers
	var source_manager: SourceManager = $/root/Studio.get_profile_manager().get_scene_manager().get_source_manager()
	var element_manager: ElementManager = $/root/Studio.get_profile_manager().get_world_manager().get_element_manager()

	# Enforce the icon sizes for the toolboxes
	for toolbox in self.get_children():
		toolbox.fixed_icon_size.x = 32
		toolbox.fixed_icon_size.y = 32

	# Attach callback for when an element item is clicked
	self.get_element_toolbox().item_clicked.connect(self.element_activated)

	# Populate the toolbox with sources
	for source_name in source_manager.get_source_names():
		var source = source_manager.get_source_by_name(source_name)
		self.add_source(source_name, source)

	# Populate the toolbox with elements
	for element_name in element_manager.get_element_names():
		var element = element_manager.get_element_by_name(element_name)
		self.add_element(element)

func get_source_toolbox() -> ItemList:
	return self.get_child(0)

func get_element_toolbox() -> ItemList:
	return self.get_child(1)

func add_source(source_type: String, source_class: Script):
	var texture_manager: TextureManager = $/root/Studio.get_texture_manager()
	var icon_texture: Texture2D = texture_manager.icon_texture_32_for(source_class.name())
	var index: int = self.get_source_toolbox().add_icon_item(icon_texture)
	self.get_source_toolbox().set_item_tooltip(index, source_class.name())
	source_class.set_meta("source_type", source_type)
	self._sourcesByType[source_type] = source_class
	self._sources.append(source_class)

func add_element(element_class):
	#self.add_icon_texture_for(element_class)
	#var icon_texture: Texture2D = self.icon_texture_32_for(element_class.name())
	#var index: int = self.get_element_toolbox().add_icon_item(icon_texture)
	#self.get_element_toolbox().set_item_tooltip(index, element_class.name())
	self._elements.append(element_class)

func get_source_from_type(source_type: String):
	return self._sourcesByType[source_type]

func show_source_toolbox():
	self.get_source_toolbox().visible = true
	self.get_element_toolbox().visible = false

func show_element_toolbox():
	self.get_source_toolbox().visible = false
	self.get_element_toolbox().visible = true

func show_no_toolbox():
	self.get_source_toolbox().visible = false
	self.get_element_toolbox().visible = false

func element_activated(_index: int, _at_position: Vector2, mouse_button_index: int):
	# Add the element to the current world
	if mouse_button_index == MOUSE_BUTTON_LEFT:
		print(mouse_button_index)
