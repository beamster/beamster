class_name EffectsTree
extends Tree


var _shader_roots: Dictionary = {}
var _effect_root: TreeItem
var _shader_texture: Texture2D
var _sky_texture: Texture2D


# When an effect is activated (chosen to be added)
signal effect_activated(effect: Effect)

# When an effect is opened (chosen to be edited)
signal effect_opened(effect: Effect)


# Called when the node enters the scene tree for the first time.
func _ready():
	var context_menu = TreeContextMenu.new(
		[
			{
				"name": "Open in Shader Editor",
				"call": self.open_effect,
			},
		]
	)
	self.add_child(context_menu)

	var filter_agent = TreeFilterAgent.new()
	self.add_child(filter_agent)

	# Create visual shaders root
	var root: TreeItem = self.create_item()
	self.hide_root = true

	var shader_types = ["Filters", "Textures", "Borders", "Skies", "Transitions"]
	for shader_type in shader_types:
		var shader_root = root.create_child(0)
		shader_root.set_text(0, shader_type)
		shader_root.visible = false

		self._shader_roots[shader_type] = shader_root

	self._effect_root = root.create_child(1)
	self._effect_root.set_text(0, "Modules")
	self._effect_root.visible = false

	# Capture activation
	self.item_activated.connect(self.on_item_activated)

	# Load shader icons (TODO: use the texture manager)
	self._shader_texture = load("res://Assets/Images/Shader.png")
	self._sky_texture = load("res://Assets/Images/Sky.png")

# Tell the editor at-large to open the given Effect.
func open_effect(item: TreeItem):
	var thing: SerializedObject = item.get_meta("class")
	if thing is Effect:
		var effect: Effect = thing
		self.effect_opened.emit(effect)

# When an item is activated (enter / double-click)
func on_item_activated():
	# Get the selected item in the effects tree
	var tree_item: TreeItem = self.get_selected()

	# Collapse categories
	if tree_item.get_child_count() > 0:
		# Allow "double click" and such expand/collapse a group
		tree_item.collapsed = not tree_item.collapsed
		return

	# Get the object
	var thing: SerializedObject = tree_item.get_meta("class")
	if thing is Effect:
		var effect: Effect = thing
		self.effect_activated.emit(effect)

# Add the given item to our listing
func add_object(thing: SerializedObject):
	if thing is Effect:
		var effect: Effect = thing
		var shader_type: String = effect.category
		var shader_name: String = effect.get_human_name()
		var shader_path: String = effect.get_path()

		if not self._shader_roots.has(shader_type):
			print("WARNING: unknown shader type found: ", shader_type)
			return

		var shader_item: TreeItem = self._shader_roots[shader_type].create_child()
		shader_item.set_text(0, shader_name)
		shader_item.set_meta("path", shader_path)
		shader_item.set_meta("type", shader_type)
		shader_item.set_meta("class", thing)

		if shader_type == "Filters":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Textures":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Borders":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Skies":
			shader_item.set_icon(0, self._sky_texture)
		shader_item.set_icon_max_width(0, 16)

# Filter out the list for things relavent for the given SerializedObject.
func filter_for(object: SerializedObject):
	# What kind of effects can we add?
	for shader_type in self._shader_roots:
		self._shader_roots[shader_type].visible = false
		self._shader_roots[shader_type].collapsed = true
	self._effect_root.visible = false

	var parent: SerializedObject = null
	var base: SerializedObject = object
	if object is SerializedObject.SerializedInstance:
		base = object.get_base()
		parent = object.get_owner()

	# If either the current selection is a TransformableSource or the parent
	# is one, we can show Visual Effects (filters/textures/etc)
	if object is TransformableSource.TransformableSourceInstance or \
	   parent is TransformableSource:
		# Visual effects allowed... shaders, etc
		self._shader_roots["Filters"].visible = true
		self._shader_roots["Textures"].visible = true
		self._shader_roots["Borders"].visible = true
	elif base is SkyElement:
		# Skies are possible, obviously
		self._shader_roots["Skies"].visible = true
