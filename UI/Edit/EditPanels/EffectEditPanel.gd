# This provides a means of creating and testing Effect objects, such as visual
# shaders.

extends EditPanel


var _code_edit_template: CodeEdit = null
var _highlighter: SyntaxHighlighter = null

static func get_associated_types() -> Array[GDScript]:
	return [ShaderTransition, Effect]

func _ready():
	# Load the panel scene
	var scene = load("res://UI/Edit/EditPanels/EffectEditPanel.tscn")
	var instance = scene.instantiate()

	# Get the code editor
	self._code_edit_template = instance.get_node("ShaderPane/Panel/TabContainer/ShaderCodeEdit")

	# Set up the code editor
	self._code_edit_template.syntax_highlighter = self.get_highlighter()

	# Add it to the scene
	self.add_child(instance)

func get_highlighter() -> SyntaxHighlighter:
	if self._highlighter == null:
		var highlighter: CodeHighlighter = CodeHighlighter.new()

		var special_color: Color = Color("8fffdb")
		var keyword_color: Color = Color("ff7085")
		var string_color: Color = Color("ffeda1")
		var control_color: Color = Color("ff8ccc")
		var comment_color: Color = Color("cdcfd280")
		highlighter.symbol_color = Color("abc9ff")
		highlighter.function_color = Color("57b3ff")
		highlighter.number_color = Color("a1ffe0")
		highlighter.member_variable_color = Color("bce0ff")

		highlighter.add_color_region('"', '"', string_color)
		highlighter.add_color_region('/*', '*/', comment_color)
		highlighter.add_color_region('//', '', comment_color, true)
		highlighter.add_color_region('#', '', string_color, true)

		var keywords = ["shader_type", "void", "int", "float", "bool",
						"bvec2", "bvec3", "bvec4", "ivec2", "ivec3", "ivec4",
						"uint", "uvec2", "uvec3", "uvec4", "vec2", "vec3", "vec4",
						"mat2", "mat3", "mat4", "lowp",
						"sampler2D", "isampler2D", "usampler2D",
						"sampler2DArray", "isampler2DArray", "usampler2DArray",
						"sampler3D", "isampler3D", "usampler3D", "samplerCube",
						"const", "uniform", "true", "false", "in", "out", "inout",
						"varying", "global", "instance"]
		var control_words = ["if", "else", "for", "while", "switch", "case",
								"break", "do", "default", "return"]
		var special_words = ["VERTEX", "COLOR", "UV", "MODULATE",
								"POINT_SIZE", "TEXTURE_PIXEL_SIZE", "TEXTURE",
								"AT_LIGHT_PASS", "INSTANCE_CUSTOM",
								"PROJECTION_MATRIX", "EXTRA_MATRIX", "WORLD_MATRIX",
								"FRAGCOORD", "NORMAL", "SCREEN_UV", "POINT_COORD",
								"LIGHT_VEC", "SHADOW_VEC", "LIGHT_HEIGHT",
								"LIGHT_COLOR", "LIGHT_UV", "SHADOW_COLOR", "LIGHT"]

		for control_word in control_words:
			highlighter.add_keyword_color(control_word, control_color)

		for keyword in keywords:
			highlighter.add_keyword_color(keyword, keyword_color)

		for special_word in special_words:
			highlighter.add_keyword_color(special_word, special_color)

		self._highlighter = highlighter

	return self._highlighter

func close_preview():
	var vbox: VBoxContainer = self.get_child(0).get_node("ShaderPane/VBoxContainer")
	vbox.visible = false

func open_preview():
	var vbox: VBoxContainer = self.get_child(0).get_node("ShaderPane/VBoxContainer")
	vbox.visible = true

func get_shader_preview() -> SubViewportContainer:
	return self.get_child(0).get_node("ShaderPane/VBoxContainer/ShaderPreview")

func get_buffer_shader_preview() -> SubViewportContainer:
	return self.get_child(0).get_node("ShaderPane/VBoxContainer/BufferShaderPreview")

func open(thing: SerializedObject, _editor: EditorManager) -> bool:
	# We might be editing a transition
	if thing is ShaderTransition:
		thing = (thing as ShaderTransition).get_effect().get_base()

		# For transition editing, we assume we have a scene preview open
		self.close_preview()
	else:
		self.open_preview()

	var effect: Effect = thing

	var tabs: TabContainer = self.get_child(0).get_node("ShaderPane/Panel/TabContainer")

	# Close any other existing tabs
	for child in tabs.get_children():
		tabs.remove_child(child)

	# Add the main tab
	var main_code_edit: CodeEdit = self._code_edit_template.duplicate()
	tabs.add_child(main_code_edit)
	tabs.set_tab_title(0, effect.get_name())
	main_code_edit.text = effect.get_shader().code
	main_code_edit.text_changed.connect(self.on_code_changed.bind(main_code_edit))

	# Create a tab for each buffer described in the main shader (including noise)
	for buffer in effect.get_buffers():
		# Create a tab for it
		var code_edit: CodeEdit = self._code_edit_template.duplicate()
		tabs.add_child(code_edit)
		if buffer is Effect:
			var sub_effect: Effect = buffer
			tabs.set_tab_title(tabs.get_child_count() - 1, sub_effect.get_name())
			code_edit.text = sub_effect.get_shader().code
			code_edit.text_changed.connect(self.on_code_changed.bind(code_edit))
		elif buffer.has_meta("name"):
			var buffer_name: String = buffer.get_meta("name")
			tabs.set_tab_title(tabs.get_child_count() - 1, buffer_name)

	self.on_code_changed(main_code_edit)
	return true

func on_code_changed(code_edit: CodeEdit):
	# Recompile shader for the preview
	var s = Shader.new()
	s.code = code_edit.text
	var m = ShaderMaterial.new()
	m.shader = s
	self.get_shader_preview().set_material(m)
