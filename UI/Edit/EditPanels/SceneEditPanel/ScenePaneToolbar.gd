extends Panel


# Called when the node enters the scene tree for the first time.
func _ready():
	$GridToggleButton.toggle($%Preview.get_grid())
	$GridToggleButton.toggled.connect(self.on_grid_toggle)
	$GridOverlayToggleButton.toggle($%Preview.get_overlay_grid())
	$GridOverlayToggleButton.toggled.connect(self.on_grid_overlay_toggle)

	$SnapToggleButton.toggle($%Preview.get_snap())
	$SnapToggleButton.toggled.connect(self.on_snap_toggle)

	$ZoomResetButton.pressed.connect(self.on_zoom_reset_pressed)

func on_grid_toggle(button_pressed: bool):
	$%Preview.set_grid(button_pressed)

func on_grid_overlay_toggle(button_pressed: bool):
	$%Preview.set_overlay_grid(button_pressed)

func on_snap_toggle(button_pressed: bool):
	$%Preview.set_snap(button_pressed)

func on_zoom_reset_pressed():
	$%Preview.zoom_reset()
