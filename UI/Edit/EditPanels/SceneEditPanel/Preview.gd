extends SubViewportContainer


# Speed of zoom movement
const ZOOM_FREELOOK_MIN = 0.01
const ZOOM_FREELOOK_MULTIPLIER = 1.08
const ZOOM_FREELOOK_MAX = 10000.0

# References to other UI elements
var _editor_manager: EditorManager = null
var _scene: Scene = null

# The background rectangle (black, typically) behind all sources.
var _background_rect: ColorRect = null

# The rendered grid behind sources.
var _grid_rect: ColorRect = null

# Grid properties.
var _grid_major: float = 120.0
var _grid_minor: float = 30.0

# Whether or not we snap to the grid.
var _snap: bool = true
var _start: Vector2
var _creating: bool
var _last: Vector2
var _dummy: Node2D
var _original_position: Vector2
var _original_size: Vector2
var _panning: bool = false
var _resizing: bool = false
var _control_point: Polygon2D
var _hovered: Node2D
var _selected: Node2D
var _selected_source: Source.SourceInstance
var _last_new_size: Vector2
var _over: Node2D
var _zoom: float = 1.0
var _pan: Vector2 = Vector2(0.0, 0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	print('loaded preview ', self)
	# Set the texture to the one being showed by the presentation window
	if self._scene:
		$%PreviewViewport/SceneViewportContainer/SceneViewport.set_world_2d(self._scene.get_viewport().get_world_2d())

	self.get_parent_control().resized.connect(self.on_resize)

	self._background_rect = self.get_parent().get_node("Background")
	self._grid_rect = self.get_parent().get_node("Grid")

func set_scene(scene: Scene):
	self._scene = scene
	print('setting scene ', scene, ' (', self, ')')
	if self._scene:
		$%PreviewViewport/SceneViewportContainer/SceneViewport.set_world_2d(self._scene.get_viewport().get_world_2d())

		# Add gizmos to canvas layer (and hide them)
		var canvasLayer: Control = $%CanvasLayer

		# Remove existing gizmos from the current layer, if any
		for child in canvasLayer.get_children():
			canvasLayer.remove_child(child)

		# Add all gizmos to the CanvasLayer of our preview panel
		for source in self._scene.get_source_list().get_node("SourceTree").get_sources():
			if source is TransformableSource.TransformableSourceInstance:
				for gizmo in source.get_gizmos(canvasLayer.get_instance_id()):
					canvasLayer.add_child(gizmo)

		# Listen to new sources
		scene.source_added.connect(func(source: Source.SourceInstance):
			if source is TransformableSource.TransformableSourceInstance:
				for gizmo in source.get_gizmos(canvasLayer.get_instance_id()):
					canvasLayer.add_child(gizmo)
		)

		# Listen to removed sources
		scene.source_removed.connect(func(source: Source.SourceInstance):
			source.remove_gizmos(canvasLayer.get_instance_id())
		)

func set_editor(editor: EditorManager):
	self._editor_manager = editor

func on_resize():
	# Resize ourself to fit, initially
	# TODO: handle width being too small
	var container_size = self.get_parent_control().size
	var viewport_size: Vector2 = _scene.get_viewport().size
	self.size = Vector2(
		container_size.y / viewport_size.y * viewport_size.x,
		container_size.y,
	)
	if self.size.x > container_size.x:
		self.size = Vector2(
			container_size.x,
			container_size.x / viewport_size.x * viewport_size.y,
		)
	var canvasLayer: Control = $%CanvasLayer
	var previewViewport: SubViewport = $%PreviewViewport
	viewport_size = previewViewport.size_2d_override
	var ratio = self.size / viewport_size
	self._grid_rect.material.set_shader_parameter("zoom", self.scale.x * min(ratio.x + 0.35, 1.0))
	self.material.set_shader_parameter("zoom", self.scale.x * min(ratio.x + 0.35, 1.0))
	canvasLayer.size = viewport_size
	canvasLayer.scale = ratio
	self.pan(self._pan)

func intersects_point(point, origin: Node = null):
	if origin == null:
		origin = self

	var left = origin.get_transform().get_origin().x
	var top = origin.get_transform().get_origin().y
	var right = left + origin.get_size().x
	var bottom = top + origin.get_size().y

	if left <= point.x and right > point.x:
		if top <= point.y and bottom > point.y:
			return true

	return false

func _process(delta: float):
	# Perform a smooth zoom
	var zoom_inertia = 0.05 # TODO: setting
	var old_scale: float = self.scale.x
	self.scale.x = lerp(
		old_scale,
		self._zoom,
		min(1.0, delta * (1.0 / zoom_inertia))
	)
	self.scale.x = max(self.scale.x, 0.1)

	# If we have scaled, also re-pan since we pan based on the center
	# This has the effect of scaling from the center
	if old_scale != self.scale.x:
		self.scale.y = self.scale.x
		var canvasLayer: Control = $%CanvasLayer
		var previewViewport: SubViewport = $%PreviewViewport
		var viewport_size: Vector2 = previewViewport.size_2d_override
		var ratio = self.size / viewport_size
		self._grid_rect.material.set_shader_parameter("zoom", self.scale.x * min(ratio.x + 0.35, 1.0))
		self.material.set_shader_parameter("zoom", self.scale.x * min(ratio.x + 0.35, 1.0))
		canvasLayer.size = viewport_size
		canvasLayer.scale = ratio
		self.pan(self._pan)

func zoom_reset():
	self._zoom = 1.0
	self.pan(Vector2(0, 0))

func pan(pan_amount: Vector2):
	self._pan = pan_amount
	var container_size = self.get_parent_control().size
	self.position = Vector2(
		self._pan.x + (container_size.x - self.size.x * self.scale.x) / 2,
		self._pan.y + (container_size.y - self.size.y * self.scale.y) / 2
	)
	var canvasLayer: Control = $%CanvasLayer
	canvasLayer.position = Vector2(0, 0)

	# Update positions of the background and grid rects behind the preview
	self._background_rect.position = self.position
	self._background_rect.size = self.size
	self._background_rect.scale = self.scale
	self._grid_rect.position = self.position
	self._grid_rect.size = self.size
	self._grid_rect.scale = self.scale

func get_grid() -> bool:
	return self._grid_rect.visible

func set_grid(value: bool):
	self._grid_rect.visible = value

func get_overlay_grid() -> bool:
	if not self.material:
		return false
	return self.material.get_shader_parameter("show")

func set_overlay_grid(value: bool):
	if not self.material:
		return
	self.material.set_shader_parameter("show", value)

func get_snap() -> bool:
	return self._snap

func set_snap(value: bool):
	self._snap = value

func scale_cursor_distance(zoom_factor: float):
	var min_distance = ZOOM_FREELOOK_MIN
	var max_distance = ZOOM_FREELOOK_MAX

	self._zoom = clamp(self._zoom * zoom_factor, min_distance, max_distance)

# This handles input seen by the underlying panel container
func input(event):
	# Ignore if the preview pane is invisible
	# Either itself being invisible or the edit tab entirely.
	if not $%ScenePane.visible or not $/root/Studio/AppTabContainer/EditContainer.visible:
		return

	# Go through the children and determine which we are over
	var workspace: PanelContainer = $%ScenePane/PanelContainer
	var canvasLayer: Control = $%CanvasLayer
	var previewViewport: SubViewport = $%PreviewViewport

	# Get the mouse coordinate in terms of presentation coordinates
	var mouse_position: Vector2 = previewViewport.get_mouse_position()
	var hovered: Node2D = null

	# Handle moving/resizing drag events
	if self._panning:
		if event is InputEventMouseMotion:
			# Get new position
			var delta: Vector2 = workspace.get_local_mouse_position() - self._start
			self._pan = self._original_position + delta
			self.pan(self._pan)
		elif event is InputEventMouseButton:
			if not event.is_pressed():
				# Finish
				self._panning = false
	elif self._resizing and not self._hovered.get_meta("locked"):
		if event is InputEventMouseMotion:
			# Get new position
			var delta: Vector2 = mouse_position - self._start

			var position_scale = Vector2(
				self._control_point.get_meta("position_scale_x", 0),
				self._control_point.get_meta("position_scale_y", 0)
			)

			var size_scale = Vector2(
				self._control_point.get_meta("size_scale_x", 0),
				self._control_point.get_meta("size_scale_y", 0)
			)

			var new_size: Vector2 = self._original_size - (delta * size_scale)

			if self._snap:
				new_size = snapped(new_size, Vector2(self._grid_minor, self._grid_minor))

			if self._last_new_size != new_size:
				self._hovered.resize(new_size)
			self._last_new_size = new_size

			var applied: Vector2 = self._original_position + (delta * position_scale)
			if self._snap:
				applied = snapped(applied, Vector2(self._grid_minor, self._grid_minor))
			self._hovered.set_transform(Transform2D(0, applied))
		elif event is InputEventMouseButton:
			if not event.is_pressed():
				# Finish
				self._resizing = false
				self._control_point = null
				self._hovered = null
		return

	# Handle zoom / pan events
	# (Only if over the ScenePane)
	if self.intersects_point(workspace.get_local_mouse_position(), workspace):
		if event is InputEventMouseButton:
			var zoom_factor: float = 1 + (ZOOM_FREELOOK_MULTIPLIER - 1) * event.factor
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				self.scale_cursor_distance(zoom_factor)
			elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				self.scale_cursor_distance(1.0 / zoom_factor)
			elif event.is_pressed() and event.button_index == MOUSE_BUTTON_MIDDLE:
				self._panning = true
				self._start = workspace.get_local_mouse_position()
				self._original_position = self._pan

	# Only if event position is over the control
	if event is InputEventMouse and not self.intersects_point(workspace.get_local_mouse_position(), workspace):
		return

	# Handle creating new sources
	if self._editor_manager.get_source():
		# Get an instance of the Source class
		var source_class = self._editor_manager.get_source()

		# Ignore if the selected source is not transformable (has no gizmo)
		if source_class.gizmos().size() > 0:
			# Wait until a click and drag to create the thing
			if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
				if event.is_pressed():
					self._start = mouse_position
					self._creating = true
					self._dummy = source_class.gizmos()[0].new()
					self._dummy.visible = true
					canvasLayer.add_child(self._dummy)

					if self._selected:
						self._selected.visible = false
				else:
					# Create the item
					var source: Source = source_class.new()

					# Call on_init() with appropriate options
					source.on_init({})

					var instance: Source.SourceInstance = source.instantiate()

					# Add to presentation
					if self._scene:
						print('adding to scene ', self._scene, ' (', self, ')')
						self._scene.add_child(instance)

					# Move it into place (if it is transformable)
					if self._dummy and instance is TransformableSource.TransformableSourceInstance:
						var gizmos: Array[SourceGizmo] = instance.get_gizmos(canvasLayer.get_instance_id())
						for gizmo in gizmos:
							if gizmo is TransformableSourceGizmo:
								gizmo.resize(self._dummy.get_size())
								gizmo.move(self._dummy.get_position())

					# Destroy dummy
					canvasLayer.remove_child(self._dummy)
					self._dummy = null

					# Unselect toolbox
					self._creating = false
					self._editor_manager.deselect_all()

					# Show positioning node (and hide current selected)
					if self._over:
						self._over.visible = false

					if self._selected:
						self._selected.visible = false

					self._over = null
					if source is TransformableSource:
						self._over = instance.get_positioning_nodes()[0]

					self._selected = self._over
					self._selected.visible = true

					# Call init on source
					source.on_add(null)

				self.get_parent().mouse_default_cursor_shape = Control.CURSOR_CROSS
				canvasLayer.mouse_default_cursor_shape = Control.CURSOR_CROSS
			elif event is InputEventMouseMotion:
				# Do not handle the motion event twice
				if self._last == event.position:
					return
				self._last = event.position

				self.get_parent().mouse_default_cursor_shape = Control.CURSOR_CROSS
				canvasLayer.mouse_default_cursor_shape = Control.CURSOR_CROSS

				if self._creating:
					# Ensure there is a dummy positioning node at "_start" to new position
					var source_position = Vector2(
						min(self._start.x, mouse_position.x),
						min(self._start.y, mouse_position.y)
					)

					var source_size = Vector2(
						abs(self._start.x - mouse_position.x),
						abs(self._start.y - mouse_position.y)
					)

					if self._snap:
						source_position = snapped(source_position, Vector2(self._grid_minor, self._grid_minor))
						source_size = snapped(source_size, Vector2(self._grid_minor, self._grid_minor))

					self._dummy.move(source_position)
					self._dummy.resize(source_size)
			return

	# Go through each gizmo to find any we are hovering over
	hovered = self._hovered

	if not hovered:
		for child in canvasLayer.get_children():
			# When the mouse is moved...
			if event is InputEventMouseMotion or event is InputEventMouseButton:
				# If something is intersected, determine what is hovered
				if child.is_hovered():
					hovered = child

	# Show positioning node (and hide current selected)
	if hovered != self._over:
		if self._over and self._over != self._selected:
			self._over.visible = false
		self._over = hovered

	# Now, perform any action on the foreground child
	if hovered:
		# Make the gizmo visible
		hovered.visible = true

		# Pass along settings to the gizmo
		hovered.snap = self.get_snap()
		hovered.snap_major = self._grid_major
		hovered.snap_minor = self._grid_minor

		# Allow the hovered gizmo to process the input event itself
		hovered.input(event, mouse_position)

		# Allow the gizmo to set the cursor
		self.get_parent().mouse_default_cursor_shape = hovered.cursor
		canvasLayer.mouse_default_cursor_shape = hovered.cursor

		# Capture further events if requested
		if hovered.capture_input:
			self._hovered = hovered
		else:
			# The gizmo is done capturing input
			self._hovered = null
	else:
		# We are clicking on the blank space around the sources
		self.get_parent().mouse_default_cursor_shape = Control.CURSOR_ARROW
		canvasLayer.mouse_default_cursor_shape = Control.CURSOR_ARROW

		if event is InputEventMouseButton:
			# Click / Release on the background
			self.deselect_all()

func deselect_all():
	if self._selected:
		var source: Source.SourceInstance = self._selected.get_source()
		var source_item: TreeItem = source.get_tree_items().front()
		if source_item:
			source_item.deselect(0)
			source_item.get_tree().on_nothing_selected()
		self._selected.visible = false
		self._selected = null
		self._selected_source = null

func select(source: Source.SourceInstance):
	# Deselect the last thing
	self.deselect_all()

	# Select the item in the sources list
	if self._selected_source == source:
		return

	self._selected_source = source
	var source_item: TreeItem = source.get_tree_items().front()
	source_item.select(0)

	# Highlight the gizmo
	if source is TransformableSource.TransformableSourceInstance:
		var transformable_source_instance: TransformableSource.TransformableSourceInstance = source
		var gizmos: Array[TransformableSourceGizmo] = transformable_source_instance.get_positioning_nodes()
		if gizmos.size() > 0:
			var gizmo: TransformableSourceGizmo = gizmos[0]
			gizmo.visible = true
			self._selected = gizmo

func preview_transition(transition: Transition.TransitionInstance, transition_manager: TransitionManager):
	transition_manager.perform(transition, self._scene, null, $%PreviewViewport/SceneViewportContainer)
