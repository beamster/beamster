# Edit Panels

Each implementation here represents the UI components and logic for implementing
the editor views for various objects in the system

All editors are implemented by inheriting `EditPanel` and are thus all based on
Godot's `PanelContainer` node. They will be placed inside a container themselves
which will push them to fill the provided space.
