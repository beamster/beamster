# This provides a visual editor for World objects.

extends EditPanel


static func get_associated_type() -> GDScript:
	return World
