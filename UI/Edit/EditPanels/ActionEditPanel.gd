# This provides a visual editor for Action objects.

extends EditPanel


static func get_associated_type() -> GDScript:
	return Action

func open(_thing: SerializedObject, editor: EditorManager) -> bool:
	# Load the panel scene
	var scene = load("res://UI/Edit/EditPanels/ActionEditPanel.tscn")
	var instance = scene.instantiate()

	var workspace: BlocklyWorkspace = instance.get_node("BlocklyWorkspace")

	# Get current scene and let the workspace know about it
	var scene_reference: Scene = editor.get_current_scene()
	workspace.set_meta("scene", scene_reference)

	# Add it to the scene
	self.add_child(instance)

	return true
