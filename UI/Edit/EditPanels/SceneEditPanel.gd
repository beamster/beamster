# This provides a way to edit scenes.

extends EditPanel


var _scene: Scene = null
var _editor: EditorManager = null
var _canvas_layer: Control = null
var _background_rect: ColorRect = null
var _grid_rect: ColorRect = null
var _preview: SubViewportContainer = null
var _source_list: VBoxContainer = null
var _action_list: Tree = null

static func get_associated_types() -> Array[GDScript]:
	return [Scene]

func _input(event):
	# Pass input into the panel viewport
	if self._preview:
		self._preview.input(event)

func _ready():
	var bg_rect: TextureRect = TextureRect.new()
	bg_rect.texture = load("res://Assets/Images/noise_black.png")
	bg_rect.texture_repeat = CanvasItem.TEXTURE_REPEAT_ENABLED
	bg_rect.stretch_mode = TextureRect.STRETCH_TILE
	var bg_resizer = func():
		bg_rect.size = self.size
	self.resized.connect(bg_resizer)

	# Load the panel scene
	var scene = load("res://UI/Edit/EditPanels/SceneEditPanel.tscn")
	var instance = scene.instantiate()

	# Get the preview component of the scene editor
	self._preview = instance.get_node("ScenePane/PanelContainer/PreviewPane/Preview")

	# Tell it about a few of the UI components it would care about
	self._preview.set_scene(self._scene)

	# Add it to the scene
	self.add_child(instance)
	instance.get_node("ScenePane/PanelContainer/PreviewPane").add_child(bg_rect)
	instance.get_node("ScenePane/PanelContainer/PreviewPane").move_child(bg_rect, 0)

	self._canvas_layer = self._preview.get_node("CanvasLayer")

	# Add the background
	self._background_rect = instance.get_node("ScenePane/PanelContainer/PreviewPane/Background")
	self._background_rect.color = Color.BLACK

	# Add a grid
	self._grid_rect = instance.get_node("ScenePane/PanelContainer/PreviewPane/Grid")
	self._grid_rect.size = Vector2(1920, 1080)

	# Attach a grid shader to that item
	var shader = Shader.new()
	shader.code = "
// 2D editor grid shader.

shader_type canvas_item;

uniform float zoom = 1.0;
uniform vec4 grid_major_color = vec4(0.2, 0.4, 0.9, 0.35);
uniform vec4 grid_minor_color = vec4(0.2, 0.2, 0.2, 0.35);
uniform bool show = true;
uniform bool mix_texture = true;

float grid_d(vec2 uv, vec2 gridSize, float gridLineWidth) {
	uv += gridLineWidth / 2.0;
	uv = mod(uv, gridSize);
	vec2 halfRemainingSpace = (gridSize - gridLineWidth) / 2.0;
	uv -= halfRemainingSpace + gridLineWidth;
	uv = abs(uv);
	uv = -(uv - halfRemainingSpace);
	return min(uv.x, uv.y);
}

float dtoa(float d) {
	const float amount = 3200.0;
	return clamp(1.0 / (clamp(d, 1.0 / amount, 1.0) * amount), 0.0, 1.0);
}

void fragment() {
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

	if (show) {
		// Minor grid lines (when zoomed in)
		float d = grid_d(UV, vec2(30.0 / 1920.0, 30.0 / 1080.0), 0.0001);
		float grid_minor_amount = grid_minor_color.a * min(zoom - 0.10, 1.0) * dtoa(d);
		color = vec4(mix(color.rgb, grid_minor_color.rgb, grid_minor_amount), 1.0);

		// Major grid lines (wider lines when zoomed out)
		d = grid_d(UV, vec2(120.0 / 1920.0, 120.0 / 1080.0), 0.001 * min((0.75 - min(zoom, 0.75)) * 20.0, 5.0));
		float grid_amount = grid_major_color.a * dtoa(d);
		vec4 grid_color = vec4(mix(color.rgb, grid_major_color.rgb, grid_amount), 1.);

		if (mix_texture) {
			COLOR.rgb = mix(texture(TEXTURE, UV).rgb + grid_color.rgb, grid_color.rgb, min(1.0, grid_amount + grid_minor_amount));
			COLOR.a = texture(TEXTURE, UV).a;
		}
		else {
			COLOR.rgb = grid_color.rgb;
			COLOR.a = 1.0;
		}
	}
	else {
		COLOR = texture(TEXTURE, UV);
	}
}"
	var grid_material = ShaderMaterial.new()
	grid_material.shader = shader

	# The grid behind the sources is not using the viewport texture
	grid_material.set_shader_parameter("mix_texture", false)
	self._grid_rect.material = grid_material

	grid_material = ShaderMaterial.new()
	grid_material.shader = shader

	# The grid in front of the sources is not on by default
	grid_material.set_shader_parameter("show", false)
	self._preview.material = grid_material

func set_grid_visible(value: bool):
	self._grid_rect.visible = value

func set_grid_zoom_level(value: float):
	self._grid_rect.material.set_shader_parameter("zoom", value)

func __process(delta: float):
	if self._scene:
		self._scene._process(delta)

func open(thing: SerializedObject, editor: EditorManager) -> bool:
	print('opening ', thing)
	self._scene = thing
	self._editor = editor
	self._preview.set_scene(self._scene)
	self._preview.set_editor(editor)
	self._source_list = self._scene.get_source_list()
	self._action_list = self._scene.get_action_list()

	# Ensure this source list is the visible one
	var item_container: TabContainer = editor.get_container().get_item_container()
	if self._source_list.get_parent():
		self._source_list.get_parent().remove_child(self._source_list)
	item_container.add_child(self._source_list)

	self._source_list.visible = false
	item_container.show_source_tree(self._source_list, self._action_list)

	return true

# Previews the given object / action if possible.
#
# Returns false if it is not understood.
func preview(thing: SerializedObject, __editor: EditorManager) -> bool:
	# We can preview Transitions
	var transition_manager: TransitionManager = self._editor.get_profile_manager().get_scene_manager().get_transition_manager()
	if thing is Transition.TransitionInstance:
		print('previewing ', thing)
		self._preview.preview_transition(thing, transition_manager)
		return true

	return false

func select(thing: SerializedObject, __editor: EditorManager) -> bool:
	if thing is Source.SourceInstance:
		self._preview.select(thing)
	else:
		self._preview.deselect_all()
	return true
