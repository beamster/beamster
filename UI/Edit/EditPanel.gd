# This is the base class for any edit panel that can be instantiated inside the
# editor view.

class_name EditPanel
extends PanelContainer


static func get_associated_types() -> Array[GDScript]:
	return []

# Opens the given type of object.
#
# Returns false if there was a problem opening the given thing.
func open(_thing: SerializedObject, _editor: EditorManager) -> bool:
	return false

# Previews the given object / action if possible.
#
# Returns false if it is not understood.
func preview(_thing: SerializedObject, _editor: EditorManager) -> bool:
	return false

# Selects the thing that may be inside the opened object.
func select(_thing: SerializedObject, _editor: EditorManager) -> bool:
	return false
