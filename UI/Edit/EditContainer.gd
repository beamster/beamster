# This manages the application's Edit view.
class_name EditContainer
extends VSplitContainer


# We remember the bottom panel height so we can always expand the top portion
# of the editor when the overall window is resized.
var _bottom_panel_height: float = 0.0


# Called when the node enters the scene tree for the first time.
func _ready():
	self.get_parent().set_tab_title(self.get_index(), "Edit")

	# When the application resizes, call the resize handler
	self.resized.connect(self._on_resize.bind())
	get_tree().get_root().size_changed.connect(self._on_app_resize.bind())

	# We want to keep track of when the bottom is resized and always keep it
	# that height when resizing the overall window.
	$BottomPanels.resized.connect(self._on_bottom_panels_resize.bind())

func get_item_container() -> TabContainer:
	return self.get_node("BottomPanels/HSplitContainer2/ItemContainer")

func get_create_container() -> TabContainer:
	return self.get_node("BottomPanels/HSplitContainer/CreateContainer")

# Retain the bottom panel's height when resizing such that it always expands
# the top portion of the editor.
func _on_resize():
	self.split_offset = int(self.size.y - self._bottom_panel_height)

func _on_app_resize():
	var total_size = get_tree().get_root().size
	if total_size.x > 0:
		$TopPanel.split_offset = total_size.x * 0.60

# Remember the bottom panel height whenever it resizes on its own.
func _on_bottom_panels_resize():
	self._bottom_panel_height = $BottomPanels.size.y
