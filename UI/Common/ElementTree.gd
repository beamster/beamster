extends Tree


var _shown_texture: Texture2D
var _hidden_texture: Texture2D
var _locked_texture: Texture2D
var _unlocked_texture: Texture2D
var _environment_root: TreeItem
var _shader_texture: Texture2D
var _sky_texture: Texture2D

# Called when the node enters the scene tree for the first time.
func _ready():
	#var parent: TabContainer = self.get_parent()
	#parent.set_tab_title(self.get_index(), "Elements")
	#parent.set_tab_hidden(self.get_index(), true)

	# Load the visibility icons
	self._shown_texture = load("res://Assets/Images/shown.png")
	self._hidden_texture = load("res://Assets/Images/hidden.png")
	self._locked_texture = load("res://Assets/Images/locked.png")
	self._unlocked_texture = load("res://Assets/Images/unlocked.png")
	self._shader_texture = load("res://Assets/Images/Shader.png")
	self._sky_texture = load("res://Assets/Images/Sky.png")

	# Capture button presses
	self.button_clicked.connect(self.on_button_clicked.bind())

	# Register a callback when an element is selected
	self.item_selected.connect(self.on_item_selected.bind())

func on_button_clicked(item: TreeItem, _column: int, id: int, _mouse_button_index: int):
	if id == 0:
		# Toggle visibility of source/shader/etc
		var element_type: String = item.get_meta("type")
		if element_type == "element":
			var element: Element = item.get_meta("reference")
			element.set_visible(!element.get_visible())

			if element.get_visible():
				item.set_button(0, 0, self._shown_texture)
			else:
				item.set_button(0, 0, self._hidden_texture)
		elif element_type == "material":
			pass
	elif id == 1:
		# Toggle position lock
		var element: Element = item.get_meta("reference")
		element.set_position_lock(!element.get_position_lock())

		if element.get_position_lock():
			item.set_button(0, 1, self._locked_texture)
		else:
			item.set_button(0, 1, self._unlocked_texture)

func on_item_selected():
	# Get the selected source tree item (or bail, if none)
	var treeItem: TreeItem = self.get_selected()
	if not treeItem:
		return

	#var PropertyTree: Tree = $/root/Studio/AppTabContainer/EditContainer/BottomPanels/HSplitContainer/ModifyContainer/PropertyTree

	# Clear properties from last selected source
	#PropertyTree.clearAll()

	#var EffectsTree: Tree = $/root/Studio/AppTabContainer/EditContainer/BottomPanels/HSplitContainer/CreateContainer/EffectsTree
	#var ActionsTree: Tree = $/root/Studio/AppTabContainer/EditContainer/BottomPanels/HSplitContainer/CreateContainer/ActionsTree

	# Get the source itself (referenced by the tree item)
	#if treeItem.has_meta("reference"):
	#	var object: SerializedObject = treeItem.get_meta("reference")
	#	if object:
	#		# Have the property tree populate the source properties
	#		var has_properties = PropertyTree.add_object_properties(object)
	#		if not has_properties:
	#			PropertyTree.hide_property_tree()
	#		EffectsTree.filter_for(object)
	#elif treeItem.has_meta("reference") and treeItem.get_meta("type") == "shader":
	#	var effect: Effect.EffectInstance = treeItem.get_meta("reference")
	#	if effect:
	#		PropertyTree.add_shader(treeItem.get_text(0), effect)
	#elif treeItem.has_meta("reference") and treeItem.get_meta("type") == "material":
	#	var reference_material: Material = treeItem.get_meta("reference")
	#	if reference_material:
	#		PropertyTree.add_material(treeItem.get_text(0), reference_material)

func on_world():
	pass

func clearAll():
	self._environment_root = null
	self.clear()

func add_element(element: Element):
	# We add the element to the root (creating if not existing)
	var root: TreeItem = self.get_root()
	if not root:
		root = self.create_item()

	# We do not show the root
	self.hide_root = true

	if element is EnvironmentalElement:
		if not self._environment_root:
			# We add to the environment tree in this case
			self._environment_root = self.create_item(root)
			self._environment_root.set_text(0, "Environment")
			self._environment_root.move_before(root.get_first_child())
		root = self._environment_root

	# We create the source item
	var element_item: TreeItem = self.create_item(root)

	# Backward-reference
	element.set_meta("element_item", element_item)

	# Set the initial name
	var element_name = element.get_script().name()
	element_item.set_text(0, element_name)

	# Set icon
	#var Toolbox = $/root/Studio/AppTabContainer/EditContainer/BottomPanels/HSplitContainer/CreateContainer/Toolbox
	#var icon_texture = Toolbox.icon_texture_16_for(name)
	#element_item.set_icon(0, icon_texture)
	element_item.set_icon_max_width(0, 16)

	element_item.add_button(0, self._shown_texture, 0, false, "Toggle Visibility")
	element_item.add_button(0, self._unlocked_texture, 1, false, "Toggle Position Lock")

	# Add a reference to the presentation source render object
	element_item.set_meta("reference", element)
	element_item.set_meta("type", "element")

	# Select it (this will also load its properties)
	#   See: self.on_item_selected() above
	element_item.select(0)

func add_shader(shader_name: String, shader_type: String, effect: Effect.EffectInstance) -> TreeItem:
	var selected_element: TreeItem = self.get_selected()
	if selected_element:
		if selected_element.get_meta("type") != "element":
			selected_element = selected_element.get_parent()
		var shader_item: TreeItem = selected_element.create_child()
		shader_item.set_text(0, shader_name)
		shader_item.add_button(0, self._shown_texture, 0, false, "Toggle Visibility")
		shader_item.set_meta("type", "shader")
		shader_item.set_meta("reference", effect)

		if shader_type == "Textures":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Filters":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Skies":
			shader_item.set_icon(0, self._sky_texture)
		shader_item.set_icon_max_width(0, 16)

		var shader_material: ShaderMaterial = ShaderMaterial.new()
		shader_material.shader = effect.get_base().get_shader()
		effect.set_meta("material", shader_material)

		var element: Element = selected_element.get_meta("reference")
		element.attach_material(shader_material)

		shader_item.select(0)

	return selected_element
