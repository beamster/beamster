class_name SourceTree
extends Tree


var _shown_texture: Texture2D
var _hidden_texture: Texture2D
var _locked_texture: Texture2D
var _unlocked_texture: Texture2D
var _script_texture: Texture2D
var _shader_texture: Texture2D
var _sky_texture: Texture2D
var _sources: Array


# When a source is selected
signal source_selected(source: SerializedObject)

# When a source is activated
signal source_activated(source: SerializedObject)

# When an effect is selected
signal effect_selected(effect: SerializedObject)

# When an effect is activated
signal effect_activated(effect: SerializedObject)

# When an action is selected
signal action_selected(action: SerializedObject)

# When an action is activated
signal action_activated(action: SerializedObject)

# When a transition is selected
signal transition_selected(transition: SerializedObject)

# When a transition is activated
signal transition_activated(transition: SerializedObject)


# Called when the node enters the scene tree for the first time.
func _ready():
	var context_menu = TreeContextMenu.new(
		[
			{
				"name": "Log",
				"call": self.log_item,
			},
			{
				"name": "Log Base",
				"call": self.log_base_item,
			},
			{},
			{
				"name": "Duplicate",
				"call": self.duplicate_item,
			},
			{
				"name": "Instantiate",
				"call": self.instantiate_item,
			},
			{},
			{
				"name": "Delete",
				"call": self.delete_item_confirm,
			},
			{},
			{
				"name": "Move to top",
				"call": self.move_item_to_top,
			},
			{
				"name": "Move to bottom",
				"call": self.move_item_to_bottom,
			},
		],
	)
	self.add_child(context_menu)

	var drag_agent = TreeDragAgent.new()
	self.add_child(drag_agent)

	var filter_agent = TreeFilterAgent.new()
	self.add_child(filter_agent)

	# Register a callback when a source is selected
	self.item_selected.connect(self.on_item_selected)
	self.item_activated.connect(self.on_item_activated)
	drag_agent.item_moved.connect(self.on_item_moved)
	self.nothing_selected.connect(self.on_nothing_selected)

	# Load the visibility icons
	self._shown_texture = load("res://Assets/Images/shown.png")
	self._hidden_texture = load("res://Assets/Images/hidden.png")
	self._locked_texture = load("res://Assets/Images/locked.png")
	self._unlocked_texture = load("res://Assets/Images/unlocked.png")
	self._script_texture = load("res://Assets/Images/Shader.png")
	self._shader_texture = load("res://Assets/Images/Shader.png")
	self._sky_texture = load("res://Assets/Images/Sky.png")

	# Capture button presses
	self.button_clicked.connect(self.on_button_clicked.bind())

	# The minimum width of the tree
	self.custom_minimum_size.x = 250

	var parent: TabContainer = self.get_parent().get_parent()
	parent.set_tab_title(self.get_index(), "Sources")
	parent.set_tab_hidden(self.get_index(), true)

func on_nothing_selected():
	# Show toolbox
	var CreateContainer = $/root/Studio/AppTabContainer/EditContainer/BottomPanels/HSplitContainer/CreateContainer
	if CreateContainer:
		CreateContainer.open_toolbox()

func on_item_activated():
	# Get the selected source tree item (or bail, if none)
	var tree_item: TreeItem = self.get_selected()
	if not tree_item:
		return

	# Toggle collapse
	tree_item.set_collapsed_recursive(not tree_item.is_any_collapsed(true))

	# Determine the type of item
	if tree_item.has_meta("class"):
		var thing: SerializedObject = tree_item.get_meta("class")

		if thing is Source.SourceInstance:
			# Activate the source
			self.source_activated.emit(thing)
		elif thing is Effect.EffectInstance:
			# Activate the effect
			self.effect_activated.emit(thing)
		elif thing is Action:
			# Activate the action
			self.action_activated.emit(thing)
		elif thing is Transition.TransitionInstance:
			# Activate the transition
			self.transition_activated.emit(thing)

func on_item_selected():
	# Get the selected item in the sources tree
	var tree_item: TreeItem = self.get_selected()
	if tree_item == null:
		return

	# Determine the type of item
	if tree_item.has_meta("class"):
		var thing: SerializedObject = tree_item.get_meta("class")

		if thing is Source.SourceInstance:
			# A source
			self.source_selected.emit(thing)
		elif thing is Effect.EffectInstance:
			# An effect
			self.effect_selected.emit(thing)
		elif thing is Action:
			# An action
			self.action_selected.emit(thing)
		elif thing is Transition.TransitionInstance:
			# A transition
			self.transition_selected.emit(thing)

func on_button_clicked(item: TreeItem, _column: int, id: int, _mouse_button_index: int):
	if id == 0:
		# Toggle visibility of source/shader/etc
		var source_type: String = item.get_meta("type")
		if source_type == "source":
			var source: SerializedObject.SerializedInstance = item.get_meta("reference")
			if source is TransformableSource.TransformableSourceInstance:
				source.set_visible(!source.get_visible())

				if source.get_visible():
					item.set_button(0, 0, self._shown_texture)
					item.clear_custom_color(0)
				else:
					item.set_button(0, 0, self._hidden_texture)
					item.set_custom_color(0, Color(0.25, 0.25, 0.25, 1.0))
		elif source_type == "shader":
			# Turn off this material on the source itself
			var source: Source.SourceInstance = item.get_parent().get_meta("reference")
			var source_base: Source = source.get_base()

			if source_base.get_material_visible(item.get_index() + 1):
				item.set_button(0, 0, self._hidden_texture)
				item.set_custom_color(0, Color(0.25, 0.25, 0.25, 1.0))
				source_base.hide_material(item.get_index() + 1)
			else:
				item.set_button(0, 0, self._shown_texture)
				item.clear_custom_color(0)
				source_base.show_material(item.get_index() + 1)
	elif id == 1:
		# Toggle position lock
		var source: Source.SourceInstance = item.get_meta("reference")
		if source is TransformableSource.TransformableSourceInstance:
			source.set_position_lock(!source.get_position_lock())

			if source.get_position_lock():
				item.set_button(0, 1, self._locked_texture)
			else:
				item.set_button(0, 1, self._unlocked_texture)

func on_scene():
	for source in self.get_sources():
		# Call on_ready()
		#for script in source.get_scripts_with("on_ready"):
		#	script.on_ready()
		pass # TODO: fix wrt SourceInstance

func get_sources() -> Array:
	return self._sources

func add_object(thing: SerializedObject):
	if thing is Effect.EffectInstance:
		var effect: Effect.EffectInstance = thing
		self.add_shader(effect.get_base().get_human_name(), effect.get_base().category, effect)
	elif thing is Source.SourceInstance:
		self.add_source(thing)

func add_source(source: Source.SourceInstance) -> TreeItem:
	# We add the source to the root (creating if not existing)
	var root: TreeItem = self.get_root()
	if not root:
		root = self.create_item()

	# We do not show the root
	self.hide_root = true

	# We create the source item
	var source_item: TreeItem = self.create_item(root)
	source_item.move_before(root.get_first_child())

	# Backward-reference
	source.append_tree_item(source_item)

	# Set the initial name
	source_item.set_text(0, source.get_base().get_script().name())

	# Set icon
	print('set_icon ', self.get_parent())
	var texture_manager: TextureManager = self.get_tree().get_root().get_node("Studio").get_texture_manager()
	var icon_texture: Texture2D = texture_manager.icon_texture_16_for(source.get_base().get_script().name())
	source_item.set_icon(0, icon_texture)
	source_item.set_icon_max_width(0, 16)

	# Set the icon used when dragging the texture (larger icon)
	var drag_texture: Texture2D = texture_manager.icon_texture_32_for(source.get_base().get_script().name())
	source_item.set_meta("drag_icon", drag_texture)

	if source.has_method("get_visible"):
		source_item.add_button(0, self._shown_texture, 0, false, "Toggle Visibility")

	if source.has_method("get_position_lock"):
		source_item.add_button(0, self._unlocked_texture, 1, false, "Toggle Position Lock")

	# Add a reference to the presentation source render object
	source_item.set_meta("reference", source)
	source_item.set_meta("class", source)
	source_item.set_meta("type", "source")

	# Retain
	self._sources.append(source)

	# Select it (this will also load its properties)
	#   See: self.on_item_selected() above
	source_item.select(0)
	return source_item

func add_shader(shader_name: String, shader_type: String, effect: Effect.EffectInstance) -> TreeItem:
	var selected_source: TreeItem = self.get_selected()

	if selected_source and selected_source.get_meta("type") != "source":
		selected_source = selected_source.get_parent()

	if selected_source:
		var shader_item: TreeItem = selected_source.create_child()
		shader_item.set_text(0, shader_name)
		shader_item.add_button(0, self._shown_texture, 0, false, "Toggle Visibility")
		shader_item.set_meta("type", "shader")
		shader_item.set_meta("reference", effect)
		shader_item.set_meta("class", effect)

		effect.append_tree_item(shader_item)

		if shader_type == "Textures":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Filters":
			shader_item.set_icon(0, self._shader_texture)
		elif shader_type == "Skies":
			shader_item.set_icon(0, self._sky_texture)
		shader_item.set_icon_max_width(0, 16)

		var source: Source.SourceInstance = selected_source.get_meta("reference")
		var source_base: Source = source.get_base()

		source_base.add_child(effect)

		shader_item.select(0)
		return shader_item

	return null

func add_action(action_name: String, action_class: GDScript):
	var selected_source: TreeItem = self.get_selected()
	if selected_source:
		if selected_source.get_meta("type") != "source":
			selected_source = selected_source.get_parent()

		var action_item: TreeItem = selected_source.create_child()
		action_item.set_text(0, action_name)
		action_item.add_button(0, self._shown_texture, 0, false, "Toggle Visibility")
		action_item.set_meta("type", "action")

		var action: Action = action_class.new()
		action_item.set_meta("class", action)

		action_item.set_icon(0, self._shader_texture)
		action_item.set_icon_max_width(0, 16)

		var source: Source.SourceInstance = selected_source.get_meta("reference")
		var source_base: Source = source.get_base()

		source_base.add_child(action)

		action_item.select(0)
		#self.on_item_activated()
		return action_item

	return null

func remove_item(item: TreeItem):
	if item.is_selected(0):
		item.deselect(0)
	item.get_parent().remove_child(item)

func delete_item_confirm(item: TreeItem):
	if item.get_meta("reference") is Source.SourceInstance:
		var source: Source.SourceInstance = item.get_meta("reference")
		if source:
			var dialog = ConfirmationDialog.new()
			dialog.set_title("Are you sure?")
			dialog.dialog_text = "Delete source '" + item.get_text(0) + "'?"
			self.add_child(dialog)
			dialog.unresizable = true
			dialog.popup_window = true
			dialog.visible = true
			dialog.popup_centered()

			dialog.get_ok_button().pressed.connect(func x(): self.delete_source_item(source))
	elif item.get_meta("reference") is Effect.EffectInstance:
		var effect: Effect.EffectInstance = item.get_meta("reference")
		if effect:
			var dialog = ConfirmationDialog.new()
			dialog.set_title("Are you sure?")
			dialog.dialog_text = "Delete effect '" + item.get_text(0) + "'?"
			self.add_child(dialog)
			dialog.unresizable = true
			dialog.popup_window = true
			dialog.visible = true
			dialog.popup_centered()

			dialog.get_ok_button().pressed.connect(func x(): self.delete_effect_item(effect))

func delete_source_item(source: Source.SourceInstance):
	# Remove the source from its own parent
	source.get_owner().remove_child(source)

func delete_effect_item(effect: Effect.EffectInstance):
	# Remove the source from its own parent
	effect.get_owner().remove_child(effect)

# Process mouse input for item drags and context menus
func _input(event):
	# Handle keys
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_DELETE:
			# Delete source
			var item: TreeItem = self.get_selected()
			if item:
				self.delete_item_confirm(item)

func move_item_to_top(item: TreeItem):
	if item.get_index() != 0:
		item.move_before(item.get_parent().get_child(0))
		self.on_item_moved(item)

func move_item_to_bottom(item: TreeItem):
	var item_count: int = item.get_parent().get_child_count()
	if item.get_index() != item_count - 1:
		item.move_after(item.get_parent().get_child(item_count - 1))
		self.on_item_moved(item)

func duplicate_item(item: TreeItem):
	if item.has_meta("reference"):
		# Duplicate the base object
		var object: SerializedObject = item.get_meta("reference")
		var new_object_base: SerializedObject = object.get_base().duplicate()
		var new_object: SerializedObject = new_object_base.instantiate()
		new_object.set_properties(object.serialize_properties())

		if object is SerializedObject.SerializedInstance:
			object.get_owner().add_child(new_object)
		else:
			pass #object.get_base().get_owner().add_child(new_object)

func instantiate_item(item: TreeItem):
	if item.has_meta("reference"):
		# Duplicate this instance
		var object: SerializedObject = item.get_meta("reference")
		var new_object: SerializedObject = object.duplicate()

		# And then add it to the same scane, etc
		if object is SerializedObject.SerializedInstance:
			object.get_owner().add_child(new_object)
		else:
			pass #object.get_base().get_owner().add_child(new_object)

func log_item(item: TreeItem):
	if item.has_meta("reference"):
		# Duplicate this instance
		var object: SerializedObject = item.get_meta("reference")
		print(object.serialize())

func log_base_item(item: TreeItem):
	if item.has_meta("reference"):
		# Duplicate this instance
		var object: SerializedObject = item.get_meta("reference")
		print(object.get_base().serialize())

func on_item_moved(item: TreeItem):
	if item.get_meta("type") == "source":
		# Move source instance
		var source: Source.SourceInstance = item.get_meta("reference")
		if source:
			var node: Node = source._container
			node.get_parent().move_child(node, self.get_root().get_child_count() - 1 - item.get_index())
	elif item.get_meta("type") == "shader":
		# Move shader within the stack
		var effect: Effect.EffectInstance = item.get_meta("reference")
		if effect.get_owner() is TransformableSource:
			var source: TransformableSource = effect.get_owner()
			source.move_child(effect, item.get_index())
