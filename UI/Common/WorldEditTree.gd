extends Tree


var _world_texture: Texture2D
var _element_tree: Tree
var _worlds: Dictionary

# Called when the node enters the scene tree for the first time.
func _ready():
	self.item_selected.connect(self.on_item_selected)

	self.get_parent().get_parent().get_parent().set_tab_title(self.get_parent().get_parent().get_index(), "Worlds")
	self.hide_root = true

	self._world_texture = load("res://Assets/Images/World.png")

func on_item_selected():
	#$%ScenePane.visible = false
	$%WorldPane.visible = true
	#$%ShaderPane.visible = false
	#$%ScriptEditPane.visible = false

	#$%WorldPane/WorldViewport/SubViewport.world_3d = load("res://World.tscn")

	#$%CreateContainer.hide_effect_tree()
	#$%CreateContainer.show_material_tree()
	$%CreateContainer.show_toolbox()
	$%Toolbox.show_element_toolbox()

	var element_tree: Tree = self._worlds[self.get_selected().get_text(0)]["elements"]
	$%ItemContainer.show_element_tree(element_tree)
	$%WorldViewport.show_world(self.get_selected())
	#element_tree.on_world()

	self._element_tree = element_tree

	var button: Button = self.get_parent().get_node("PanelContainer/Button")
	button.pressed.connect(self.on_add_button_pressed.bind())

	$%ElementTree.visible = false

func on_add_button_pressed():
	print("add world")
	self.add_world("New World")

# Called when the tab is changed to this one
func on_activation():
	if not self.get_selected():
		var root: TreeItem = self.get_root()
		if root:
			var first_item: TreeItem = root.get_first_child()
			if first_item:
				first_item.select(0)

	self.on_item_selected()

func add_world(world_name: String) -> TreeItem:
	var world_root: TreeItem = self.get_root()
	if not world_root:
		world_root = self.create_item()

	self.hide_root = true

	var new_name: String = world_name
	var i: int = 1
	while self._worlds.has(new_name):
		i += 1
		new_name = world_name + " " + str(i)

	var world_item: TreeItem = self.create_item(world_root)
	world_item.set_text(0, new_name)
	world_item.set_icon(0, self._world_texture)
	world_item.set_icon_max_width(0, 16)

	var element_tree: Tree = $%ItemContainer.add_element_tree()

	var new_world = $%WorldViewport.add_world(world_item)

	self._worlds[new_name] = {
		"item": world_item,
		"world": new_world,
		"elements": element_tree
	}

	return world_item
