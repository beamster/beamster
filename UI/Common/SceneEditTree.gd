class_name SceneEditTree
extends Tree


var _scenes: Dictionary
var _scene_texture: Texture2D
var _frame_texture: Texture2D
var _frame_alt_texture: Texture2D
var _open_aside_texture: Texture2D
var _frame_play_texture: Texture2D


signal scene_activated(item: TreeItem, scene: Scene)
signal frame_activated(item: TreeItem, frame: Frame)


# Called when the node enters the scene tree for the first time.
func _ready():
	# Load the scene texture
	self._scene_texture = load("res://Assets/Images/Scene.png")
	self._open_aside_texture = load("res://Assets/Images/open_aside.png")
	self._frame_texture = load("res://Assets/Images/Frame.png")
	self._frame_alt_texture = load("res://Assets/Images/Frame_alt.png")
	#self._frame_play_texture = load("res://Assets/Images/preview_transition.png")
	self._frame_play_texture = load("res://Assets/Images/frame_play.png")

	# Connect to events to switch scenes
	self.item_selected.connect(self.on_item_selected)
	self.item_activated.connect(self.on_item_activated)

	# Hmm
	self.get_parent().get_parent().get_parent().set_tab_title(self.get_parent().get_parent().get_index(), "Scenes")

	# Hide the root so it looks like a normal list
	self.hide_root = true

	# Add the 'add' button
	var button: Button = self.get_parent().get_node("PanelContainer/Button")
	button.pressed.connect(self.on_add_button_pressed.bind())

	# Capture button presses
	self.button_clicked.connect(self.on_button_clicked.bind())
	var context_menu = TreeContextMenu.new(
		[
			{
				"name": "Log",
				"call": self.log_item,
			},
			{
				"name": "Log Base",
				"call": self.log_base_item,
			},
			{},
			{
				"name": "Add Frame",
				"call": self.create_frame,
			},
			{},
			{
				"name": "Duplicate",
				"call": self.duplicate_item,
			},
			#{
			#	"name": "Instantiate",
			#	"call": self.instantiate_item,
			#},
			{},
			{
				"name": "Delete",
				"call": self.delete_item_confirm,
			},
			{},
			{
				"name": "Move to top",
				"call": self.move_item_to_top,
			},
			{
				"name": "Move to bottom",
				"call": self.move_item_to_bottom,
			},
		],
	)
	self.add_child(context_menu)

	var drag_agent = TreeDragAgent.new()
	self.add_child(drag_agent)

	var filter_agent = TreeFilterAgent.new()
	self.add_child(filter_agent)

func can_move_item(item: TreeItem, to_index: int) -> bool:
	var object: SerializedObject = self.get_object_for_item(item)
	if object is Frame:
		# Do not move the initial frame
		if item.get_index() == 0:
			return false

		# Do not move any frame into the 0th position to maintain the initial
		# frame's position.
		if to_index == 0:
			return false

	return true

func on_add_button_pressed():
	self.add_scene("New Scene")

func on_item_activated():
	# Add effect to selected source
	var item: TreeItem = self.get_selected()

	# Allow "double click" and such expand/collapse a group
	item.collapsed = not item.collapsed

var _selected_frame: TreeItem = null
func on_item_selected():
	var selected_item = self.get_selected()
	var item_icon: Texture2D = selected_item.get_icon(0)
	var scene_manager: SceneManager = $/root/Studio.get_profile_manager().get_scene_manager()

	if item_icon == self._scene_texture:
		# This is a Scene
		# Show this scene
		var scene_item = selected_item

		if scene_item.get_metadata(0) == null:
			return

		# Get the scene
		var scene: Scene = scene_manager.get_scene(scene_item.get_metadata(0))
		scene_manager.set_editing_scene(scene)

		# Select the scene by telling the editor to load that scene
		self.scene_activated.emit(scene_item, scene)

		# Now, the scene is loaded.

		# Get the frame
		var frame: Frame = scene.get_frames()[0]
		var frame_item: TreeItem = frame.tree_item

		# Collapse the other scene groups
		var root_item = scene_item.get_parent()
		root_item.set_collapsed_recursive(true)
		root_item.collapsed = false

		# Expand the scene group
		scene_item.collapsed = false

		# Select the frame (deferred to the next frame since the Scene is still
		# established as the selected item after this event... boo)
		self._selected_frame = frame_item
		self.call_deferred("_select_frame")
	elif not selected_item == self.get_root():
		# This is a frame
		var frame_item: TreeItem = selected_item

		var object: SerializedObject = frame_item.get_metadata(0)
		var editor_manager: EditorManager = $/root/Studio.get_interface_manager().get_editor_manager()
		if object is Frame:
			var frame: Frame = object

			# Select it by loading it into the active Edit Panel
			editor_manager.select_frame(frame)

# This is called when a Scene is selected such that it highlights the initial
# Frame for the Scene.
# We defer this call since when selecting the Scene, the final setting of the
# selected item in the Tree happens *after* the event.
func _select_frame():
	if self._selected_frame != null:
		var frame_item: TreeItem = self._selected_frame
		frame_item.select(0)
	self._selected_frame = null

# Called when the tab is changed to this one
func on_activation():
	if not self.get_selected():
		var root: TreeItem = self.get_root()
		if root:
			var first_item: TreeItem = root.get_first_child()
			if first_item:
				first_item.select(0)

	self.on_item_selected()

func add_scene(scene_name: String) -> TreeItem:
	var scene_manager: SceneManager = $/root/Studio.get_profile_manager().get_scene_manager()

	var scene: Scene = scene_manager.create_scene()
	scene_manager.add_scene(scene)
	scene = scene_manager.get_scene(scene.id())

	var scene_root: TreeItem = self.get_root()
	if scene_root == null:
		scene_root = self.create_item()

	var new_name: String = scene_name
	var i: int = 1
	while self._scenes.has(new_name):
		i += 1
		new_name = scene_name + " " + str(i)
	scene.set_name(new_name)

	var scene_item: TreeItem = self.create_item(scene_root)
	scene_item.set_text(0, new_name)
	scene_item.set_icon(0, self._scene_texture)
	scene_item.set_metadata(0, scene.id())
	scene_item.set_icon_max_width(0, 16)
	scene_item.add_button(0, self._open_aside_texture, 0, false, "Show Aside")

	var source_tree: VBoxContainer = scene.get_source_list()
	$%ItemContainer.add_source_tree(source_tree)

	var action_tree: Tree = scene.get_action_list()
	$%ItemContainer.add_action_tree(action_tree)

	self._scenes[new_name] = {
		"item": scene_item,
		"scene": scene,
		"sources": source_tree,
	}

	# Add "frames"
	for frame in scene.get_frames():
		var frame_item: TreeItem = self.create_item(scene_item)
		frame_item.set_text(0, frame.get_name())
		if frame_item.get_index() == 0:
			frame_item.set_icon(0, self._frame_texture)
		else:
			frame_item.set_icon(0, self._frame_alt_texture)
		frame_item.set_metadata(0, frame)
		frame_item.set_icon_max_width(0, 16)
		frame_item.add_button(0, self._frame_play_texture, 0, false, "Preview")
		frame.tree_item = frame_item

	# Collapse the scene at first
	scene_item.collapsed = true

	return scene_item

func create_frame(item: TreeItem):
	# Add a new frame to the given scene item
	var object: SerializedObject = self.get_object_for_item(item)
	var scene: Scene = object
	var scene_item: TreeItem = item
	var frame: Frame = Frame.new()
	frame.set_name("New Frame")
	scene.add_child(frame)

	var frame_item: TreeItem = self.create_item(scene_item)
	frame_item.set_text(0, frame.get_name())
	frame_item.set_icon(0, self._frame_alt_texture)
	frame_item.set_metadata(0, frame)
	frame_item.set_icon_max_width(0, 16)
	frame_item.add_button(0, self._frame_play_texture, 0, false, "Preview")
	frame.tree_item = frame_item

func on_button_clicked(item: TreeItem, _column: int, id: int, _mouse_button_index: int):
	var item_icon: Texture2D = item.get_icon(0)
	if item_icon == self._scene_texture:
		if id == 0:
			# Show this scene
			var scene: Scene = self._scenes[item.get_text(0)]["scene"]
			var panel_manager: PanelManager = $/root/Studio.get_interface_manager().get_editor_manager().get_panel_manager()
			panel_manager.update_panel(scene, 1)
	elif item_icon == self._frame_texture or item_icon == self._frame_alt_texture:
		if id == 0:
			# Animate this Frame
			var frame: Frame = self.get_object_for_item(item)

			var scene_manager: SceneManager = $/root/Studio.get_profile_manager().get_scene_manager()
			if scene_manager.current_edited_frame == frame:
				frame.rewind()
			scene_manager.current_previewing_frame = frame

func delete_item_confirm(_item: TreeItem):
	pass

func move_item_to_top(item: TreeItem):
	var object: SerializedObject = self.get_object_for_item(item)
	if item.get_index() != 0:
		if object is Frame:
			# Do not put something before the 'initial' Frame
			item.move_before(item.get_parent().get_child(1))
		else:
			item.move_before(item.get_parent().get_child(0))

func move_item_to_bottom(item: TreeItem):
	var object: SerializedObject = self.get_object_for_item(item)
	var item_count: int = item.get_parent().get_child_count()
	# Do not move the initial frame
	if not object is Frame or item.get_index() != 0:
		if item.get_index() != item_count - 1:
			item.move_after(item.get_parent().get_child(item_count - 1))

func get_object_for_item(item: TreeItem) -> SerializedObject:
	var ret: SerializedObject = null
	var scene_manager: SceneManager = $/root/Studio.get_profile_manager().get_scene_manager()

	var item_icon: Texture2D = item.get_icon(0)
	if item_icon == self._scene_texture:
		# Scene
		var scene_item = item

		if scene_item.get_metadata(0) != null:
			# Get the scene
			var scene: Scene = scene_manager.get_scene(scene_item.get_metadata(0))
			ret = scene
	elif not item == self.get_root():
		# Frame
		var frame_item: TreeItem = item
		return frame_item.get_metadata(0)

	return ret

func duplicate_item(_item: TreeItem):
	pass

func instantiate_item(_item: TreeItem):
	pass

func log_item(item: TreeItem):
	var object: SerializedObject = self.get_object_for_item(item)
	if object:
		print(object.serialize())

func log_base_item(item: TreeItem):
	var object: SerializedObject = self.get_object_for_item(item)
	if object and object is SerializedObject.SerializedInstance:
		print(object.get_base().serialize())
	else:
		print("Object is not an instance.")
