extends MeshInstance3D


var grid_size = 1.0
var _mesh = ImmediateMesh.new()
var _material = ORMMaterial3D.new()

func cell_create(size, offset):
	var s = size / 2
	var vertices = []
	vertices.append( Vector3( -s + offset.x, 0, -s + offset.z) )
	vertices.append( Vector3( s + offset.x, 0, -s + offset.z) )
	vertices.append( Vector3( s + offset.x, 0, -s + offset.z) )
	vertices.append( Vector3( s + offset.x, 0, s  + offset.z) )
	vertices.append( Vector3( s + offset.x, 0, s  + offset.z) )
	vertices.append( Vector3( -s + offset.x, 0, s  + offset.z) )
	vertices.append( Vector3( -s + offset.x, 0, s  + offset.z) )
	vertices.append( Vector3( -s + offset.x, 0, -s + offset.z) )

	for v in vertices:
		_mesh.surface_add_vertex(v)

func _ready():
	_mesh.surface_begin(Mesh.PRIMITIVE_LINES, _material)

	for x_offset in range(-20,20):
		for z_offset in range(-20,20):
			var offset = Vector3(x_offset, 0, z_offset)*grid_size + Vector3(1,0,1)*grid_size/2
			cell_create(grid_size, offset)
	_mesh.surface_end()

	_material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
	_material.albedo_color = Color(0.5, 0.5, 0.5, 1)

	set_mesh( _mesh )
