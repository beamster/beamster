extends TabContainer


func _ready():
	# TODO: move to editor manager
	self.set_tab_title($Toolbox.get_index(), "Toolbox")
	self.set_tab_title($EffectsTree.get_index(), "Effects")
	self.set_tab_title($ActionsTree.get_index(), "Actions")
	self.set_tab_title($MaterialTree.get_index(), "Materials")
	self.set_tab_title($TransitionsTree.get_index(), "Transitions")

	# Do not show the Material tree
	self.set_tab_hidden($MaterialTree.get_index(), true)

func get_toolbox() -> PanelContainer:
	return $Toolbox

func show_toolbox():
	self.set_tab_hidden($Toolbox.get_index(), false)

func open_toolbox():
	self.current_tab = $Toolbox.get_index()

func hide_toolbox():
	self.set_tab_hidden($Toolbox.get_index(), true)

func show_effect_tree():
	self.set_tab_hidden($EffectsTree.get_index(), false)

func open_effect_tree():
	self.current_tab = $EffectsTree.get_index()

func hide_effect_tree():
	self.set_tab_hidden($EffectsTree.get_index(), true)

func show_material_tree():
	self.set_tab_hidden($MaterialTree.get_index(), false)

func hide_material_tree():
	self.set_tab_hidden($MaterialTree.get_index(), true)
