extends CodeEdit


# Called when the node enters the scene tree for the first time.
func _ready():
	self.text_changed.connect(self.on_text_changed.bind())

	var highlighter: CodeHighlighter = CodeHighlighter.new()

	var special_color: Color = Color("8fffdb")
	var keyword_color: Color = Color("ff7085")
	var string_color: Color = Color("ffeda1")
	var control_color: Color = Color("ff8ccc")
	var comment_color: Color = Color("cdcfd280")
	highlighter.symbol_color = Color("abc9ff")
	highlighter.function_color = Color("57b3ff")
	highlighter.number_color = Color("a1ffe0")
	highlighter.member_variable_color = Color("bce0ff")

	highlighter.add_color_region('"', '"', string_color)
	highlighter.add_color_region("'", "'", string_color)
	highlighter.add_color_region('#', '', comment_color, true)

	var keywords = ["var", "self", "func", "in", "extends"]
	var control_words = ["if", "else", "for", "while",
							"break", "continue", "return", "pass"]
	var special_words = ["Color"]

	for control_word in control_words:
		highlighter.add_keyword_color(control_word, control_color)

	for keyword in keywords:
		highlighter.add_keyword_color(keyword, keyword_color)

	for special_word in special_words:
		highlighter.add_keyword_color(special_word, special_color)

	self.syntax_highlighter = highlighter

func on_text_changed():
	pass
