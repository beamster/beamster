# This source wraps a video file or resource.

# TODO: on_finish signal propagation

class_name MediaSource
extends TransformableSource


var _source: String


static func icon() -> String:
	return "res://Sources/Icons/MediaSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/MediaSource.small.png"

static func name() -> String:
	return "Media Source"

static func properties() -> Dictionary:
	return {
		"loop": {
			"type": "bool",
			"label": "Loop"
		},
		"source": {
			"type": "file",
			"label": "Source",
			"filter": ["*.m4a", "*.mkv", "*.avi", "*.mp4", "*.ogg", "*.webm"]
		}
	}

static func get_native_node_class():
	return FFMpegStreamPlayer

func get_native_size() -> Vector2:
	var texture: Texture2D = self.get_native_node().get_texture()
	if texture:
		return texture.get_size()

	return Vector2(2.0, 2.0)

func set_loop(value: bool):
	self.get_native_node().loops = value
	self.play()

func get_loop() -> bool:
	return self.get_native_node().loops

func resize(width: float, height: float):
	# Resize the native widget based on the resize
	# But only if the widget has the means to understand that.
	var node: Node = self.get_native_node()

	# Allow the stream to assert its size
	if node.has_method("get_size"):
		var native_size: Vector2 = node.get_size()
		width = native_size.x
		height = native_size.y

	super.resize(width, height)

func set_source(path: String):
	self._source = path
	self.play()

func get_source() -> String:
	return self._source

func on_init(options: Dictionary = {}):
	super.on_init(options)

	self.set_source("e:/love-lentils.mp4")

func play():
	self.get_native_node().set_source(self.get_source())
	self.get_native_node().centered = false

	var size = self.get_native_node().get_size()
	self.resize(size.x, size.y)

func on_activate():
	self.play()
