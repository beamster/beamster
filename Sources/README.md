# Sources

A `Source` is a `SerializedObject` that represents a component of a `Scene`.
These are typically 2D visual components. A visualized component is specifically
a `TransformableSource`, which means it can have an instanced position and size.

## CameraSource

## ColorSource

## ImageSource

## MediaSource

## NineSliceSource

## ScriptSource

## TextSource

## TwitchChatSource

## VideoCaptureSource
