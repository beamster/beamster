# This source wraps a tileable image.

class_name NineSliceSource
extends ImageSource

var _flip_h: bool
var _flip_v: bool

static func icon() -> String:
	return "res://Sources/Icons/NineSliceSource.png"

static func name() -> String:
	return "Nine Slice Source"

static func properties() -> Dictionary:
	return {
		"scale_x": {
			"type": "float",
			"label": "Scale X"
		},
		"scale_y": {
			"type": "float",
			"label": "Scale Y"
		},
		"slice_left": {
			"type": "int",
			"label": "Slice Left Width",
			"min": 0
		},
		"slice_right": {
			"type": "int",
			"label": "Slice Right Width",
			"min": 0
		},
		"slice_top": {
			"type": "int",
			"label": "Slice Top Height",
			"min": 0
		},
		"slice_bottom": {
			"type": "int",
			"label": "Slice Bottom Height",
			"min": 0
		}
	}

static func get_native_node_class():
	return NinePatchRect

func get_native_size() -> Vector2:
	var nprect: NinePatchRect = self.get_native_node()
	var texture: ImageTexture = nprect.get_texture()
	if texture:
		return texture.get_size()

	return self._size

func get_slice_left() -> int:
	var nprect: NinePatchRect = self.get_native_node()
	return nprect.patch_margin_left

func set_slice_left(value: int):
	var nprect: NinePatchRect = self.get_native_node()
	nprect.patch_margin_left = value

func get_slice_right() -> int:
	var nprect: NinePatchRect = self.get_native_node()
	return nprect.patch_margin_right

func set_slice_right(value: int):
	var nprect: NinePatchRect = self.get_native_node()
	nprect.patch_margin_right = value

func get_slice_top() -> int:
	var nprect: NinePatchRect = self.get_native_node()
	return nprect.patch_margin_top

func set_slice_top(value: int):
	var nprect: NinePatchRect = self.get_native_node()
	nprect.patch_margin_top = value

func get_slice_bottom() -> int:
	var nprect: NinePatchRect = self.get_native_node()
	return nprect.patch_margin_bottom

func set_slice_bottom(value: int):
	var nprect: NinePatchRect = self.get_native_node()
	nprect.patch_margin_bottom = value

func get_scale_x() -> float:
	var nprect: NinePatchRect = self.get_native_node()
	return nprect.scale.x

func set_scale_x(value: float):
	var nprect: NinePatchRect = self.get_native_node()
	nprect.scale.x = value
	self._on_resize()

func get_scale_y() -> float:
	var nprect: NinePatchRect = self.get_native_node()
	return nprect.scale.y

func set_scale_y(value: float):
	var nprect: NinePatchRect = self.get_native_node()
	nprect.scale.y = value
	self._on_resize()

func _on_init():
	#self.resize(Vector2(150, 150))
	self.set_source("e:/win31-9slice.png")

func set_source(path: String):
	var sprite: NinePatchRect = self.get_native_node()
	var image = Image.new()
	var err = image.load(path)
	self._source = path
	if err != OK:
		# Failed
		print("failed")
	var texture = ImageTexture.create_from_image(image)
	sprite.set_texture(texture)

	var size = texture.get_size()
	sprite.patch_margin_bottom = size.y / 3
	sprite.patch_margin_top = size.y / 3
	sprite.patch_margin_left = size.x / 3
	sprite.patch_margin_right = size.x / 3

	self._on_move()

func get_source() -> String:
	return self._source

func set_flip_h(value: bool):
	var sprite: NinePatchRect = self.get_native_node()
	var texture: Texture2D = sprite.get_texture()
	if value != self._flip_h:
		texture.get_image().flip_x()
	self._flip_h = value

func get_flip_h() -> bool:
	return self._flip_h

func set_flip_v(value: bool):
	var sprite: NinePatchRect = self.get_native_node()
	var texture: Texture2D = sprite.get_texture()
	if value != self._flip_v:
		texture.get_image().flip_y()
	self._flip_v = value

func get_flip_v() -> bool:
	return self._flip_v

func get_fill() -> String:
	return self._fill

func set_fill(value: String):
	self._fill = value

	# Move/Resize native widget to fill
	self._on_move()

func _on_move():
	#var sprite: NinePatchRect = self.get_native_node()
	#var position: Vector2 = Vector2(self.get_inner_position())

	#sprite.position = position
	self._on_resize()

func _on_resize():
	var sprite: NinePatchRect = self.get_native_node()
	sprite.size = self._size / sprite.scale
