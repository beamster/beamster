# This is a Twitch chat integration source.

extends TransformableSource
class_name TwitchChatSource


var _gift: Gift
var _chat_container: VBoxContainer
var _scroll_container: ScrollContainer
var _chat_messages_container: VBoxContainer
var _label_settings: LabelSettings
var _font_size: int = 32


# TODO: when Godot merges typed signals, we can remove the hints in names.
signal has_new_post(str_sender: String, str_message: String)


static func icon() -> String:
	return "res://Sources/Icons/TwitchChatSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/TwitchChatSource.small.png"

static func name() -> String:
	return "Twitch Chat Source"

static func properties() -> Dictionary:
	return {
		"font_size": {
			"type": "int",
			"unit": "pixels",
			"label": "Font Size",
			"min": 1,
			"max": 300,
			"step": 1
		},
	}

static func get_native_node_class():
	return PanelContainer

func set_font_size(value: int):
	for child in self._chat_messages_container.get_children():
		var label = child.get_child(0)
		label.add_theme_font_size_override("normal_font_size", value)
		label.add_theme_font_size_override("bold_font_size", value)
		label.add_theme_font_size_override("italics_font_size", value)
		label.add_theme_font_size_override("bold_italics_font_size", value)
		label.add_theme_font_size_override("mono_font_size", value)
		var old = "img=" + str(self._font_size) + "x" + str(self._font_size)
		self._font_size = value
		var new = "img=" + str(self._font_size) + "x" + str(self._font_size)
		var bbcode_text = label.get_meta("bbcode_text").replace(old, new)
		label.set_meta("bbcode_text", bbcode_text)
		label.bbcode_text = bbcode_text

func get_font_size() -> int:
	return self._font_size

func on_init(options: Dictionary = {}):
	self._label_settings = LabelSettings.new()
	self._size = Vector2(512.0, 1024.0)
	super.on_init(options)

	# Create all the things for the chat box
	var node: PanelContainer = self.get_native_node()

	# It initally doesn't handle the size very well
	node.size = self._size

	# Add IRC engine
	self._gift = Gift.new()
	self._gift.get_images = true
	self._gift.disk_cache = true
	self._gift.disk_cache_path = ""

	node.add_child(self._gift)

	# Add chat display
	self._chat_container = VBoxContainer.new()
	self._chat_container.anchors_preset = Control.LayoutPreset.PRESET_FULL_RECT
	self._chat_container.size_flags_horizontal = Control.SizeFlags.SIZE_EXPAND_FILL
	self._chat_container.size_flags_vertical = Control.SizeFlags.SIZE_EXPAND_FILL

	var chat_panel = Panel.new()
	chat_panel.size_flags_horizontal = Control.SizeFlags.SIZE_EXPAND_FILL
	chat_panel.size_flags_vertical = Control.SizeFlags.SIZE_EXPAND_FILL

	self._scroll_container = ScrollContainer.new()
	self._scroll_container.set_anchors_preset(Control.LayoutPreset.PRESET_FULL_RECT, true)

	self._chat_messages_container = VBoxContainer.new()
	self._chat_messages_container.size_flags_horizontal = Control.SizeFlags.SIZE_EXPAND_FILL
	self._chat_messages_container.size_flags_vertical = Control.SizeFlags.SIZE_EXPAND_FILL

	self._scroll_container.add_child(self._chat_messages_container)
	chat_panel.add_child(self._scroll_container)
	self._chat_container.add_child(chat_panel)
	node.add_child(self._chat_container)

func chat_message(data : SenderData, msg : String) -> void:
	self.put_chat(data, msg)

# Check the CommandInfo class for the available info of the cmd_info.
func command_test(_cmd_info : CommandInfo) -> void:
	print("A")

func hello_world(_cmd_info : CommandInfo) -> void:
	self._gift.chat("HELLO WORLD!")

func streamer_only(_cmd_info : CommandInfo) -> void:
	self._gift.chat("Streamer command executed")

func no_permission(_cmd_info : CommandInfo) -> void:
	self._gift.chat("NO PERMISSION!")

func greet(_cmd_info : CommandInfo, arg_ary : PackedStringArray) -> void:
	#print("greeting folks...", self.get_scripts_with("on_greet"), self.get_scripts())
	#for script in self.get_scripts_with("on_greet"):
	#	script.on_greet(arg_ary[0])
	self._gift.chat("Greetings, " + arg_ary[0])

func greet_me(cmd_info : CommandInfo) -> void:
	self._gift.chat("Greetings, " + cmd_info.sender_data.tags["display-name"] + "!")

func list(_cmd_info : CommandInfo, arg_ary : PackedStringArray) -> void:
	self._gift.chat(", ".join(arg_ary))

func put_chat(senderdata : SenderData, msg : String):
	# Create a container for the message contents
	var container = HBoxContainer.new()
	container.size_flags_horizontal = Control.SizeFlags.SIZE_EXPAND_FILL

	# Create a rich text label for the message and append it
	var label = RichTextLabel.new()
	label.bbcode_enabled = true
	label.fit_content = true
	label.size_flags_horizontal = Control.SizeFlags.SIZE_EXPAND_FILL
	label.size_flags_vertical = Control.SizeFlags.SIZE_EXPAND_FILL
	#label.theme = self._theme
	label.add_theme_font_size_override("normal_font_size", self._font_size)
	label.add_theme_font_size_override("bold_font_size", self._font_size)
	label.add_theme_font_size_override("italics_font_size", self._font_size)
	label.add_theme_font_size_override("bold_italics_font_size", self._font_size)
	label.add_theme_font_size_override("mono_font_size", self._font_size)

	container.add_child(label)

	var msgnode : Control = container
	var time = Time.get_time_dict_from_system()
	var badges : String = ""

	if self._gift.image_cache:
		for badge in senderdata.tags["badges"].split(",", false):
			badges += "[img=" + str(self._font_size) + "x" + str(self._font_size) + "]" + self._gift.image_cache.get_badge(badge, senderdata.tags["room-id"]).resource_path + "[/img] "

		var locations : Array = []
		for emote in senderdata.tags["emotes"].split("/", false):
			var data : Array = emote.split(":")
			for d in data[1].split(","):
				var start_end = d.split("-")
				locations.append(EmoteLocation.new(data[0], int(start_end[0]), int(start_end[1])))

		locations.sort_custom(Callable(EmoteLocation, "smaller"))
		var offset = 0
		for loc in locations:
			var emote_string = "[img=" + str(self._font_size) + "x" + str(self._font_size) + "]" + self._gift.image_cache.get_emote(loc.id).resource_path +"[/img]"
			msg = msg.substr(0, loc.start + offset) + emote_string + msg.substr(loc.end + offset + 1)
			offset += emote_string.length() + loc.start - loc.end - 1

	var bottom : bool = self._scroll_container.scroll_vertical == self._scroll_container.get_v_scroll_bar().max_value - self._scroll_container.get_v_scroll_bar().size.y

	self.set_message(label, str(time["hour"]) + ":" + ("0" + str(time["minute"]) if time["minute"] < 10 else str(time["minute"])), senderdata, msg, badges)
	self._chat_messages_container.add_child(msgnode)
	await self._chat_messages_container.get_tree().process_frame
	if bottom:
		self._scroll_container.scroll_vertical = self._scroll_container.get_v_scrollbar().max_value

	self.has_new_post.emit(senderdata.tags["display-name"], msg)

func set_message(label: RichTextLabel, stamp : String, data : SenderData, msg : String, badges : String) -> void:
	var bbcode_text = stamp + " " + badges + "[b][color="+ data.tags["color"] + "]" + data.tags["display-name"] +"[/color][/b]: " + msg
	label.set_meta("bbcode_text", bbcode_text)
	label.bbcode_text = bbcode_text

class EmoteLocation extends RefCounted:
	var id : String
	var start : int
	var end : int

	func _init(emote_id, start_idx, end_idx):
		self.id = emote_id
		self.start = start_idx
		self.end = end_idx

	static func smaller(a : EmoteLocation, b : EmoteLocation):
		return a.start < b.start

func on_activate():
	var botname = "wilkiefaerie"
	var token = "ertv2jr7sc1x87achsj25f1aobwex1"
	var initial_channel = "wilkiefaerie"

	self._gift.connect_to_twitch()
	await self._gift.twitch_connected

	# Login using your username and an oauth token.
	# You will have to either get a oauth token yourself or use
	# https://twitchapps.com/tokengen/
	# to generate a token with custom scopes.
	self._gift.authenticate_oauth(botname, token)

	if await self._gift.login_attempt == false:
		print("Invalid username or token.")
		return

	self._gift.join_channel(initial_channel)

	self._gift.cmd_no_permission.connect(self.no_permission)
	self._gift.chat_message.connect(self.chat_message)

	# Adds a command with a specified permission flag.
	# All implementations must take at least one arg for the command info.
	# Implementations that recieve args requrires two args,
	# the second arg will contain all params in a PoolStringArray
	# This command can only be executed by VIPS/MODS/SUBS/STREAMER
	self._gift.add_command("test", self, "command_test", 0, 0, Gift.PermissionFlag.NON_REGULAR)

	# These two commands can be executed by everyone
	self._gift.add_command("helloworld", self, "hello_world")
	self._gift.add_command("greetme", self, "greet_me")

	# This command can only be executed by the streamer
	self._gift.add_command("streamer_only", self, "streamer_only", 0, 0, Gift.PermissionFlag.STREAMER)

	# Command that requires exactly 1 arg.
	self._gift.add_command("greet", self, "greet", 1, 1)

	# Command that prints every arg seperated by a comma (infinite args allowed), at least 2 required
	self._gift.add_command("list", self, "list", -1, 2)

	# Adds a command alias
	self._gift.add_alias("test","test1")
	self._gift.add_alias("test","test2")
	self._gift.add_alias("test","test3")
	# Or do it in a single line
	# add_aliases("test", ["test1", "test2", "test3"])

	# Remove a single command
	self._gift.remove_command("test2")

	# Now only knows commands "test", "test1" and "test3"
	self._gift.remove_command("test")
	# Now only knows commands "test1" and "test3"

	# Remove all commands that call the same function as the specified command
	self._gift.purge_command("test1")
	# Now no "test" command is known

	# Send a chat message to the only connected channel (<channel_name>)
	# Fails, if connected to more than one channel.
#	chat("TEST")

	# Send a chat message to channel <channel_name>
#	chat("TEST", initial_channel)

	# Send a whisper to target user
#	whisper("TEST", initial_channel)
