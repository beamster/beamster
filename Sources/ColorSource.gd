# This is a simple colored rectangle.

class_name ColorSource
extends TransformableSource


static func icon() -> String:
	return "res://Sources/Icons/ColorSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/ColorSource.small.png"

static func name() -> String:
	return "Color Source"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Color"
		}
	}

static func get_native_node_class():
	return Polygon2D

func set_color(color: Color):
	var polygon = self.get_native_node()
	polygon.color = color

func get_color() -> Color:
	var polygon = self.get_native_node()
	return polygon.color

func on_init(_options: Dictionary = {}):
	var polygon = self.get_native_node()
	polygon.color = Color(1.0, 1.0, 0.0)
	var points = []
	points.append(Vector2(0, 0))
	points.append(Vector2(0, self._size.y))
	points.append(Vector2(self._size.x, self._size.y))
	points.append(Vector2(self._size.x, 0))
	polygon.set_polygon(points)

# Called when the object instance is resized within its parent.
func on_resize(_instance: TransformableSourceInstance):
	var polygon = self.get_native_node()
	var points = []
	points.append(Vector2(0, 0))
	points.append(Vector2(0, self._size.y))
	points.append(Vector2(self._size.x, self._size.y))
	points.append(Vector2(self._size.x, 0))
	polygon.set_polygon(points)
