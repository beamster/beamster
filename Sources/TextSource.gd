# This is a simple text source.

class_name TextSource
extends TransformableSource


var _label: Label
var _font_size: int
var _horizontal_alignment: int
var _vertical_alignment: int

static func icon() -> String:
	return "res://Sources/Icons/TextSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/TextSource.small.png"

static func name() -> String:
	return "Text Source"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Color"
		},
		"text": {
			"type": "string",
			"label": "Text"
		},
		"horizontal_alignment": {
			"type": ["left", "center", "right", "justify"],
			"label": "Horizontal Alignment"
		},
		"vertical_alignment": {
			"type": ["top", "center", "bottom", "justify"],
			"label": "Vertical Alignment"
		},
		"font_size": {
			"type": "int",
			"unit": "pixels",
			"label": "Font Size",
			"min": 1,
			"max": 300,
			"step": 1
		}
	}

static func get_native_node_class():
	return Label

func set_color(color: Color):
	self._label.label_settings.font_color = color

func get_color() -> Color:
	return self._label.label_settings.font_color

func set_text(value: String):
	self._label.text = value

func get_text() -> String:
	return self._label.text

func set_horizontal_alignment(value: String):
	self._horizontal_alignment = TextSource.properties()["horizontal_alignment"]["type"].find(value)
	self._label.horizontal_alignment = self._horizontal_alignment as HorizontalAlignment

func get_horizontal_alignment() -> String:
	return TextSource.properties()["horizontal_alignment"]["type"][self._horizontal_alignment]

func set_vertical_alignment(value: String):
	self._vertical_alignment = TextSource.properties()["vertical_alignment"]["type"].find(value)
	self._label.vertical_alignment = self._vertical_alignment as VerticalAlignment

func get_vertical_alignment() -> String:
	return TextSource.properties()["vertical_alignment"]["type"][self._vertical_alignment]

func set_font_size(value: int):
	self._font_size = value
	self._label.label_settings.font_size = value

func get_font_size() -> int:
	return self._font_size

func on_init(options: Dictionary = {}):
	super.on_init(options)

	#self._viewport = SubViewport.new()
	#self._viewport.transparent_bg = true
	self._label = self.get_native_node()
	#self._viewport.add_child(self._label)
	#self.get_native_node().add_child(self._viewport)
	self._label.label_settings = LabelSettings.new()

	# Instantiate font
	var font = load("res://Fonts/NotoSans-Regular.ttf")
	self._label.label_settings.font = font
	self._font_size = 32
	self._label.label_settings.font_size = self._font_size
	self._label.text = "Text Source"
	self._label.clip_text = true
	self._label.queue_redraw()
