# This wraps a real-time video source such as a capture card or physical camera.

class_name VideoCaptureSource
extends TransformableSource


static func icon() -> String:
	return "res://Sources/Icons/VideoCaptureSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/VideoCaptureSource.small.png"

static func name() -> String:
	return "Video Capture Source"

static func properties() -> Dictionary:
	return {
	}

static func get_native_node_class():
	return FFMpegCamera

func get_native_size() -> Vector2:
	var texture: Texture2D = self.get_native_node().get_texture()
	if texture:
		return texture.get_size()

	return Vector2(2.0, 2.0)

func on_init(options: Dictionary = {}):
	super.on_init(options)

func on_activate():
	self.get_native_node().set_source('x')
	self.get_native_node().centered = false
	var size = self.get_native_node().get_size()
	self.resize(size.x, size.y)
