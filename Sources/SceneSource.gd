# This source allows rendering an existing source into it.

class_name SceneSource
extends TransformableSource


var _source: Scene


static func icon() -> String:
	return "res://Sources/Icons/SceneSource.png"

static func name() -> String:
	return "Scene Source"

static func properties() -> Dictionary:
	return {
		"source": {
			"type": "scene",
			"label": "Source",
		},
	}

static func get_native_node_class():
	return Sprite2D

func get_native_size() -> Vector2:
	var sprite: Sprite2D = self.get_native_node()
	var texture: ViewportTexture = sprite.get_texture()
	if texture:
		return texture.get_size()

	return super.get_native_size()

func set_source(scene: Scene):
	var sprite: Sprite2D = self.get_native_node()
	var texture = scene.get_viewport().get_texture()
	sprite.set_texture(texture)
	sprite.centered = false
	sprite.texture_filter = self.get_viewport_node().get_parent().texture_filter

	self._source = scene
	self.recalculate_size()

func set_filter_mode(value: String):
	super.set_filter_mode(value)
	var sprite: Sprite2D = self.get_native_node()
	sprite.texture_filter = self.get_viewport_node().get_parent().texture_filter

func get_source() -> Scene:
	return self._source
