# This source wraps an image file.

class_name ImageSource
extends TransformableSource


var _source: String


static func icon() -> String:
	return "res://Sources/Icons/ImageSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/ImageSource.small.png"

static func name() -> String:
	return "Image Source"

static func properties() -> Dictionary:
	return {
		"source": {
			"type": "file",
			"label": "Source",
			"filter": ["*.png ; PNGs", "*.svg ; SVGs", "*.jpg,*.jpeg ; JPEGs"]
		}
	}

static func get_native_node_class():
	return Sprite2D

func get_native_size() -> Vector2:
	var sprite: Sprite2D = self.get_native_node()
	var texture: ImageTexture = sprite.get_texture()
	if texture:
		return texture.get_size()

	return super.get_native_size()

func set_source(path: String):
	var sprite: Sprite2D = self.get_native_node()
	var image = Image.new()
	var err = image.load(path)
	self._source = path
	if err != OK:
		# Failed
		print("failed")
	var texture = ImageTexture.create_from_image(image)
	sprite.set_texture(texture)
	sprite.centered = false
	sprite.texture_filter = self.get_viewport_node().get_parent().texture_filter

	var size = texture.get_size()
	self.resize(size.x, size.y)

func set_filter_mode(value: String):
	super.set_filter_mode(value)
	var sprite: Sprite2D = self.get_native_node()
	sprite.texture_filter = self.get_viewport_node().get_parent().texture_filter

func get_source() -> String:
	return self._source

func resize(width: float, height: float):
	# Resize the native widget based on the resize
	# But only if the widget has the means to understand that.
	var sprite: Sprite2D = self.get_native_node()

	# Basically, do not allow the image to be resized, if it is a raster
	# image.
	# TODO: allow scaling anyway, particularly for vector images
	var native_size: Vector2 = sprite.get_texture().get_size()
	width = native_size.x
	height = native_size.y

	super.resize(width, height)

func on_init(_options: Dictionary = {}):
	self.set_source("e:/camera_overlay.png")
