# This wraps a Camera3D from a World and allows rendering a 3D scene into the
# 2D plane of a Scene as a Source.

class_name CameraSource
extends TransformableSource


var _camera: Camera3D
var _viewport: SubViewport


static func icon() -> String:
	return "res://Sources/Icons/CameraSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/CameraSource.small.png"

static func name() -> String:
	return "Camera Source"

static func properties() -> Dictionary:
	return {
		"color": {
			"type": "color",
			"label": "Color"
		}
	}

static func get_native_node_class():
	return SubViewportContainer

func get_native_size() -> Vector2:
	if self._viewport:
		return self._viewport.size

	return Vector2(2.0, 2.0)

func resize(width: float, height: float):
	self._viewport.size = Vector2(width, height)
	super.resize(width, height)

func on_init(options: Dictionary = {}):
	self._size = Vector2(512.0, 512.0)
	super.on_init(options)

	self._viewport = SubViewport.new()
	self._viewport.size = self._size
	self._viewport.transparent_bg = true

	var camera: Camera3D = Camera3D.new()
	self._camera = camera;

	# Do not render the grid/gizmos
	camera.cull_mask = 0xffff
	self._viewport.add_child(camera)

	self.get_native_node().add_child(self._viewport)

func on_activate():
	var studio_node: Node = self.get_native_node().get_tree().get_root().get_child(0)
	var world_viewport = studio_node.get_node("%WorldPane/WorldViewport/SubViewport")
	self._viewport.set_world_3d(world_viewport.get_world_3d())
	var source_camera = studio_node.get_node("%WorldPane/WorldViewport/SubViewport/World/Camera3D")
	self._camera.set_transform(source_camera.get_transform())
