# This source captures an application window.

class_name WindowSource
extends TransformableSource


var _source: String


static func icon() -> String:
	return "res://Sources/Icons/WindowSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/WindowSource.small.png"

static func name() -> String:
	return "Window Source"

static func properties() -> Dictionary:
	return {
	}

static func get_native_node_class():
	return Sprite2D

func get_native_size() -> Vector2:
	var sprite: Sprite2D = self.get_native_node()
	var texture: ImageTexture = sprite.get_texture()
	if texture:
		return texture.get_size()

	return super.get_native_size()

func set_source(path: String):
	self._source = path
	pass

	self.recalculate_size()

func set_filter_mode(value: String):
	super.set_filter_mode(value)
	var sprite: Sprite2D = self.get_native_node()
	sprite.texture_filter = self.get_viewport_node().get_parent().texture_filter

func get_source() -> String:
	return self._source

func on_init(_options: Dictionary = {}):
	self.set_source("Notepad")
