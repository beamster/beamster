# This is a simple colored rectangle.

class_name OverlaySource
extends TransformableSource


static func icon() -> String:
	return "res://Sources/Icons/ColorSource.png"

static func icon_small() -> String:
	return "res://Sources/Icons/ColorSource.small.png"

static func name() -> String:
	return "Color Source"

static func properties() -> Dictionary:
	return {
	}

static func get_native_node_class():
	return Sprite2D

func on_init(_options: Dictionary = {}):
	var sprite: Sprite2D = self.get_native_node()
	var shader: Shader = Shader.new()
	shader.code = "shader_type canvas_item;

uniform sampler2D SCREEN : hint_screen_texture, repeat_disable, filter_nearest;

void fragment() {
	COLOR = textureLod(SCREEN, SCREEN_UV, 0.0);
}"
	var material: ShaderMaterial = ShaderMaterial.new()
	material.shader = shader
	var texture: GradientTexture1D = GradientTexture1D.new()
	sprite.texture = texture
	sprite.material = material
	sprite.centered = false
	sprite.texture_filter = self.get_viewport_node().get_parent().texture_filter
