extends TabContainer


var _source_tree : VBoxContainer
var _element_tree : Tree

func show_source_tree(source_tree: VBoxContainer, action_tree: Tree):
	for child in self.get_children():
		self.set_tab_hidden(child.get_index(), true)
	self.set_tab_hidden(source_tree.get_index(), false)
	self.set_tab_hidden(action_tree.get_index(), false)
	self.current_tab = source_tree.get_index()
	self.move_child(source_tree, 0)
	self.move_child(action_tree, 1)

	# Clear properties from last selected source
	$%PropertyTree.clear()

	# Retain which source list is active
	self._source_tree = source_tree

	# Re-select item
	self._source_tree.get_node("SourceTree").on_item_selected()

func show_element_tree(element_tree: Tree):
	for child in self.get_children():
		self.set_tab_hidden(child.get_index(), true)
	self.set_tab_hidden(element_tree.get_index(), false)
	self.current_tab = element_tree.get_index()

	# Clear properties from last selected source
	$%PropertyTree.clear()

	# Retain which element list is active
	self._element_tree = element_tree

	# Re-select item
	self._element_tree.on_item_selected()

func show_no_tree():
	for child in self.get_children():
		self.set_tab_hidden(child.get_index(), true)
	self.current_tab = -1

	# Clear properties from last selected source
	$%PropertyTree.clear()

func add_source_tree(source_tree: VBoxContainer) -> VBoxContainer:
	self.add_child(source_tree)
	self.move_child(source_tree, 0)

	var index: int = source_tree.get_index()
	self.set_tab_title(index, "Sources")
	source_tree.custom_minimum_size.x = 250
	#tree.custom_minimum_size.x = 250

	return source_tree

func add_action_tree(action_tree: Tree) -> Tree:
	self.add_child(action_tree)
	self.move_child(action_tree, 1)

	var index: int = action_tree.get_index()
	self.set_tab_title(index, "Actions")
	action_tree.custom_minimum_size.x = 250

	return action_tree

func add_source(source: Source.SourceInstance) -> TreeItem:
	return self._source_tree.get_node("SourceTree").add_source(source)

func delete_source(source: Source.SourceInstance):
	self._source_tree.delete_source(source)

func add_element_tree() -> Tree:
	var element_tree = $ElementTree.duplicate()
	self.add_child(element_tree)
	self.move_child(element_tree, 0)

	var index: int = element_tree.get_index()
	self.set_tab_title(index, "Elements")

	return element_tree

func add_element(element: Element) -> TreeItem:
	if self._element_tree:
		return self._element_tree.add_element(element)

	return null

func delete_element(element: Element):
	self._element_tree.delete_element(element)
