# This represents the 2D positioning widget.

# This widget controls the resize, move, rotation, etc of a 2D source object
# that is transformable (can move, etc. That is, extends TransformableSource.)

# The positioning widget consists of a visual node that acts as an overlay on
# top of the actual source object. It, itself, has several control nodes that
# act as interactable points to engage in resizing, moving, rotating, etc.

# Specifically, there are control nodes at each corner. These allow resizing in
# two axes. The borders are interactable. They control resizing in just one
# axis.

# The "bounding box" for the source dictates how the visual rendering of the
# source relates to the "native" representation of the source. For instance, an
# image might be 640px by 480px, but the source instance is resized to a 300px
# square. If the source is marked to "fit" the contents to the bounding box, the
# actual source dimensions are useful to visually indicate. The "origin" box
# is placed within the bounding box as a dashed-lined box to show what content
# is coming from the original source.

class_name TransformableSourceGizmo
extends SourceGizmo


var _borderTop: Polygon2D
var _borderBottom: Polygon2D
var _borderLeft: Polygon2D
var _borderRight: Polygon2D
var _nodes: Array[Polygon2D] = []
var _originBoxNodes: Array[Polygon2D] = []

# The instanced size of the source's bounding box
var _size: Vector2

# The last calculated native size of the instance box (accounting for crop)
var _native_size: Vector2
var _position: Vector2
var _skew_x: float = 0.0
var _skew_y: float = 0.0
var _rotate: float = 0.0

# Crop dimensions
var _crop_tl: Vector2
var _crop_br: Vector2

var _onmove: Callable = func(_new_position: Vector2): pass
var _onresize: Callable = func(_new_size: Vector2): pass
var _prop_item_left: TreeItem
var _prop_item_top: TreeItem
var _prop_item_width: TreeItem
var _prop_item_height: TreeItem
var _origin_box: OriginBox

# Tracking mouse events for resizing, etc

# Whether or not we are already moving/resizing (recursion detection)
var _resizing: bool = false
var _moving: bool = false

# Tracks the mouse position when a resize or move starts
var _start: Vector2

# Tracks the original position before a move or resize
var _original_position: Vector2

# Tracks the original size before a resize
var _original_size: Vector2

# Tracks the aspect ratio when starting an operation
var _original_scale: Vector2

# Tracks the rotation when starting an operation
var _original_rotation: float

# The control point that is being interacted with
var _control_point: Polygon2D

# The last size we applied to the source during a resize
var _last_new_size: Vector2

# The different modes with which we can be interacting with the source
enum TransformMode {
	None = 0,
	Moving = 1,
	Resizing = 2,
	Cropping = 3,
	Rotating = 4,
	Skewing = 5,
}

# What transform mode we are currently undergoing
var _transform_mode: TransformMode = TransformMode.None


class OriginBox:
	extends Node2D

	var _crop_tl: Vector2
	var _crop_br: Vector2
	var _size: Vector2
	var _position: Vector2
	var _points: PackedVector2Array = [
		Vector2(0, 0),
		Vector2(0, 0),
		Vector2(0, 0),
		Vector2(0, 0),
	]

	func _draw():
		# Draw a dashed line around the edges of the bounding box
		var uncropped_clr: Color = Color(1.0, 0.35, 0.35, 1.0)
		var crop_clr: Color = Color(0.35, 1.0, 0.35, 1.0)
		# Top line
		var clr: Color = uncropped_clr if self._crop_tl.y == 0.0 else crop_clr
		draw_dashed_line(self._points[0], self._points[1], clr, 5, 10, false)
		# Right line
		clr = uncropped_clr if self._crop_br.x == 0.0 else crop_clr
		draw_dashed_line(self._points[1], self._points[2], clr, 5, 10, false)
		# Bottom line
		clr = uncropped_clr if self._crop_br.y == 0.0 else crop_clr
		draw_dashed_line(self._points[2], self._points[3], clr, 5, 10, false)
		# Left line
		clr = uncropped_clr if self._crop_tl.x == 0.0 else crop_clr
		draw_dashed_line(self._points[3], self._points[0], clr, 5, 10, false)

	func get_size() -> Vector2:
		return self._size

	func get_calculated_position() -> Vector2:
		return self._position

func calculate_origin_box():
	var origin_box: OriginBox = self.get_origin_box()

	var current_position: Vector2
	var current_size: Vector2 = self._size

	var native_size: Vector2 = Vector2(1.0, 1.0)
	if self.source:
		native_size = self.source.get_native_size()

		# Account for the cropping
		native_size -= self.source._crop_tl + self.source._crop_br

	var aspect_ratio_x: float = native_size.x / native_size.y
	var aspect_ratio_y: float = native_size.y / native_size.x
	var box_ratio_x: float = self._size.x / self._size.y

	var fill: String = "fit"
	if self.source:
		fill = self.source.get_bounding_box_fill()

	if fill == "fit":
		# We either keep the width or height of the content
		# and calculate the other with respect to the aspect ratio.
		if aspect_ratio_x > box_ratio_x:
			fill = "fit width"
		else:
			fill = "fit height"

	if fill == "actual size":
		# Self-explanatory
		current_size = native_size
		current_position = Vector2(0.0, 0.0)
	elif fill == "fit width":
		# Always keep the width
		current_size.x = self._size.x

		# Calculate height with respect to original aspect ratio
		current_size.y = aspect_ratio_y * self._size.x
	elif fill == "fit height":
		# Always keep the height
		current_size.y = self._size.y

		# Calculate width with respect to original aspect ratio
		current_size.x = aspect_ratio_x * self._size.y
	elif fill == "stretch":
		# Fill the whole box
		current_size = self._size
		current_position = Vector2(0.0, 0.0)
	elif fill == "stretch width":
		# Fill width
		current_position.x = 0.0
		current_size.x = self._size.x

		# Keep height
		current_size.y = native_size.y
	elif fill == "stretch height":
		# Fill height
		current_position.y = 0.0
		current_size.y = self._size.y

		# Keep width
		current_size.x = native_size.x

	# Now determine the position
	var alignment = "center"
	if self.source:
		alignment = self.source.get_bounding_box_alignment()

	if alignment == "top left":
		current_position = Vector2(0.0, 0.0)
	elif alignment == "top center":
		current_position = Vector2((self._size.x - current_size.x) / 2.0, 0.0)
	elif alignment == "top right":
		current_position = Vector2(self._size.x - current_size.x, 0.0)
	elif alignment == "center left":
		current_position = Vector2(0.0, (self._size.y - current_size.y) / 2.0)
	elif alignment == "center":
		current_position = Vector2((self._size.x - current_size.x) / 2.0, (self._size.y - current_size.y) / 2.0)
	elif alignment == "center right":
		current_position = Vector2(self._size.x - current_size.x, (self._size.y - current_size.y) / 2.0)
	elif alignment == "bottom left":
		current_position = Vector2(0.0, self._size.y - current_size.y)
	elif alignment == "bottom center":
		current_position = Vector2((self._size.x - current_size.x) / 2.0, self._size.y - current_size.y)
	elif alignment == "bottom right":
		current_position = Vector2(self._size.x - current_size.x, self._size.y - current_size.y)

	origin_box._crop_tl = self._crop_tl
	origin_box._crop_br = self._crop_br
	origin_box._position = current_position
	origin_box.position = Vector2(0, 0)
	origin_box._points = self.apply_transforms(self._size, [
		Vector2(current_position.x, current_position.y),
		Vector2(current_position.x + current_size.x, current_position.y),
		Vector2(current_position.x + current_size.x, current_size.y + current_position.y),
		Vector2(current_position.x, current_size.y + current_position.y),
	])
	origin_box._size = current_size

	if fill == "keep ratio":
		origin_box.visible = false
	else:
		origin_box.visible = true

	origin_box.queue_redraw()

	for i in 4:
		self._originBoxNodes[i].position = origin_box._points[i]

	if self.source:
		var current_scale: Vector2 = current_size / native_size
		if native_size.x <= 0.0:
			current_scale.x = 0.0
		if native_size.y <= 0.0:
			current_scale.y = 0.0

		var tl_adjust: Vector2 = self._crop_tl * current_scale

		var container_size: Vector2 = self.source._sub_viewport.size
		self.transform = Transform2D(
			0, Vector2(-1.0 if self.source._flip_h else 1.0, -1.0 if self.source._flip_v else 1.0), self.source._skew_x + self.source._skew_y, -container_size / 2.0 + tl_adjust
		).rotated(deg_to_rad(self.source._rotate) - self.source._skew_y).translated(self.source._container.position - tl_adjust + container_size / 2)

		#self.position = self.source._position - tl_adjust + container_size / 2

func get_origin_box() -> OriginBox:
	if not self._origin_box:
		self._origin_box = OriginBox.new()
		self.calculate_origin_box()

	return self._origin_box

func set_source(value: Source.SourceInstance):
	super.set_source(value)

	# Now, resize, etc
	var transform_source: TransformableSource.TransformableSourceInstance = value
	self.resize(transform_source._size)
	self.move(transform_source._container.position)

	transform_source.moved.connect(func():
		self.move(transform_source._container.position, true)
	)

	transform_source.resized.connect(func():
		self.resize(transform_source._size, true)
	)

func _init():
	# Create structure
	var polygon: Polygon2D = null

	# Border Top
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#5b32df", Color.BLUE_VIOLET)
	polygon.polygon = [
		Vector2(0, 0),
		Vector2(100, 0),
		Vector2(100, 6),
		Vector2(0, 6),
	]
	polygon.set_meta("position_scale_x", 0)
	polygon.set_meta("position_scale_y", 1)
	polygon.set_meta("size_scale_x", 0)
	polygon.set_meta("size_scale_y", 1)
	polygon.set_meta("crop_tl_scale_x", 0)
	polygon.set_meta("crop_tl_scale_y", 1)
	polygon.set_meta("crop_br_scale_x", 0)
	polygon.set_meta("crop_br_scale_y", 0)
	polygon.set_meta("cursor", 9)
	self._borderTop = polygon
	self.add_child(polygon)

	# Border Right
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#5b32df", Color.BLUE_VIOLET)
	polygon.polygon = [
		Vector2(0, 0),
		Vector2(6, 0),
		Vector2(6, 100),
		Vector2(0, 100),
	]
	polygon.set_meta("position_scale_x", 0)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", -1)
	polygon.set_meta("size_scale_y", 0)
	polygon.set_meta("crop_tl_scale_x", 0)
	polygon.set_meta("crop_tl_scale_y", 0)
	polygon.set_meta("crop_br_scale_x", 1)
	polygon.set_meta("crop_br_scale_y", 0)
	polygon.set_meta("cursor", 10)
	self._borderRight = polygon
	self.add_child(polygon)

	# Border Bottom
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#5b32df", Color.BLUE_VIOLET)
	polygon.polygon = [
		Vector2(0, 0),
		Vector2(100, 0),
		Vector2(100, 6),
		Vector2(0, 6),
	]
	polygon.set_meta("position_scale_x", 0)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 0)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("crop_tl_scale_x", 0)
	polygon.set_meta("crop_tl_scale_y", 0)
	polygon.set_meta("crop_br_scale_x", 0)
	polygon.set_meta("crop_br_scale_y", 1)
	polygon.set_meta("cursor", 9)
	self._borderBottom = polygon
	self.add_child(polygon)

	# Border Left
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#5b32df", Color.BLUE_VIOLET)
	polygon.polygon = [
		Vector2(0, 0),
		Vector2(6, 0),
		Vector2(6, 100),
		Vector2(0, 100),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", 0)
	polygon.set_meta("crop_tl_scale_x", 1)
	polygon.set_meta("crop_tl_scale_y", 0)
	polygon.set_meta("crop_br_scale_x", 0)
	polygon.set_meta("crop_br_scale_y", 0)
	polygon.set_meta("cursor", 10)
	self._borderLeft = polygon
	self.add_child(polygon)

	# Node 1 (Top Left)
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#6f55f4", Color.MAGENTA)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 1)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", 1)
	polygon.set_meta("crop_tl_scale_x", 1)
	polygon.set_meta("crop_tl_scale_y", 1)
	polygon.set_meta("crop_br_scale_x", 0)
	polygon.set_meta("crop_br_scale_y", 0)
	polygon.set_meta("cursor", 12)
	self._nodes.append(polygon)
	self.add_child(polygon)

	# Node 2 (Top Right)
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#6f55f4", Color.MAGENTA)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 0)
	polygon.set_meta("position_scale_y", 1)
	polygon.set_meta("size_scale_x", -1)
	polygon.set_meta("size_scale_y", 1)
	polygon.set_meta("crop_tl_scale_x", 0)
	polygon.set_meta("crop_tl_scale_y", 1)
	polygon.set_meta("crop_br_scale_x", 1)
	polygon.set_meta("crop_br_scale_y", 0)
	polygon.set_meta("cursor", 11)
	self._nodes.append(polygon)
	self.add_child(polygon)

	# Node 3 (Bottom Right)
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#6f55f4", Color.MAGENTA)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 0)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", -1)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("crop_tl_scale_x", 0)
	polygon.set_meta("crop_tl_scale_y", 0)
	polygon.set_meta("crop_br_scale_x", 1)
	polygon.set_meta("crop_br_scale_y", 1)
	polygon.set_meta("cursor", 12)
	self._nodes.append(polygon)
	self.add_child(polygon)

	# Node 4 (Bottom Left)
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#6f55f4", Color.MAGENTA)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("crop_tl_scale_x", 1)
	polygon.set_meta("crop_tl_scale_y", 0)
	polygon.set_meta("crop_br_scale_x", 0)
	polygon.set_meta("crop_br_scale_y", 1)
	polygon.set_meta("cursor", 11)
	self._nodes.append(polygon)
	self.add_child(polygon)

	# Origin Box Node 1
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#d5007b", Color.RED)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("cursor", 11)
	self._originBoxNodes.append(polygon)
	self.add_child(polygon)

	# Origin Box Node 2
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#d5007b", Color.RED)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("cursor", 11)
	self._originBoxNodes.append(polygon)
	self.add_child(polygon)

	# Origin Box Node 3
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#d5007b", Color.RED)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("cursor", 11)
	self._originBoxNodes.append(polygon)
	self.add_child(polygon)

	# Origin Box Node 4
	polygon = Polygon2D.new()
	polygon.color = Color.from_string("#d5007b", Color.RED)
	polygon.polygon = [
		Vector2(-5, -5),
		Vector2(5, -5),
		Vector2(5, 5),
		Vector2(-5, 5),
	]
	polygon.set_meta("position_scale_x", 1)
	polygon.set_meta("position_scale_y", 0)
	polygon.set_meta("size_scale_x", 1)
	polygon.set_meta("size_scale_y", -1)
	polygon.set_meta("cursor", 11)
	self._originBoxNodes.append(polygon)
	self.add_child(polygon)

func _ready():
	self.set_meta("visible", true)
	self.set_meta("locked", false)
	self.add_child(self.get_origin_box())

func intersects_point(point: Vector2):
	var left = self.get_transform().get_origin().x
	var top = self.get_transform().get_origin().y
	var right = left + self._size.x
	var bottom = top + self._size.y

	# Account for the borders
	left = min(left, left + self._nodes[0].polygon[0].x)
	top = min(top, top + self._nodes[0].polygon[0].y)
	right = max(right, right + self._nodes[3].polygon[2].x)
	bottom = max(bottom, bottom + self._nodes[3].polygon[2].y)

	if left <= point.x and right > point.x:
		if top <= point.y and bottom > point.y:
			return true

	return false

func intersects_control_point(point: Vector2):
	point -= Vector2(
		self.get_transform().get_origin().x,
		self.get_transform().get_origin().y
	)

	for child in self.get_children():
		if child is OriginBox:
			continue
		var left = child.get_transform().get_origin().x + child.polygon[0].x
		var top = child.get_transform().get_origin().y + child.polygon[0].y
		var right = child.get_transform().get_origin().x + child.polygon[2].x
		var bottom = child.get_transform().get_origin().y + child.polygon[2].y

		if left <= point.x and right > point.x:
			if top <= point.y and bottom > point.y:
				return child

	return null

func resize(size: Vector2, silent: bool = false):
	if self._resizing:
		return

	self._resizing = true

	var native_size: Vector2 = Vector2(1.0, 1.0)
	var fill = "fit"
	if self.source:
		native_size = self.source.get_native_size()

		# Account for the cropping
		native_size -= self.source._crop_tl + self.source._crop_br

		# Get the current fill for the instance
		fill = self.source.get_bounding_box_fill()

	if fill == "keep ratio":
		var aspect_ratio_x: float = native_size.x / native_size.y
		var aspect_ratio_y: float = native_size.y / native_size.x

		# Determine the new size based on the old size
		if self._native_size.y != native_size.y or self._size.x != size.x:
			# New width, calculate the new height
			size.y = size.x * aspect_ratio_y
		elif self._native_size.x != native_size.x or self._size.y != size.y:
			# New height, calculate the new width
			size.x = size.y * aspect_ratio_x

		# If we are currently resizing, we should ensure we run through the
		# cursor we are dragging if we are dragging at a corner.
		if self._transform_mode == TransformMode.Resizing:
			# If it is a corner control point... (sizing on both axes)
			if self._control_point.get_meta("size_scale_x", 0) != 0 and \
			   self._control_point.get_meta("size_scale_y", 0) != 0:
				# The resulting size must run through the dimensions of the size
				# requested in some way.
				if size.x < self._last_new_size.x:
					size.x = self._last_new_size.x
					size.y = size.x * aspect_ratio_y
				elif size.y < self._last_new_size.y:
					size.y = self._last_new_size.y
					size.x = size.y * aspect_ratio_x

		self._size = size

		# Turn off origin box calculations
		var origin_box = self.get_origin_box()
		origin_box.visible = false
		origin_box._size = size
		origin_box._position = Vector2(0, 0)
	else:
		# Set the bounding box size
		self._size = size

		# Negotiate origin box based on fill
		self.get_origin_box().visible = true
		self.calculate_origin_box()

	# Resize source itself
	if self.source:
		self.source.resize(self._size)
		self.source.set_property('bounding_box_width', self.source.get_bounding_box_width(), silent)
		self.source.set_property('bounding_box_height', self.source.get_bounding_box_height(), silent)

	self._native_size = native_size

	self._on_resize()
	self._resizing = false

func get_size() -> Vector2:
	return self._size

func apply_transform(size: Vector2, point: Vector2) -> Vector2:
	var ret: Vector2 = point - (size / 2)

	# Apply skew x
	var skew_x = self._skew_x + self._skew_y
	ret += Vector2(-sin(skew_x), cos(skew_x) - 1) * Vector2(ret.y * 2, ret.y * 2) / 2

	# Get proper rotation and apply that as well
	var current_rotate = self._rotate - self._skew_y
	ret = Vector2(
		ret.x * cos(current_rotate) - ret.y * sin(current_rotate),
		ret.x * sin(current_rotate) + ret.y * cos(current_rotate)
	)

	# Apply the transform back
	ret += size / 2

	# Supply result
	return ret

func apply_transforms(size: Vector2, points: PackedVector2Array = []) -> PackedVector2Array:
	var ret: PackedVector2Array = points

	if ret.size() == 0:
		ret = [
			Vector2(0, 0),
			Vector2(size.x, 0),
			size,
			Vector2(0, size.y)
		]

	for i in ret.size():
		ret[i] = self.apply_transform(size, ret[i])

	# Supply result
	return ret

# Returns true if the current mouse position is within the gizmo bounds.
func is_hovered() -> bool:
	# Ignore an instance where the gizmo is source-less
	# (This happens when creating a source using the gizmo)
	if not self.source:
		return false

	# Ignore any invisible sources
	if not self.source.get_visible():
		return false

	# Ignore hovering over a locked source
	if self.source.get_position_lock():
		return false

	# We are hovered if the mouse is within the bounds of the gizmo.
	return self.intersects_point(
		self.get_local_mouse_position() + self.get_transform().get_origin()
	)

# Called when input is received within an editor view.
func input(event: InputEvent, mouse_position: Vector2):
	# Handle a button event
	if event is InputEventMouseButton:
		var button_event: InputEventMouseButton = event

		if button_event.is_pressed() and button_event.button_index == MOUSE_BUTTON_LEFT:
			# Retain the original positions and size
			self._original_position = self._position
			self._original_size = self._size
			self._original_scale = self._native_size / self._size

			# Store the current position as a reference
			self._start = mouse_position

			# Clear our cached resize vector
			self._last_new_size = Vector2(INF, INF)

			# Determine if we have clicked on a resize control point
			var control_point: Polygon2D = self.intersects_control_point(
				self.get_local_mouse_position() + self.get_transform().get_origin()
			)

			# If we are clicking on a control point, we do a particular thing
			if control_point:
				# We are resizing or cropping...
				self._transform_mode = TransformMode.Resizing
				self._control_point = control_point
				self.cursor = control_point.get_meta("cursor", Control.CURSOR_MOVE)

				# We crop when ALT is pressed
				if button_event.alt_pressed:
					self._transform_mode = TransformMode.Cropping

					# Determine if the control point we are using keeps the top left or
					# bottom right crop point. We need to set our original_size to the
					# crop vector that is changing.
					if self._control_point.get_meta("crop_tl_scale_x", 0) + \
					   self._control_point.get_meta("crop_tl_scale_y", 0) > 0:
						self._original_size = self._crop_tl * -1.0
					else:
						self._original_size = self._crop_tl + self._native_size
				elif button_event.ctrl_pressed:
					# We skew when CTRL is pressed over a line
					self._transform_mode = TransformMode.Skewing
					if self.source:
						if self._control_point.get_meta("size_scale_x", 0) != 0:
							self._original_rotation = self.source.get_skew_y()
						else:
							self._original_rotation = self.source.get_skew_x()
					else:
						self._original_rotation = 0.0
			elif button_event.ctrl_pressed:
				# We rotate when CTRL is pressed over the source itself
				self._transform_mode = TransformMode.Rotating
				if self.source:
					self._original_rotation = self.source.get_rotate()
				else:
					self._original_rotation = 0.0
				self._control_point = null
				self.cursor = Control.CURSOR_MOVE
			else:
				# Otherwise, we are moving...
				self._transform_mode = TransformMode.Moving
				self._control_point = null
				self.cursor = Control.CURSOR_MOVE

			# Select the source on click, no matter what
			var source_instance: SerializedObject.SerializedInstance = self.source
			var source_item: TreeItem = source_instance.get_tree_items().front()
			source_item.select(0)

			# Capture any other input events
			self.capture_input = true
		elif button_event.button_index == MOUSE_BUTTON_LEFT:
			# Release the button, stop moving / resizing / etc
			self._transform_mode = TransformMode.None
			self._control_point = null

			# Stop capturing the input events
			self.capture_input = false

	# Handle a motion event
	if event is InputEventMouseMotion:
		# If we are moving a source
		if self._transform_mode == TransformMode.Moving:
			# Determine the new position
			var delta: Vector2 = mouse_position - self._start
			var new_position: Vector2 = self._original_position + delta

			# Apply the 'snap'
			if self.snap and not event.shift_pressed:
				new_position = snapped(new_position, Vector2(self.snap_minor, self.snap_minor))

			# Move the source to the new location
			self.move(new_position)
		elif self._transform_mode == TransformMode.Resizing or \
			 self._transform_mode == TransformMode.Cropping:
			# We are resizing or cropping

			# How far have we moved
			var delta: Vector2 = mouse_position - self._start

			# Get the size scale factor for the control point
			var size_scale = Vector2(
				self._control_point.get_meta("size_scale_x", 0),
				self._control_point.get_meta("size_scale_y", 0)
			)

			# Determine the new size by interpreting the control point 'scale'
			var new_size: Vector2 = self._original_size - (delta * size_scale)

			# Apply any snapping
			if self.snap and not event.shift_pressed:
				new_size = snapped(new_size, Vector2(self.snap_minor, self.snap_minor))

			# The snap might put us in the same size we already were, so let's limit the events
			# getting triggered by not resizing if we are already this size.
			if self._last_new_size != new_size:
				self._last_new_size = new_size

				# Resize or crop, depending
				if self._transform_mode == TransformMode.Resizing:
					self.resize(new_size)

					# Get the position scale factor for the control point
					var position_scale = Vector2(
						self._control_point.get_meta("position_scale_x", 0),
						self._control_point.get_meta("position_scale_y", 0)
					)

					# Resizing from the top or left might, effectively, move the source
					var new_position: Vector2 = self._original_position + (delta * position_scale)

					# Apply any snapping
					if self.snap and not event.shift_pressed:
						new_position = snapped(new_position, Vector2(self.snap_minor, self.snap_minor))

					# Actually move the source
					self.move(new_position)
				else:
					# Recompute with scalar
					new_size = self._original_size - (delta * size_scale * self._original_scale)

					# Determine if the control point we are using keeps the top left or
					# bottom right crop point.
					if self._control_point.get_meta("crop_tl_scale_x", 0) + \
					   self._control_point.get_meta("crop_tl_scale_y", 0) > 0:
						new_size *= -1.0

						# Only apply the dimensions allowed via the control point's metadata
						if self._control_point.get_meta("crop_tl_scale_y", 0) > 0:
							self.source.set_crop_top(
								max(0, new_size.y)
							)

						if self._control_point.get_meta("crop_tl_scale_x", 0) > 0:
							self.source.set_crop_left(
								max(0, new_size.x)
							)
					else:
						if self.source:
							var native_size = self.source.get_native_size()
							var new_crop_br = native_size - new_size

							# Only apply the dimensions allowed via the control point's metadata
							if self._control_point.get_meta("crop_br_scale_y", 0) > 0:
								self.source.set_crop_bottom(
									max(0, new_crop_br.y)
								)

							if self._control_point.get_meta("crop_br_scale_x", 0) > 0:
								self.source.set_crop_right(
									max(0, new_crop_br.x)
								)
		elif self._transform_mode == TransformMode.Rotating:
			var container_size: Vector2 = self.source._sub_viewport.size
			var origin: Vector2 = self._original_position + (container_size / 2.0)

			var original_angle = origin.angle_to_point(self._start)
			var angle = original_angle - origin.angle_to_point(mouse_position)
			angle = TAU - angle
			var new_rotate = fmod(self._original_rotation + rad_to_deg(angle) + 360.0, 360.0)

			if self.snap and not event.shift_pressed:
				new_rotate = snapped(new_rotate, self.angle_snap)

			if self.source:
				self.source.set_rotate(new_rotate)
		elif self._transform_mode == TransformMode.Skewing:
			var container_size: Vector2 = self.source._sub_viewport.size
			var origin: Vector2 = self._original_position + (container_size / 2.0)

			var original_angle = origin.angle_to_point(self._start)
			var angle = original_angle - origin.angle_to_point(mouse_position)
			var new_skew = fmod(rad_to_deg(self._original_rotation + angle) + 360, 360) - 360.0

			if self.snap and not event.shift_pressed:
				new_skew = snapped(new_skew, self.angle_snap)

			if self.source:
				if self._control_point.get_meta("size_scale_x", 0) != 0:
					self.source.set_skew_y(deg_to_rad(new_skew))
				else:
					self.source.set_skew_x(deg_to_rad(new_skew))
		else:
			# Determine if we are hovering over a control point
			var control_point: Polygon2D = self.intersects_control_point(
				self.get_local_mouse_position() + self.get_transform().get_origin()
			)

			# Update cursor
			if control_point:
				self.cursor = control_point.get_meta("cursor", Control.CURSOR_MOVE)
			else:
				self.cursor = Control.CURSOR_MOVE

			# Stop capturing the input events
			self.capture_input = false

func crop(top_left: Vector2, bottom_right: Vector2):
	self._crop_tl = top_left
	self._crop_br = bottom_right

	self.resize(self._size)

func move(offset: Vector2, silent: bool = false):
	if self._moving or self._resizing:
		return

	self._moving = true

	self._position = offset

	self.resize(self._size, silent)

	# Set that position
	self.position = self._position

	# Move source itself
	if self.source:
		self.source.move(self._position)
		self.source.set_property('left', self.source.get_left(), silent)
		self.source.set_property('top', self.source.get_top(), silent)

	self._on_move()
	self._moving = false

func set_onmove(f: Callable):
	self._onmove = f

func set_onresize(f: Callable):
	self._onresize = f

func set_prop_item_left(propItem: TreeItem):
	self._prop_item_left = propItem

func set_prop_item_top(propItem: TreeItem):
	self._prop_item_top = propItem

func set_prop_item_width(propItem: TreeItem):
	self._prop_item_width = propItem

func set_prop_item_height(propItem: TreeItem):
	self._prop_item_height = propItem

func _on_resize():
	# Position control points
	var points: PackedVector2Array = self.apply_transforms(self._size)
	for i in 4:
		self._nodes[i].position = points[i]

	var polygon: PackedVector2Array = []

	var border_width: float = 5

	# Border colors
	var fill: String = "fit"
	if self.source:
		fill = self.source.get_bounding_box_fill()

	var uncropped_clr: Color = Color.from_string("#5b32df", Color.BLUE_VIOLET)
	var crop_clr: Color = uncropped_clr if fill != "keep ratio" else Color(0.35, 1.0, 0.35, 1.0)

	# Top border
	self._borderTop.position = Vector2(0, 0)
	self._borderTop.polygon = self._line_between(points[0], points[1], border_width)
	self._borderTop.color = uncropped_clr if self._crop_tl.y == 0.0 else crop_clr

	# Right border
	self._borderRight.position = Vector2(0, 0)
	self._borderRight.polygon = self._line_between(points[1], points[2], border_width)
	self._borderRight.color = uncropped_clr if self._crop_br.x == 0.0 else crop_clr

	# Bottom border
	self._borderBottom.position = Vector2(0, 0)
	self._borderBottom.polygon = self._line_between(points[2], points[3], border_width)
	self._borderBottom.color = uncropped_clr if self._crop_br.y == 0.0 else crop_clr

	# Left border
	self._borderLeft.position = Vector2(0, 0)
	self._borderLeft.polygon = self._line_between(points[3], points[0], border_width)
	self._borderLeft.color = uncropped_clr if self._crop_tl.x == 0.0 else crop_clr

	if self._prop_item_left:
		self._prop_item_left.set_range(1, self.get_transform().get_origin().x)
		self._prop_item_top.set_range(1, self.get_transform().get_origin().y)
		self._prop_item_width.set_range(1, self._size.x)
		self._prop_item_height.set_range(1, self._size.y)

func _line_between(a: Vector2, b: Vector2, width: float) -> PackedVector2Array:
	if b.y < a.y:
		var tmp = a
		a = b
		b = tmp

	return [
		Vector2(a.x + width if a.x > b.x else a.x - width, a.y + width),
		Vector2(a.x - width if a.x > b.x else a.x + width, a.y - width),
		Vector2(b.x - width if a.x > b.x else b.x + width, b.y - width),
		Vector2(b.x + width if a.x > b.x else b.x - width, b.y + width),
	]

func _on_move():
	if self._prop_item_left:
		self._prop_item_left.set_range(1, self.get_transform().get_origin().x)
		self._prop_item_top.set_range(1, self.get_transform().get_origin().y)
