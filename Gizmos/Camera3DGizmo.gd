class_name Camera3DGizmo
extends MeshInstance3D


# From the Godot source code
# editor/plugins/node_3d_editor_gizmos.cpp : 1844 : 8a98110e3e353a9a3b
# void Camera3DGizmoPlugin::redraw(EditorNode3DGizmo *p_gizmo) { ... }

# Add as a child to a camera and cull the gizmo layer.

var _mesh = ImmediateMesh.new()
var _material = ORMMaterial3D.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	var handles = []
	var vertices = []

	var ADD_TRIANGLE = func(a, b, c):
		vertices.append(a)
		vertices.append(b)
		vertices.append(b)
		vertices.append(c)
		vertices.append(c)
		vertices.append(a)

	var ADD_QUAD = func(a, b, c, d):
		vertices.append(a)
		vertices.append(b)
		vertices.append(b)
		vertices.append(c)
		vertices.append(c)
		vertices.append(d)
		vertices.append(d)
		vertices.append(a)

	var camera = self.get_parent()

	if camera and camera is Camera3D:
		var viewport_size = camera.get_viewport().size
		var viewport_aspect = viewport_size.aspect() if viewport_size.x > 0 and viewport_size.y > 0 else 1.0
		var size_factor = Vector2(1.0, 1.0 / viewport_aspect) if viewport_aspect > 1.0 else Vector2(viewport_aspect, 1.0)

		_mesh.surface_begin(Mesh.PRIMITIVE_LINES, _material)

		# For the camera's far plane, build the lines
		if camera.projection == Camera3D.PROJECTION_PERSPECTIVE:
			# The real FOV is halved for accurate representation
			var fov: float = camera.fov / 2.0

			var hsize: float = sin(deg_to_rad(fov))
			var depth: float = -cos(deg_to_rad(fov))
			var side: Vector3 = Vector3(hsize * size_factor.x, 0, depth)
			var nside: Vector3 = Vector3(-side.x, side.y, side.z)
			var up: Vector3 = Vector3(0, hsize * size_factor.y, 0)

			ADD_TRIANGLE.call(Vector3(), side + up, side - up)
			ADD_TRIANGLE.call(Vector3(), nside + up, nside - up)
			ADD_TRIANGLE.call(Vector3(), side + up, nside + up)
			ADD_TRIANGLE.call(Vector3(), side - up, nside - up)

			handles.append(side)
			side.x = min(side.x, hsize * 0.25)
			nside.x = -side.x
			var tup: Vector3 = Vector3(0, up.y + hsize / 2, side.z)
			ADD_TRIANGLE.call(tup, side + up, nside + up)
		elif camera.projection == Camera3D.PROJECTION_ORTHOGONAL:
			var size: float = camera.size

			var hsize: float = size * 0.5
			var right: Vector3 = Vector3(hsize * size_factor.x, 0, 0)
			var up: Vector3 = Vector3(0, hsize * size_factor.y, 0)
			var back: Vector3 = Vector3(0, 0, -1.0)
			var _front: Vector3 = Vector3(0, 0, 0)

			ADD_QUAD.call(-up - right, -up + right, up + right, up - right)
			ADD_QUAD.call(-up - right + back, -up + right + back, up + right + back, up - right + back)
			ADD_QUAD.call(up + right, up + right + back, up - right + back, up - right)
			ADD_QUAD.call(-up + right, -up + right + back, -up - right + back, -up - right)

			handles.append(right + back)

			right.x = min(right.x, hsize * 0.25)
			var tup: Vector3 = Vector3(0, up.y + hsize / 2, back.z)
			ADD_TRIANGLE.call(tup, right + up + back, -right + up + back)

		for v in vertices:
			_mesh.surface_add_vertex(v)

		self.layers = 0x80000
		_mesh.surface_end()

		_material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
		_material.albedo_color = Color("b380ffff")

	self.set_mesh( _mesh )

func _process(_delta):
	var camera = self.get_parent()

	if camera and camera is Camera3D:
		self.set_transform(camera.get_transform())
