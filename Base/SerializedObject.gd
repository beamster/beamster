# This represents anything serialized that can be saved and restored.

# Everything in the program that is added to a profile/scene/etc is a
# SerializedObject. So, Source, Element, etc, are all this way.

# A SerializedObject has two major aspects that are common to all:
#   1. They contain a set of properties that can be set.
#   2. They contain, optionally, a set of children.

# A SerializedObject can have one and only one owner. If an object is to
# have multiple parents, it would have to be "instantiated" in order to create
# a unique identifier and context for that child. When instantiated, what is
# created is a SerializedObject.SerializedInstance which has the original
# SerializedObject as its "base".

class_name SerializedObject
extends Object


var uuid: GDScript = preload("res://addons/uuid/uuid.gd")

var _id: String
var _name: String
var _children: Array = []
var _child_ids: Array = []
var _parents: Dictionary = {}
var _parent_ids: Array = []
var _instances: Array = []

# The owning parent for this object is created within
var _owner: SerializedObject


signal child_added(object: SerializedObject)
signal child_removed(object: SerializedObject)
signal updated(key: String, value: Variant)


# This represents an instance of a serialized object.
#
# Instances are unique copies per a parent container. They share the bulk of an
# object, but they have their own set of unique properties.
#
# One obvious example would be a "TransformableSource". This is when an object
# can appear in more than one context. The difference being _where_ the object
# resides within that container. The `left`, `top`, `width`, and `height`
# differ so that the same source (image, video, webcam) can be used in different
# scenes and share the bulk of the configuration, children, filters, etc, but
# appears in different places. The "transform" properties, therefore, are
# "instance properties". These properties are handled here.
class SerializedInstance:
	extends SerializedObject

	# The object this is an instance of
	var _base: SerializedObject

	# Keeps track of TreeItem objects that refer to this instance
	var _tree_items: Array[TreeItem] = []

	# A set of instanced properties.
	static func properties() -> Dictionary:
		return {}

	# A set of instanced properties where the set of properties differs from
	# instance to instance or even from base to base. These take precedence
	# over normal statically defined properties when serialized.
	func dynamic_properties() -> Dictionary:
		return {}

	# Instantiates an instance of the given object based on the given options.
	func _init(base: SerializedObject, options: Dictionary):
		# Ensure it gets the normal initialization
		super._init(options)

		# Retain a reference to the base class
		self._base = base

		# Call the 'init' event on the instance
		self.on_init(options)

	# Returns a copy of this instanced object
	func duplicate() -> SerializedObject:
		# Re-instantiate the base and set the properties
		var ret: SerializedObject = self.get_base().instantiate()

		# Copy and paste property sets
		ret.set_properties(self.serialize_properties())

		# Return the new instance
		return ret

	# Returns the base object.
	func get_base() -> SerializedObject:
		return self._base

	# Keeps track of TreeItem objects the refer to this object.
	func append_tree_item(item: TreeItem):
		self._tree_items.append(item)

	# Returns the list of TreeItem objects that refer to this object.
	func get_tree_items() -> Array[TreeItem]:
		return self._tree_items

	func on_remove(_parent: SerializedObject):
		for item in self._tree_items:
			item.get_tree().remove_item(item)

	# Return a serialized form the instance to serve as a child serialization.
	func serialize() -> Dictionary:
		var ret: Dictionary = super.serialize()
		ret.merge(self.serialize_minor(), true)
		return ret

	func serialize_minor() -> Dictionary:
		var ret: Dictionary = super.serialize_minor()
		ret["type"] = self._base.system_name()
		ret["name"] = self._base._name
		ret["base"] = {
			"id": self._base._id,
			"name": self._base._name,
			"type": self._base.system_name(),
		}
		return ret

# Basic constructor
#
# The serialized form can be passed to realize the object from that state.
func _init(options: Dictionary = {}):
	# Determine the global identifier (or re-use the existing one)
	if not options.has("id"):
		self._id = self.uuid.v4()

	# Get a default name for the component
	if not options.has("name"):
		self._name = self.get_script().name()

	# Restore any given serialization
	self.restore(options)

# Serializes the object to a dictionary that can be used to restore the object.
#
# This serialization has several common properties:
# - `id` is the unique global identifier for the object
# - `name` is the human name for the object
# - `type` is the canonical type of the object
# - `children` is an ordered listing of children
# - `properties` is the serialized collection of properties.
#
# Each child in `children` is a shallow block of metadata consisting of the
# object id, name, and type. The `name` should not be considered canonical. The
# actual name is the name recorded by the object that "owns" the child object.
#
# The child information also contains `properties` which consist of the values
# for instanced properties of the child object. For instance, transform
# properties (left, top, width, height) are instanced.
#
# Properties found as part of children or as part of the object itself are
# unordered and appear as a key/value dictionary. The keys of each property
# will match those described by the `properties` function of each object's
# implementation class.
func serialize() -> Dictionary:
	# This is the basic serialization
	var ret: Dictionary = self.serialize_minor()

	# Add children
	if self._children.size() > 0:
		for child in self._children:
			# Ignore children that have no 'id'
			if not child.has("id"):
				continue

			var childInfo: Dictionary = {
				"id": child["id"],
				"name": child.get("name", "Unknown"),
				"type": child.get("type", "Unknown"),
			}

			# Retain instance properties, if any
			if child.has("properties"):
				# Otherwise, just use the prior serialization
				childInfo["properties"] = child["properties"]

			# If we are serializing an active child, serialize that instance
			if child.has("instance"):
				# Get the actual instance
				var child_source: SerializedObject = child["instance"]

				# Get its serialization
				if child_source is SerializedObject.SerializedInstance:
					childInfo = child_source.serialize()
				elif child_source.get_owner() == self:
					# Or, we are the owner. We are responsible for serializing.
					childInfo = child_source.serialize()
				else:
					# Get its current name
					childInfo["name"] = child_source.get_name()

			# Add the child info to the serialized list
			if not ret.has("children"):
				ret["children"] = []
			ret["children"].append(childInfo)

	# Serialize the properties
	ret["properties"] = self.serialize_properties()

	# Return the result
	return ret

func serialize_minor() -> Dictionary:
	var ret: Dictionary = {
		"id": self._id,
		"name": self._name,
		"type": self.system_name(),
	}

	# Return the result
	return ret


func serialize_properties() -> Dictionary:
	# We will return a dictionary
	var ret: Dictionary = {}

	# Get a reference to the source implementation class
	var source_class: Script = self.get_script()

	# Add all property values (recurse up the class hierarchy)
	while not source_class == null:
		var source_properties: Dictionary = source_class.properties()
		if not source_properties.is_empty():
			for key in source_properties:
				# Only pick non-instanced properties
				var info = source_properties[key]
				if not info.has("instance") or not info["instance"]:
					# Get current value, if any
					var value: Variant = self.get_property(key)

					# Serialize values to just String, int, float, arrays, etc
					var property_type = info.get("type")
					if typeof(property_type) == TYPE_ARRAY:
						pass
					elif property_type == "color" and value is Color:
						value = value.to_html()

					# Add property
					ret[key] = value

		# Now get any inherited properties
		var parent: Script = source_class.get_base_script()
		source_class = parent

	return ret

# Restores the object to the state of the given serialization.
func restore(serialization: Dictionary):
	if serialization.has("id"):
		self._id = serialization["id"]

	if serialization.has("name"):
		self._name = serialization["name"]

	# If there are "properties", realize them
	if serialization.has("properties"):
		self.set_properties(serialization["properties"])

	# If there are "children", add a record of them.
	# We will realize the children at a later time, lazily, since that
	# requires instantiating them.
	if serialization.has("children"):
		for child in serialization["children"]:
			self._children.append(child)
			self._child_ids.append(child["id"])

# The graphic the represents this type of object in a toolbox or UI.
static func icon() -> String:
	return "res://icon.svg"

# The graphic that represents this type of object in a smaller listing.
static func icon_small() -> String:
	return ""

# The human name for the component.
static func name() -> String:
	return "Unknown Component"

# The categorical type for the component
static func type() -> String:
	return "Object"

# The human header for properties of this component.
static func properties_header() -> String:
	return "Unknown Component"

# A description of all properties unique and available to this component.
static func properties() -> Dictionary:
	return {}

# A set of dynamic properties where the set of properties differs based on the
# configuration of the object.
static func dynamic_base_properties(_base: SerializedObject, _base_class: GDScript) -> Dictionary:
	return {}

# A reference to the implementation of an instance of this object.
static func instance() -> GDScript:
	return SerializedInstance

# The globally unique identifier for this object.
func id() -> String:
	return self._id

# Returns a Dictionary keyed with ids to realized parent objects for instances
# of this object.
func get_parents() -> Dictionary:
	return self._parents

# Returns the realized parent for the given id only if that object contains
# this object instance as a child.
func get_parent(parent_id: String) -> SerializedObject:
	return self._parents[parent_id]

# Create an instance of this object.
func instantiate(options: Dictionary = {}) -> SerializedInstance:
	var ret: SerializedInstance = self.get_script().instance().new(self, options)

	# Retain a reference to this instance
	self._instances.append(ret)

	# Return the instance
	return ret

func destroy(existing_instance: SerializedInstance):
	self._instances.remove_at(self._instances.find(existing_instance))
	if self._instances.size() == 0:
		# TODO: Remove children
		# TODO: Destroy the resources
		pass

func instances() -> Array:
	return self._instances

func set_name(value: String):
	self._name = value

func get_name() -> String:
	return self._name

func get_human_name() -> String:
	var ret: String = self.get_name()

	# Capitalize the first letters (maybe depending on locale)
	var words: PackedStringArray = ret.split(" ")
	ret = ""
	for word in words:
		if word.length() > 0:
			word = word.substr(0, 1).capitalize() + word.substr(1)
		ret = ret + word + " "

	return ret.strip_edges()

func is_a(_of_type: Script):
	pass

func get_child_by_index(index: int) -> SerializedObject:
	return self._children[index]["instance"]

func get_child_by_type(index: int, of_type: String) -> SerializedObject:
	var found = -1
	for child_index in self._children.size():
		var child: SerializedObject = self.get_child_by_index(child_index)
		if child.get_script().type() == of_type:
			found += 1
			if found == index:
				return child

	return null

func get_child(child_id: String) -> SerializedObject:
	# TODO: lookup instead
	for child in self._children:
		if child["id"] == child_id:
			return child["instance"]

	return null

func get_property_info(key: String) -> Dictionary:
	# Get a reference to the source implementation class
	var source_class: Script = self.get_script()

	# Look at all property values (recurse up the class hierarchy)
	while not source_class == null:
		var source_properties: Dictionary = source_class.properties()
		if not source_properties.is_empty():
			for current_key in source_properties:
				if current_key == key:
					return source_properties[key]

		# Now get any inherited properties
		var parent: Script = source_class.get_base_script()
		source_class = parent

	# Return an empty set for an unknown property
	return {}

func has_property(key: String) -> bool:
	return self.has_method('get_' + key)

func get_property(key: String) -> Variant:
	if self.has_method("get_" + key):
		return self.call("get_" + key)

	return null

func set_property(key: String, value: Variant, silent: bool = false):
	if self.has_method("set_" + key):
		self.call("set_" + key, value)
		if not silent:
			self.updated.emit(key, value)

func set_properties(properties_data: Dictionary):
	for key in properties_data:
		# De-serialize the value, if necessary
		var value = properties_data.get(key)
		var info: Dictionary = self.get_property_info(key)
		var property_type = info.get("type")
		if typeof(property_type) == TYPE_ARRAY:
			pass
		elif property_type == "color" and value is String:
			# De-serialize to a Color
			value = Color(value)

		self.set_property(key, value, true)

# Add the given realized object instance as a child to ourselves.
func add_child(object: SerializedObject) -> bool:
	if object._owner:
		return false

	# Objects can only have a single owning parent (they get instantiated
	# in order to appear in multiple parents.)
	object._owner = self

	var child_info: Dictionary = object.serialize_minor()
	child_info["instance"] = object
	self._child_ids.append(object._id)
	self._children.append(child_info)

	# Maintain the back-reference
	if object is SerializedInstance:
		object._base._parent_ids.append(self._id)
		object._base._parents[self._id] = self

	# Call the 'add' event.
	object.on_add(self)
	if object is SerializedInstance:
		object._base.on_add(self)

		# Report to parents
		# TODO: handle loops?
		for parent_id in self.get_parents():
			var parent: SerializedObject = self._parents[parent_id]
			parent.on_new_descendant(object)

	self.child_added.emit(object)
	return true

func on_new_descendant(_object: SerializedInstance):
	pass

# Returns a copy of this object
func duplicate() -> SerializedObject:
	# Re-create the object
	var ret: SerializedObject = self.get_script().new()

	# Copy and paste property sets
	ret.set_properties(self.serialize_properties())

	# TODO: Duplicate children?

	# Return the new object
	return ret

func find_child(object: SerializedObject) -> int:
	return self._child_ids.find(object._id)

func find_child_by_type(object: SerializedObject, of_type: String) -> int:
	var found = -1
	for child_index in self._children.size():
		var child: SerializedObject = self.get_child_by_index(child_index)
		if child.get_script().type() == of_type:
			found += 1

		if child == object:
			return found

	return -1

func move_child(object: SerializedObject, to: int, from: int = -1):
	if from == -1:
		from = self.find_child(object)

	self._child_ids.insert(to, self._child_ids.pop_at(from))
	self._children.insert(to, self._children.pop_at(from))

# Remove the given realized object as a child of ourselves
func remove_child(object: SerializedObject, index: int = -1):
	# Try to find the child in our listing (it may appear twice)
	if index == -1:
		index = self.find_child(object)

	# Remove the child at the given index
	if index > -1:
		# Remove that specific instance of the child.
		self._child_ids.remove_at(index)
		self._children.remove_at(index)

		# Remove the reference back to the parent (it also may appear twice)
		var parent_id_index: int = object._parent_ids.find(self._id)
		if parent_id_index > -1:
			# Remove the first instance of the parent.
			object._parent_ids.remove_at(index)

			# If there are no further instances of the parent, remove it from
			# the parents Dictionary lookup.
			parent_id_index = object._parent_ids.find(self._id)
			if parent_id_index == -1:
				object._parents.erase(self._id)

		# Call the internal 'remove' event.
		object.on_remove(self)
		if object is SerializedInstance:
			# Call the internal event for the base SerializedObject
			object._base.on_remove_instance(self, object)

			# Remove the owner
			object._owner = null

# The class name for the component
func system_name() -> String:
	return self.get_script().resource_path.get_file().split('.')[0]

# Returns the object that is the owning parent of this instance.
func get_owner() -> SerializedObject:
	return self._owner

# Called when the object is initialized.
func on_init(_options: Dictionary = {}):
	pass

# Called when the object is added as a child.
func on_add(_parent: SerializedObject):
	pass

# Called when the object is removed from something.
func on_remove(_parent: SerializedObject):
	pass

# Called when an instanced version of the object is removed from something.
func on_remove_instance(_parent: SerializedObject, _child: SerializedInstance):
	pass

# Called when the object is instantiated.
func on_instantiate(_new_instance: SerializedObject.SerializedInstance):
	pass

# Called when the object is added to the scene:
func on_activate():
	pass

# Called when the object is removed from the scene:
func on_deactivate():
	pass
