# This represents a transition between one Scene and another.

# A transition can be made of a few different types of techniques:
# 0. A cut transition, which effectively does nothing
# 1. A 'stinger' video which is just a normal video to play.
# 2. A chroma-keyed stinger video with adjustable colors.
# 3. A shader that takes the two textures (old, new) to transition.

class_name Transition
extends SerializedObject


var _duration: int = 300


# The instance of a source.
class TransitionInstance:
	extends SerializedInstance


	var _duration_override: int = 0


	# The categorical type for the component
	static func type() -> String:
		return "Transition"

	# A description of all properties unique and available to this component.
	static func properties() -> Dictionary:
		return {
			# All transitions have a duration property
			"duration_override": {
				"type": "int",
				"label": "Duration (Override)",
				"description": "The overriden time it takes to complete the transition (in ms). 0 will defer to the general duration.",
				"unit": "milliseconds",
				"default": 0
			},
		}

	func get_duration_override() -> int:
		return self._duration_override

	func set_duration_override(value: int):
		self._duration_override = value

	# Upcall the animation manager queue method, if possible
	func process(delta: float) -> bool:
		if self.get_base().has_method('process'):
			return self.get_base().process(delta)

		return false

	# Construct a new source from the given metadata.
	func _init(base: SerializedObject, options: Dictionary = {}):
		super(base, options)


static func name() -> String:
	return "Unnamed Transition"

static func properties_header() -> String:
	return "Transition Properties"

static func instance() -> GDScript:
	return TransitionInstance

# A description of all properties unique and available to this component.
static func properties() -> Dictionary:
	return {
		# All transitions have a duration property
		"duration": {
			"type": "int",
			"label": "Duration",
			"description": "The time it takes to complete the transition (in ms).",
			"unit": "milliseconds",
			"min": 1,
			"max": 20000,
			"step": 1,
			"default": 300
		},
	}

func get_duration() -> int:
	return self._duration

func set_duration(value: int):
	self._duration = value

# Perform a transition between the given scenes.
func perform(_transition_instance: Transition.TransitionInstance, _from: Scene, _to: Scene, _within: SubViewportContainer, _global_time: float):
	# The base implementation does nothing
	pass
