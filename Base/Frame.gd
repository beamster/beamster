# This represents a set of properties that reflect a framing of a Scene.

# A Frame is a set of properties. When the Frame is switched to, those
# properties are updated against their "original" or "current" values based on
# an "interpolation method".

# For instance, a frame might have set a property for an instance with a
# particular id. Let's say the "left" property. So, if it were currently set to
# 500 and the frame has it set to 1000, then the frame will be responsible
# for getting the value to 1000.

# The interpolation value is how the value will change over time. It requires
# two things. 1. The amount of time to transform the property; and 2. The method
# in which the value is transformed.

# Since this is a decimal value (500.0), we can transform this value
# mathematically. The easiest manner (outside of setting the value directly to
# 1000.0, of course), is to apply the transformation linearly. This method would
# be `prop = start + (end - start) * (elapsed / time)` and is the "linear"
# method.

# There are alternatives. Traditional interpolation methods include "easing"
# methods which allow for a sense of acceleration. Instead of moving a linear
# distance per unit of time, you might move a small amount to begin with and
# then an increasing amount for the next few units of time (in easing) and
# eventually end up with a linear transformation before, perhaps, doing the same
# as we get closer to the goal. (out easing)
class_name Frame
extends SerializedObject


var _progress: float = 0.0
var _duration: float = 0.0
var _frame_properties: Dictionary = {}

var _executing: Array = []
var tree_item: TreeItem:
	set = set_tree_item,
	get = get_tree_item

var _scene: Scene = null
var _tween: Tween = null

func get_scene() -> Scene:
	return self._scene

func set_tree_item(item: TreeItem):
	tree_item = item

func get_tree_item() -> TreeItem:
	return tree_item

# Set the known objects to their starting points for the Frame.
func reset():
	# Go through every object we have properties
	for object_id in self._frame_properties.keys():
		var object: SerializedObject = self._frame_properties[object_id]["object"]
		var properties_data = self._frame_properties[object_id]["properties"]

		for key in properties_data.keys():
			var value: Variant = properties_data[key]["value"]
			properties_data[key]["original"] = object.get_property(key)
			object.set_property(key, value, true)

func revert():
	for object_id in self._frame_properties.keys():
		var object: SerializedObject = self._frame_properties[object_id]["object"]
		var properties_data = self._frame_properties[object_id]["properties"]

		for key in properties_data.keys():
			if properties_data[key].has("original"):
				var value: Variant = properties_data[key]["original"]
				object.set_property(key, value, true)

func apply():
	for object_id in self._frame_properties.keys():
		var object: SerializedObject = self._frame_properties[object_id]["object"]
		var properties_data = self._frame_properties[object_id]["properties"]

		for key in properties_data.keys():
			var value: Variant = properties_data[key]["value"]
			object.set_property(key, value, true)

func update_property(object: SerializedObject, key: String, value: Variant, time: float):
	if not self._frame_properties.has(object.id()):
		self._frame_properties[object.id()] = {
			"properties": {},
			"object": object,
		}

	var properties_data: Dictionary = self._frame_properties[object.id()]["properties"]

	if not properties_data.has(key):
		properties_data[key] = {
			"value": null,
			"time": 0.0,
			"original": object.get_property(key),
		}

	properties_data[key]["value"] = value
	properties_data[key]["time"] = time
	#print("updating property ", key, ' ', value, ' ', time, ' ', properties_data[key])

func on_activate():
	self._progress = 0.0
	self._duration = 0.0
	self._executing = []

	if self._scene and not self._frame_properties.is_empty():
		var node: Node = self._scene.get_viewport_container()
		var tween: Tween = null

		# For each property... keep track of it
		for object_id in self._frame_properties.keys():
			var object: SerializedObject = self._frame_properties[object_id]["object"]
			var properties_data: Dictionary = self._frame_properties[object_id]["properties"]

			# Determine if we are setting physics properties
			var set_position: bool = false
			var new_position: Vector2 = Vector2.ZERO

			var ts: TransformableSource.TransformableSourceInstance = null
			if object is TransformableSource.TransformableSourceInstance:
				ts = object
				new_position = ts.get_position()

			for key in properties_data.keys():
				var info = properties_data[key]
				var start: Variant = object.get_property(key)
				var end: Variant = info["value"]
				var time: float = info["time"]

				if start == end:
					continue

				self._duration = max(self._duration, time)
				print("executing on ", key, " start: ", start, ' end: ', end)

				# Perform a physics move instead if it is a TranformableSource property
				var is_physics: bool = false
				if ts:
					# TODO: handle left and top having different durations somehow
					if key == 'left':
						is_physics = true
						set_position = true
						new_position.x = end
					elif key == 'top':
						is_physics = true
						set_position = true
						new_position.y = end
					elif key == 'bounding_box_width' or key == 'bounding_box_height':
						is_physics = false

				if not is_physics:
					if not tween:
						tween = node.create_tween()
						self._tween = tween
						tween.set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
						tween.set_parallel(true)
					tween.tween_method(func callback(value):
						object.set_property(key, value, true),
					start, end, time)

			if set_position:
				# We need to register a physics operation
				print('we need to move the object to ', new_position)
				self._executing.append([ts, ts.get_position(), new_position, self._duration])

func serialize() -> Dictionary:
	var ret: Dictionary = super.serialize()
	ret["frame_properties"] = {}

	for object_id in self._frame_properties.keys():
		var object: SerializedObject = self._frame_properties[object_id]["object"]
		var properties_data = self._frame_properties[object_id]["properties"]

		ret["frame_properties"][object_id] = {
			"properties": {},
			"object": object.serialize_minor(),
		}

		var frame_info: Dictionary = ret["frame_properties"][object_id]["properties"]

		for key in properties_data.keys():
			var value: Variant = properties_data[key]["value"]
			frame_info[key] = properties_data[key]

			if value is Color:
				frame_info["value"] = value.to_html()

	return ret

func process(delta: float) -> bool:
	self._progress += delta

	# Perform physics
	for list in self._executing:
		var body: CharacterBody2D = list[0]._container
		var start_position: Vector2 = list[1]
		var new_position: Vector2 = list[2]
		var time: float = list[3]

		# Compute the current velocity
		var speed: float = start_position.distance_to(new_position) / time
		var direction: Vector2 = start_position.direction_to(new_position)
		body.velocity = direction * speed

		# Set it when the progress overshoots
		if self._progress > time:
			body.position = new_position
		else:
			# Use the physics process to move it instead
			body.move_and_slide()

	# If we are done, stop the tweens and just set the properties
	if self._progress > self._duration:
		if self._tween:
			self._tween.kill()

		self.apply()

	return self._progress < self._duration

func rewind():
	self._progress = 0.0
