# This represents a script that can affect other objects based on input of some
# kind.

class_name Action
extends SerializedObject

var studio: PanelContainer

var signals: Array
var processes: Array

var _purge_delay: float = 10.0


# Common signals an action can connect to
signal on_key_press


# The categorical type for the component
static func type() -> String:
	return "Action"

func _input(event):
	if event is InputEventKey:
		var key_event: InputEventKey = event
		if key_event.is_pressed():
			self.on_key_press.emit(key_event.as_text_key_label())

# Perform processing
# TODO: don't always have a _process thing
func _process(elapsed: float):
	for process in self.processes:
		if process[0] >= 0.0:
			process[0] -= elapsed
			process[1] += elapsed
			process[2].call(process[1], min(process[1] / (process[0] + process[1]), 1.0))

	# Every once and awhile, we clean up the list
	self._purge_delay -= elapsed
	if self._purge_delay < 0.0:
		self._purge_delay = 10.0

		# Go backwards in the list and remove anything completed
		for i in self.processes.size():
			var index = -i - 1
			if self.processes[index][0] < 0:
				self.processes.remove_at(index)

func get_scene(scene_id: String) -> Scene:
	return self.studio.get_scene_manager().get_scene(scene_id)

func get_source(source_id: String) -> Source.SourceInstance:
	return self.studio.get_scene_manager().get_editing_scene().get_child(source_id)

func bind_process(time: float, callable: Callable):
	self.processes.append([time, 0.0, callable])

func bind_signal(bound: Signal, callable: Callable):
	self.signals.append([bound, callable])
	return bound.connect(callable)

func unbind_all():
	for bound_info in self.signals:
		bound_info[0].disconnect(bound_info[1])
	self.signals = []
	self.processes = []

# TODO: wrap the app container somehow for the scene manager
func invoke(studio_instance: PanelContainer):
	self.studio = studio_instance
