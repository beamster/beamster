class_name Scene
extends SerializedObject


# TODO: lazily instantiate sources only when they would first be rendered.

# Holds the scene's viewport container
var _base_viewport_container: SubViewportContainer

# This is the rendering context of the scene
var _base_viewport: SubViewport

# This is the source listing UI
var source_list: VBoxContainer = null:
	set = set_source_list,
	get = get_source_list

# Gets the source listing component.
func get_source_list() -> VBoxContainer:
	return source_list

func set_source_list(list: VBoxContainer):
	source_list = list

# This is the action listing UI
var _action_list: Tree = null

# Effects
var _viewports: Array = []
var _materials: Array = []

# Frames
var _frames: Array[Frame] = []
var _frame_by_id: Dictionary = {}


signal source_added(source: Source.SourceInstance)
signal source_removed(source: Source.SourceInstance)
signal frame_added(frame: Frame)
signal frame_removed(frame: Frame)


# The categorical type for the component
static func type() -> String:
	return "Scene"

static func properties_header() -> String:
	return "Scene Properties"

func get_native_size() -> Vector2:
	return Vector2(1920, 1080)

# Constructs the scene object
func _init(options: Dictionary = {}):
	# Generate the base node
	super(options)

# Adds the given instance to the Scene.
func add_child(child_instance):
	super.add_child(child_instance)

	# If this is a source, add its rendering context to our own
	if child_instance is Source.SourceInstance:
		var source: Source.SourceInstance = child_instance

		# We need to add the root node somewhere in the graph, if it has no parent
		var root: Node = source.get_root_node()
		if root.get_parent() == null:
			# Add the root node
			# TODO: probably add this to a root viewport we set up once
			if self._base_viewport:
				self._base_viewport.add_child(root)

			# Make sure the root of the source is invisible
			# (We only see instances of the source)
			root.visible = false

			# Call its initial activation event
			source.get_base().on_activate()

		# Add the source to the source list
		self.source_list.get_node("SourceTree").add_source(source)

		# Add the instance to the scene
		if self._base_viewport:
			self._base_viewport.add_child(source._container)
		source._container.visible = true

		self.source_added.emit(child_instance)
	elif child_instance is Frame:
		self._frames.append(child_instance)
		self._frame_by_id[child_instance.id()] = child_instance
		child_instance._scene = self
		self.frame_added.emit(child_instance)

func remove_child(child_instance, index: int = -1):
	# If this is a source, remove its rendering context
	if child_instance is Source.SourceInstance:
		var source: Source.SourceInstance = child_instance

		# We need to add the root node somewhere in the graph, if it has no parent
		var root: Node = source.get_root_node()
		if root.get_parent():
			# Add the root node
			# TODO: probably add this to a root viewport we set up once
			if self._base_viewport:
				self._base_viewport.remove_child(root)

			# Call its deactivation event
			source.get_base().on_deactivate()

		# Remove the instance from the scene
		if self._base_viewport:
			self._base_viewport.remove_child(source._container)
		source._container.visible = false

		self.source_removed.emit(child_instance)
	elif child_instance is Frame:
		# Remove from our frame list
		self._frames.erase(child_instance)
		self.frame_removed.emit(child_instance)

	super.remove_child(child_instance, index)

# Removes the given instance from the Scene.
func remove_instance(_child_instance):
	pass

# Re-positions the given instance to the given position.
func reorder_instance(_child_instance, _position):
	pass

# Returns the number of source instances within the Scene.
func size() -> int:
	return 0

# Gets the action listing component.
func get_action_list() -> Tree:
	return self._action_list

func set_action_list(action_list: Tree):
	self._action_list = action_list

func get_frame(frame_id: String) -> Frame:
	return self._frame_by_id[frame_id]

func get_frames() -> Array[Frame]:
	return self._frames

# Maintains the rendering viewport for this scene.
#
# This is the rendering context and holds the scene's resulting texture.
func get_viewport() -> SubViewport:
	# Ensure there is a viewport
	self.get_viewport_container()

	# Return it
	return self._base_viewport

func get_viewport_container() -> SubViewportContainer:
	# Lazily create the rendering context only when somebody asks for it.
	if not self._base_viewport_container:
		self._base_viewport_container = SubViewportContainer.new()
		self._base_viewport_container.size = self.get_native_size()

		# Now, place it within a SubViewport
		self._base_viewport = SubViewport.new()
		self._base_viewport.size = self.get_native_size()
		self._base_viewport.transparent_bg = true
		self._base_viewport_container.add_child(self._base_viewport)

		# Keep track of our viewport stack (filters, etc)
		self._viewports = []
		self._materials = []
	return self._base_viewport_container

# A set of properties.
static func properties() -> Dictionary:
	return {
		"name": {
			"type": "string",
			"reset": "disabled",
		},
	}

# Maintains the canvas layer for this scene.
#
# The canvas layer holds the gizmos that scene editors use to manipulate sources
# found within the scene. This layer is not visible when rendering the viewport.
func get_canvas_layer() -> CanvasLayer:
	return null
