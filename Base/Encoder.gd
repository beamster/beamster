# This class represents an output of a stream.
class_name Encoder
extends Object


var video_source: Texture = null:
	set = set_video_source,
	get = get_video_source

func set_video_source(value: Texture):
	video_source = value

func get_video_source() -> Texture:
	return video_source

var url: String = "":
	set = set_url,
	get = get_url

func set_url(value: String):
	url = value

func get_url() -> String:
	return url


# Start encoding the audio and video content to the given texture.
func start():
	pass

# Stop encoding the audio and video content to the given texture.
func stop():
	pass
