# This is the base class for any renderable Source.

# These have a position and a size.

# They are represented by a "positioning node" which visually allows updating
# the size and position on the preview.

# The source itself will be a set of viewports wrapping the native node.
# The instance will be a subviewport container that serves as a rendering
# context for that base source.
class_name TransformableSource
extends Source


class TransformableSourceInstance:
	extends SourceInstance

	# The actual viewport container and collision body
	# This also represents its position via the position property
	var _container: CharacterBody2D

	# The instance texture
	var _sprite: Sprite2D

	# The subviewport underneath
	var _sub_viewport: SubViewport

	# The size of the instance relative to its parent
	var _size: Vector2

	# The top-left cropping of the instance
	var _crop_tl: Vector2

	# The bottom-right cropping of the instance
	var _crop_br: Vector2

	# The rotation of the node
	var _rotate: float = 0.0

	# The skew amounts
	var _skew_x: float = 0.0
	var _skew_y: float = 0.0

	# Flip
	var _flip_h: bool = false
	var _flip_v: bool = false

	# Source Opacity
	var _opacity: float = 1.0

	# Whether or not the position and size is locked
	var _position_lock: bool
	var _bounding_box_fill: String = "keep ratio"
	var _bounding_box_alignment: String = "center"

	# Guard recursion for signals
	var _in_moved: bool = false
	var _in_resized: bool = false


	signal moved
	signal resized


	func get_root_node() -> Node:
		return self.get_base()._base_viewport_container

	# The transform properties.
	static func properties() -> Dictionary:
		return {
			"opacity": {
				"type": "float",
				"label": "Opacity",
				"step": 0.01,
				"min": 0.0,
				"max": 1.0,
				"default": 1.0,
			},
			"visible": {
				"type": "bool",
				"label": "Visible",
			},
			"position_lock": {
				"type": "bool",
				"label": "Lock Position",
			},
			"left": {
				"type": "float",
				"label": "Left",
				"step": 0.001,
			},
			"top": {
				"type": "float",
				"label": "Top",
				"step": 0.001,
			},
			"bounding_box_width": {
				"type": "float",
				"label": "Bounding Box Width",
				"min": 0,
				"max": 1920 * 5,
				"step": 0.001,
			},
			"bounding_box_height": {
				"type": "float",
				"label": "Bounding Box Height",
				"min": 0,
				"max": 1920 * 5,
				"step": 0.001,
			},
			"bounding_box_fill": {
				"type": [
					"actual size",
					"keep ratio",
					"stretch",
					"stretch width",
					"stretch height",
					"fit",
					"fit width",
					"fit height"
				],
				"label": "Bounding Box Fill",
				"default": "keep ratio",
			},
			"bounding_box_alignment": {
				"type": [
					"top left", "top center", "top right",
					"center left", "center", "center right",
					"bottom left", "bottom center", "bottom right"
				],
				"label": "Bounding Box Alignment",
				"default": "center",
			},
			"crop_left": {
				"type": "float",
				"label": "Crop Left",
				"min": 0,
				"step": 0.001,
			},
			"crop_top": {
				"type": "float",
				"label": "Crop Top",
				"min": 0,
				"step": 0.001,
			},
			"crop_right": {
				"type": "float",
				"label": "Crop Right",
				"min": 0,
				"step": 0.001,
			},
			"crop_bottom": {
				"type": "float",
				"label": "Crop Bottom",
				"min": 0,
				"step": 0.001,
			},
			"skew_x": {
				"type": "float",
				"label": "Skew X",
				"min": -PI / 2,
				"max": PI / 2,
				"step": 0.001,
			},
			"skew_y": {
				"type": "float",
				"label": "Skew Y",
				"min": -PI / 2,
				"max": PI / 2,
				"step": 0.001,
			},
			"rotate": {
				"type": "float",
				"label": "Rotate",
				"min": -360.0,
				"max": 360.0,
				"step": 0.5,
			},
			"flip_h": {
				"type": "bool",
				"label": "Flip Horizontal",
			},
			"flip_v": {
				"type": "bool",
				"label": "Flip Vertical",
			},
		}

	# Returns a reference to the positioning widget.
	func get_positioning_nodes() -> Array[TransformableSourceGizmo]:
		var ret: Array[TransformableSourceGizmo] = []
		for index in self._gizmos.keys():
			for gizmo in self._gizmos[index]:
				if gizmo is TransformableSourceGizmo:
					ret.append(gizmo)

		return ret

	func get_visible() -> bool:
		return self._container.visible

	func set_visible(value: bool):
		self._container.visible = value
		for gizmo in self.get_positioning_nodes():
			gizmo.visible = value

	func get_bounding_box_fill() -> String:
		return self._bounding_box_fill

	func set_bounding_box_fill(value: String):
		if value == "keep ratio":
			# Enforce a new size that 'fits' the current size
			self._bounding_box_fill = "fit"
			for gizmo in self.get_positioning_nodes():
				gizmo.calculate_origin_box()
				gizmo.resize(gizmo.get_origin_box().get_size())

		self._bounding_box_fill = value
		self._position_viewport()

	func get_bounding_box_alignment() -> String:
		return self._bounding_box_alignment

	func set_bounding_box_alignment(value: String):
		self._bounding_box_alignment = value
		self._position_viewport()

	func get_bounding_box_width() -> float:
		return self._size.x

	func set_bounding_box_width(value: float):
		self._size.x = value
		self.resize(self._size)

	func get_bounding_box_height() -> float:
		return self._size.y

	func set_bounding_box_height(value: float):
		self._size.y = value
		self.resize(self._size)

	func set_flip_h(value: bool):
		self._flip_h = value
		self.resize(self._size)

	func get_flip_h() -> bool:
		return self._flip_h

	func set_flip_v(value: bool):
		self._flip_v = value
		self.resize(self._size)

	func get_flip_v() -> bool:
		return self._flip_v

	func get_position_lock() -> bool:
		return self._position_lock

	func set_position_lock(value: bool):
		self._position_lock = value

	func get_width():
		return self._size.x

	func set_width(value: float):
		self._size.x = value
		self.resize(self._size)

	func get_height():
		return self._size.y

	func set_height(value: float):
		self._size.y = value
		self.resize(self._size)

	func get_size() -> Vector2:
		return self._size

	func get_native_size() -> Vector2:
		return self.get_base().get_native_size()

	func get_opacity() -> float:
		return self._opacity

	func set_opacity(value: float):
		self._opacity = value
		self._sprite.material.set_shader_parameter("opacity", self._opacity)

	func get_left() -> float:
		return self._container.position.x

	func set_left(value: float):
		self._container.position.x = value
		self.move(self._container.position)
		for gizmo in self.get_positioning_nodes():
			gizmo.move(self._container.position, true)

	func get_top() -> float:
		return self._container.position.y

	func set_top(value: float):
		self._container.position.y = value
		self.move(self._container.position)
		for gizmo in self.get_positioning_nodes():
			gizmo.move(self._container.position, true)

	func get_crop_left() -> float:
		return self._crop_tl.x

	func set_crop_left(value: float):
		var old_tl = self._crop_tl
		self._crop_tl.x = min(self.get_native_size().x, max(value, 0))
		if self.get_bounding_box_fill() == "keep ratio":
			var native_size: Vector2 = self.get_native_size()
			native_size -= old_tl + self._crop_br
			var scale: float = (self._size / native_size).x
			if native_size.x <= 0.0 or native_size.y <= 0.0:
				scale = 0.0

			var difference: float = (old_tl.x - value) * scale
			self.set_left(self.get_left() - difference)

		self._sprite.material.set_shader_parameter("crop_tl_offset", self._crop_tl / self.get_native_size())

		for gizmo in self.get_positioning_nodes():
			gizmo.crop(self._crop_tl, self._crop_br)
		self.move(self._container.position)

	func get_crop_top() -> float:
		return self._crop_tl.y

	func set_crop_top(value: float):
		var old_tl = self._crop_tl
		self._crop_tl.y = min(self.get_native_size().y, max(value, 0))
		if self.get_bounding_box_fill() == "keep ratio":
			var native_size: Vector2 = self.get_native_size()
			native_size -= old_tl + self._crop_br
			var scale: float = (self._size / native_size).y
			if native_size.x <= 0.0 or native_size.y <= 0.0:
				scale = 0.0

			var difference: float = (old_tl.y - value) * scale
			self.set_top(self.get_top() - difference)

		self._sprite.material.set_shader_parameter("crop_tl_offset", self._crop_tl / self.get_native_size())

		for gizmo in self.get_positioning_nodes():
			gizmo.crop(self._crop_tl, self._crop_br)
		self.move(self._container.position)

	func get_crop_right() -> float:
		return self._crop_br.x

	func set_crop_right(value: float):
		self._crop_br.x = min(self.get_native_size().x, max(value, 0))
		self._sprite.material.set_shader_parameter("crop_br_offset", (self.get_native_size() - self._crop_br) / self.get_native_size())

		for gizmo in self.get_positioning_nodes():
			gizmo.crop(self._crop_tl, self._crop_br)
		self.move(self._container.position)

	func get_crop_bottom() -> float:
		return self._crop_br.y

	func set_crop_bottom(value: float):
		self._crop_br.y = min(self.get_native_size().y, max(value, 0))
		self._sprite.material.set_shader_parameter("crop_br_offset", (self.get_native_size() - self._crop_br) / self.get_native_size())

		for gizmo in self.get_positioning_nodes():
			gizmo.crop(self._crop_tl, self._crop_br)
		self.move(self._container.position)

	func get_rotate() -> float:
		return self._rotate

	func set_rotate(value: float):
		self._rotate = fmod(value, 360.0)
		self.rotate(self._rotate)

	func get_skew_x() -> float:
		return self._skew_x

	func set_skew_x(value: float):
		self._skew_x = value
		self.move(self._container.position)

	func get_skew_y() -> float:
		return self._skew_y

	func set_skew_y(value: float):
		self._skew_y = value
		self.move(self._container.position)

	func get_position() -> Vector2:
		return self._container.position

	func _position_viewport():
		var native_size: Vector2 = self.get_native_size()
		native_size -= self._crop_tl + self._crop_br
		var scale: Vector2 = self._size / native_size
		if native_size.x <= 0.0:
			scale.x = 0.0
		if native_size.y <= 0.0:
			scale.y = 0.0

		var tl_adjust: Vector2 = self._crop_tl * scale
		var br_adjust: Vector2 = self._crop_br * scale

		# The container should be the size, uncropped
		self._sub_viewport.size = self._size + tl_adjust + br_adjust
		var container_size: Vector2 = self._sub_viewport.size

		for gizmo in self.get_positioning_nodes():
			gizmo.calculate_origin_box()
		self._sprite.transform = Transform2D(
			0, Vector2(-1.0 if self._flip_h else 1.0, -1.0 if self._flip_v else 1.0), self._skew_x + self._skew_y, -self._size / 2
		).rotated(deg_to_rad(self._rotate) - self._skew_y).translated(self._size / 2 - self._container.position)

		self._sprite.position = -tl_adjust + (container_size / 2.0) #+ self._positioning_node.get_origin_box().get_calculated_position()

		if self._sub_viewport.size_2d_override != Vector2i(self.get_base().get_native_size()):
			self._sub_viewport.size_2d_override = self.get_base().get_native_size()

	func move(position: Vector2):
		if self._container.position == position:
			return
		self._container.position = position

		self._position_viewport()

		# Call the 'move' event
		self._base.on_move(self)

		# Signal
		if not self._in_moved:
			self._in_moved = true
			self.moved.emit()
			self._in_moved = false

	func rotate(amount: float):
		self._rotate = fmod(amount, 360.0)

		self._position_viewport()

		# Call the 'move' event
		self._base.on_move(self)

	func resize(size: Vector2):
		if self._size == size:
			return

		self._size = size

		self._position_viewport()

		self._base.on_resize(self)

		# Signal
		if not self._in_resized:
			self._in_resized = true
			self.resized.emit()
			self._in_resized = false

	# Called when the object is instantiated.
	func on_init(options: Dictionary = {}):
		super(options)

		# Create the actual viewport
		self._sub_viewport = SubViewport.new()
		self._sub_viewport.size = self.get_native_size()
		self._sub_viewport.size_2d_override = self.get_native_size()
		self._sub_viewport.size_2d_override_stretch = true
		self._sub_viewport.transparent_bg = true
		self._sub_viewport.set_world_2d(self.get_base()._base_viewport.get_world_2d())

		self._container = CharacterBody2D.new()
		self._sprite = Sprite2D.new()
		self._container.add_child(self._sprite)
		self._sprite.add_child(self._sub_viewport)
		self._sprite.texture = self._sub_viewport.get_texture()
		# TODO: ensure only one instance of the crop shader
		var shader: Shader = Shader.new()
		shader.code = "shader_type canvas_item;

uniform vec2 crop_tl_offset = vec2(0.0, 0.0);
uniform vec2 crop_br_offset = vec2(1.0, 1.0);
uniform float opacity = 1.0;

void fragment() {
	if (UV.x > crop_tl_offset.x && UV.y > crop_tl_offset.y &&
		UV.x < crop_br_offset.x && UV.y < crop_br_offset.y) {
		vec4 clr = texture(TEXTURE, UV);
		COLOR = vec4(clr.r, clr.g, clr.b, clr.a * opacity);
	}
	else {
		COLOR = vec4(0.0, 0.0, 0.0, 0.0);
	}
}"
		self._sprite.material = ShaderMaterial.new()
		self._sprite.material.shader = shader

		self._position_viewport()

var _base_viewport_container: SubViewportContainer
var _base_viewport: SubViewport
var _viewports: Array = []
var _materials: Array = [null]
var _sibling_viewports: Array = []
var _filter_mode: int = CanvasItem.TEXTURE_FILTER_LINEAR

static func name() -> String:
	return "Transformable Source"

static func properties() -> Dictionary:
	return {
		"width": {
			"type": "float",
			"label": "Width",
			"min": 0,
			"step": 0.001
		},
		"height": {
			"type": "float",
			"label": "Height",
			"min": 0,
			"step": 0.001
		},
		"filter_mode": {
			"type": ["nearest", "linear"],
			"label": "Filter Mode",
			"default": "linear"
		}
	}

static func properties_header() -> String:
	return "Transform Properties"

static func icon() -> String:
	return "res://Base/Icons/TransformableSource.svg"

static func instance() -> GDScript:
	return TransformableSourceInstance

static func get_native_node_class() -> Variant:
	return Node2D

# Returns the gizmo that handles any interactive updating of this object.
static func gizmos() -> Array[GDScript]:
	var list = Source.gizmos()
	list.append(TransformableSourceGizmo)
	return list

func resize(width: float, height: float):
	self._size.x = width
	self._size.y = height

	# Inform all known instances
	for inst in self.instances():
		inst.resize(inst._size)

	# Recalculate things based on the new size of the widget
	self.recalculate_size()

func get_width() -> float:
	return self._size.x

func set_width(value: float):
	self.resize(value, self.get_height())

func get_height() -> float:
	return self._size.y

func set_height(value: float):
	self.resize(self.get_width(), value)

func get_filter_mode() -> String:
	return TransformableSource.properties()["filter_mode"]["type"][self._filter_mode - 1]

func set_filter_mode(value: String):
	self._filter_mode = TransformableSource.properties()["filter_mode"]["type"].find(value) + 1

	# Set all viewport containers
	for container in self._viewports:
		if container is SubViewportContainer:
			container.texture_filter = self._filter_mode

	self._base_viewport_container.texture_filter = self._filter_mode as CanvasItem.TextureFilter

func _init(options: Dictionary = {}):
	# Generate the base node
	super(options)

	self._base_viewport_container = SubViewportContainer.new()
	self._base_viewport_container.size = self.get_native_size()

	# Now, place it within a SubViewport
	self._base_viewport = SubViewport.new()
	self._base_viewport.size = self.get_native_size()
	self._base_viewport.add_child(super.get_native_node())
	self._base_viewport_container.add_child(self._base_viewport)

	# Keep track of our viewport stack (filters, etc)
	# Initially, the stack is the native node
	self._viewports = [super.get_native_node()]
	self._materials = [null]
	self._sibling_viewports = [[]]

var _size: Vector2 = Vector2(1.0, 1.0)

# Returns the actual dimensions of the source.
#
# If this returns a size of 0, then the size is considered indeterminate.
# If this is the case, bounding box fills effectively always stretch. This is
# the case with, say, a ColorSource. These sources generally do not have actual
# dimensions. Their rendering is more of an effect within a space, instead.
func get_native_size() -> Vector2:
	return self._size

# Returns the native viewport for the source.
func get_viewport_node() -> SubViewport:
	return self._base_viewport

# Creates a new visual entry in our viewport stack.
func new_viewport_node() -> SubViewportContainer:
	# How it looks:
	# - bvpc = base_viewport_container
	# - bvp = base_viewport
	# - svp = sub_viewport
	# - svpc = sub_viewport_container
	# - node = native node

	# Original node with no added viewports (materials, effects, etc):
	# bvpc -> bvp -> node ; viewports: [node]

	# Add one effect: (adding svpc and svp)
	# bvpc -> bvp -> svpc -> svp -> node ; viewports: [node, svpc]

	# Add a second effect: (adding svpc2 and svp2)
	# bvpc -> bvp -> svpc2 -> svp2 -> svpc -> svp -> node ; viewports: [node, svpc, svpc2]

	# Create a new viewport container the size of the source
	var sub_viewport_container = SubViewportContainer.new()
	sub_viewport_container.size = self.get_native_size()
	sub_viewport_container.position = Vector2(0.0, 0.0)

	# Create a new viewport.
	var sub_viewport: SubViewport = SubViewport.new()
	sub_viewport.size = self.get_native_size()
	sub_viewport.transparent_bg = true
	sub_viewport.render_target_clear_mode = SubViewport.CLEAR_MODE_NEVER
	sub_viewport.render_target_update_mode = SubViewport.UPDATE_ALWAYS
	sub_viewport_container.add_child(sub_viewport)
	sub_viewport_container.texture_filter = self._filter_mode

	# The new viewport holds the old visual stack
	# And our old viewport holds the new subviewport container
	self._viewports[-1].reparent(sub_viewport)
	self._base_viewport.add_child(sub_viewport_container)

	self._viewports.append(sub_viewport_container)
	self._materials.append(null)
	self._sibling_viewports.append([])

	return sub_viewport_container

# Creates a backbuffer inside the current top viewport.
func new_viewport_sibling_node(material: Material) -> Sprite2D:
	# Create a new viewport with same "world" as the current "main" viewport.
	var sub_viewport = SubViewport.new()
	sub_viewport.size = self.get_native_size()
	sub_viewport.size_2d_override = self.get_native_size()
	sub_viewport.size_2d_override_stretch = true
	sub_viewport.transparent_bg = true
	sub_viewport.set_world_2d(self._viewports[-1].get_child(0).get_world_2d())
	self._sibling_viewports[-1].append(sub_viewport)

	# Create the backbuffer (via a Sprite2D)
	var container = Sprite2D.new()
	container.add_child(sub_viewport)
	container.texture = sub_viewport.get_texture()
	container.material = material
	container.centered = false
	container.position = Vector2(0.0, 0.0)

	# And the the backbuffer viewport!
	var buffer_viewport = SubViewport.new()
	buffer_viewport.size = self.get_native_size()
	buffer_viewport.size_2d_override = self.get_native_size()
	buffer_viewport.size_2d_override_stretch = true
	buffer_viewport.transparent_bg = true
	buffer_viewport.add_child(container)
	buffer_viewport.render_target_clear_mode = SubViewport.CLEAR_MODE_NEVER
	buffer_viewport.render_target_update_mode = SubViewport.UPDATE_ALWAYS
	self._sibling_viewports[-1].append(buffer_viewport)

	self._base_viewport_container.get_parent().add_child(buffer_viewport)

	return container

func add_child(thing: SerializedObject):
	if thing is Effect.EffectInstance:
		var effect: Effect.EffectInstance = thing
		var shader_material: ShaderMaterial = effect.get_material()

		# Create a new entry on our visual stack
		self.new_viewport_node()

		# Create buffers for the effect's buffers, if any
		var buffer_index = -1
		var sub_mats: Array = []
		for sub_material in effect.get_buffers():
			buffer_index += 1

			if sub_material is ShaderMaterial:
				var container = self.new_viewport_sibling_node(sub_material)
				var sub_effect: Effect = effect.get_base().get_buffers()[buffer_index]
				sub_mats.append([sub_effect.get_name(), container])

				# Set variables
				# TODO: use AnimationManager.get_frame_seconds for this to better
				#       match the behavior of 'TIME'
				sub_material.set_shader_parameter("START_TIME", Time.get_ticks_msec() / 1000.0)
			elif sub_material is Texture:
				sub_mats.append([sub_material.get_meta("name"), sub_material])

		# Attach the effect material to this new viewport
		self.attach_material(self._viewports.size() - 1, shader_material)

		# Set variables
		shader_material.set_shader_parameter("START_TIME", Time.get_ticks_msec() / 1000.0)

		# Assign buffer textures
		for sub_mat in sub_mats:
			var buffer_name: String = sub_mat[0]
			var texture: Texture = null
			if sub_mat[1] is Texture:
				texture = sub_mat[1]
			else:
				var buffer: Sprite2D = sub_mat[1]
				texture = buffer.get_parent().get_texture()

			# Assign to main texture
			shader_material.set_shader_parameter(buffer_name, texture)

			# Assign texture parameters
			for parameter in texture.get_meta("parameters", []):
				shader_material.set_shader_parameter(parameter[0], parameter[1])

			# Assign to all other sub_mats
			for sub_material in effect.get_buffers():
				if sub_material is ShaderMaterial:
					sub_material.set_shader_parameter(buffer_name, texture)

					# Assign texture parameters
					for parameter in texture.get_meta("parameters", []):
						sub_material.set_shader_parameter(parameter[0], parameter[1])

	super.add_child(thing)

func move_child(object: SerializedObject, to: int, from: int = -1):
	if from == -1:
		from = self.find_child(object)

	# Move the viewports
	if object is Effect.EffectInstance:
		# Get the position in the visual stack this effect currently sits
		var viewport_index = self.find_child_by_type(object, "Effect")

		# Bail if we cannot find it
		if viewport_index == -1:
			return

		# The 'index' of the viewports in the visual stack is one more than the
		# child count. The source itself is at index 0.

		# Get the position of the next Effect after 'to'
		var index_to = to
		var cur_child = self.get_child_by_index(index_to)
		while (index_to + 1) < self._children.size() and not cur_child is Effect.EffectInstance:
			index_to += 1
			cur_child = self.get_child_by_index(index_to)

		# If there is an effect that would come after ours (and it isn't
		# ourselves), then we will move them.
		if cur_child is Effect.EffectInstance and cur_child != object:
			index_to = self.find_child_by_type(cur_child, "Effect") + 1

			# Detach 'from' from the visual stack (retain the viewports)
			var items = self.detach_viewport(viewport_index)

			# Add it to the visual stack at the point requested
			var viewport_container: SubViewportContainer = items[0]
			var material: ShaderMaterial = items[1]
			var sub_viewports = items[2]

			# Therefore, viewport 0 (index_to - 1) is now child of Object
			# and Object is child of Effect (index_to)

			self._viewports[index_to - 1].reparent(viewport_container.get_child(0))
			if index_to < self._viewports.size():
				self._viewports[index_to].get_child(0).add_child(viewport_container)
			else:
				self._base_viewport.add_child(viewport_container)

			# Add buffer viewports
			for buffer_viewport in sub_viewports:
				if not buffer_viewport.get_parent():
					self._base_viewport_container.get_parent().add_child(buffer_viewport)

			# Add back the viewport, etc, to the requested spot
			self._viewports.insert(index_to, viewport_container)
			self._materials.insert(index_to, material)
			self._sibling_viewports.insert(index_to, sub_viewports)

		super.move_child(object, to, from)

func detach_viewport(index: int = -1) -> Array:
	if index == -1:
		# The viewports contains the native node, so we want to make sure that
		# the index reflects that '0' is the first viewport (index 1 in the
		# _viewports array)
		index = self._viewports.size() - 2

	# The 'index' of the viewports in the visual stack is one more than the
	# child count. The source itself is at index 0.
	index += 1

	# Retain and remove the viewport at the position
	var viewport_container: SubViewportContainer = self._viewports[index]

	# Remove the visual effect from the visual stack
	var viewport_parent = viewport_container.get_parent()
	viewport_parent.remove_child(viewport_container)

	# Reparent the children of this viewport
	if (index + 1) < self._viewports.size():
		self._viewports[index - 1].reparent(self._viewports[index + 1].get_child(0))
	else:
		self._viewports[index - 1].reparent(self._base_viewport)

	# Remove the reference
	self._viewports.remove_at(index)

	var material: ShaderMaterial = self._materials.pop_at(index)

	var sub_viewports = self._sibling_viewports.pop_at(index)
	for buffer_viewport in sub_viewports:
		if buffer_viewport.get_parent() == self._base_viewport_container.get_parent():
			buffer_viewport.get_parent().remove_child(buffer_viewport)

	return [viewport_container, material, sub_viewports]

func remove_child(thing: SerializedObject, index: int = -1):
	# Try to find the child in our listing (it may appear twice)
	if index == -1:
		index = self.find_child(thing)

	if thing is Effect.EffectInstance:
		var viewport_index = self.find_child_by_type(thing, "Effect")
		self.detach_viewport(viewport_index)

	super.remove_child(thing, index)

func attach_material(index: int, material: Material):
	self._viewports[index].set_material(material)
	self._materials[index] = material

func get_material_visible(index: int):
	return self._viewports[index].get_material() != null

func hide_material(index: int):
	self._viewports[index].set_material(null)

func show_material(index: int):
	self._viewports[index].set_material(self._materials[index])

func recalculate_size():
	for inst in self.instances():
		inst._position_viewport()

	for viewport in self._viewports:
		if viewport is SubViewportContainer:
			viewport.size = self.get_native_size()
			viewport.get_child(0).size = self.get_native_size()

	for sibling_viewports in self._sibling_viewports:
		for sibling_viewport in sibling_viewports:
			sibling_viewport.size = self.get_native_size()
			sibling_viewport.size_2d_override = self.get_native_size()

# Called when the object instance is moved within its parent.
func on_move(_instance: TransformableSourceInstance):
	pass

# Called when the object instance is resized within its parent.
func on_resize(_instance: TransformableSourceInstance):
	pass

func get_root_node() -> Node:
	return self.get_viewport_node()
