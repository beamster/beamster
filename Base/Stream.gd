class_name Stream
extends SerializedObject


var encoder: Encoder:
	set = set_encoder,
	get = get_encoder

func set_encoder(value: Encoder):
	encoder = value

func get_encoder() -> Encoder:
	return encoder

var video_source: Texture = null:
	set = set_video_source,
	get = get_video_source

func set_video_source(texture: Texture):
	encoder.video_source = texture

func get_video_source() -> Texture:
	return encoder.video_source


# Start the stream.
func start():
	encoder.start()

# Stop the stream.
func stop():
	encoder.stop()
