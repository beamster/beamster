class_name Manager
extends Object


# Logger
var _log_manager: LogManager


func _init(log_manager: LogManager, log_color: Color, log_classes: Array = [], log_class_color: Color = Color.CORNFLOWER_BLUE):
	# Register terms in the logger
	self._log_manager = log_manager
	self._log_manager.register_context(self.name(), log_color)

	for log_class in log_classes:
		self._log_manager.register_class(log_class, log_class_color)

func name() -> String:
	return self.get_script().resource_path.get_file().split('.')[0]

func get_log_manager() -> LogManager:
	return self._log_manager

func log(message: String, context: String = ""):
	if self._log_manager:
		if context == "":
			context = self.name()

		self._log_manager.log(message, context)

func warn(message: String, context: String = ""):
	if self._log_manager:
		if context == "":
			context = self.name()

		self._log_manager.warn(message, context)

func error(message: String, context: String = ""):
	if self._log_manager:
		if context == "":
			context = self.name()

		self._log_manager.error(message, context)
