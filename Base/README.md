# Base Objects

These are the "base" objects that implement various types of objects in the
system. The ultimate base object is the `SerializedObject` which represents any
object that will be serialized, deduplicated, and stored. Each other object is,
at its base, a `SerializedObject`.

A `SerializedObject` has a globally unique identifier that is used to refer to
it from other objects. Each object has its usage tracked by a reference counter.
Deleting an object will not remove it from the object store. Instead that
reference counter is decremented and only when it hits zero can the object
possibly be deleted (upon exit of the program and the clearing of the undo
buffer.)

A `SerializedObject` is part of a hierarchy. One can add `SerializedObject`
children to or remove them from a `SerializedObject`. For instance, a `Source`
is a child of a `Scene`. This makes that `Scene` dependent on that `Source` and
also reflective of any change made to that `Source`. Since sources may exist in
a variety of contexts and such names are difficult to query, child objects have
unique names in the context of their parent. That is, the parent names the
child. When the object's name changes, the parent may not reflect that change.
This is also important when considering adding the same object as a child of
an object twice. The motivation for this is discussed in the next section.

A `SerializedObject` just describes the base structure of such an object. Many
"instances" of the object can exist. For instance, a `SerializedObject` may
represent a video. This video might be a common asset useful for many scenes in
a collection. Therefore, each `Scene` may have, as its child, not the object,
but rather the `SerializedInstance` of that object. Instances may have unique
properties that the underlying base object does not. However, the essence of
the object is represented in base properties that when changed affect all
instances. In our example, that would be the video file being played. An
example of an instanced property would be the size and position of the video as
it exists in each separate scene. It is possible to have an instance of an
object exist twice in one `Scene` or other object.

A `Scene` is a 2D plane consisting of `Source` objects. Those `Source` objects
are usually some kind of 2D object. However, a `Source` can be any object that
can be part of such a scene.

A `TransformableSource` is a more traditional type of `Source` that has a size
and position. Ultimately, such a `Source` is some kind of texture... an image,
video, etc... that can be manipulated as a texture.

Since a `TransformableSource` is merely a texture, an `Effect` can be placed
upon them that alters the rendering of the `Source`. These are 2D shaders
(`canvasitem` shaders in the Godot sense.)

TBD: AudioSource

A `World` is a 3D space consisting of `Element` objects. The `World` can be
further manipulated by adding any `Element` that affects the environment (sky,
fog, etc) via an `EnvironmentalElement`.
