# This is the base class for any presentation element
class_name Source
extends SerializedObject


# The instance of a source.
class SourceInstance:
	extends SerializedInstance


	var _gizmos: Dictionary = {}


	# The categorical type for the component
	static func type() -> String:
		return "Source"

	# Construct a new source from the given metadata.
	func _init(base: SerializedObject, options: Dictionary = {}):
		super(base, options)

	func get_native_node():
		return self._base.get_native_node()

	func get_root_node() -> Node:
		return self.get_native_node()

	# Removes all gizmos added for this source on the particular index.
	#
	# The `index` specifies an unique context, such as a canvas or preview
	# window. When you specify an `index`, here, it will ensure that all gizmos
	# created to serve that layer are removed from the application UI.
	func remove_gizmos(index: int):
		if self.get_base().gizmos():
			if self._gizmos.has(index):
				for gizmo in self._gizmos[index]:
					var parent: Node = gizmo.get_parent()
					if parent:
						parent.remove_child(gizmo)

	# Gets a list of gizmos for a particular `index`.
	#
	# The `index` specifies an unique context for the gizmos. This is so that
	# multiple editing and previewing contexts can exist at the same time. When
	# an `index` is specified that has not been seen before, or has been since
	# removed prior to this call, the gizmos will all be created and returned.
	# Otherwise, if the gizmos already exist, the same gizmos will be returned.
	func get_gizmos(index: int) -> Array[SourceGizmo]:
		if self.get_base().gizmos():
			if not self._gizmos.has(index):
				var gizmos: Array[SourceGizmo] = []
				self._gizmos[index] = gizmos

			if self._gizmos[index].size() == 0:
				for gizmo_class in self.get_base().gizmos():
					var gizmo: SourceGizmo = gizmo_class.new()
					gizmo.source = self
					self._gizmos[index].append(gizmo)

		else:
			return []

		# Return the instantiated gizmo
		return self._gizmos[index]


# The wrapped Node
var _node: Node


static func name() -> String:
	return "Basic Source"

# The categorical type for the component
static func type() -> String:
	return "Source"

static func properties_header() -> String:
	return "General Properties"

# The instance of a source is a SourceInstance.
static func instance() -> GDScript:
	return SourceInstance

# The native node type this source wraps.
static func get_native_node_class() -> Variant:
	return Node

# Returns the gizmo that handles any interactive updating of this object.
static func gizmos() -> Array[GDScript]:
	return []

# Construct a new source from the given metadata.
func _init(options: Dictionary = {}):
	var base_node = self.get_script().get_native_node_class()
	self._node = base_node.new()

	super(options)

# Get the native node that this source instance wraps.
func get_native_node() -> Node:
	return self._node

func get_root_node() -> Node:
	return self.get_native_node()
