class_name SourceGizmo
extends Node2D


var source: Source.SourceInstance = null:
	set = set_source,
	get = get_source

func set_source(value: Source.SourceInstance):
	source = value

func get_source() -> Source.SourceInstance:
	return source

# Whether or not we should 'snap' any movements
var snap: bool = false:
	set = set_snap,
	get = get_snap

func set_snap(value: bool):
	snap = value

func get_snap() -> bool:
	return snap

var snap_major: float = 120.0:
	set = set_snap_major,
	get = get_snap_major

func set_snap_major(value: float):
	snap_major = value

func get_snap_major() -> float:
	return snap_major

var snap_minor: float = 30.0:
	set = set_snap_minor,
	get = get_snap_minor

func set_snap_minor(value: float):
	snap_minor = value

func get_snap_minor() -> float:
	return snap_minor

var angle_snap: float = 22.5:
	set = set_angle_snap,
	get = get_angle_snap

func set_angle_snap(value: float):
	angle_snap = value

func get_angle_snap() -> float:
	return angle_snap

var cursor: Control.CursorShape = Control.CursorShape.CURSOR_ARROW:
	set = set_cursor,
	get = get_cursor

func set_cursor(value: Control.CursorShape):
	cursor = value

# Returns the cursor this gizmo requests the system cursor be changed to.
func get_cursor() -> Control.CursorShape:
	return cursor

# Whether or not we should continue to get all input events
var capture_input: bool = false:
	set = set_capture_input,
	get = get_capture_input

func set_capture_input(value: bool):
	capture_input = value

func get_capture_input() -> bool:
	return capture_input

# Returns true if the current mouse position is within the gizmo bounds.
func is_hovered() -> bool:
	return false

# Called when input is received within an editor view.
func input(_event: InputEvent, _mouse_position: Vector2):
	pass
