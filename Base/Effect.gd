# This class wraps a shader that can be applied to a SerializedObject.

# Some objects can have a visual effect added. TransformableSources are 2D
# visual objects which can have a chain of such effects turned on and off.

# These effects have "properties" much like any SerializedObject does. In our
# case, these properties are defined within the shader code. The Effect class
# handles lazily parsing the code for those properties via the
# `realize_properties` function.

class_name Effect
extends SerializedObject


# The physical path of the shader code
var _path: String = ""

# Whether or not the shader code has been read
var _realized: bool = false

# The realized shader object
var _shader: Shader = null

# A lazy cache of the property listing for the shader
var _properties: Dictionary = {}

# A lazy cache of buffers
var _buffers: Array = []

# A list of loaded texture files keyed by name
var _texture_buffers: Dictionary = {}

# The category for the effect
var category: String = "Textures":
	set = set_category,
	get = get_category

func get_category() -> String:
	return category

func set_category(value: String):
	category = value


# Effectively, this is the material or other object that is linked to the
# shader.
class EffectInstance:
	extends SerializedInstance


	var _material: ShaderMaterial = null
	var _buffers: Array = []


	# The categorical type for the component
	static func type() -> String:
		return "Effect"

	# A set of static instanced properties, dynamically
	func dynamic_properties() -> Dictionary:
		return self.get_base().get_shader_properties()

	func get_material() -> ShaderMaterial:
		return self._material

	func has_property(key: String) -> bool:
		return self.get_base().get_shader_properties().has(key) or super.has_property(key)

	func get_property(key: String) -> Variant:
		# Just get the property from the base material
		var material: ShaderMaterial = self.get_material()
		var value: Variant = material.get_shader_parameter(key)

		# Coerce value, if needed
		var info: Dictionary = self.get_base().get_shader_properties()[key]
		var uniform_type = info['type']
		if typeof(uniform_type) == TYPE_ARRAY and not value is String:
			value = uniform_type[value]

		return value

	func set_property(key: String, value: Variant, silent: bool = false):
		# Set the property in the main material
		print('ok!!!')
		if self.has_property(key):
			# Coerce value, if needed
			var info: Dictionary = self.get_base().get_shader_properties()[key]
			var uniform_type = info['type']
			if typeof(uniform_type) == TYPE_ARRAY and value is String:
				value = uniform_type.find(value)

			print('ah!!!', silent)
			var material: ShaderMaterial = self.get_material()
			material.set_shader_parameter(key, value)

			# Set in all extra buffers, too
			for buffer in self.get_buffers():
				if buffer is ShaderMaterial:
					buffer.set_shader_parameter(key, value)

			if not silent:
				print('ok!!!')
				self.updated.emit(key, value)

		# Upcall
		super.set_property(key, value, silent)

	func get_buffers() -> Array:
		return self._buffers

	# Called when the object is instantiated.
	func on_init(options: Dictionary = {}):
		super(options)

		# Create the base material
		self._material = ShaderMaterial.new()
		self._material.shader = self.get_base().get_shader()

		# Create buffer materials
		for sub_shader in self.get_base().get_buffers():
			if sub_shader is Effect:
				var sub_material = ShaderMaterial.new()
				sub_material.shader = sub_shader.get_shader()
				self._buffers.append(sub_material)
			elif sub_shader is Texture:
				# Add the texture wholesale
				self._buffers.append(sub_shader)

		for shader_property in self.dynamic_properties().keys():
			var info = self.dynamic_properties()[shader_property]
			if info.has("default"):
				self.set_property(shader_property, info["default"])

static func name() -> String:
	return "Effect"

# The categorical type for the component
static func type() -> String:
	return "Effect"

# A reference to the implementation of an instance of this object.
static func instance() -> GDScript:
	return EffectInstance

# Returns the physical path to the shader code.
func get_path() -> String:
	return self._path

func set_path(value: String):
	self._path = value
	self._realized = false
	self._properties = {}

	# Update the existing shader, if it is in use
	if self._shader:
		self._shader.code = load(self.get_path()).code

func get_name() -> String:
	var filename = self.get_path().get_file()
	return filename.split(".")[0].replace("_", " ")

static func properties_header() -> String:
	return "Effect Properties"

# A description of all properties unique and available to this component.
static func properties() -> Dictionary:
	return {
		"path": {
			"type": "file",
			"label": "Path",
			"description": "The path of the shader to use.",
		},
	}

# Creates the shader object and reads in the shader code if it has not seen it
# before.
func get_shader() -> Shader:
	if not self._shader:
		var shader = Shader.new()
		var path = self.get_path()
		shader.code = load(path).code
		self._shader = shader

	return self._shader

# Returns a set of names of buffers this shader uses.
# This will parse the shader code if it hasn't been before.
func get_buffers() -> Array:
	# Ensure the list if populated.
	self.get_shader_properties()

	# Return our internal list.
	return self._buffers

# Returns a set of properties for the shader.
# This will parse the shader code if it hasn't been before.
func get_shader_properties() -> Dictionary:
	# If we have read this shader code already, just return the parsed
	# properties list from last time.
	if self._realized:
		return self._properties

	# Get the shader code
	var shader: Shader = self.get_shader()

	# Get the code itself
	var code: String = shader.code

	# We want to eventually fill this with all properties from all uniforms
	# in the shader code.
	var properties_data: Dictionary = {}

	# This contains top-level metadata from the top of the shader code
	var globalInfo: Dictionary = {}

	# Build up parser context
	var item: Dictionary = {}
	var lastProp: String = ""

	# Parse code for uniforms
	# We assume uniforms are defined just on one line
	var shader_started = false

	var regex = RegEx.new()
	regex.compile("[^ \t=]+|=")

	for line in code.split("\n"):
		line = line.strip_edges()

		var tokens = []
		for token in regex.search_all(line):
			tokens.push_back(token.get_string())

		# Look for a hint continuation (to span multiple lines.)
		if line.begins_with("//  "):
			line = line.substr(2).strip_edges()

			# A continuation
			if lastProp != "":
				# Append the line to the last property we set.
				if shader_started and item.has(lastProp):
					item[lastProp] += " " + line
				elif globalInfo.has(lastProp):
					globalInfo[lastProp] += " " + line
				else:
					lastProp = ""
		elif tokens.size() > 2 and tokens[0] == "//":
			# This is a comment line... look for a hint.
			# Hints are of the form:
			# [type] <value>
			if tokens[1].length() > 2 and tokens[1][0] == "[" and tokens[1][tokens[1].length() - 1] == "]":
				# Get the item key
				var key: String = tokens[1].substr(1, tokens[1].length() - 2)
				var value: String = line.substr(line.find(']') + 1).strip_edges()
				if shader_started:
					# We set items ONLY if we have seen the 'shader_type' directive
					item[key] = value
				else:
					# Set global properties of the shader
					if globalInfo.has(key):
						if not globalInfo[key] is Array:
							globalInfo[key] = [globalInfo[key]]
						globalInfo[key].append(value)
					else:
						globalInfo[key] = value
				lastProp = key
		elif tokens.size() > 1 and tokens[0] == "shader_type":
			shader_started = true
			item = {}
			lastProp = ""
		elif tokens.size() > 2 and tokens[0] == "uniform":
			# OK! We found a uniform so we can look at it
			var uniform_type = tokens[1]
			var uniform_name = tokens[2].rstrip(";")

			for i in tokens.size():
				tokens[i] = tokens[i].rstrip(";")

			if uniform_type == "vec2":
				# Assume vec2 is a coordinate, perhaps
				# TODO: handle vec2 fields (possibly just create two fields)
				pass

			if uniform_type == "vec4":
				# Assume vec4 is a color
				uniform_type = "color"

			if not item.has("type"):
				item["type"] = uniform_type

			if item["type"].contains('['):
				# Parse the likely enumerated value
				item["type"] = JSON.parse_string(item["type"])

			uniform_type = item["type"]
			if not item.has("label"):
				item["label"] = uniform_name.capitalize()

			if tokens.size() > 4:
				# Find '=' and determine value from that
				var equals = line.find("=")
				if equals >= 0:
					var value: String = line.substr(equals + 1).strip_edges().rstrip(";")

					# Default value
					if not item.has("default"):
						item["default"] = value

						if typeof(uniform_type) == TYPE_ARRAY:
							item["default"] = item['type'][value]
						elif uniform_type == "bool":
							item["default"] = value == "true"
						elif uniform_type == "color":
							# Parse the 'vec4'
							# It's gonna be something like vec4(<float>, <float>, <float>, <float>)
							# But let's assume we can have an Expression inside.
							# Parse the arguments
							var paren: int = value.find("(")
							if paren >= 0:
								var rparen: int = value.find(")")
								if rparen > paren:
									value = value.substr(paren + 1, rparen - paren - 1)

									# Now split into components
									var components: Array = value.split(",")
									for i in components.size():
										components[i] = float(components[i].strip_edges())

									item["default"] = Color(
										components[0],
										components[1],
										components[2],
										components[3]
									)

			var typeCastedItems = ["default", "min", "max", "step"]
			for key in typeCastedItems:
				if item.has(key):
					if typeof(uniform_type) == TYPE_ARRAY:
						pass
					elif uniform_type == "int":
						item[key] = int(item[key])
					elif uniform_type == "float":
						item[key] = float(item[key])

			# Ensure that an 'int' type has a step of 1, if none are provided
			if typeof(uniform_type) != TYPE_ARRAY and uniform_type == "int" and not item.has("step"):
				item["step"] = 1

			item["name"] = uniform_name
			if typeof(uniform_type) != TYPE_ARRAY and uniform_type == "sampler2D":
				# We have a buffer
				var buffer_path = self._path.get_basename().path_join(uniform_name + ".gdshader")
				var buffer_exists: bool = FileAccess.file_exists(buffer_path)
				if buffer_exists:
					var sub_effect = Effect.new()
					sub_effect.set_path(buffer_path)
					self._buffers.append(sub_effect)
				else:
					for extension in ["png", "jpg", "jpeg"]:
						var texture_path = self._path.get_basename().path_join(uniform_name + "." + extension)
						var texture_exists: bool = FileAccess.file_exists(texture_path)
						if texture_exists:
							# Load the image and create a texture from it
							var image: Image = Image.load_from_file(texture_path)
							var texture: ImageTexture = ImageTexture.create_from_image(image)

							# Retain sampler texture name
							texture.set_meta("name", uniform_name)

							# Append the texture to our buffer list
							self._buffers.append(texture)

							# Key track of it by name
							self._texture_buffers[uniform_name] = texture

							# Cancel looking for a generated texture
							buffer_exists = true

				# Detect a "Noise" filter of a particular size
				if not buffer_exists and uniform_name.begins_with("NOISE_") or uniform_name == "NOISE":
					var noise_type: FastNoiseLite.NoiseType = FastNoiseLite.NoiseType.TYPE_SIMPLEX
					var noise_type_try = [
						"SIMPLEX",
						"SIMPLEX_SMOOTH",
						"CELLULAR",
						"PERLIN",
						"CUBIC",
						"VALUE",
					].find(item.get("noise_type", "SIMPLEX").to_upper())

					# If it is in the list, set it (otherwise it retains the
					# default SIMPLEX value set prior.)
					if noise_type_try > -1:
						noise_type = noise_type_try as FastNoiseLite.NoiseType

					# Determine the size.
					var noise_size = min(
						max(
							int(item.get("noise_width", 256)),
							16
						),
						4096
					)

					# Generate the noise texture
					# TODO: generate at initialize via on_init
					# TODO: generate in helper methods
					var noise_texture = NoiseTexture2D.new()
					noise_texture.width = noise_size

					if item.get("repeat", "") == "seamless":
						noise_texture.seamless = true

					if item.get("as_normal_map", "") == "true":
						noise_texture.as_normal_map = true

					var noise_generator: FastNoiseLite = FastNoiseLite.new()
					noise_generator.noise_type = noise_type

					# Set other options, if they exist
					if item.has("cellular_return_type"):
						var try = [
							"CELL_VALUE",
							"DISTANCE",
							"DISTANCE2",
							"DISTANCE2_ADD",
							"DISTANCE2_SUB",
							"DISTANCE2_MUL",
							"DISTANCE2_DIV",
						].find(item["cellular_return_type"].to_upper())
						if try > -1:
							noise_generator.cellular_return_type = try as FastNoiseLite.CellularReturnType

					if item.has("cellular_distance_function"):
						var try = [
							"EUCLIDEAN",
							"EUCLIDEAN_SQUARED",
							"MANHATTAN",
							"HYBRID",
						].find(item["cellular_distance_function"].to_upper())
						if try > -1:
							noise_generator.cellular_distance_function = try as FastNoiseLite.CellularDistanceFunction

					if item.has("cellular_jitter"):
						noise_generator.cellular_jitter = float(item["cellular_jitter"])

					if item.has("domain_warp_amplitude"):
						noise_generator.domain_warp_amplitude = float(item["domain_warp_amplitude"])

					if item.has("domain_warp_enabled"):
						noise_generator.domain_warp_enabled = item["domain_warp_enabled"] != "false"

					if item.has("domain_warp_fractal_gain"):
						noise_generator.domain_warp_fractal_gain = float(item["domain_warp_fractal_gain"])

					if item.has("domain_warp_fractal_lacunarity"):
						noise_generator.domain_warp_fractal_lacunarity = float(item["domain_warp_fractal_lacunarity"])

					if item.has("domain_warp_fractal_octaves"):
						noise_generator.domain_warp_fractal_octaves = int(item["domain_warp_fractal_octaves"])

					if item.has("domain_warp_frequency"):
						noise_generator.domain_warp_frequency = float(item["domain_warp_frequency"])

					if item.has("domain_warp_fractal_type"):
						var try = [
							"NONE",
							"PROGRESSIVE",
							"INDEPENDENT",
						].find(item["domain_warp_fractal_type"].to_upper())
						if try > -1:
							noise_generator.domain_warp_fractal_type = try as FastNoiseLite.DomainWarpFractalType

					if item.has("domain_warp_type"):
						var try = [
							"SIMPLEX",
							"SIMPLEX_REDUCED",
							"BASIC_GRID",
						].find(item["domain_warp_type"].to_upper())
						if try > -1:
							noise_generator.domain_warp_type = try as FastNoiseLite.DomainWarpType

					if item.has("fractal_gain"):
						noise_generator.fractal_gain = float(item["fractal_gain"])

					if item.has("fractal_lacunarity"):
						noise_generator.fractal_lacunarity = float(item["fractal_lacunarity"])

					if item.has("fractal_octaves"):
						noise_generator.fractal_octaves = int(item["fractal_octaves"])

					if item.has("fractal_ping_pong_strength"):
						noise_generator.fractal_ping_pong_strength = float(item["fractal_ping_pong_strength"])

					if item.has("fractal_type"):
						var try = [
							"NONE",
							"FBM",
							"RIDGED",
							"PING_PONG",
						].find(item["fractal_type"].to_upper())
						if try > -1:
							noise_generator.fractal_type = try as FastNoiseLite.FractalType

					if item.has("fractal_weighted_strength"):
						noise_generator.fractal_weighted_strength = float(item["fractal_weighted_strength"])

					if item.has("frequency"):
						noise_generator.frequency = float(item["frequency"])

					if item.has("offset_x"):
						noise_generator.offset.x = float(item["offset_x"])

					if item.has("offset_y"):
						noise_generator.offset.y = float(item["offset_y"])

					if item.has("offset_z"):
						noise_generator.offset.z = float(item["offset_z"])

					if item.has("seed"):
						noise_generator.seed = int(item["seed"])
					else:
						# Default seed is some randomness
						# TODO: allow for this noise texture to be regenerated
						noise_generator.seed = RandomNumberGenerator.new().randi()

					# Set the noise generator to the texture
					noise_texture.noise = noise_generator

					# Retain sampler texture name
					noise_texture.set_meta("name", uniform_name)

					# Append the texture to our buffer list
					self._buffers.append(noise_texture)
			elif uniform_name.ends_with("_PIXEL_SIZE"):
				# This is keeping track of the size of a texture
				var texture_name = uniform_name.substr(0, uniform_name.length() - 11)
				var texture = self._texture_buffers.get(texture_name)
				if texture:
					var texture_properties = texture.get_meta("parameters", [])
					texture_properties.append([uniform_name, texture.get_size()])
					texture.set_meta("parameters", texture_properties)
			else:
				properties_data[uniform_name] = item

			# Clear out item for the next item
			item = {}

	# Retain this for next time we are asked
	self._realized = true
	self._properties = properties_data

	# Return the properties list
	return self._properties
