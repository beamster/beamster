class_name Element
extends SerializedObject


var _node: Node3D
var _position_lock: bool


func _init():
	var base = self.get_native_node_class()
	self._node = base.new()

	self._on_init()

static func icon() -> String:
	return "res://icon.svg"

static func name() -> String:
	return "Basic Element"

# The categorical type for the component
static func type() -> String:
	return "Element"

static func properties_header() -> String:
	return "General Properties"

static func properties() -> Dictionary:
	return {}

func get_native_node_class():
	return Node3D

func get_native_node() -> Node3D:
	return self._node

func get_visible() -> bool:
	var node: Node3D = self.get_native_node()
	if node:
		return node.visible

	return false

func set_visible(value: bool):
	self.get_native_node().visible = value

func get_position_lock() -> bool:
	return self._position_lock

func set_position_lock(value: bool):
	self._position_lock = value

func get_world_viewport():
	var parent: Node
	parent = self.get_native_node()
	while parent and not parent is SubViewportContainer:
		parent = parent.get_parent()

	return parent

func _on_init():
	pass

func _on_add():
	pass
