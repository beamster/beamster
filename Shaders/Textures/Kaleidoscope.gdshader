shader_type canvas_item;

// [min] 1.0
// [max] 30.0
// [step] 1.0
uniform float parts = 7.0;

// [min] 0.0
// [max] 1.0
// [step] 0.01
uniform float offset = 0.0;

// [min] 0.0
// [max] 2.0
// [step] 0.1
uniform float warp = 1.0;

// [min] 0.1
// [max] 4.0
// [step] 0.1
uniform float zoom = 2.0;

// [min] 0.0
// [max] 5.0
// [step] 0.01
uniform float rotate_speed = 0.02;

// Take the UV coordinates and get the corresponding polar for the kaleidoscope
// effect.
vec2 kaleido_transform(vec2 uv) {
	// Get the angle from the origin to the current point in range: (-1.0, 1.0)
	float theta = atan(uv.y, uv.x);

	// Get the radius of our circle going through our uv point
	float r = pow(length(uv), warp);

	// Subdivide the circle into parts
	float freq = (PI * 2.0) / parts;

	// Get the actual theta angle between [-freq/2.0, freq/2.0]
	theta = abs(
		mod(theta, freq) - (freq / 2.0)
	) / (r + offset);

	// Convert back to a relative point in our UV space
	return vec2(cos(theta), sin(theta)) * (r + offset);
}

vec2 rotate(vec2 at) {
	vec2 v;

	// Rotate over time
	float th = rotate_speed * TIME;
	v.x = at.x * cos(th) - at.y * sin(th);
	v.y = at.x * sin(th) + at.y * cos(th);

	// Ensure it is between 0.0 and 1.0 tho (tiles the texture)
	v.x = mod(v.x, 1.0);
	v.y = mod(v.y, 1.0);

	return v;
}

void fragment() {
	// Start with our UV
	vec2 uv = UV;

	// Transform UV from the range (0.0, 1.0) to the range (-1.0, 1.0)
	uv.x = mix(-1.0, 1.0, uv.x);
	uv.y = mix(-1.0, 1.0, uv.y);

	// Ensure the transformed coordinate matches the aspect ratio
	uv.y *= TEXTURE_PIXEL_SIZE.y / TEXTURE_PIXEL_SIZE.x;

	uv *= 1.0 / zoom;

	// Get the rotated coordinate
	vec2 kaleido_uv = kaleido_transform(uv);
	vec4 color = texture(TEXTURE, rotate(kaleido_uv));
	COLOR = color;
}