shader_type canvas_item;

uniform sampler2D BUFFER0;

// [min] 0.0
// [max] 5.0
// [step] 0.01
const vec2 blur_scale = vec2(2.0);

// [min] 1
// [max] 50
// [step] 1
uniform int blur_samples = 21;

uniform bool use_simplified_gaussian = true;

float Gaussian1D(float x, float sigma) {
	if (use_simplified_gaussian) {
		return exp(-(x * x) / (2.0 * sigma * sigma));
	}

	float o = sigma * sigma;
	float a = 1.0 / sqrt(TAU * o);
	float b = (x * x) / (2.0 * o);

	return a * exp(-b);
}

vec4 GaussianBlur1D(
	sampler2D sp,
	vec2 uv,
	vec2 dir,
	float sigma,
	int samples
) {
	float halfSamples = float(samples) * 0.5;

	vec4 color = vec4(0.0);
	float accum = 0.0;

	uv -= halfSamples * dir;

	for (int i = 0; i < samples; ++i)
	{
		float weight = Gaussian1D(float(i) - halfSamples, sigma);

		color += texture(sp, uv) * weight;
		accum += weight;

		uv += dir;
	}

	return color / accum;
}

// Horizontal blur

void fragment() {
	vec3 ps = vec3(TEXTURE_PIXEL_SIZE.xy, 0.0);
	vec2 uv = UV;
	float blur_sigma = sqrt(float(blur_samples));

	COLOR = GaussianBlur1D(
		TEXTURE,
		uv,
		ps.xz * blur_scale,
		blur_sigma,
		blur_samples
	);
}