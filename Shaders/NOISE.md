# Predefined Noise Textures

You can have the program generate several types of noise textures for a shader
just by specifying a sampler2D with the name `NOISE` where you can add another
identifier afterward in this form: (or just have it called `NOISE` by itself.)

`NOISE_<anything>`

Then there are a series of parameters that one can annotate this uniform that
will affect the noise that will be generated. The noise texture is generated
just once when the shader is loaded for use.

Most of these parameters are a one-to-one mapping of the parameters and features
available using the Godot `FastNoiseLite` class. Most of this documentation is
a copy of what would be found there.

## Examples

For instance, to generate a `SIMPLEX` noise texture of 4096 by 4096 pixels, just
add a sampler2D uniform to a shader or buffer that is named as such: (We add the
`repeat_enable` here just for common convenience with such textures where we
want it to loop around.)

```
// [noise_type] SIMPLEX
// [noise_width] 4096
uniform sampler2D NOISE_MY_TEXTURE : repeat_enable;
```

## Parameters

You can specify a series of parameters that may affect the generation of the
noise texture. Each of these will be specified using the comment syntax that
is used for other uniform parameters. For example, for the hypothetical
parameter named `parameter_name`, we are specifying the float value `3.0`.

```
// [parameter_name] 3.0
uniform sampler2D NOISE;
```

What follows are the other possible parameters.

### `noise_width`

`int`: The number of pixels, square, to make the resulting texture. Defaults to
`128`. With a minimum of `16` and a maximum of `4096`.

### `noise_type`

Any of the following strings to affect the algorithm used to generate the noise.
Which of these is in use will affect which other parameters become relevant.

* `SIMPLEX`: As opposed to `PERLIN`, gradients exist in a simplex lattice rather than a grid lattice, avoiding directional artifacts.
* `SIMPLEX_SMOOTH`: Modified, higher quality version of `SIMPLEX`, but slower.
* `CELLULAR`: Cellular includes both Worley noise and Voronoi diagrams which creates various regions of the same value.
* `PERLIN`: A lattice of random gradients. Their dot products are interpolated to obtain values in between the lattices.
* `CUBIC`: Similar to Value noise, but slower. Has more variance in peaks and valleys.
* `VALUE`: A lattice of points are assigned random values then interpolated based on neighboring values.

### `seed`:

`int`: The random number seed for all noise types. Defaults to `0`.

### `offset_x`, `offset_y`, `offset_z`:

`float`: Translate the noise input coordinates by the given position on the
assumed axis. Defaults to `0.0`.

Defaults to `SIMPLEX`.

### `cellular_distance_function`

Determines how the distance to the nearest/second-nearest point is computed.
Any of the following values:

* `EUCLIDEAN`: Euclidean distance to the nearest point.
* `EUCLIDEAN_SQUARED`: Squared Euclidean distance to the nearest point.
* `MANHATTAN`: Manhattan distance (taxicab metric) to the nearest point.
* `HYBRID`: Blend of `EUCLIDEAN` and `MANHATTAN` to give curved cell boundaries.

### `cellular_jitter`

`float`: Maximum distance a point can move off of its grid position. Set to `0`
for an even grid. Defaults to `0.45`.

### `cellular_return_type`

Return type from cellular noise calculations. Any of the following values:

* `CELL_VALUE`: The cellular distance function will return the same value for all points within a cell.
* `DISTANCE`: The cellular distance function will return a value determined by the distance to the nearest point.
* `DISTANCE2`: The cellular distance function returns the distance to the second-nearest point.
* `DISTANCE2_ADD`: The distance to the nearest point is added to the distance to the second-nearest point.
* `DISTANCE2_SUB`: The distance to the nearest point is subtracted from the distance to the second-nearest point.
* `DISTANCE2_MUL`: The distance to the nearest point is multiplied with the distance to the second-nearest point.
* `DISTANCE2_DIV`: The distance to the nearest point is divided by the distance to the second-nearest point.

### `domain_warp_amplitude`:

'float': Sets the maximum warp distance from the origin. Defaults to `30.0`.

### `domain_warp_enabled`:

'bool': If enabled, another FastNoiseLite instance is used to warp the space,
resulting in a distortion of the noise. Defaults to `false`.

### `domain_warp_fractal_gain`:

`float`: Determines the strength of each subsequent layer of the noise which is
used to warp the space.

A low value places more emphasis on the lower frequency base layers, while a
high value puts more emphasis on the higher frequency layers. Defaults to `0.5`.

### `domain_warp_fractal_lacunarity`:

`float`: Octave lacunarity of the fractal noise which warps the space.
Increasing this value results in higher octaves producing noise with finer
details and a rougher appearance. Defaults to `6.0`.

### `domain_warp_fractal_octaves`:

`int`: The number of noise layers that are sampled to get the final value for
the fractal noise which warps the space. Defaults to `5`.

### `domain_warp_fractal_type`:

The method for combining octaves into a fractal which is used to warp the space.
Any of the following values:

* `NONE`: No fractal noise for warping the space.
* `PROGRESSIVE`: Warping the space progressively, octave for octave, resulting in a more "liquified" distortion.
* `INDEPENDENT`: Warping the space independently for each octave, resulting in a more chaotic distortion.

Defaults to `PROGRESSIVE`.

### `domain_warp_frequency`:

`float`: Frequency of the noise which warps the space. Low frequency results in
smooth noise while high frequency results in rougher, more granular noise.
Defaults to `0.05`.

### `domain_warp_type`:

Sets the warp algorithm. Any of the following values:

* `SIMPLEX`: The domain is warped using the simplex noise algorithm.
* `SIMPLEX_REDUCED`: The domain is warped using a simplified version of the simplex noise algorithm.
* `BASIC_GRID`: The domain is warped using a simple noise grid (not as smooth as the other methods, but more performant).

Defaults to `SIMPLEX`.

### `fractal_gain`:

`float`: Determines the strength of each subsequent layer of noise in fractal
noise.

A low value places more emphasis on the lower frequency base layers, while a
high value puts more emphasis on the higher frequency layers. Defaults to `0.5`.

### `fractal_lacunarity`:

`float`: Frequency multiplier between subsequent octaves. Increasing this value
results in higher octaves producing noise with finer details and a rougher
appearance. Defaults to `2.0`.

### `fractal_octaves`:

`int`: The number of noise layers that are sampled to get the final value for
fractal noise types. Defaults to `5`.

### `fractal_ping_pong_strength':

`float`: Sets the strength of the fractal ping pong type. Defaults to `2.0`.

### `fractal_type`:

The method for combining octaves into a fractal. Any of the following values:

* `NONE`: No fractal noise.
* `FBM`: Method using Fractional Brownian Motion to combine octaves into a fractal.
* `RIDGED`: Method of combining octaves into a fractal resulting in a "ridged" look.
* `PING_PONG: Method of combining octaves into a fractal with a ping pong effect.

Defaults to `FBM`.

### `fractal_weighted_strength`:

`float`: Higher weighting means higher octaves have less impact if lower octaves
have a large impact. Defaults to `0`.

### `frequency`:

`float`: The frequency for all noise types. Low frequency results in smooth
noise while high frequency results in rougher, more granular noise. Defaults to
`0.01`.
