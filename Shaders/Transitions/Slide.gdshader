// This transitions between TEXTURE_FROM and TEXTURE_TO using a configurable set
// of sliding bars. Can be 'slid' in from each of the four cardinal directions
// and allows for alternation and staggering animations.

// You can slide the TEXTURE_TO into place or just have TEXTURE_FROM slide away
// via the `mode` enumerated value.

// The `bounce` effect will 'bounce' the strips as they hit the edge of the scene
// the amount of time indicated by the variable. They will bounce back the given
// percentage of the distance given by `bounce_vigor` during that time.

// The duration is respected. The stagger and bounce times will count against the
// duration. If you have 2 strips and stagger that second strip by 100ms and set
// the duration to 1s, it will make sure the second strip finishes at 1s. Same
// with bouncing. It will make sure the end of the last bounce happens at 1s or
// whatever value is set for duration. You will see strips speed up as you
// adjust either value as a result.

// [author] wilkie
// [license] CC0

shader_type canvas_item;

// [description] The source texture whereas the destination is what is in TEXTURE_TO
uniform sampler2D TEXTURE_FROM;

// [description] The destination texture.
uniform sampler2D TEXTURE_TO;

// [description] The global time since startup relative to TIME when the transition started.
uniform float START_TIME = 0.0;

// [description] The time in milliseconds to complete the transition
// [max] 60000.0
// [min] 0.0
uniform float duration = 1000.0;

// [description] The direction the initial slide takes place.
uniform int direction = 0;

// [description] Number of strips to use.
uniform int strips = 5;

// [description] Whether or not strip direction changes.
uniform bool alternate = true;

// [description] The amount of time to stagger animating strips.
uniform float stagger = 100.0;

// [description] The amount of time to bounce the strips as they hit the edges.
uniform float bounce = 0.0;

// [description] The percentage of distance the bounce makes.
uniform float bounce_vigor = 0.1;

// [description] What kind of sliding mode we want.
uniform int mode = 0;

void fragment() {
	// Mix the color from TEXTURE_FROM to the new TEXTURE_TO at a ratio relative
	// to the current duration (TIME) via the user-provided 'duration' adjusted
	// into seconds.

	// We do this by 'sliding' TEXTURE_TO into view based on the elapseed time

	// We first consider it assuming left-to-right and then swap things if the
	// `direction` indicates something top-to-bottom.

	// Calculate the position within the strip
	float position = (direction / 2) == 0 ? UV.y : UV.x;

	// Get the position along the slide
	float progress = (direction / 2) == 0 ? UV.x : UV.y;

	// Get the height of a strip as a percentage of the whole
	float strip_height = 1.0 / float(strips);

	// Get the height of the 'stride' which is the repeating set of strips
	float stride_height = strip_height * 2.0;

	// Get the index of the current strip
	float strip_index = floor(position / strip_height);

	// Determine maximum amount of stagger which we have to account for
	float max_stagger = stagger * float(strips - 1);

	// Determine if the bounce happens (when the TO texture moves into place)
	float max_bounce = mode == 1 ? 0.0 : bounce;

	// Get the elapsed time as a percentage where 0.0 is the beginning and 1.0 the end.
	float elapsed = max((TIME - START_TIME) / ((duration - max_stagger - max_bounce) / 1000.0), 0.0);
	elapsed -= strip_index * (stagger / 1000.0);

	// Determine how far over the end-time we are for bouncing
	float overflow = elapsed - 1.0;
	elapsed = min(max(elapsed, 0.0), 1.0);

	// If we are looking at a strip moving in the alternate direction, we switch that here.
	int real_direction = ((direction % 2) + (alternate ? int(step(0.5, mod(position, stride_height) / stride_height)) : 0)) % 2;

	// Compute the movement direction for the current strip
	float movement = real_direction == 0 ? 1.0 : -1.0;

	// Compute the UV translation for the FROM texture (unless we are not moving it)
	vec2 UV_from = UV + vec2(
		(direction / 2) == 0 ? (mode == 2 ? 0.0 : movement) * elapsed : 0.0,
		(direction / 2) != 0 ? (mode == 2 ? 0.0 : movement) * elapsed : 0.0
	);

	// If we are 'revealing' the next image, then we are not moving that image
	// So we set the movement to 0.0
	movement = mode == 1 ? 0.0 : movement;

	// Affect the elapsed slightly for the bounce
	float bounce_amount = step(0.0, overflow) * step(overflow, max_bounce / 1000.0) * overflow;
	elapsed -= max_bounce > 0.0 ? bounce_vigor * sin(PI * (bounce_amount / (max_bounce / 1000.0))) : 0.0;

	// Compute the UV translation for the TO texture
	vec2 UV_to = UV + vec2(
		(direction / 2) == 0 ? movement * (elapsed - 1.0) : 0.0,
		(direction / 2) != 0 ? movement * (elapsed - 1.0) : 0.0
	);

	// Get the cut-off bounds for the strip
	float start = real_direction == 0 ? progress : elapsed;
	float end = real_direction == 0 ? (1.0 - elapsed) : progress;

	// Paint it
	COLOR = vec4(
	    (texture(TEXTURE_FROM, UV_from).rgb * step(start, end)) + (texture(TEXTURE_TO, UV_to).rgb * step(end, start)),
		1.0
	);
}