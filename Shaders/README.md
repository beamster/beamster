# Shaders (Effects)

## Textures

You can specify `sampler2D` textures in the shader that are supplied by actual
image files. These image files will be loaded as textures and supplied to the
shader when the shader is loaded for use.

To do so, one must create a subdirectory located alongside the `gdshader` file
that has the same name as the shader file's base name. In this subdirectory,
the Effect loader will look for a file name with a base name (excluding file
extension) matching the exact case sensitive name of the uniform. This is the
same mechanism to supply buffer shaders (see [Buffers](#Buffers) below.)

This image should not be called `NOISE`, otherwise it will be confused for a
generated noise texture instead. Although, image texture samples like this take
precedence. They will be considered even if the sampler's name is `NOISE` as
long as a `NOISE.png` or other supported image file exists.

### Example

```
// My_Shader.gdshader
// will find IMAGE.png located at ./My_Shader/IMAGE.png
uniform sampler2D IMAGE;
```

## Buffers

You can specify `sampler2D` textures in the shader that are supplied by other
shader files. These shader files will be loaded as their own rendering buffer
and supplied to the shader when the main shader is loaded for use.

To do so, one must create a subdirectory located alongside the main `gdshader`
file that has the same name as the shader file's base name. In this
subdirectory, the Effect loader will look for a file name with a base name
matching the exact case sensitive name of the uniform with a `gdshader`
extension. This is a similar mechanism to supply texture images as their own
texture samples above.

This buffer should not be called `NOISE`, otherwise it will be confused for a
generated noise texture instead. Although, `gdshader` buffers like this take
precedence. They will be considered even if the buffer's name is `NOISE` as long
as a `NOISE.gdshader` exists. Buffers take precedence, also, over image samples.
So, if a uniform sampler is called `MY_IMAGE`, if both `MY_IMAGE.gdshader` and
`MY_IMAGE.png` exist, the shader wins and is used to supply the texture to the
main shader that is using it as a uniform.

Secondary buffers can themselves use other buffers. These are co-located with
this secondary buffer's own code. Therefore, if a secondary buffer wants to find
`BUFFER1`, then it looks for `BUFFER1.gdshader` in the same directory as itself.
This way, the main shader and a secondary buffer may both make use of another
secondary buffer at the same time.

Such buffers can "self-reference" themselves. In this case, `BUFFER1.gdshader`
might have a uniform defined as `BUFFER1`. This means, when it samples that
buffer, it will get the previous frame that was rendered. However, if the
`render_mode` is not `blend_disabled`, it may get something unexpected or a
black texture.

Buffers have an update mode that never clears the prior frame. This is useful
for referencing the prior frame in some way. Such secondary buffers can access
the original upstream texture at the same time. One thing you can do is have
a secondary buffer "remember" the texture at certain times. For instance, it can
draw the texture every 5 frames to implement a kind of strobe effect. Secondary
buffers are good at providing a kind of texture memory to main shaders.

## Predefined Noise Textures

You can have the program generate several types of noise textures for a shader
just by specifying a sampler2D with the name `NOISE` where you can add another
identifier afterward in this form: (or just have it called `NOISE` by itself.)

`NOISE_<anything>`

Then there are a series of parameters that one can annotate this uniform that
will affect the noise that will be generated. The noise texture is generated
just once when the shader is loaded for use.

Most of these parameters are a one-to-one mapping of the parameters and features
available using the Godot `FastNoiseLite` class. Most of this documentation is
a copy of what would be found there.

### Example

For instance, to generate a `SIMPLEX` noise texture of 4096 by 4096 pixels, just
add a sampler2D uniform to a shader or buffer that is named as such: (We add the
`repeat_enable` here just for common convenience with such textures where we
want it to loop around.)

```
// [noise_type] SIMPLEX
// [noise_width] 4096
uniform sampler2D NOISE_MY_TEXTURE : repeat_enable;
```

For more information about possible parameters, see [NOISE.md](./NOISE.md).
