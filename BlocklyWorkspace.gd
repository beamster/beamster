# TODO: remove bespoke field types and allow them to be plugins
# TODO: allow extensions and signals to faciliate the above
# TODO: shadow fields (mostly done)
# TODO: do not allow connections to connecting things
# TODO: do allow connections to override shadow inputs
# TODO: disconnect blocks via long hold or double click
# TODO: accessibility via keyboard interaction
# TODO: highlight pip when a connection would be made
# TODO: sound effects maybe!
# TODO: pip types and type checking
# TODO: render "errors" (highlight blocks)

# BlocklyField
# register_field("field_input", LineEdit,

class_name BlocklyWorkspace
extends Control


var _script: Action
var _toolbox: Tree = null
var _open_panel: Panel = null
var _toolbox_items: Dictionary = {}
var _panels: Dictionary = {}
var _fields: Dictionary = {}
var _blocks: Dictionary = {}


const DETACH_RESISTANCE: float = 32.0


# Registers a BlocklyField that will handle interaction with a particular type
# of field.
#
# For instance, we can register a class for "field_dropdown" types to render an
# OptionButton or some kind of Popup control. When we see this type of field, we
# will use this class to understand how to draw and present the control.
#
# This allows for custom extensibility with the block system.
func register_field(blockly_field_class):
	self._fields[blockly_field_class.field_type()] = blockly_field_class

# Whether or not the particular block input type is well understood.
func can_instantiate_field(type: String):
	return self._fields.has(type)

# Instantiates a BlocklyField based on the input type.
func instantiate_field(type: String, metadata: Dictionary = {}):
	if self._fields.has(type):
		return self._fields[type].new(self, metadata)

	return self._fields["field_input"].new(metadata)

func register_block(block: BlocklyBlock):
	if self._blocks.has(block.type):
		print("WARNING: block already defined: ", block.type)
		return

	self._blocks[block.type] = block

func get_block_by_type(type: String) -> BlocklyBlock:
	if not self._blocks.has(type):
		return null

	return self._blocks[type]

# This base class represents any field type that a block might contain.
class BlocklyField:
	var control: Control
	var type: String = ""
	var check: String = ""
	var metadata: Dictionary = {}
	var workspace: BlocklyWorkspace
	var output: String = "String"
	var default: Variant = null;
	var editable: bool = true:
		get:
			return editable
		set(new_value):
			editable = new_value
			if editable == true:
				self.control.mouse_filter = Control.MOUSE_FILTER_PASS
			else:
				self.control.mouse_filter = Control.MOUSE_FILTER_IGNORE
	var value: Variant = null:
		get:
			return self.get_value()
		set(new_value):
			self.set_value(new_value)

	signal resized
	signal changed


	static func field_type() -> String:
		return "field_dummy"

	static func control_class() -> Variant:
		return Control

	func get_value() -> Variant:
		return ''

	func set_value(_value: Variant):
		pass

	func serialize_to_code() -> String:
		return ''

	func duplicate() -> BlocklyField:
		var ret: BlocklyField = self.workspace.instantiate_field(self.get_script().field_type(), self.metadata)
		ret.control = self.control.duplicate()
		ret.bind_events()
		return ret

	func update(_block: BlocklyBlockInstance, _message: BlocklyMessage, _arg: BlocklyArgument, _updated_metadata: Dictionary = {}):
		pass

	func on_connected(_block: BlocklyBlockInstance, _message: BlocklyMessage, _arg: BlocklyArgument):
		pass

	func bind_events():
		if self.control.has_signal("resized"):
			var on_resize = func():
				self.control.reset_size()
				self.control.size.x = 1

				# Queue a redraw, which helps the resizing
				self.control.queue_redraw()
				self.resized.emit()
			self.control.resized.connect(on_resize)

		if self.control.has_signal("text_changed"):
			var on_update = func(_text: String):
				self.control.reset_size()
				self.control.size.x = 1

				# We might update the script itself
				self.changed.emit()

			self.control.text_changed.connect(on_update)

		if self.control.has_signal("item_selected"):
			var on_selected = func(_idx: int):
				# Update the size, if that matters
				self.control.reset_size()

				# We might update the script itself
				self.changed.emit()

			self.control.item_selected.connect(on_selected)

	func _init(target_workspace: BlocklyWorkspace, initial_metadata: Dictionary = {}):
		self.type = self.get_script().field_type()
		self.check = initial_metadata.get("check", "")
		self.metadata = initial_metadata
		self.control = self.get_script().control_class().new()
		self.workspace = target_workspace
		self.bind_events()

class BlocklyFieldDummy:
	extends BlocklyField

# Implements the "field_input" input type.
class BlocklyFieldInput:
	extends BlocklyField

	static func control_class() -> Variant:
		return LineEdit

	static func field_type() -> String:
		return "field_input"

	func get_value() -> Variant:
		var line_edit: LineEdit = self.control
		return line_edit.text

	func set_value(new_value: Variant):
		var line_edit: LineEdit = self.control
		line_edit.text = str(new_value)

	func serialize_to_code() -> String:
		var line_edit: LineEdit = self.control
		var ret = line_edit.text
		if line_edit.text == '':
			ret = self.default

		# Coerce type to number, if needed
		if self.check == "Number":
			ret = str(float(ret))

		return ret

	func update(_block: BlocklyBlockInstance, _message: BlocklyMessage, _arg: BlocklyArgument, updated_metadata: Dictionary = {}):
		var line_edit: LineEdit = self.control
		line_edit.expand_to_text_length = true
		line_edit.text = updated_metadata.get("text", "")
		self.default = updated_metadata.get("default", "")

# Implements a LineEdit that represents the keys pressed while in focus.
class KeyBindingControl:
	extends LineEdit

	var code

	func _init():
		# Do not allow traditional input events to write text
		self.editable = false

		# But, now, make sure it *looks* like a normal editable field
		self.add_theme_color_override("font_uneditable_color",
			self.get_theme_color("font_color")
		)

		# And then expand to fit the resulting message
		self.expand_to_text_length = true

	func _input(event: InputEvent):
		if self.has_focus():
			if event is InputEventKey and event.is_pressed():
				# Get the key event
				var key_event: InputEventKey = event

				# Resetting the size allows it to shrink
				self.reset_size()

				# Set the text to Godot's nifty event-to-text thing.
				# So, it might say "Ctrl+Alt+T", e.g.
				self.text = key_event.as_text_key_label()
				self.text_changed.emit(self.text)

				# Queue a redraw, which helps the resizing
				self.queue_redraw()

# Represents a "field_key" input type which captures keystrokes.
class BlocklyFieldKey:
	extends BlocklyField


	static func field_type() -> String:
		return "field_key"

	static func control_class() -> Variant:
		return KeyBindingControl

	func update(_block: BlocklyBlockInstance, _message: BlocklyMessage, _arg: BlocklyArgument, _updated_metadata: Dictionary = {}):
		var line_edit: LineEdit = self.control
		line_edit.expand_to_text_length = true
		line_edit.text = "A"

# Represents a "field_dropdown" input type.
class BlocklyFieldDropdown:
	extends BlocklyField


	func get_value() -> Variant:
		var option_button: OptionButton = self.control
		var option_id = option_button.get_item_index(
			option_button.get_selected_id()
		)
		if option_id >= 0:
			return option_button.get_item_metadata(
				option_id
			)

		return ''

	func set_value(new_value: Variant):
		var option_button: OptionButton = self.control
		print("finding ", new_value)
		for item in option_button.item_count:
			print("finding with ", option_button.get_item_metadata(item))
			if option_button.get_item_metadata(item) == new_value:
				option_button.selected = item
				return

	static func field_type() -> String:
		return "field_dropdown"

	static func control_class() -> Variant:
		return OptionButton

	func _init(target_workspace: BlocklyWorkspace, initial_metadata: Dictionary = {}):
		super._init(target_workspace, initial_metadata)
		var option_button: OptionButton = self.control
		option_button.fit_to_longest_item = false

	func duplicate() -> BlocklyField:
		var ret: BlocklyField = super.duplicate()

		# We have to duplicate OptionButton metadata, blah
		if ret.control is OptionButton:
			var orig_button: OptionButton = self.control
			var option_button: OptionButton = ret.control
			for idx in option_button.item_count:
				option_button.set_item_metadata(
					idx,
					orig_button.get_item_metadata(idx)
				)

		return ret

	func update(_block: BlocklyBlockInstance, _message: BlocklyMessage, _arg: BlocklyArgument, updated_metadata: Dictionary = {}):
		var option_button: OptionButton = self.control
		option_button.fit_to_longest_item = false

		var options = []
		if updated_metadata.has("options"):
			options = updated_metadata["options"]

		# Each option is a 2 part tuple of the visual string and the key
		# value to use as the actual value.
		for option in options:
			option_button.add_item(option[0])
			var id: String = option[1] if option.size() > 1 else option[0]
			var idx: int = option_button.item_count - 1
			option_button.set_item_metadata(idx, id)

	func serialize_to_code() -> String:
		return self.get_value()

# A custom field type for "field_source" in blocks.
class BlocklyFieldSource:
	extends BlocklyFieldDropdown


	static func field_type() -> String:
		return "field_source"

	func on_connected(block: BlocklyBlockInstance, _message: BlocklyMessage, _arg: BlocklyArgument):
		var option_button: OptionButton = self.control
		option_button.clear()

		# Get scene and add sources... keep it up-to-date
		var base_workspace: BlocklyWorkspace = block.get_workspace()
		if base_workspace and base_workspace.has_meta("scene"):
			var scene: Scene = base_workspace.get_meta("scene")

			# TODO: connect to signal for 'source_added' and update listing

			var source_list: VBoxContainer = scene.get_source_list()
			var root: TreeItem = source_list.get_node("SourceTree").get_root()

			if root:
				for item in root.get_children():
					print("SOURCE: ", item)
					var source: Variant = item.get_meta("reference")
					if source is Source.SourceInstance:
						option_button.add_item(item.get_text(0))
						option_button.set_item_metadata(option_button.item_count - 1, source.id())

			if option_button.item_count == 0:
				option_button.add_item("---")
				option_button.set_item_metadata(option_button.item_count - 1, "---")

# A custom field type for "field_property" in blocks.
class BlocklyFieldProperty:
	extends BlocklyFieldDropdown

	var properties: Dictionary = {}
	var block: BlocklyBlockInstance
	var message: BlocklyMessage
	var arg: BlocklyArgument


	static func field_type() -> String:
		return "field_property"

	func on_connected(with_block: BlocklyBlockInstance, with_message: BlocklyMessage, with_arg: BlocklyArgument):
		self.block = with_block
		self.message = with_message
		self.arg = with_arg

		var option_button: OptionButton = self.control

		# We need to find the "variant" field in this message, if it exists. This will
		# be updated to the appropriate control for the property type.
		var variant_arg: BlocklyArgument = null
		var variant_arg_index: int = -1
		var variant_arg_shadow_msg_index: int = -1
		var variant_arg_shadow_arg_index: int = -1
		var variant_arg_shadow: BlocklyBlockInstance = null

		for sibling_arg in message.args:
			variant_arg_index += 1
			if sibling_arg.type == "field_variant":
				variant_arg = sibling_arg
				break

			# Investigate the shadowed argument for a variant field
			if sibling_arg.type == "input_value" and sibling_arg.shadow:
				variant_arg_shadow_msg_index = -1
				for shadow_msg in sibling_arg.shadow.messages:
					variant_arg_shadow_msg_index += 1
					variant_arg_shadow_arg_index = -1
					for shadow_arg in shadow_msg.args:
						variant_arg_shadow_arg_index += 1
						if shadow_arg.type == "field_variant":
							variant_arg = sibling_arg
							variant_arg_shadow = block.shadows[message.index][sibling_arg.index]
							break

					if variant_arg != null:
						break

				if variant_arg == null:
					variant_arg_shadow = null
					variant_arg_shadow_arg_index = -1
					variant_arg_shadow_msg_index = -1

		if variant_arg == null:
			variant_arg_index = -1

		# Clear the option button
		option_button.clear()

		var source_block: BlocklyBlockInstance = block.get_ancestor_by_type("with_source")
		if source_block:
			var base_workspace: BlocklyWorkspace = block.get_workspace()
			var scene: Scene = null
			if base_workspace and base_workspace.has_meta("scene"):
				scene = base_workspace.get_meta("scene")

			if scene == null:
				return

			var inner_msg_index = -1
			for inner_message in source_block.base.messages:
				inner_msg_index += 1
				var inner_arg_index = -1
				for inner_arg in inner_message.args:
					inner_arg_index += 1
					if inner_arg.type == "field_source":
						var inner_option_button: OptionButton = source_block.controls[inner_msg_index][inner_arg_index]
						var source_id: String = inner_option_button.get_selected_metadata()

						if source_id:
							var source: Source.SourceInstance = scene.get_child(source_id)

							# Populate properties list
							# TODO: get a PropertyManager or something to parse these things
							if source:
								var props: Dictionary = source.get_script().properties()
								for prop in props:
									var label: String = prop
									var info: Dictionary = props[prop]
									if info.has("label"):
										label = info["label"]

									option_button.add_item(label)
									option_button.set_item_metadata(option_button.item_count - 1, prop)

								var variant_field: BlocklyField = null
								var variant_controls: Array = []
								var variant_control_index: int = variant_arg_index

								if variant_arg_shadow:
									# Variant field is shadowed
									variant_field = variant_arg_shadow.fields[variant_arg_shadow_msg_index][variant_arg_shadow_arg_index]
									variant_controls = variant_arg_shadow.controls[variant_arg_shadow_msg_index]
									variant_control_index = variant_arg_shadow_arg_index
								elif variant_arg:
									# Variant field is inline and part of ourselves
									variant_field = self.fields[message.index][variant_arg_index]
									variant_controls = self.controls[message.index]
									variant_control_index = variant_arg_index

								self.update(
									block,
									message,
									arg,
									{
										"properties": props,
										"variant_field": variant_field,
										"variant_arg_index": variant_control_index,
										"base_arg": block.base.messages[message.index].args[variant_arg_index],
										"base_arg_index": variant_arg_index,
										"block": self,
										"controls": variant_controls,
										"shadow": variant_arg_shadow,
									}
								)

		if option_button.item_count == 0:
			option_button.add_item("---")
			option_button.set_item_metadata(option_button.item_count - 1, "---")

	func update(updated_block: BlocklyBlockInstance, _updated_message: BlocklyMessage, _updated_arg: BlocklyArgument, _updated_metadata: Dictionary = {}):
		if not metadata.is_empty():
			self.properties = metadata

		var controls = self.properties.get("controls", [])
		var variant_arg_index = self.properties.get("variant_arg_index", -1)
		var base_arg_index = self.properties.get("base_arg_index", -1)
		var variant_field = self.properties.get("variant_field", null)
		var props = self.properties.get("properties", {})
		var variant_shadow: BlocklyBlockInstance = self.properties.get("variant_shadow", null)

		# Disable any properties that are the incorrect type
		var option_button: OptionButton = self.control
		for i in option_button.item_count:
			var item_prop = option_button.get_item_metadata(i)
			var info = props[item_prop]
			var option_type = info.get("type", "string")
			var option_check = "String"
			if typeof(option_type) == TYPE_ARRAY:
				option_check = "String"
			elif option_type == "int" or option_type == "float":
				option_check = "Number"
			elif option_type == "bool":
				option_check = "Boolean"

			if updated_block.base.output != null and updated_block.base.output != "" and updated_block.base.output != "Variant" and updated_block.base.output != option_check:
				option_button.set_item_disabled(i, true)
			else:
				option_button.set_item_disabled(i, false)

		if option_button.is_item_disabled(option_button.selected):
			for i in range(option_button.selected, option_button.item_count):
				if not option_button.is_item_disabled(i):
					option_button.selected = i
					break

		var prop = option_button.get_selected_metadata()

		# Update the selected item if the property does not match the type

		if prop:
			var info: Dictionary = props[prop]
			if variant_field:
				variant_field.update(block, message, arg, info)
				controls[variant_arg_index] = variant_field.control

				if variant_shadow:
					# Update the shadow's output type
					variant_shadow.output_type_override = variant_field.output

				if block:
					# And the input type of the argument
					block.check_overrides[message.index][base_arg_index] = variant_field.output
					print("updating base arg of ", message.message, " to ", variant_field.output)

				if block:
					block.queue_redraw()

	func bind_events():
		super.bind_events()

		self.changed.connect(func():
			if self.block:
				self.update(self.block, self.message, self.arg, {})
		)

# A custom field type for "field_signal" in blocks.
class BlocklyFieldSignal:
	extends BlocklyFieldDropdown

	var argument_list_arg: BlocklyArgument = null
	var block: BlocklyBlockInstance
	var message: BlocklyMessage
	var arg: BlocklyArgument


	static func field_type() -> String:
		return "field_signal"

	func get_value() -> Variant:
		var option_button: OptionButton = self.control
		var option_id = option_button.get_item_index(
			option_button.get_selected_id()
		)
		if option_id >= 0:
			return option_button.get_item_metadata(
				option_id
			)["name"]

		return ''

	func set_value(new_value: Variant):
		var option_button: OptionButton = self.control
		for item in option_button.item_count:
			if option_button.get_item_metadata(item)["name"] == new_value:
				option_button.selected = item
				return

	func on_connected(with_block: BlocklyBlockInstance, with_message: BlocklyMessage, with_arg: BlocklyArgument):
		var option_button: OptionButton = self.control
		self.block = with_block
		self.message = with_message
		self.arg = with_arg

		# We need to find the "argument_list" field in this message, if it
		# exists. This will be updated to house the extra arguments listing.
		var args_list_arg: BlocklyArgument = null

		for sibling_arg in with_message.args:
			if sibling_arg.type == "field_argument_list":
				args_list_arg = sibling_arg
				break

		# Retain the argument list
		self.argument_list_arg = args_list_arg

		# Clear the option button
		option_button.clear()

		var source_block: BlocklyBlockInstance = with_block.get_ancestor_by_type("with_source")
		if source_block:
			var source_workspace: BlocklyWorkspace = with_block.get_workspace()
			var scene: Scene = null
			if source_workspace and source_workspace.has_meta("scene"):
				scene = source_workspace.get_meta("scene")

			if scene == null:
				return

			var inner_msg_index = -1
			for inner_message in source_block.base.messages:
				inner_msg_index += 1
				var inner_arg_index = -1
				# Only show signals that are specific to the source
				var dummy: Source = Source.new()
				for inner_arg in inner_message.args:
					inner_arg_index += 1
					if inner_arg.type == "field_source":
						var inner_option_button: OptionButton = source_block.controls[inner_msg_index][inner_arg_index]
						var source_id: String = inner_option_button.get_selected_metadata()

						if source_id:
							var source: Source.SourceInstance = scene.get_child(source_id)

							# Populate signals list
							if source:
								for signal_info in source.get_base().get_signal_list():
									if not dummy.has_signal(signal_info["name"]):
										option_button.add_item(signal_info["name"])
										for arg_info in signal_info.get("args", []):
											arg_info["check"] = "Variant"
											if arg_info["name"].begins_with("str_"):
												arg_info["name"] = arg_info["name"].substr(4)
												arg_info["check"] = "String"
										signal_info["name"] = "get_base()." + signal_info["name"]
										option_button.set_item_metadata(option_button.item_count - 1, signal_info)
								for signal_info in source.get_signal_list():
									if not dummy.has_signal(signal_info["name"]):
										option_button.add_item(signal_info["name"])
										for arg_info in signal_info.get("args", []):
											arg_info["check"] = "Variant"
											arg_info["class"] = "Variant"
											if arg_info["name"].begins_with("str_"):
												arg_info["name"] = arg_info["name"].substr(4)
												arg_info["check"] = "String"
												arg_info["class"] = "String"
										option_button.set_item_metadata(option_button.item_count - 1, signal_info)

		if option_button.item_count == 0:
			option_button.add_item("---")
			option_button.set_item_metadata(option_button.item_count - 1, {"name": "child_added"})

		self.update(with_block, with_message, with_arg, {})

	func update(updated_block: BlocklyBlockInstance, updated_message: BlocklyMessage, updated_arg: BlocklyArgument, _updated_metadata: Dictionary = {}):
		if not self.argument_list_arg:
			return

		# Tell the argument list
		if self.argument_list_arg.field:
			var info: Dictionary = {}
			var option_button: OptionButton = self.control
			var option_id = option_button.get_item_index(
				option_button.get_selected_id()
			)
			if option_id >= 0:
				info = option_button.get_item_metadata(option_id)

			var field: BlocklyFieldArgumentList = block.fields[message.index][argument_list_arg.index]
			if field:
				field.update(updated_block, updated_message, updated_arg, info)

	func serialize_to_code() -> String:
		var info: Dictionary = {}
		var option_button: OptionButton = self.control
		var option_id = option_button.get_item_index(
			option_button.get_selected_id()
		)
		if option_id >= 0:
			info = option_button.get_item_metadata(option_id)

		return info.get("name", "child_added")

	func bind_events():
		super.bind_events()

		self.changed.connect(func():
			if self.block:
				self.update(self.block, self.message, self.arg, {})
		)

class BlocklyFieldArgumentList:
	extends BlocklyFieldDummy


	var args: Array = []

	static func field_type() -> String:
		return "field_argument_list"

	func update(updated_block: BlocklyBlockInstance, updated_message: BlocklyMessage, _updated_arg: BlocklyArgument, updated_metadata: Dictionary = {}):
		updated_block.reset_extra_arguments(updated_message.index)
		for arg_info in updated_metadata.get("args", []):
			updated_block.add_extra_argument(updated_message.index, {
				"type": "variable",
				"name": arg_info["name"],
				"check": "String",
			})
			print("ARG: ", arg_info)
		self.args = updated_metadata.get("args", [])

	func serialize_to_code() -> String:
		var serialized_args: String = ""
		var i = 0
		for arg_info in self.args:
			i += 1
			serialized_args += arg_info.get("name", "arg" + str(i)) + ": " + arg_info.get("class", "Variant") + ", "

		serialized_args = serialized_args.substr(0, serialized_args.length() - 2)
		return serialized_args


# A custom field type for "field_variable" in blocks.
class BlocklyFieldVariable:
	extends BlocklyFieldDropdown

	var variables: Dictionary = {}
	var variableTypes: Array = []


	static func field_type() -> String:
		return "field_variable"

	func update(updated_block: BlocklyBlockInstance, updated_message: BlocklyMessage, updated_arg: BlocklyArgument, updated_metadata: Dictionary = {}):
		# Look at the variableTypes
		self.variableTypes = updated_metadata.get("variableTypes", [])

		# Also gather the current variables
		self.on_connected(updated_block, updated_message, updated_arg)

	func on_connected(with_block: BlocklyBlockInstance, _with_message: BlocklyMessage, _with_arg: BlocklyArgument):
		# Update our listing to include all variables in scope
		var items: Array[String] = with_block.get_variables()
		var option_button: OptionButton = self.control

		var current_value = self.get_value()

		option_button.clear()
		for item in items:
			option_button.add_item(item)
			option_button.set_item_metadata(option_button.item_count - 1, item)

		self.set_value(current_value)

# A custom field type for "field_variant" in blocks.
class BlocklyFieldVariant:
	extends BlocklyFieldInput

	var property_info: Dictionary = {}


	static func field_type() -> String:
		return "field_variant"

	func update(_updated_block: BlocklyBlockInstance, _updated_message: BlocklyMessage, _updated_arg: BlocklyArgument, updated_metadata: Dictionary = {}):
		self.property_info = updated_metadata
		var property_type = self.property_info.get("type", "string")

		if self.property_info.has("type"):
			property_type = self.property_info["type"]

		var new_control: Control = null
		if typeof(property_type) == TYPE_ARRAY:
			# Dropdown
			self.output = "String"
			new_control = OptionButton.new()
			new_control.fit_to_longest_item = false
			for item in property_type:
				new_control.add_item(item)
				new_control.set_item_metadata(new_control.item_count - 1, item)
		elif property_type == "bool":
			# Dropdown (true/false)
			self.output = "Boolean"
			new_control = OptionButton.new()
			new_control.fit_to_longest_item = false
			for item in ["true", "false"]:
				new_control.add_item(item)
				new_control.set_item_metadata(new_control.item_count - 1, item)
		elif property_type == "int" or property_type == "string" or property_type == "float" or property_type == "file":
			# LineEdit
			if property_type == "int" or property_type == "float":
				self.output = "Number"
			else:
				self.output = "String"
			new_control = LineEdit.new()
		elif property_type == "color":
			# Color
			self.output = "Color"
		elif property_type == "scene":
			# Scene Selector
			self.output = "String"

		if new_control != null:
			# Update the control being used
			var old_control: Control = self.control
			var old_parent = old_control.get_parent()
			if old_parent:
				old_control.get_parent().remove_child(old_control)
			self.control = new_control
			if old_parent:
				old_parent.add_child(new_control)

			# Rebind our events
			self.bind_events()
			self.resized.emit()

	func serialize_to_code() -> String:
		var property_type = self.property_info.get("type", "string")
		if self.property_info.has("type"):
			property_type = self.property_info["type"]

		if typeof(property_type) == TYPE_ARRAY:
			# Dropdown
			var option_button: OptionButton = self.control
			var option_id = option_button.get_item_index(
				option_button.get_selected_id()
			)
			if option_id >= 0:
				return option_button.get_item_metadata(
					option_id
				)
		elif property_type == "bool":
			# Dropdown (true/false)
			var option_button: OptionButton = self.control
			var option_id = option_button.get_item_index(
				option_button.get_selected_id()
			)
			if option_id >= 0:
				return option_button.get_item_metadata(
					option_id
				)
			return 'false'
		elif property_type == "string" or property_type == "file":
			var line_edit: LineEdit = self.control
			return '"' + line_edit.text + '"'
		elif property_type == "int":
			var line_edit: LineEdit = self.control
			return str(int(line_edit.text))
		elif property_type == "float":
			var line_edit: LineEdit = self.control
			return str(float(line_edit.text))
		elif property_type == "color":
			# Color
			pass
		elif property_type == "scene":
			# Scene Selector
			pass

		return ''

class BlocklyStyle:
	var colour_primary: float
	var colour_secondary: float
	var colour_tertiary: float
	var hat: bool = false

class BlocklyTheme:
	var _categories: Dictionary = {}

	var hsv_saturation: float = 0.45
	var hsv_value: float = 0.65

	var workspace_background_colour: float
	var toolbox_background_colour: float
	var toolbox_foreground_colour: float
	var flyout_background_colour: float
	var flyout_foreground_colour: float
	var flyout_opacity: float
	var scrollbar_colour: float
	var scrollbar_opacity: float
	var insertion_marker_colour: float
	var insertion_marker_opacity: float
	var marker_colour: float
	var cursor_colour: float

	func set_category(category: String, style: BlocklyStyle):
		self._categories[category] = style

class BlocklyMessage:
	var message: String = ""
	var args: Array = []
	var _realized: Array = []

	var index: int

	func _init(message_index: int):
		self.index = message_index

	func realized() -> Array:
		if self._realized.size() == 0:
			# Tokenize the message string
			var msg = self.message
			var message_index = msg.find("%")
			while (message_index >= 0):
				var start: String = msg.substr(0, message_index)
				message_index += 1
				var arg_str_index: int = msg.find(" ", message_index)
				if arg_str_index == -1:
					arg_str_index = msg.length()
				var arg_str: String = msg.substr(message_index, arg_str_index - message_index)
				var arg_index = int(arg_str)
				msg = msg.substr(arg_str_index)
				message_index = msg.find("%")
				self._realized.append(start.strip_edges())
				self._realized.append(int(arg_index))

			if msg != "":
				self._realized.append(msg.strip_edges())

		return self._realized

	func _calculate_argument_geometry(arg: BlocklyArgument, canvas: BlocklyBlockInstance, inputs: Array, position: Vector2, height: float, item_idx: int, item_count: int) -> Vector3:
		# And then the input of the argument (and any set value) defines
		# the width of that input
		if arg.type == "input_value" and item_idx + 1 == item_count:
			# Outward pip influences our height
			var input = inputs[self.index][arg.index]
			if input:
				height = input.total_size.y
		elif arg.type == "variable" or (arg.type == "input_value" and item_idx + 1 != item_count):
			# If no set value, this is a hole
			var input_width = 22
			var input = inputs[self.index][arg.index]
			if input:
				input_width = input.total_size.x + 3
				height = input.total_size.y + 8
			else:
				# Empty hole
				# This gap is as high as an output block
				height = max(height, 36.0)
			position.x += 16 + input_width
		elif arg.type == "input_statement":
			# Embedded blocks
			var input = inputs[self.index][arg.index]
			if input:
				height = max(height, input.get_total_height())
		elif arg.field and arg.field.control:
			# Some kind of UI component, probably
			var field: Control = canvas.controls[self.index][arg.index]
			position.x += field.size.x + 6
			height = max(height, field.size.y)

		return Vector3(position.x, position.y, height)

	func calculate_geometry(canvas: BlocklyBlockInstance, font: Font, font_size: int) -> Vector2:
		# Measure for each string
		var ret: Vector2 = Vector2(0, 0)

		var height = 0.0
		var inputs = canvas.inputs

		var item_idx = 0
		var extra_args: Array[BlocklyArgument] = canvas.get_extra_arguments(self.index)
		var item_count = self.realized().size() + extra_args.size()

		for item in self.realized():
			if item is String:
				var size = font.get_string_size(item, HORIZONTAL_ALIGNMENT_LEFT, -1, font_size)
				ret.x += 4 + size.x
				height = max(height, 28.0)
			else:
				var arg: BlocklyArgument = self.args[item - 1]
				var result: Vector3 = self._calculate_argument_geometry(arg, canvas, inputs, ret, height, item_idx, item_count)
				ret = Vector2(result.x, result.y)
				height = max(height, result.z)
			item_idx += 1

		# Look at extra arguments
		for arg in extra_args:
			item_idx += 1
			var result: Vector3 = self._calculate_argument_geometry(arg, canvas, inputs, ret, height, item_idx, item_count)
			ret = Vector2(result.x, result.y)
			height = max(height, result.z)

		if self.args.size() == 1 and self.args[0].type == "input_value" and self.realized()[-1] is int and extra_args.size() == 0:
			ret.x += 6

		ret.x += 16
		if height > ret.y:
			ret.y = height
		return ret

	func _draw_argument(arg: BlocklyArgument, total_size: Vector2, x_direction: int, canvas: BlocklyBlockInstance, inputs: Array, position: Vector2, height: float, item_idx: int, item_count: int, hole_color: Color, border_light_color: Color, border_dark_color: Color, actually_draw: bool):
		if arg.type == "variable" or (arg.type == "input_value" and item_idx + 1 != item_count):
			# This gap is as high as an output block
			height = max(height, 36.0)

			# Draw that block
			var points: PackedVector2Array = []
			position.x += 6
			position.y += 3
			var input_width = 22
			var input = inputs[self.index][arg.index]
			if input:
				input_width = input.total_size.x + 3
				height = input.total_size.y + 8
			points.append(Vector2((position.x + 4) * x_direction, position.y))
			points.append(Vector2((position.x + 4 + input_width) * x_direction, position.y))
			points.append(Vector2((position.x + 4 + input_width) * x_direction, position.y + height - 6.0))
			points.append(Vector2((position.x + 4) * x_direction, position.y + height - 6.0))
			points.append(Vector2((position.x + 4) * x_direction, position.y + 22))
			canvas._append_pip(arg.check, points, Vector2(position.x + 4, position.y + 21), true)
			if canvas.connectable and actually_draw:
				if canvas._pip_index >= canvas.pips.size():
					canvas.pips.append(canvas._add_pip(false, "value", Vector2(position.x - 3, position.y + 9), Vector2(4, 12), self.index, arg.index))
				else:
					canvas._update_pip(canvas._pip_index, Vector2(position.x - 3, position.y + 9), Vector2(4, 12))

			if input:
				var x_offset = 0
				if canvas.is_right_to_left():
					x_direction = -1
					x_offset = 4
				input.position = Vector2(position.x - 3, position.y + 9) + Vector2((9 - x_offset) * x_direction, -8)
			canvas._pip_index += 1
			if actually_draw:
				canvas.draw_colored_polygon(points, hole_color)
			if !input:
				if actually_draw:
					canvas.draw_polyline(points, border_light_color, 0.5, true)
				var dark_points: PackedVector2Array = []
				canvas._append_pip(arg.check, dark_points, Vector2(position.x + 4, position.y + 21), true, true)
				dark_points.append(Vector2((position.x + 4) * x_direction, position.y))
				dark_points.append(Vector2((position.x + 4 + input_width) * x_direction, position.y))
				if actually_draw:
					canvas.draw_polyline(dark_points, border_dark_color, 0.5, true)
				dark_points = []
				dark_points.append(Vector2((position.x + 4) * x_direction, position.y + 30))
				dark_points.append(Vector2((position.x + 4) * x_direction, position.y + 22))
				if actually_draw:
					canvas.draw_polyline(dark_points, border_dark_color, 0.5, true)
			position.y -= 3
			position.x += 10 + input_width
		elif arg.type == "input_statement":
			# Embedded blocks
			var input = inputs[self.index][arg.index]
			if input:
				height = max(height, input.get_total_height())
		elif arg.field and arg.field.control:
			# Some kind of UI component
			var field_control: Control = canvas.controls[self.index][arg.index]
			field_control.position.x = position.x
			field_control.position.y = position.y + ((total_size.y - field_control.size.y) / 2)
			position.x += field_control.size.x + 6

		return Vector3(position.x, position.y, height)

	func draw(canvas: BlocklyBlockInstance, position: Vector2, font: Font, font_size: int, hole_color: Color, border_light_color: Color, border_dark_color: Color, actually_draw: bool = true) -> Vector2:
		var total_size: Vector2 = self.calculate_geometry(canvas, font, font_size)
		var ret: Vector2 = Vector2(0, 0)
		var original_position = position
		var height = 0.0
		var item_idx = 0
		var extra_args: Array[BlocklyArgument] = canvas.get_extra_arguments(self.index)
		var item_count = self.realized().size() + extra_args.size()
		var inputs = canvas.inputs

		var x_direction = 1
		if canvas.is_right_to_left():
			x_direction = -1

		for item in self.realized():
			if item is String:
				var size = font.get_string_size(item, HORIZONTAL_ALIGNMENT_LEFT, -1, font_size)
				var text_height = total_size.y
				if total_size.y > 36:
					text_height = max(height, 28.0)

				var text_x = position.x
				var alignment: HorizontalAlignment = HORIZONTAL_ALIGNMENT_LEFT
				if canvas.is_right_to_left():
					text_x = -(text_x + size.x)
					alignment = HORIZONTAL_ALIGNMENT_RIGHT

				if actually_draw:
					canvas.draw_string(font, Vector2(text_x, position.y + ((text_height - size.y) / 2) + font_size), item, alignment, -1, font_size, Color(1, 1, 1))
				position.x += size.x + 4
				if (position - original_position).y + size.y > height:
					height = (position - original_position).y + size.y
			else:
				var arg: BlocklyArgument = self.args[item - 1]
				var result: Vector3 = self._draw_argument(arg, total_size, x_direction, canvas, inputs, position, height, item_idx, item_count, hole_color, border_light_color, border_dark_color, actually_draw)
				position = Vector2(result.x, result.y)
				height = result.z
			item_idx += 1

			if (position - original_position).x > ret.x:
				ret.x = (position - original_position).x

		# Look at extra arguments
		for arg in extra_args:
			var result: Vector3 = self._draw_argument(arg, total_size, x_direction, canvas, inputs, position, height, item_idx, item_count, hole_color, border_light_color, border_dark_color, actually_draw)
			position = Vector2(result.x, result.y)
			height = result.z

			if (position - original_position).x > ret.x:
				ret.x = (position - original_position).x
			item_idx += 1

		return total_size

class BlocklyArgument:
	var type: String
	var name: String
	var variable: String = ""
	var default: Variant = null
	var variableTypes: Array = []
	var variableBlock: BlocklyBlock = null
	var index: int
	var field: BlocklyField = null
	var shadow: BlocklyBlock = null
	var check: String = ""
	var metadata: Dictionary = {}

	func _init(target_workspace: BlocklyWorkspace, initial_metadata: Dictionary = {}, initial_index: int = -1):
		self.metadata = initial_metadata

		for k in ["type", "name", "variable", "default", "check"]:
			if initial_metadata.has(k):
				self.set(k, initial_metadata[k])

		if initial_metadata.has("variable_types"):
			self.variableTypes = initial_metadata["variableTypes"]

		if initial_metadata.has("shadow"):
			# Create the shadow block definition
			if not initial_metadata["shadow"].has("output"):
				initial_metadata["shadow"]["output"] = null
			self.shadow = BlocklyBlock.import_json(target_workspace, initial_metadata["shadow"])

		if initial_metadata.get("type", "") == "variable":
			# Create the variable block
			var block_base: BlocklyBlock = target_workspace.get_block_by_type("get_variable")
			if block_base:
				self.variableBlock = block_base
				if initial_metadata.has("name"):
					self.variable = metadata["name"]

		# Instantiate the field control
		if target_workspace.can_instantiate_field(self.type):
			self.field = target_workspace.instantiate_field(self.type, initial_metadata)

		self.index = initial_index

	func instantiate_variable_block() -> BlocklyBlockInstance:
		var ret: BlocklyBlockInstance = self.variableBlock.instantiate()

		# Set output type
		if self.check:
			ret.output_type_override = self.check

		# Set variable
		if self.variable:
			ret.connected.connect(func():
				for message in ret.base.messages:
					for arg in message.args:
						var with_field: BlocklyField = ret.fields[message.index][arg.index]
						if with_field:
							if with_field.type == "field_variable":
								# We need to make sure it has the variable listing
								with_field.on_connected(ret, message, arg)

								# Set the variable
								with_field.set_value(self.variable)
			)

		return ret

# Blocks have categories and then attributes.
class BlocklyBlock:
	var style: BlocklyStyle
	var type: String
	var messages: Array = []
	var tooltip: String
	var theme: BlocklyTheme
	var colour: float
	var variables: Array[String] = []

	var output: String = ""
	var previous_statement: String = ""
	var next_statement: String = ""
	var serialize: Array = []

	static func import_json(workspace: BlocklyWorkspace, metadata: Dictionary) -> BlocklyBlock:
		var ret: BlocklyBlock = BlocklyBlock.new()

		if metadata.has("type"):
			ret.type = str(metadata["type"])

		if metadata.has("previousStatement"):
			ret.previous_statement = str(metadata["previousStatement"])

		if metadata.has("nextStatement"):
			ret.next_statement = str(metadata["nextStatement"])

		if metadata.has("output"):
			ret.output = str(metadata["output"])

		if metadata.has("colour"):
			ret.colour = float(metadata["colour"]) / 360.0
		else:
			ret.colour = 120.0 / 360.0

		if metadata.has("serialize"):
			ret.serialize = metadata["serialize"]

		for k in metadata.keys():
			var s: String = k
			if k.begins_with("message"):
				var index = int(s.substr(7))

				# Place a max on the argument count
				if index > 16:
					continue

				# Ensure that are message listing fits this message
				while ret.messages.size() <= index:
					ret.messages.append(BlocklyMessage.new(index))

				ret.messages[index].message = metadata[k]
			elif k.begins_with("args"):
				var index = int(s.substr(4))

				# Place a max on the argument count
				if index > 16:
					continue

				# Ensure that are message listing fits this message
				while ret.messages.size() <= index:
					ret.messages.append(BlocklyMessage.new(index))

				ret.messages[index].args = []
				var arg_index = -1
				for arg in metadata[k]:
					arg_index += 1
					var blockly_arg = BlocklyArgument.new(workspace, arg, arg_index)
					ret.messages[index].args.append(blockly_arg)

					if blockly_arg.variable:
						ret.variables.append(blockly_arg.variable)

		return ret

	func _init():
		self.theme = BlocklyTheme.new()

	func instantiate(duplicating: BlocklyBlockInstance = null) -> BlocklyBlockInstance:
		return BlocklyBlockInstance.new(self, duplicating)

const RADIUS = 8

class BlocklyBlockInstance:
	extends Control

	var base: BlocklyBlock
	var polygon: PackedVector2Array
	var pips: Array = []
	var inputs: Array = []
	var check_overrides: Array = []
	var controls: Array = []
	var fields: Array = []
	var shadows: Array = []
	var extra_args: Array = []
	var extra_variables: Array[String] = []
	var output: BlocklyBlockInstance = null
	var output_type_override: String = ""
	var output_message_index: int = -1
	var output_arg_index: int = -1
	var previous: BlocklyBlockInstance = null
	var previous_message_index: int = -1
	var previous_arg_index: int = -1
	var next: BlocklyBlockInstance = null
	var right_to_left: bool = false
	var opacity: float = 1.0
	var shadowing: bool = false
	var spawning: bool = false
	var anchored: bool = false
	var movable: bool = true
	var connectable: bool = true
	var editable: bool = true:
		get:
			return editable
		set(value):
			editable = value

			# Disable/Enable all fields
			var msg_index = -1
			for message in self.base.messages:
				msg_index += 1
				var arg_index = -1
				for arg in message.args:
					arg_index += 1
					var field: BlocklyField = self.fields[msg_index][arg_index]
					if field:
						field.editable = editable
	var total_size: Vector2 = Vector2(0.0, 0.0)

	var _pending_connection: BlocklyBlockInstance
	var _pending_connecting_pip: Area2D
	var _pending_connection_pip: Area2D
	var _pip_index: int = 0

	var _font: Font
	var _font_size: int


	signal connected


	func _init(with_base: BlocklyBlock, duplicating: BlocklyBlockInstance = null):
		self.base = with_base
		self._font = ThemeDB.fallback_font
		self._font_size = ThemeDB.fallback_font_size

		# Create a space for attaching blocks / values to arguments
		for message in self.base.messages:
			self.inputs.append([])
			self.check_overrides.append([])
			self.fields.append([])
			self.controls.append([])
			self.shadows.append([])
			var empty_args_list: Array[BlocklyArgument] = []
			self.extra_args.append(empty_args_list)
			for arg in message.args:
				self.inputs[-1].append(null)
				self.check_overrides[-1].append(arg.check)
				self.shadows[-1].append(null)

				# Create the shadow block!
				if arg.shadow:
					var shadow_block: BlocklyBlockInstance = BlocklyBlockInstance.new(arg.shadow)
					shadow_block.position = Vector2(0, 0)
					shadow_block.shadowing = true
					shadow_block.opacity = 0.1
					self.inputs[message.index][arg.index] = shadow_block
					self.shadows[message.index][arg.index] = shadow_block
					shadow_block.output = self
					self.add_child(shadow_block)

				if arg.variableBlock:
					var variable_block: BlocklyBlockInstance = arg.instantiate_variable_block()
					variable_block.position = Vector2(0, 0)
					variable_block.spawning = true
					variable_block.opacity = 0.5
					self.inputs[message.index][arg.index] = variable_block
					variable_block.output = self
					self.add_child(variable_block)
					variable_block.emit_all("connected")

				if arg.field:
					var new_field = null
					if duplicating:
						new_field = duplicating.fields[message.index][arg.index].duplicate()
					else:
						new_field = arg.field.duplicate()
						if arg.type == "field_argument_list":
							print("hey")
					new_field.editable = self.editable
					self.fields[-1].append(new_field)
					self.controls[-1].append(new_field.control)
					self.add_child(new_field.control)
					new_field.resized.connect(func():
						self._draw(false)
						self.queue_redraw_ancestors()
					)
					new_field.changed.connect(func():
						self.queue_serialize()
					)
					if not duplicating:
						new_field.update(self, message, arg, arg.metadata)
				else:
					self.fields[-1].append(null)
					self.controls[-1].append(null)

		var msg_index = -1
		for message in self.base.messages:
			msg_index += 1
			var arg_index = -1
			for arg in message.args:
				arg_index += 1
				if arg.field:
					var field: BlocklyField = self.fields[msg_index][arg_index]
					if field:
						self.connected.connect(func():
							field.on_connected(
								self,
								self.base.messages[msg_index],
								self.base.messages[msg_index].args[arg_index]
							)
						)

		# By default, blocks that have previous statements or are
		# inputs are see-through.
		if self.base.previous_statement:
			self.opacity = 0.5

		if self.base.output:
			self.opacity = 0.5

		# Duplicate some properties if we are cloning from an instance and not a
		# block definition.
		if duplicating:
			if duplicating.output_type_override != "":
				self.output_type_override = duplicating.output_type_override

		self.size = Vector2(100, 25)
		self._draw(false)

	func get_ancestor_by_type(type: String) -> BlocklyBlockInstance:
		var parent: BlocklyBlockInstance = self.get_parent()
		while parent:
			if parent.base.type == type:
				return parent
			var next_ancestor = parent.get_parent()
			if next_ancestor is BlocklyBlockInstance:
				parent = next_ancestor
			else:
				parent = null

		return null

	func has_ancestor_by_type(type: String) -> bool:
		return self.get_ancestor_by_type(type) != null

	func get_workspace() -> BlocklyWorkspace:
		var parent: Node = self.get_parent()
		while parent:
			if parent is BlocklyWorkspace:
				return parent
			parent = parent.get_parent()

		return null

	func emit_all(signal_name: String):
		if self.has_signal(signal_name):
			self.emit_signal(signal_name)

		# Go through connected nodes
		for args in self.inputs:
			for arg in args:
				if arg:
					arg.emit_all(signal_name)

		if self.next:
			self.next.emit_all(signal_name)

	# Causes the script to re-serialize upon a change
	func queue_serialize():
		if self.base.previous_statement:
			if self.previous:
				self.previous.queue_serialize()
		elif self.base.output:
			if self.output:
				self.output.queue_serialize()
		else:
			# We are the root?
			self.get_parent().get_parent().get_parent().serialize_to_code(self)

	func is_right_to_left() -> bool:
		# Negotiate with parent
		#if self.get_parent() is BlocklyBlockInstance:
		#	return self.get_parent().is_right_to_left()

		if self.previous:
			return self.previous.is_right_to_left()

		if self.output:
			return self.output.is_right_to_left()

		# Otherwise, just use our own value
		return self.right_to_left

	func get_block_at_point(point: Vector2, ignore_if_input: bool = true, inherit_shadow: bool = true) -> BlocklyBlockInstance:
		for index in self.get_child_count():
			var child = self.get_child(index)
			if child is BlocklyBlockInstance:
				if child.contains_point(point - self.position):
					if child.shadowing:
						if child.get_block_at_point(point - self.position, ignore_if_input, inherit_shadow):
							print("shadowed ok! ", ignore_if_input)
							return self
						return null
					else:
						return child.get_block_at_point(point - self.position, ignore_if_input, inherit_shadow)

		if self.contains_point(point, false):
			# Check to see if we are *actually* over a field.
			if ignore_if_input:
				var msg_index = -1
				for message in self.base.messages:
					msg_index += 1
					var arg_index = -1
					for arg in message.args:
						arg_index += 1
						var field: BlocklyField = self.fields[msg_index][arg_index]
						if field:
							#print("looking at field inside (", msg_index, ")(", arg_index, ") ", message.message," includes? ", point - self.position, " at ", field.control.get_rect())
							if field.control.get_rect().has_point(point - self.position):
								#print("ignored via input")
								return null
			return self

		return null

	func contains_point(coord: Vector2, recurse: bool = true) -> bool:
		# If we have the points for the outer polygon, use them
		if self.polygon:
			var ret = Geometry2D.is_point_in_polygon(coord - self.position, self.polygon)
			if not ret and recurse:
				for child in self.get_children():
					if child is BlocklyBlockInstance:
						ret = child.contains_point(coord - self.position)
						if ret:
							return ret
			return ret

		# Otherwise, we look at the bounding rectangle instead
		var rect: Rect2 = self.get_rect()
		return rect.has_point(coord)

	func _append_arc(points: PackedVector2Array, pos: Vector2, radius: Vector2, start_angle: float, end_angle: float):
		var x_direction = 1
		if self.is_right_to_left():
			x_direction = -1

		for i in 16:
			var angle = start_angle + (i * ((end_angle - start_angle) / 16.0))
			if i == 15:
				angle = end_angle
			points.append(
				Vector2(
					(pos.x + (radius.x * sin(angle))) * x_direction,
					pos.y + (radius.y * cos(angle))
				)
			)

	func _append_cos(points: PackedVector2Array, pos: Vector2, region_size: Vector2):
		var x_direction = 1
		if self.is_right_to_left():
			x_direction = -1

		for i in 16:
			var angle = i * (2.0 * PI / 16.0)
			if i == 15:
				angle = 2 * PI
			points.append(
				Vector2(
					(pos.x + (i / 15.0) * region_size.x) * x_direction,
					pos.y + ((cos(angle) - 1) / 2) * region_size.y
				)
			)

	func _append_pip(check: String, points: PackedVector2Array, pos: Vector2, left: bool, half: bool = false):
		if check == "Number":
			return self._append_pip_round(points, pos, left, half)
		elif check == "String":
			return self._append_pip_square(points, pos, left, half)
		else:
			return self._append_pip_puzzle(points, pos, left, half)

	func _append_pip_square(points: PackedVector2Array, pos: Vector2, left: bool, half: bool = false):
		var offset = Vector2(-6.0, 0)

		# Only append the first point if we are drawing the whole thing
		# or we are drawing half of the inward pip (!left)
		if not half:
			points.append(pos)

		pos += offset
		if left or not half:
			# Top (!left) / Bottom (left) line
			points.append(pos)

		offset.y = -offset.x * 2
		if left:
			offset.y = -offset.y
		offset.x = 0
		pos += offset

		# Middle line
		points.append(pos)
		offset.x = offset.y / 2
		if left:
			offset.x = -offset.x
		offset.y = 0
		pos += offset
		points.append(pos)

	func _append_pip_round(points: PackedVector2Array, pos: Vector2, left: bool, half: bool = false):
		var radius: float = 6.0
		var offset: Vector2 = Vector2(
			0,
			radius
		)

		if left:
			if half:
				self._append_arc(points, pos - offset, Vector2(radius, radius), 7 * PI / 4, 4 * PI / 4)
			else:
				self._append_arc(points, pos - offset, Vector2(radius, radius), 8 * PI / 4, 4 * PI / 4)
		else:
			if half:
				self._append_arc(points, pos + offset, Vector2(radius, radius), 7 * PI / 4, 8 * PI / 4)
			else:
				self._append_arc(points, pos + offset, Vector2(radius, radius), 4 * PI / 4, 8 * PI / 4)

		if left:
			pos.y -= radius * 2
		else:
			pos.y += radius * 2

	func _append_pip_puzzle(points: PackedVector2Array, pos: Vector2, left: bool, half: bool = false):
		var radius: float = 2.0
		var offset: Vector2 = Vector2(
			-radius / sqrt(2),
			radius / sqrt(2)
		)

		if left:
			if not half:
				self._append_arc(points, pos + offset, Vector2(radius, radius), 3 * PI / 4, 5 * PI / 4)
			pos.x += offset.x * 2
			offset.y = -offset.y

			if not half:
				self._append_arc(points, pos + offset, Vector2(radius, radius), 9 * PI / 4, 7 * PI / 4)

			pos.x += offset.x * 2

			if not half:
				self._append_arc(points, pos + Vector2(6, -6), Vector2(6 * sqrt(2), 6 * sqrt(2)), 7 * PI / 4, 5 * PI / 4)
			else:
				self._append_arc(points, pos + Vector2(6, -6), Vector2(6 * sqrt(2), 6 * sqrt(2)), 6.75 * PI / 4, 5 * PI / 4)
			pos.y -= 12

			offset.y = -offset.y
			offset.x = -offset.x
			self._append_arc(points, pos + offset, Vector2(radius, radius), 5 * PI / 4, 3 * PI / 4)
			pos.x += offset.x * 2
			offset.y = -offset.y
			self._append_arc(points, pos + offset, Vector2(radius, radius), 7 * PI / 4, 9 * PI / 4)
		else:
			offset.y = -offset.y
			if not half:
				self._append_arc(points, pos + offset, Vector2(radius, radius), 9 * PI / 4, 7 * PI / 4)
			pos.x += offset.x * 2
			offset.y = -offset.y
			if not half:
				self._append_arc(points, pos + offset, Vector2(radius, radius), 3 * PI / 4, 5 * PI / 4)
			pos.x += offset.x * 2

			if not half:
				self._append_arc(points, pos + Vector2(6, 6), Vector2(6 * sqrt(2), 6 * sqrt(2)), 5 * PI / 4, 7 * PI / 4)
			else:
				self._append_arc(points, pos + Vector2(6, 6), Vector2(6 * sqrt(2), 6 * sqrt(2)), 6.85 * PI / 4, 7 * PI / 4)
			pos.y += 12

			offset.y = -offset.y
			offset.x = -offset.x
			self._append_arc(points, pos + offset, Vector2(radius, radius), 7 * PI / 4, 9 * PI / 4)
			pos.x += offset.x * 2
			offset.y = -offset.y
			self._append_arc(points, pos + offset, Vector2(radius, radius), 5 * PI / 4, 3 * PI / 4)

	func _add_pip(input: bool, type: String, pip_position: Vector2, pip_size: Vector2, message_index: int = -1, arg_index: int = -1) -> Area2D:
		var pip_area: Area2D = Area2D.new()
		var pip_collider: CollisionShape2D = CollisionShape2D.new()
		var pip_shape: RectangleShape2D = RectangleShape2D.new()
		pip_shape.set_size(pip_size)
		pip_collider.shape = pip_shape
		pip_area.add_child(pip_collider)
		self.add_child(pip_area)
		if self.is_right_to_left():
			pip_position = Vector2(pip_position.x + pip_size.x, pip_position.y)
			pip_position.x *= -1
		pip_area.position = pip_position
		pip_area.set_meta("input", input)
		pip_area.set_meta("type", type)
		pip_area.set_meta("message_index", message_index)
		pip_area.set_meta("arg_index", arg_index)

		var callback = func (area_rid: RID, area: Area2D, area_shape_index: int, local_shape_index: int):
			self.pip_entered(pip_area, area_rid, area, area_shape_index, local_shape_index)

		pip_area.area_shape_entered.connect(callback)
		pip_area.area_exited.connect(pip_exited)

		return pip_area

	func _update_pip(pip_index: int, pip_position: Vector2, pip_size: Vector2):
		var pip_area: Area2D = self.pips[pip_index]
		var pip_collider: CollisionShape2D = pip_area.get_child(0)
		var pip_shape: RectangleShape2D = pip_collider.shape

		pip_shape.set_size(pip_size)
		if self.is_right_to_left():
			pip_position = Vector2(pip_position.x + pip_size.x, pip_position.y)
			pip_position.x *= -1
		pip_area.position = pip_position

	func pip_exited(_area: Area2D):
		self._pending_connection = null

	func pip_entered(pip_area: Area2D, _area_rid: RID, area: Area2D, _area_shape_index: int, _local_shape_index: int):
		# Get the type to see if it is compatible to attach
		if area.get_meta("type") == pip_area.get_meta("type") and area.get_meta("input") != pip_area.get_meta("input"):
			# Keep track of the connection we are hovering over
			# When we release (or otherwise commit) the movement of the block,
			# we will look at this to make the connection.
			var parent = area.get_parent()
			if parent == self:
				parent = pip_area.get_parent()
				self._pending_connecting_pip = area
				self._pending_connection_pip = pip_area
			else:
				self._pending_connecting_pip = pip_area
				self._pending_connection_pip = area
			self._pending_connection = parent

	func get_total_height() -> float:
		var height = self.size.y

		if self.next:
			height += self.next.get_total_height() - 5

		return height + 14

	func get_font() -> Font:
		return self._font

	func set_font(value: Font):
		self._font = value

	func get_font_size() -> int:
		return self._font_size

	func set_font_size(value: int):
		self._font_size = value

	# Determine the size by looking at ourselves and our children's boxes.
	func compute_size():
		# Start with our own size. Everything is relative to this.
		self.total_size = self.size

		# Look at the thing below us and add it's size
		if self.next:
			var next_position = self.next.position
			var next_size = self.next.total_size
			self.total_size.x = max(self.total_size.x, next_position.x + next_size.x)
			self.total_size.y = max(self.total_size.y, next_position.y + next_size.y)

		# Look at all inputs
		for args in self.inputs:
			for arg in args:
				if arg:
					var input_position = arg.position
					var input_size = arg.total_size
					self.total_size.x = max(self.total_size.x, input_position.x + input_size.x)
					self.total_size.y = max(self.total_size.y, input_position.y + input_size.y)

		return self.total_size

	func queue_redraw_ancestors():
		var block = self
		while block is BlocklyBlockInstance:
			block._draw(false)
			var parent = block.get_parent()
			if not parent is BlocklyBlockInstance:
				parent = block.previous
			block = parent

	func queue_redraw_all(with_opacity: float = -1):
		print("qra ", self)
		for args in self.inputs:
			for arg in args:
				if arg:
					if with_opacity != -1 and not arg.shadowing:
						arg.opacity = with_opacity
					arg._draw(false)
					arg.queue_redraw_all()

		if self.next:
			self.next._draw(false)
			self.next.queue_redraw_all()

	func _draw(actually_draw: bool = true):
		var font: Font = self.get_font()
		var font_size: int = self.get_font_size()

		var border_light_color: Color = Color.from_hsv(self.base.colour, self.base.theme.hsv_saturation, min(1.0, self.base.theme.hsv_value + 0.3))
		var border_dark_color: Color = Color.from_hsv(self.base.colour, self.base.theme.hsv_saturation, max(0, self.base.theme.hsv_value - 0.3))

		var with_opacity = self.opacity
		if self.previous and not self.shadowing:
			with_opacity = self.previous.opacity
		if self.output and not self.shadowing:
			with_opacity = self.output.opacity
		border_light_color.s = border_light_color.s * with_opacity
		border_light_color.a = with_opacity
		border_dark_color.s = border_dark_color.s * with_opacity
		border_dark_color.a = with_opacity
		#border_dark_color = border_light_color
		var y = 0

		# Reset pips
		self._pip_index = 0

		# Negotiate messages to figure out the width of the base block
		var width: float = 30.0
		var top_width: float = -1.0
		var msg_index = -1
		for msg in self.base.messages:
			msg_index += 1

			# Determine width of message
			var msg_size = msg.calculate_geometry(self, font, font_size)
			if msg.args.size() > 0 and msg.args[-1].type == "input_statement":
				msg_size.x += 32 + 8

			if msg_index + 1 < self.base.messages.size():
				if self.base.messages[msg_index + 1].args[-1].type == "input_statement":
					var next_size = self.base.messages[msg_index + 1].calculate_geometry(self, font, font_size)
					msg_size.x = max(next_size.x + 32 + 8, msg_size.x)

			width = max(width, msg_size.x + 3)
			if top_width < 0:
				top_width = width

		var x = 0
		var x_direction = 1
		if self.is_right_to_left():
			x_direction = -1

		var points: PackedVector2Array = []
		var light_border: Array = []
		light_border.append(PackedVector2Array())

		# The comments following will describe drawing it from the perspective
		# of a left-to-right written language. Therefore, "top-left" will be the
		# top-left corner. For right-to-left languages, the object will render
		# the "top-left" as described below, for instance, in reality at the
		# top-right corner.

		# Top-left corner
		if (self.base.output == "" and not self.previous) or self.previous_message_index != -1:
			x += 8
			self._append_arc(points, Vector2(x, 8), Vector2(8, 8), PI * 3 / 2, PI)
			self._append_arc(light_border[-1], Vector2(x, 8), Vector2(8, 8), PI * 3 / 2, PI)
			y = 0
		else:
			# Rigid top-left point
			points.append(Vector2(x, 0))
			light_border[-1].append(Vector2(x, 0))

		# Top line (is there a pip?)
		if self.base.previous_statement:
			points.append(Vector2(16 * x_direction, 0))
			light_border[-1].append(Vector2(16 * x_direction, 0))
			self._append_cos(points, Vector2(16, 0), Vector2(16, -4))
			self._append_cos(light_border[-1], Vector2(16, 0), Vector2(16, -4))

			if self._pip_index >= self.pips.size():
				if self.connectable and actually_draw:
					self.pips.append(self._add_pip(true, "expression", Vector2(16, 0), Vector2(16, 4)))
			else:
				if self.connectable and actually_draw:
					self._update_pip(self._pip_index, Vector2(16, 0), Vector2(16, 4))
			self._pip_index += 1

		x = top_width

		# Top-right corner
		points.append(Vector2(x * x_direction, 0))
		light_border[-1].append(Vector2(x * x_direction, 0))

		# Add the extra top padding, if any
		if y != 0:
			points.append(Vector2(x * x_direction, y))

		# Render all input "messages"
		var last_was_input_statement = false
		msg_index = -1
		for msg in self.base.messages:
			msg_index += 1
			last_was_input_statement = false

			# Determine width of message
			var msg_size = msg.calculate_geometry(self, font, font_size)

			if msg_index == 0 and msg.args.size() > 0 and msg.args[-1].type == "input_statement":
				y += 8
				points.append(Vector2((msg_size.x + 32 + 8) * x_direction, y))

			# We should accommodate the next field if we need this one to be longer
			# For instance, an input_statement after this one needs some clearance
			if msg_index + 1 < self.base.messages.size():
				if self.base.messages[msg_index + 1].args[-1].type == "input_statement":
					var next_size = self.base.messages[msg_index + 1].calculate_geometry(self, font, font_size)
					msg_size.x = max(next_size.x + 32 + 8, msg_size.x)

			if msg_size.x + 6 < x:
				if msg.args.size() > 0 and msg.args[-1].type == "input_statement":
					# Inner content
					points.append(Vector2((msg_size.x + 32 + 8) * x_direction, y))
					self._append_cos(points, Vector2(msg_size.x + 32 + 8, y), Vector2(-16, -4))
					if self.connectable and actually_draw:
						if self._pip_index >= self.pips.size():
							self.pips.append(self._add_pip(false, "expression", Vector2(msg_size.x + 32 + 8 - 14, y), Vector2(12, 4), msg_index, msg.args.size() - 1))
						else:
							self._update_pip(self._pip_index, Vector2(msg_size.x + 32 + 8 - 14, y), Vector2(12, 4))
					self._pip_index += 1
					last_was_input_statement = true

				x = msg_size.x + 6
				points.append(Vector2((x + 8) * x_direction, y))
				self._append_arc(points, Vector2(x + 8, y + 8), Vector2(8, 8), PI, PI * 3 / 2)
			else:
				x = msg_size.x + 3
				points.append(Vector2(x * x_direction, y))

			# If the last argument is an input in our message, expose an inward
			# pip for a connection.
			if msg.args.size() >= 1 and msg.args[-1].type == "input_value" and msg.realized()[-1] is int:
				# We need a pip here
				var spacing = 4
				points.append(Vector2(x * x_direction, y + spacing + 3))
				self._append_pip(self.check_overrides[msg.index][-1], points, Vector2(x, y + spacing + 4), false)
				if self.connectable and actually_draw:
					if self._pip_index >= self.pips.size():
						self.pips.append(self._add_pip(false, "value", Vector2(x - 8, y + spacing + 4), Vector2(4, 12), msg_index, msg.args.size() - 1))
					else:
						self._update_pip(self._pip_index, Vector2(x - 8, y + spacing + 4), Vector2(4, 12))
				if self.inputs[msg_index][-1]:
					var input = self.inputs[msg_index][-1]
					var x_offset = 0
					if self.is_right_to_left():
						x_direction = -1
						x_offset = 4
					input.position = Vector2(x - 8, y + spacing + 4) + Vector2((9 - x_offset) * x_direction, -8)
				self._pip_index += 1
				light_border.append(PackedVector2Array())
				self._append_pip(self.check_overrides[msg.index][-1], light_border[-1], Vector2(x, y + spacing + 4), false, true)

			y += msg_size.y
			points.append(Vector2(x * x_direction, y))

		# Sometimes, the last "message" wants a statement block. We might need
		# to extend the bottom a little bit. That padding makes it look a little
		# cleaner.
		if last_was_input_statement:
			# Extend the bottom a little bit
			points[-1].y -= 8
			y = points[-1].y
			light_border.append(PackedVector2Array())
			self._append_arc(points, Vector2(x + 8, y), Vector2(8, 8), 3 * PI / 2, 2 * PI)
			self._append_arc(light_border[-1], Vector2(x + 8, y), Vector2(8, 8), 7 * PI / 4, 2 * PI)
			x += 8
			y += 8
			points.append(Vector2((x + 32) * x_direction, y))
			light_border[-1].append(Vector2((x + 32) * x_direction, y))
			x += 32

		if self.base.output == "":
			y += 8

		points.append(Vector2(x * x_direction, y))

		# Bottom line (is there a pip?)
		if self.base.next_statement:
			points.append(Vector2(33 * x_direction, y))
			self._append_cos(points, Vector2(33, y), Vector2(-16, -4))
			if self.connectable and actually_draw:
				if self._pip_index >= self.pips.size():
					self.pips.append(self._add_pip(false, "expression", Vector2(16, y), Vector2(16, 4)))
				else:
					self._update_pip(self._pip_index, Vector2(16, y), Vector2(16, 4))
				self._pip_index += 1

			# Ensure the 'next' item is positioned well, now that we have
			# computed the size of ourselves.
			if self.next:
				self.next.position = Vector2(16, y) + Vector2(-16 * x_direction, 1)

		light_border.append(PackedVector2Array())
		if self.base.output == "" and not self.next:
			points.append(Vector2(8 * x_direction, y))
			self._append_arc(points, Vector2(8, y - 8), Vector2(8, 8), 8 * PI / 4, 6 * PI / 4)
			self._append_arc(light_border[-1], Vector2(8, y - 8), Vector2(8, 8), 7 * PI / 4, 6 * PI / 4)
		else:
			points.append(Vector2(0, y))
			light_border[-1].append(Vector2(0, y - 1))

		# left line... is this an output?
		if self.base.output:
			var output_type: String = self.base.output
			if self.output_type_override != "":
				output_type = self.output_type_override

			points.append(Vector2(0, 21))
			self._append_pip(output_type, points, Vector2(0, 20), true)
			points.append(Vector2(0, 0))
			light_border[-1].append(Vector2(0, 20))
			light_border.append(PackedVector2Array())
			self._append_pip(output_type, light_border[-1], Vector2(0, 20), true, true)
			if self.connectable and actually_draw:
				if self._pip_index >= self.pips.size():
					self.pips.append(self._add_pip(true, "value", Vector2(-8, 8), Vector2(4, 12)))
				else:
					self._update_pip(self._pip_index, Vector2(-8, 8), Vector2(4, 12))
				self._pip_index += 1
			light_border[-1].append(Vector2(0, 0))
		elif self.previous and self.previous_message_index == -1:
			points.append(Vector2(0, 0))
			light_border[-1].append(Vector2(0, 0))
		else:
			points.append(Vector2(0, 8))
			light_border[-1].append(Vector2(0, 8))

		var fill_color: Color = Color.from_hsv(
			self.base.colour,
			self.base.theme.hsv_saturation,
			self.base.theme.hsv_value
		)
		fill_color.s = fill_color.s * opacity
		fill_color.a = opacity

		if actually_draw:
			self.draw_colored_polygon(points, fill_color)

		x = 12

		y = 0
		if self.base.output == "":
			y = 0

		msg_index = -1
		for msg in self.base.messages:
			msg_index += 1
			if msg_index == 0 and msg.args.size() > 0 and msg.args[-1].type == "input_statement":
				y += 8

			var message_size: Vector2 = msg.draw(
				self,
				Vector2(x, y),
				font,
				font_size,
				Color.from_hsv(
					self.base.colour,
					max(0, self.base.theme.hsv_saturation - 0.1),
					max(0, self.base.theme.hsv_value - 0.4)
				),
				border_light_color,
				border_dark_color,
				actually_draw)
			y += message_size.y

		if actually_draw:
			self.draw_polyline(points, border_dark_color, 0.5, true)
			for lines in light_border:
				self.draw_polyline(lines, border_light_color, 0.5, true)
		self.polygon = points
		self.size = Vector2(width, y)
		self.compute_size()

		#for pip in self.pips:
		#	if pip.get_meta("input"):
		#		self.draw_rect(Rect2(pip.position, pip.get_child(0).shape.size), Color.DARK_RED)
		#	else:
		#		self.draw_rect(Rect2(pip.position, pip.get_child(0).shape.size), Color.DARK_GREEN)

	# Returns a listing of variables that are within the scope of this block
	func get_variables() -> Array[String]:
		var ret: Array[String] = []

		if self.previous:
			ret.append_array(self.previous.get_variables())

		if self.output:
			ret.append_array(self.output.get_variables())

		# Do we define a new variable? Add it to the listing
		ret.append_array(self.base.variables)
		ret.append_array(self.extra_variables)
		print("get_variables: ", self.base.type, " : ", self.base.messages[0].message, " : ", ret)
		return ret

	func reset_extra_arguments(msg_index: int):
		var empty_args: Array[BlocklyArgument] = []
		for arg in self.extra_args[msg_index]:
			if arg.type == "variable":
				self.extra_variables.remove_at(self.extra_variables.find(arg.name))
		self.extra_args[msg_index] = empty_args
		self.queue_redraw_all()

	func add_extra_argument(msg_index: int, arg_info: Dictionary):
		var total_args: int = self.base.messages[msg_index].args.size()
		total_args += self.extra_args[msg_index].size()

		var arg: BlocklyArgument = BlocklyArgument.new(self.get_workspace(), arg_info, total_args)
		total_args += 1

		while self.inputs[msg_index].size() < total_args:
			self.inputs[msg_index].append(null)
			self.fields[msg_index].append(null)
			self.controls[msg_index].append(null)
			self.shadows[msg_index].append(null)

		self.extra_args[msg_index].append(arg)

		if arg.type == "variable":
			self.extra_variables.append(arg.name)

		if arg.variableBlock:
			var variable_block: BlocklyBlockInstance = arg.instantiate_variable_block()
			variable_block.position = Vector2(0, 0)
			variable_block.spawning = true
			variable_block.opacity = 0.5
			self.inputs[msg_index][arg.index] = variable_block
			variable_block.output = self
			self.add_child(variable_block)
			variable_block.emit_all("connected")

		self.queue_redraw_all()

	func get_extra_arguments(msg_index: int) -> Array[BlocklyArgument]:
		var total_args: int = self.base.messages[msg_index].args.size()
		total_args += self.extra_args[msg_index].size()

		while self.inputs[msg_index].size() < total_args:
			self.inputs[msg_index].append(null)
			self.fields[msg_index].append(null)
			self.controls[msg_index].append(null)
			self.shadows[msg_index].append(null)

		return self.extra_args[msg_index]

	func spawn() -> BlocklyBlockInstance:
		var ret: BlocklyBlockInstance = self.base.instantiate(self)

		# Duplicate field values
		var msg_index = -1
		for message in self.base.messages:
			msg_index += 1
			var arg_index = -1
			for arg in message.args:
				arg_index += 1
				var field: BlocklyField = self.fields[msg_index][arg_index]
				if field:
					ret.fields[msg_index][arg_index] = field.duplicate()
					ret.fields[msg_index][arg_index].value = field.value

		return ret

	func serialize_to_code(indent: int = 0, tokens = null, next_serialized: Array = [], context: Dictionary = {}):
		var ret = ""
		var indent_str = ""
		for i in indent:
			indent_str += " "

		if self.output and self.base.messages.size() == 1 and self.base.messages[0].args.size() == 0:
			return self.base.messages[0].message

		if next_serialized.size() == 0:
			next_serialized = [false]

		var tokenizing_default = false
		if tokens == null:
			tokenizing_default = true
			tokens = self.base.serialize

		for token in tokens:
			if typeof(token) == TYPE_STRING:
				ret += indent_str

				# Split for arguments so we can put in values
				var index = token.find("%")
				while (index >= 0):
					var start: String = token.substr(0, index)
					var msg_str = token.substr(index + 1, 1)

					ret += start

					if msg_str == "o" and token.substr(index + 1, 3) == "oid":
						ret += str(self.get_instance_id())
					else:
						var msg_index = int(msg_str)
						var arg_index = int(token.substr(index + 3, 1))
						var input: BlocklyBlockInstance = self.inputs[msg_index][arg_index - 1]
						var field = self.fields[msg_index][arg_index - 1]
						var arg: BlocklyArgument = self.base.messages[msg_index].args[arg_index - 1]

						if field:
							ret += field.serialize_to_code()
						elif input:
							ret += input.serialize_to_code()
						elif arg:
							# TODO: serializing raw values is a language domain issue
							ret += str(arg.default)
						else:
							# Error! Need an input to serialize
							pass

					token = token.substr(index + 4)
					index = token.find("%")

				ret += token
				if not context.has("inline") or not context["inline"]:
					ret += "\n"
			elif token is Dictionary:
				if token.has("yield"):
					var new_indent = indent
					if token.has("indent") and token["indent"]:
						new_indent += 2

					if typeof(token["yield"]) == TYPE_STRING:
						# Yield to an expression in an argument
						var index = 0
						var msg_str = token["yield"].substr(index + 1, 1)
						var msg_index = int(msg_str)
						var arg_index = int(token["yield"].substr(index + 3, 1))
						var input = self.inputs[msg_index][arg_index - 1]

						if input:
							ret += input.serialize_to_code(new_indent)
						elif token.has("default") && token["default"] is Array:
							ret += self.serialize_to_code(new_indent, token["default"], next_serialized, token)
					elif token["yield"] is Array:
						# Recurse
						ret += self.serialize_to_code(new_indent, token["yield"], next_serialized, token)
					else:
						# Yield to the next block (true)
						if self.next:
							ret += self.next.serialize_to_code(new_indent)
							next_serialized[0] = true
						elif token.has("default") && token["default"] is Array:
							ret += self.serialize_to_code(new_indent, token["default"], next_serialized, token)

		if tokenizing_default and not next_serialized[0] and self.next:
			ret += self.next.serialize_to_code(indent)

		return ret

# Called when the node enters the scene tree for the first time.
func _ready():
	# Instantiate common field types
	self.register_field(BlocklyFieldInput)
	self.register_field(BlocklyFieldDropdown)
	self.register_field(BlocklyFieldKey)
	self.register_field(BlocklyFieldVariant)
	self.register_field(BlocklyFieldVariable)

	# Instantiate our custom ones
	self.register_field(BlocklyFieldSource)
	self.register_field(BlocklyFieldProperty)
	self.register_field(BlocklyFieldSignal)
	self.register_field(BlocklyFieldArgumentList)

	# Setup view...
	# Left-hand side is the toolbox categories
	# Right-hand side is the workspace

	var container: HBoxContainer = HBoxContainer.new()
	self.add_child(container)
	container.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	container.size_flags_vertical = Control.SIZE_EXPAND_FILL

	self._toolbox = Tree.new()
	self._toolbox.custom_minimum_size.x = 150
	self._toolbox.size_flags_vertical = Control.SIZE_EXPAND_FILL
	container.add_child(self._toolbox)

	self._workspace = Control.new()
	self._workspace.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	self._workspace.size_flags_vertical = Control.SIZE_EXPAND_FILL
	container.add_child(self._workspace)

	var bg_rect: TextureRect = TextureRect.new()
	bg_rect.texture = load("res://Assets/Images/noise_med_gray.png")
	bg_rect.texture_repeat = CanvasItem.TEXTURE_REPEAT_ENABLED
	bg_rect.stretch_mode = TextureRect.STRETCH_TILE
	bg_rect.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	bg_rect.size_flags_vertical = Control.SIZE_EXPAND_FILL
	container.size = self.size
	bg_rect.size = container.size
	var bg_resizer = func():
		container.size = self.size
		bg_rect.size = container.size
	self._workspace.add_child(bg_rect)
	self.resized.connect(bg_resizer)

	self.clip_children = CanvasItem.CLIP_CHILDREN_AND_DRAW
	self.clip_contents = true

	# Setup toolbox
	var root: TreeItem = self._toolbox.create_item()
	self._toolbox.hide_root = true

	var colour = 200

	# Create block categories for the toolbox listing
	for category in ["Logic", "Loops", "Events", "Math", "Text", "Variables", "Sources"]:
		var item: TreeItem = root.create_child(1)
		item.set_text(0, category)
		var icon: GradientTexture1D = GradientTexture1D.new()
		var gradient = Gradient.new()
		var hsv_saturation: float = 0.45
		var hsv_value: float = 0.65
		var color: Color = Color.from_hsv(colour / 360.0, hsv_saturation, hsv_value)
		colour = (colour + 30) % 360
		gradient.colors = [color]
		icon.width = 1
		icon.gradient = gradient
		item.set_icon(0, icon)
		item.set_icon_region(0, Rect2(Vector2(0, 0), Vector2(20, 12)))

		# Create the pull out
		var panel: Panel = Panel.new()
		panel.position = Vector2(0, 0)
		panel.size = Vector2(100.0, self.size.y)
		panel.visible = false
		self.add_child(panel)
		item.set_metadata(0, category)
		self._toolbox_items[category] = item
		self._panels[category] = panel

	self._open_panel = null
	self._toolbox.item_mouse_selected.connect(func (_position: Vector2, _mouse_button_index: int):
		if self._open_panel:
			self._open_panel.visible = false
		var category: String = self._toolbox.get_selected().get_metadata(0)
		var panel: Panel = self._panels[category]
		panel.position = Vector2(self._toolbox.size.x, 0.0)
		panel.size.y = self._toolbox.size.y
		panel.visible = true
		self._open_panel = panel
	)

	self._toolbox.focus_exited.connect(func ():
		if self._open_panel:
			self._open_panel.visible = false
			self._open_panel = null
	)

	# Load initial blocks
	self.setup()

# The currently dragging block
var _dragging: BlocklyBlockInstance = null

# A block we are dragging that's still attached
var _detaching: BlocklyBlockInstance = null

# The starting point of a drag and the original properties of the block pre-drag
var _start: Vector2
var _original_position: Vector2
var _original_opacity: float = 1.0
var _original_editable: bool = false

# Whether or not we are dragging the block from the toolbox
var _dragging_is_new_block: bool = false

# The root workspace node
var _workspace: Control = null

# Process frame
func _process(elapsed: float):
	if self._script:
		self._script._process(elapsed)

# Process mouse / keyboard input
func _input(event):
	if self._script:
		self._script._input(event)

	# Get mouse
	var mouse_position: Vector2 = self._workspace.get_local_mouse_position()

	# Is there a block underneath?
	var hovered: BlocklyBlockInstance = null
	var children = self._workspace.get_children()
	for child in children:
		if child is BlocklyBlockInstance:
			if child.contains_point(mouse_position):
				# This is the hovered block
				hovered = child

				# However, if this is an anchored block, we want to know the
				# actual block that is here.
				if hovered.anchored:
					hovered = hovered.get_block_at_point(mouse_position)

	# Detect clicking on a visible toolbox block
	if event is InputEventMouseButton and event.is_pressed():
		for panel in self._panels.values():
			if not panel.visible:
				continue

			var sub_mouse_position: Vector2 = panel.get_local_mouse_position()
			for child in panel.get_children():
				if child is BlocklyBlockInstance:
					if child.contains_point(sub_mouse_position):
						# Close the panel
						panel.visible = false

						# Spawn this actual block
						hovered = child.base.instantiate()
						self._workspace.add_child(hovered)
						self._original_editable = true
						self._dragging_is_new_block = true
						hovered.position = child.position
						hovered.editable = false

						# Ignore all other blocks
						break

	# We are over a particular block?
	if hovered and hovered.movable and !self._dragging:
		if event is InputEventMouseButton and event.is_pressed():
			if event.button_index == MOUSE_BUTTON_LEFT:
				# If this is an attached node, we have to offer resistance to
				# being pulled
				if hovered.previous or hovered.output:
					self._detaching = hovered
				else:
					self._dragging = hovered

					# Move this block to the top
					hovered.move_to_front()

					# Make it now visible
					hovered.opacity = 1.0
					hovered.queue_redraw()

				self._start = mouse_position
				self._original_position = hovered.position
				self._original_opacity = hovered.opacity
				if not self._dragging_is_new_block:
					self._original_editable = hovered.editable

	# Close panel
	if event is InputEventMouseButton and event.is_pressed():
		if self._open_panel:
			self._open_panel.visible = false
			self._open_panel = null

	# Even if we aren't over a block
	if event is InputEventMouseButton and not event.is_pressed():
		# Done moving the block
		if self._dragging:
			self._dragging.editable = self._original_editable
			self._dragging_is_new_block = false
			self._dragging.opacity = self._original_opacity
			self._dragging.queue_redraw()

			# Detect releasing over the toolbox panel
			if self._toolbox.get_rect().has_point(self.get_local_mouse_position()):
				self._dragging.get_parent().remove_child(self._dragging)
				self._dragging = null
				return

			# Check for connections
			var base = self._dragging._pending_connection
			var input = self._dragging
			var pip = self._dragging._pending_connection_pip
			if pip and base and not input.shadowing and not base.shadowing:
				if pip.get_meta("input"):
					input = base
					base = self._dragging
					pip = self._dragging._pending_connecting_pip

				var pip_type = pip.get_meta("type")
				var msg_index = pip.get_meta("message_index")
				var arg_index = pip.get_meta("arg_index")

				var x_direction = 1
				var x_offset = 0
				if base.is_right_to_left():
					x_direction = -1
					x_offset = pip.get_child(0).shape.size.x

				# Do not allow connecting an already connected input
				if (pip_type == "expression" and input.previous) or \
					(pip_type != "expression" and input.output):
					msg_index = -1
					pip_type = "null"

				# Do not allow connections that are the wrong type
				if msg_index >= 0 and base.base.messages[msg_index].args[arg_index]:
					var arg: BlocklyArgument = base.base.messages[msg_index].args[arg_index]
					var check: String = base.check_overrides[msg_index][arg_index]

					# Allow Variant to attach to anything
					if check == "Variant" or input.base.output == "Variant":
						# But it should inherit the type that's enforced
						if input.base.output == "Variant":
							input.output_type_override = arg.check
						elif input.output_type_override:
							base.check_overrides[msg_index][arg_index] = input.output_type_override
						else:
							base.check_overrides[msg_index][arg_index] = input.base.output
					elif check != input.base.output:
						print("Rejected input: ", check, " != ", input.base.output)
						msg_index = -1
						pip_type = "null"

				if msg_index >= 0 && arg_index >= 0 && (base.inputs[msg_index][arg_index] == null || base.inputs[msg_index][arg_index].shadowing):
					# It already has a shadowed input, remove it first
					if base.inputs[msg_index][arg_index]:
						base.remove_child(base.inputs[msg_index][arg_index])

					# Match the base opacity
					input.opacity = base.opacity

					# Assign the input to its slot
					base.inputs[msg_index][arg_index] = input

					# Remove input from the workspace itself
					input.get_parent().remove_child(input)

					# Add the input as a child of the base
					base.add_child(input)

					if pip_type == "expression":
						input.previous = base
						input.previous_message_index = msg_index
						input.previous_arg_index = arg_index

						# Move input
						input.position = pip.position + Vector2((-19 - x_offset) * x_direction, 1)
					else:
						input.output = base
						input.output_message_index = msg_index
						input.output_arg_index = arg_index

						# Move input
						input.position = pip.position + Vector2((9 - x_offset) * x_direction, -8)

					# Let the input know it is connected
					input.connected.emit()

					# And subsequent items
					input.emit_all("connected")

					# Redraw inputs to our input block
					input.queue_redraw_all(base.opacity)

					# Redraw input and all parents
					var block = input
					while block is BlocklyBlockInstance:
						#block._draw()
						block.queue_redraw()
						var parent = block.get_parent()
						if not parent is BlocklyBlockInstance:
							parent = block.previous
						block = parent

					base.compute_size()
				elif pip_type == "expression":
					base.next = input
					input.previous = base
					input.previous_message_index = -1
					input.previous_arg_index = -1

					# Match the base opacity
					input.opacity = base.opacity

					# Add 'input' to base
					input.get_parent().remove_child(input)
					input.position = pip.position + Vector2(-16 * x_direction, 1)
					base.add_child(input)

					# Let the input know it is connected
					input.connected.emit()

					# And subsequent items
					input.emit_all("connected")

					# Redraw inputs to our input block
					input.queue_redraw_all(base.opacity)

					# Redraw input and all parents
					var block = input
					while block is BlocklyBlockInstance:
						block.queue_redraw()
						var parent = block.get_parent()
						if not parent is BlocklyBlockInstance:
							parent = block.previous
						block = parent

					base.compute_size()

			# We are no longer dragging this block
			self._dragging = null

			# Commit the code changes
			self.serialize_to_code()
	elif event is InputEventMouseMotion and self._detaching:
		# Detect when we can actually detach a block
		if self._start.distance_to(mouse_position) >= DETACH_RESISTANCE:
			self._dragging = self._detaching

			# We are no longer 'detaching'
			self._detaching = null

			# If this is a 'spawning' block, we actually create a new block
			# instead.
			var input: BlocklyBlockInstance = self._dragging
			if self._dragging.spawning:
				var new_block: BlocklyBlockInstance = self._dragging.spawn()
				new_block.position = self._dragging.global_position - self._workspace.global_position
				print(self._workspace.global_position, self._dragging.global_position)
				self._workspace.add_child(new_block)
				self._original_editable = true
				self._dragging_is_new_block = true
				self._dragging.editable = false
				self._dragging = new_block
				self._original_position = new_block.position
				input = self._dragging

			# Also decouple the connections
			if input.previous:
				var msg_index = input.previous_message_index
				var arg_index = input.previous_arg_index
				if msg_index > -1 and arg_index > -1:
					input.previous.inputs[msg_index][arg_index] = null
				else:
					input.previous.next = null
				input.previous.queue_redraw()
				input.previous = null

			if input.output:
				var msg_index = input.output_message_index
				var arg_index = input.output_arg_index
				if msg_index > -1 and arg_index > -1:
					input.output.inputs[msg_index][arg_index] = null

					if input.output.shadows[msg_index][arg_index]:
						input.output.inputs[msg_index][arg_index] = input.output.shadows[msg_index][arg_index]
						input.output.add_child(input.output.shadows[msg_index][arg_index])

				input.output.queue_redraw()
				input.output = null

			# Move this block to the top
			self._dragging.move_to_front()

			input.queue_redraw()
	elif event is InputEventMouseMotion and self._dragging:
		# Move block
		var delta = mouse_position - self._start
		self._dragging.set_position(self._original_position + delta)

# Yields a JSON compatible Dictionary representation of the current workspace.
func serialize():
	pass

# Yields a String representing the code the blocks represent.
func serialize_to_code(root: BlocklyBlockInstance = null):
	var start_block = root
	if start_block == null:
		start_block = self._workspace.get_child(1)
	var code = start_block.serialize_to_code()
	print(code)
	var script: GDScript = GDScript.new()
	script.source_code = code
	script.reload()
	if script.can_instantiate():
		var studio = self.get_tree().get_root().get_node("Studio")
		var action = script.new()
		if self._script:
			if self._script.has_method("unbind_all"):
				self._script.unbind_all()
		if action.has_method("invoke"):
			action.invoke(studio)
		self._script = action

func add_block_to_toolbox(base: BlocklyBlock, category: String):
	var panel: Panel = self._panels[category]
	var block: BlocklyBlockInstance = base.instantiate()
	var y = panel.get_meta("next_y", 24.0)
	block.connectable = false
	block.opacity = 1.0
	block.editable = false
	block.position = Vector2(24.0, y)
	panel.add_child(block)
	block._draw(false)
	panel.size.x = max(panel.size.x, block.total_size.x + 48.0)
	panel.set_meta("next_y", y + block.total_size.y + 32.0)

func setup():
	var block_base: BlocklyBlock
	var block: BlocklyBlockInstance

	block_base = BlocklyBlock.import_json(self, {
		"type": "get_variable",
		"output": "Variant",
		"colour": 0,
		"message0": "%1",
		"args0": [
			{
				"type": "field_variable",
				"name": "VARIABLE",
			},
		],
		"serialize": [
			{
				"yield": [
					"%0-1",
				],
				"inline": true,
			},
		],
	})

	# Add to Variables category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Variables")

	block_base = BlocklyBlock.import_json(self, {
		"type": "start",
		"colour": 230,
		"nextStatement": null,
		"message0": "ready",
		"args0": [],
		"serialize": [
			"extends Action",
			"",
			"func invoke(studio):",
			{
				"yield": [
					"super.invoke(studio)",
					{
						"yield": true,
						"default": ["pass"],
						"indent": false,
					},
				],
				"indent": true,
			},
		],
	})

	self.register_block(block_base)
	block = block_base.instantiate()
	self._workspace.add_child(block)
	block.position = Vector2(10, 10)
	block.movable = false
	block.anchored = true

	block_base = BlocklyBlock.import_json(self, {
		"type": "logic_if",
		"colour": 200,
		"previousStatement": null,
		"nextStatement": null,
		"message0": "if %1",
		"args0": [
			{
				"type": "input_value",
				"name": "IF_EXPRESSION",
				"check": "Boolean",
				"default": false,
			},
		],
		"message1": "then %1",
		"args1": [
			{
				"type": "input_statement",
				"name": "DO",
			},
		],
		"serialize": [
			"if %0-1:",
			{
				"yield": "%1-1",
				"default": ["pass"],
				"indent": true,
			},
		],
	})

	# Add to Logic category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Logic")

	block_base = BlocklyBlock.import_json(self, {
		"type": "event_key_press",
		"previousStatement": null,
		"nextStatement": null,
		"message0": "when key %1 is pressed",
		"args0": [
			{
				"type": "field_key",
				"name": "FIELDNAME",
			},
		],
		"message1": "do %1",
		"args1": [
			{
				"type": "input_statement",
				"name": "DO",
			},
		],
		"serialize": [
			"self.bind_signal(self.on_key_press, (func(key: String):",
			{
				"yield": [
					"if key == \"%0-1\":",
					{
						"yield": "%1-1",
						"default": ["pass"],
						"indent": true,
					},
				],
				"indent": true,
			},
			"))",
		],
	})

	# Add to Event category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Events")

	block_base = BlocklyBlock.import_json(self, {
		"type": "event_key_press",
		"previousStatement": null,
		"nextStatement": null,
		"message0": "when source moves",
		"message1": "do %1",
		"args1": [
			{
				"type": "input_statement",
				"name": "DO",
			},
		],
		"serialize": [
			"self.bind_signal(source.moved, (func():",
			{
				"yield": "%1-1",
				"default": ["pass"],
				"indent": true,
			},
			"))",
		],
	})

	# Add to Event category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Events")

	block_base = BlocklyBlock.import_json(self, {
		"type": "source_event",
		"previousStatement": null,
		"nextStatement": null,
		"message0": "when source %1 with %2",
		"args0": [
			{
				"type": "field_signal",
				"name": "WHAT",
			},
			{
				"type": "field_argument_list",
				"name": "ARGS",
			},
		],
		"message1": "do %1",
		"args1": [
			{
				"type": "input_statement",
				"name": "DO",
			},
		],
		"serialize": [
			"self.bind_signal(source.%0-1, (func(%0-2):",
			{
				"yield": "%1-1",
				"default": ["pass"],
				"indent": true,
			},
			"))",
		],
	})

	# Add to Event category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Events")

	block_base = BlocklyBlock.import_json(self, {
		"type": "with_source",
		"previousStatement": null,
		"nextStatement": null,
		"message0": "with source %1",
		"args0": [
			{
				"type": "field_source",
				"name": "SOURCE",
				"options": [
					[
						"---",
					],
				],
			},
		],
		"message1": "%1",
		"args1": [
			{
				"type": "input_statement",
				"name": "DO",
			},
		],
		"serialize": [
			"(func(source: Source.SourceInstance):",
			{
				"yield": [
					"if source == null:",
					{
						"yield": [
							"return"
						],
						"indent": true,
					},
					{
						"yield": "%1-1",
						"default": ["pass"],
						"indent": false,
					},
				],
				"indent": true,
			},
			").call(self.get_source(\"%0-1\"))",
		],
	})

	# Add to Event category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Sources")

	block_base = BlocklyBlock.import_json(self, {
		"type": "set_property",
		"previousStatement": null,
		"nextStatement": null,
		"message0": "set %1 to %2",
		"args0": [
			{
				"type": "field_property",
				"name": "PROPERTY",
				"options": [
					[
						"---",
					],
				],
			},
			{
				"type": "input_value",
				"name": "VALUE",
				"check": "Variant",
				"shadow": {
					"type": "text",
					"output": "Variant",
					"message0": "%1",
					"args0": [
						{
							"type": "field_variant",
							"name": "VALUE",
							"text": "hello",
						},
					],
					"serialize": [
						{
							"yield": [
								"%0-1",
							],
							"inline": true,
						},
					],
				},
			},
		],
		"serialize": [
			"source.set_property(\"%0-1\", %0-2)"
		],
	})

	# Add to Event category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Sources")

	block_base = BlocklyBlock.import_json(self, {
		"type": "get_property",
		"message0": "get %1",
		"output": "Variant",
		"args0": [
			{
				"type": "field_property",
				"name": "PROPERTY",
				"options": [
					[
						"---",
					],
				],
			},
		],
		"serialize": [
			"source.get_property(\"%0-1\")"
		],
	})

	# Add to Event category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Sources")

	block_base = BlocklyBlock.import_json(self, {
		"type": "print",
		"colour": 250,
		"previousStatement": null,
		"nextStatement": null,
		"message0": "print %1",
		"args0": [
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "text",
					"output": "String",
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "hello",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				},
			},
		],
		"serialize": [
			{
				"yield": ["print(%0-1)"],
				"default": ["print(\"\")"],
			},
		],
	})

	# Add to Text category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Text")

	block_base = BlocklyBlock.import_json(self, {
		"type": "string_begins_with",
		"colour": 250,
		"output": "Boolean",
		"message0": "%1 begins with %2",
		"args0": [
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "text",
					"output": "String",
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "hello",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				},
			},
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "text",
					"output": "String",
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "h",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				},
			},
		],
		"serialize": [
			{
				"yield": [
					"%0-1.begins_with(%0-2)"
				],
				"inline": true,
			},
		],
	})

	# Add to Text category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Text")

	block_base = BlocklyBlock.import_json(self, {
		"type": "string_ends_with",
		"colour": 250,
		"output": "Boolean",
		"message0": "%1 ends with %2",
		"args0": [
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "text",
					"output": "String",
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "hello",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				},
			},
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "text",
					"output": "String",
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "o",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				},
			},
		],
		"serialize": [
			{
				"yield": [
					"%0-1.ends_with(%0-2)"
				],
				"inline": true,
			},
		],
	})

	# Add to Text category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Text")

	block_base = BlocklyBlock.import_json(self, {
		"type": "to_string",
		"colour": 250,
		"output": "String",
		"message0": "\" %1 \"",
		"args0": [
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "Variant",
				"default": "''",
			},
		],
		"serialize": [
			{
				"yield": [
					"str(%0-1)",
				],
				"inline": true,
			},
		],
	})

	# Add to Text category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Text")

	block_base = BlocklyBlock.import_json(self, {
		"type": "key",
		"message0": "%1 %2 %3 ",
		"args0": [
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "Number",
				"shadow": {
					"type": "number",
					"output": "Number",
					"colour": 330,
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "1337",
							"check": "Number",
						},
					],
					"serialize": [
						{
							"yield": [
								"%0-1",
							],
							"inline": true,
						},
					],
				}
			},
			{
				"type": "field_dropdown",
				"name": "OPERATOR",
				"options": [
					[
						"+",
						"+",
					],
					[
						"-",
						"-",
					],
					[
						"×",
						"*",
					],
					[
						"÷",
						"/",
					],
					[
						"and",
						"&&",
					],
					[
						"or",
						"||",
					],
				],
			},
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "Number",
				"shadow": {
					"type": "number",
					"output": "Number",
					"colour": 330,
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "42",
							"check": "Number",
						},
					],
					"serialize": [
						{
							"yield": [
								"%0-1",
							],
							"inline": true,
						},
					],
				},
			},
		],
		"serialize": [
			{
				"yield": [
					"(%0-1 %0-2 %0-3)",
				],
				"inline": true,
			},
		],
		"output": "Number",
		"colour": 330,
		"tooltip": "",
		"helpUrl": ""
	})

	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Math")

	block_base = BlocklyBlock.import_json(self, {
		"type": "key",
		"message0": "%1 + %2 ",
		"args0": [
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "a",
					"output": "String",
					"colour": 330,
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "foo",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				}
			},
			{
				"type": "input_value",
				"name": "TEXT",
				"check": "String",
				"shadow": {
					"type": "b",
					"output": "String",
					"colour": 330,
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"text": "blah",
						},
					],
					"serialize": [
						{
							"yield": [
								"\"%0-1\"",
							],
							"inline": true,
						},
					],
				}
			},
		],
		"serialize": [
			{
				"yield": [
					"(%0-1 + %0-2)",
				],
				"inline": true,
			},
		],
		"output": "String",
		"colour": 330,
		"tooltip": "",
		"helpUrl": ""
	})

	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Text")

	block_base = BlocklyBlock.import_json(self, {
		"type": "during",
		"previousStatement": null,
		"nextStatement": null,
		"message0": "for the next %1 seconds with %2 %3",
		"args0": [
			{
				"type": "input_value",
				"name": "TIME",
				"check": "Number",
				"shadow": {
					"type": "number",
					"output": "Number",
					"colour": 330,
					"message0": "%1",
					"args0": [
						{
							"type": "field_input",
							"name": "TEXT",
							"check": "Number",
							"text": "3",
							"default": "0",
						},
					],
					"serialize": [
						{
							"yield": [
								"%0-1",
							],
							"inline": true,
						},
					],
				},
			},
			{
				"type": "variable",
				"name": "elapsed",
				"check": "Number",
			},
			{
				"type": "variable",
				"name": "ratio",
				"check": "Number",
			},
		],
		"message1": "do %1",
		"args1": [
			{
				"type": "input_statement",
				"name": "DO",
				"variables": {
					"elapsed": {
						"check": "Number",
					},
					"ratio": {
						"check": "Number",
					},
				}
			},
		],
		"serialize": [
			"self.bind_process(%0-1, func(elapsed: float, ratio: float):",
			{
				"yield": [
					{
						"yield": "%1-1",
						"default": ["pass"],
						"indent": false,
					},
				],
				"indent": true,
			},
			")",
		],
	})

	# Add to Loops category
	self.register_block(block_base)
	self.add_block_to_toolbox(block_base, "Loops")
