extends Control


func chat_message(data : SenderData, msg : String) -> void:
	$ChatContainer.put_chat(data, msg)

# Check the CommandInfo class for the available info of the cmd_info.
func command_test(_cmd_info : CommandInfo) -> void:
	print("A")

func hello_world(_cmd_info : CommandInfo) -> void:
	$Gift.chat("HELLO WORLD!")

func streamer_only(_cmd_info : CommandInfo) -> void:
	$Gift.chat("Streamer command executed")

func no_permission(_cmd_info : CommandInfo) -> void:
	$Gift.chat("NO PERMISSION!")

func greet(_cmd_info : CommandInfo, arg_ary : PackedStringArray) -> void:
	$Gift.chat("Greetings, " + arg_ary[0])

func greet_me(cmd_info : CommandInfo) -> void:
	$Gift.chat("Greetings, " + cmd_info.sender_data.tags["display-name"] + "!")

func list(_cmd_info : CommandInfo, arg_ary : PackedStringArray) -> void:
	$Gift.chat(", ".join(arg_ary))
